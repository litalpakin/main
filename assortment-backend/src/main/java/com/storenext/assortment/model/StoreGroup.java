package com.storenext.assortment.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "store_group")
public class StoreGroup extends BaseEntity {
	private static final long serialVersionUID = -6439682716452974921L;

	@NotNull
	private String name;
	@Lob
	private String description;
	private Long clusterId;
	@Type(type = "com.storenext.assortment.model.type.HibernateSetLongClobType")
	private Set<Long> stores = new HashSet<>();

	public Set<Long> getStores() {
		return stores;
	}

	public void setStores(Set<Long> stores) {
		this.stores = stores;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getClusterId() {
		return clusterId;
	}

	public void setClusterId(Long clusterId) {
		this.clusterId = clusterId;
	}

}
