package com.storenext.assortment.model.type;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;

public class HibernateListLongClobType extends HibernateCollectionClobType {
	@Override
	public Class<?> returnedClass() {
		return List.class;
	}

	@Override
	protected Object stringToCollection(String str) {
		List<String> strings = Arrays.asList(str.split(DELIMITER));
		List<Long> longs = strings.stream().filter(s -> !s.isEmpty()).map(Long::valueOf).collect(Collectors.toList());
		return longs;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		} else if (!(x instanceof List) || !(y instanceof List)) {
			return false;
		} else {
			return x.equals(y);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value instanceof List ? ((List) value).stream().collect(Collectors.toList()) : null;
	}
}
