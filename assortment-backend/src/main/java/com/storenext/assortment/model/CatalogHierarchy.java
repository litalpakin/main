package com.storenext.assortment.model;

public enum CatalogHierarchy {

	SUPPLIER("ספק","ספק"),CLASS("מחלקה","מחלקת"),CATEGORY("קטגוריה","קטגורית"),SUB_CATEGORY("תת קטגוריה","תת קטגורית"),BRAND("מותג","מותג");

	String caption;
	String captionSmihud;
	CatalogHierarchy(String caption, String captionSmihud){
		this.caption = caption;
		this.captionSmihud = captionSmihud;
	}

	public String getCaption() {
		return caption;
	}

	public String getCaptionSmihud() {
		return captionSmihud;
	}
}
