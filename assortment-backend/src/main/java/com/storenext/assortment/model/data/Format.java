package com.storenext.assortment.model.data;

import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.security.User;

public class Format {
	Long formatId;
	String formatName;
	String englishFormatName;
	
	public Long getFormatId() {
		return formatId;
	}
	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public String getEnglishFormatName() {
		return englishFormatName;
	}

	public void setEnglishFormatName(String englishFormatName) {
		this.englishFormatName = englishFormatName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((englishFormatName == null) ? 0 : englishFormatName.hashCode());
		result = prime * result + ((formatId == null) ? 0 : formatId.hashCode());
		result = prime * result + ((formatName == null) ? 0 : formatName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Format other = (Format) obj;
		if (englishFormatName == null) {
			if (other.englishFormatName != null)
				return false;
		} else if (!englishFormatName.equals(other.englishFormatName))
			return false;
		if (formatId == null) {
			if (other.formatId != null)
				return false;
		} else if (!formatId.equals(other.formatId))
			return false;
		if (formatName == null) {
			if (other.formatName != null)
				return false;
		} else if (!formatName.equals(other.formatName))
			return false;
		return true;
	}
	
	public String getName(User user){
		if(user.getLanguage() != null && user.getLanguage() == Language.ENGLISH){
			return getEnglishFormatName();
		}else{
			return getFormatName();
		}
	}
	
}
