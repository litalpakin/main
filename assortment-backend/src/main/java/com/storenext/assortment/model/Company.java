package com.storenext.assortment.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Company extends BaseEntity {
	private static final long serialVersionUID = 6964620874745139095L;

	@NotNull
	private boolean enabled;
	@NotNull
	private String name;
	@Lob
	private String logoUrl;
	@Enumerated(EnumType.STRING)
	private CompanyType type;

	@OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<CompanySupplier> suppliers = new ArrayList<>();

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public CompanyType getType() {
		return type;
	}

	public void setType(CompanyType type) {
		this.type = type;
	}

	public List<CompanySupplier> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(List<CompanySupplier> suppliers) {
		this.suppliers = suppliers;
	}

}
