package com.storenext.assortment.model.data;

import java.util.Map;

import com.storenext.assortment.enums.Language;

public class LanguageBaseEntity {
	private Long id;
	private Map<Language,String> names;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Map<Language, String> getNames() {
		return names;
	}
	public void setNames(Map<Language, String> names) {
		this.names = names;
	}

	public String getNameByLanguage(Language language){
		return names.get(language);
	}
}
