package com.storenext.assortment.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.storenext.assortment.model.composite.UserUserFunctionId;
import com.storenext.assortment.model.security.User;

@Entity
@Table(name = "users_user_functions")
public class UserUserFunction implements Serializable {

    private static final long serialVersionUID = -2282016060520483059L;

    @EmbeddedId
    private UserUserFunctionId id;
    @OneToOne(fetch = FetchType.EAGER)
    @MapsId("userId")
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("userFunctionId")
    private UserFunction userFunction;

    public UserUserFunction() {
    }

    public UserUserFunction(User user, UserFunction userFunction) {
        this.id = new UserUserFunctionId(user.getId(), userFunction.getId());
        this.user = user;
        this.userFunction = userFunction;
    }

    public UserUserFunction(User user, Long functionId) {
        this.id = new UserUserFunctionId(user.getId(), functionId);
        this.user = user;
    }

    public UserUserFunction(Long userId, Long functionId) {
        this.id = new UserUserFunctionId(userId, functionId);
    }

    public UserUserFunctionId getId() {
        return id;
    }

    public void setId(UserUserFunctionId id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserFunction getUserFunction() {
        return userFunction;
    }

    public void setUserFunction(UserFunction userFunction) {
        this.userFunction = userFunction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserUserFunction that = (UserUserFunction) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(user, that.user) &&
                Objects.equals(userFunction, that.userFunction);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, user, userFunction);
    }
}
