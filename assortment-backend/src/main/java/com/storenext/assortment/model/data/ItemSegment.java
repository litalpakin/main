package com.storenext.assortment.model.data;

public class ItemSegment extends Segment{
	 private String englishItemName;
	 private String itemId;
	 private String itemName;
	 private Long dwItemId;
	 private String retailerItemCode;
	 
	public Long getDwItemId() {
		return dwItemId;
	}
	public void setDwItemId(Long dwItemId) {
		this.dwItemId = dwItemId;
	}
	public String getEnglishItemName() {
		return englishItemName;
	}
	public void setEnglishItemName(String englishItemName) {
		this.englishItemName = englishItemName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getRetailerItemCode() {
		return retailerItemCode;
	}

	public void setRetailerItemCode(String retailerItemCode) {
		this.retailerItemCode = retailerItemCode;
	}
}
