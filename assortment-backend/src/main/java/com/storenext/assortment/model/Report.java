package com.storenext.assortment.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "report")
public class Report extends BaseEntity {
	private static final long serialVersionUID = 9064126123533618021L;

	private String name;
	@Enumerated(EnumType.STRING)
	private ReportType reportType;
	@Lob
	private String description;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = "report_user_functions", joinColumns = { @JoinColumn(name = "report_id") }, inverseJoinColumns = {
			@JoinColumn(name = "user_function_id") })
	private Set<UserFunction> userFunctions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<UserFunction> getUserFunctions() {
		return userFunctions;
	}

	public void setUserFunctions(Set<UserFunction> userFunctions) {
		this.userFunctions = userFunctions;
	}

}
