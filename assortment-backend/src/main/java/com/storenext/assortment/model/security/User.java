package com.storenext.assortment.model.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.BaseEntity;
import com.storenext.assortment.model.Gender;
import com.storenext.assortment.model.StoreGroup;
import com.storenext.assortment.model.UserSupplierSubCategory;
import com.storenext.assortment.model.UserUserFunction;

@Entity
public class User extends BaseEntity implements UserDetails{

    private static final long serialVersionUID = -5712842287502605624L;

    @Column(length = 100)
    @NotNull
    @Size(min = 2, max = 100)
    private String password;
    @Column( length = 50)
    @NotNull
    @Size(min = 2, max = 50)
    private String firstName;
    @Column( length = 50)
    @NotNull
    @Size(min = 2, max = 50)
    private String lastName;
    @Column(length = 50,  unique = true)
    @NotNull
    @Size(min = 4, max = 50)
    private String username;
	@Enumerated(EnumType.STRING)
	@Column( name="sex")
    private Gender gender;
	@Column( name="date_of_birth")
	private Date dateOfBirth;


    @NotNull
    private boolean enabled;
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date lastPasswordResetDate;
    private String emailValidationToken;
    private Date emailValidationTokenCreateDate;
    @NotNull
    private Long companyId;
    @Enumerated(EnumType.STRING)
    private Authority authority;
    @OneToOne(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private UserUserFunction function;

    private Long storeId;
    
    private Boolean initialPasswordUsed;
    
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull
    private Date lastLoginDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Set<Date> lastLogins;
	@OneToMany(
			mappedBy = "user",
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER,
			orphanRemoval = true
	)
	private Set<UserSupplierSubCategory> supplierSubCategories = new HashSet<>();

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="group_shop_id")
	private StoreGroup storeGroup;
	
	@Enumerated(EnumType.STRING)
	private Language language = Language.HEBREW;
	
    private Long tokenCreated;
 
    private Long lastRequest;
    
	public StoreGroup getStoreGroup() {
		return storeGroup;
	}

	public void setStoreGroup(StoreGroup storeGroup) {
		this.storeGroup = storeGroup;
	}

	public Set<UserSupplierSubCategory> getSupplierSubCategories() {
		return supplierSubCategories;
	}

	public void setSupplierSubCategories(Set<UserSupplierSubCategory> supplierSubCategories) {
		this.supplierSubCategories = supplierSubCategories;
	}


	public UserUserFunction getFunction() {
		return function;
	}

	public void setFunction(UserUserFunction function) {
		this.function = function;
	}


	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    @Override
    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
		this.username = username;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	@Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(authority.toString()));
		return authorities;
	}

	public String getEmailValidationToken() {
		return emailValidationToken;
	}

	public void setEmailValidationToken(String emailValidationToken) {
		this.emailValidationToken = emailValidationToken;
	}

	public Date getEmailValidationTokenCreateDate() {
		return emailValidationTokenCreateDate;
	}

	public void setEmailValidationTokenCreateDate(Date emailValidationTokenCreateDate) {
		this.emailValidationTokenCreateDate = emailValidationTokenCreateDate;
	}

	

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	@Transient
	public boolean getOnBoarding(){
		if(dateOfBirth == null){
			return true;
		}
		if(gender == null){
			return true;
		}
		if(function == null){
			return true;
		}
		if(!function.getUserFunction().getFullCatalogSupport() && 
				(supplierSubCategories == null || supplierSubCategories.isEmpty())){
			return true;
		}
		if(!function.getUserFunction().getAllStoresSupport() && 
				storeGroup == null && storeId  == null){
			return true;
		}
		return false;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Long getTokenCreated() {
		return tokenCreated;
	}

	public void setTokenCreated(long tokenCreated) {
		this.tokenCreated = tokenCreated;
	}

	public Long getLastRequest() {
		return lastRequest;
	}

	public void setLastRequest(Long lastRequest) {
		this.lastRequest = lastRequest;
	}

	public Boolean isInitialPasswordUsed() {
		return initialPasswordUsed;
	}

	public void setInitialPasswordUsed(Boolean initialPasswordUsed) {
		this.initialPasswordUsed = initialPasswordUsed;
	}
}
