package com.storenext.assortment.model;

import java.util.Map;

public class ItemInStore {

	
	private Long dwItemKey;
	private String itemId;
	private String itemName;
	private Map<String,Object> additionalColumns;
	
	public ItemInStore(Long dwItemKey, String itemId, String itemName,  Map<String,Object> additionalColumns) {
		this.dwItemKey = dwItemKey;
		this.itemId = itemId;
		this.itemName = itemName;
		this.additionalColumns = additionalColumns;
	}
	public ItemInStore() {
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Long getDwItemKey() {
		return dwItemKey;
	}
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dwItemKey == null) ? 0 : dwItemKey.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemInStore other = (ItemInStore) obj;
		if (dwItemKey == null) {
			if (other.dwItemKey != null)
				return false;
		} else if (!dwItemKey.equals(other.dwItemKey))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		return true;
	}
	public Map<String, Object> getAdditionalColumns() {
		return additionalColumns;
	}
	public void setAdditionalColumns(Map<String, Object> additionalColumns) {
		this.additionalColumns = additionalColumns;
	}
	
}
