package com.storenext.assortment.model.type;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;

public class HibernateSetLongClobType extends HibernateCollectionClobType {
	@Override
	public Class<?> returnedClass() {
		return Set.class;
	}

	@Override
	protected Object stringToCollection(String str) {
		List<String> strings = Arrays.asList(str.split(DELIMITER));
		Set<Long> longs = strings.stream().filter(s -> !s.isEmpty()).map(Long::valueOf).collect(Collectors.toSet());
		return longs;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		} else if (!(x instanceof Set) || !(y instanceof Set)) {
			return false;
		} else {
			return x.equals(y);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value instanceof Set ? ((Set) value).stream().collect(Collectors.toSet()) : null;
	}
}
