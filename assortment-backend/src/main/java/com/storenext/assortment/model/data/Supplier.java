package com.storenext.assortment.model.data;

public class Supplier extends BaseDataEntity {

	public Supplier(){}
	
	public Supplier(Long id, String name) {
		super(id,name);
	}
}
