package com.storenext.assortment.model.data;

public class Brand extends BaseDataEntity{
	
    public Brand(){}
	
	public Brand(Long id, String name) {
		super(id,name);
	}
	
}
