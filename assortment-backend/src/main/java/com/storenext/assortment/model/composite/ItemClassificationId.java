package com.storenext.assortment.model.composite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ItemClassificationId implements Serializable{

	private static final long serialVersionUID = 7525374491965763377L;
	

	@Column(name = "format_code")
	private Long formatCode;
	@Column(name = "dw_item_key")
	private Long dwItemKey;
	
	public ItemClassificationId(){
		
	}
	
	public ItemClassificationId(Long formatCode, Long dwItemKey) {
		this.formatCode = formatCode;
		this.dwItemKey = dwItemKey;
	}
	
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	public Long getDwItemKey() {
		return dwItemKey;
	}
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dwItemKey == null) ? 0 : dwItemKey.hashCode());
		result = prime * result + ((formatCode == null) ? 0 : formatCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemClassificationId other = (ItemClassificationId) obj;
		if (dwItemKey == null) {
			if (other.dwItemKey != null)
				return false;
		} else if (!dwItemKey.equals(other.dwItemKey))
			return false;
		if (formatCode == null) {
			if (other.formatCode != null)
				return false;
		} else if (!formatCode.equals(other.formatCode))
			return false;
		return true;
	}
}
