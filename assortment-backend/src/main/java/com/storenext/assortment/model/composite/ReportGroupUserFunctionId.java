package com.storenext.assortment.model.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ReportGroupUserFunctionId implements Serializable {

	private static final long serialVersionUID = -8736018349012992606L;
	@Column(name = "user_function_id")
	private Long userFunctionId;
	@Column(name = "report_group_id")
	private Long reportGroupId;

	public ReportGroupUserFunctionId() {
	}

	public ReportGroupUserFunctionId(Long userFunctionId, Long reportGroupId) {
		this.userFunctionId = userFunctionId;
		this.reportGroupId = reportGroupId;
	}

	public Long getUserFunctionId() {
		return userFunctionId;
	}

	public void setUserFunctionId(Long userFunctionId) {
		this.userFunctionId = userFunctionId;
	}

	public Long getReportGroupId() {
		return reportGroupId;
	}

	public void setReportGroupId(Long reportGroupId) {
		this.reportGroupId = reportGroupId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ReportGroupUserFunctionId that = (ReportGroupUserFunctionId) o;
		return Objects.equals(userFunctionId, that.userFunctionId) && Objects.equals(reportGroupId, that.reportGroupId);
	}

	@Override
	public int hashCode() {

		return Objects.hash(userFunctionId, reportGroupId);
	}
}
