package com.storenext.assortment.model.type;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

public abstract class HibernateCollectionClobType implements UserType, Serializable {
	public static final String DELIMITER = ",";

	@Override
	public int[] sqlTypes() {
		return new int[] { Types.CLOB };
	}

	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
			throws HibernateException, SQLException {
		final StringBuilder buffer = new StringBuilder();
		try {
			Reader inputStream = resultSet.getCharacterStream(names[0]);
			if (inputStream == null) {
				return null;
			}
			char[] buf = new char[1024];
			int read = -1;

			while ((read = inputStream.read(buf)) > 0) {
				buffer.append(new String(buf, 0, read));
			}
			inputStream.close();
		} catch (IOException exception) {
			throw new HibernateException("Unable to read from resultset", exception);
		}
		return stringToCollection(buffer.toString());
	}

	protected abstract Object stringToCollection(String s);

	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object data, int index, SessionImplementor session)
			throws HibernateException, SQLException {
		if (data instanceof Collection) {
			String s = collectionToString(data);
			StringReader r = new StringReader(s);
			preparedStatement.setCharacterStream(index, r, s.length());
		} else {
			preparedStatement.setNull(index, sqlTypes()[0]);
		}
	}

	public String collectionToString(Object data) {
		if (data instanceof Collection) {
			return (String) ((Collection) data).stream().filter(Objects::nonNull).map(Object::toString).collect(Collectors.joining(DELIMITER));
		}
		return "";
	}

	@Override
	public Object replace(Object original, Object target, Object owner) {
		return this.deepCopy(original);
	}

	@Override
	public Object assemble(Serializable cached, Object owner) {
		return cached;
	}

	@Override
	public Serializable disassemble(Object value) {
		return (Serializable) value;
	}

	@Override
	public int hashCode(Object x) {
		return x.hashCode();
	}

	@Override
	public boolean isMutable() {
		return true;
	}
}
