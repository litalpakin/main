package com.storenext.assortment.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.storenext.assortment.model.composite.UserSupplierSubCategoryId;
import com.storenext.assortment.model.security.User;

@Entity
@Table(name = "users_suppliers")
public class UserSupplierSubCategory  implements Serializable {

	private static final long serialVersionUID = -2282016060520483059L;

	@EmbeddedId
	private UserSupplierSubCategoryId id;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("userId")
	private User user;

	public UserSupplierSubCategory() {
	}

	public UserSupplierSubCategory(User user,  Long supplierId, Long subCategoryId) {
		this.id = new UserSupplierSubCategoryId(user.getId(), supplierId, subCategoryId);
		this.user = user;
	}

	public UserSupplierSubCategoryId getId() {
		return id;
	}

	public void setId(UserSupplierSubCategoryId id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserSupplierSubCategory other = (UserSupplierSubCategory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
