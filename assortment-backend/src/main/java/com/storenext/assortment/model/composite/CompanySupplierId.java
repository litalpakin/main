package com.storenext.assortment.model.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompanySupplierId implements Serializable {
   
	private static final long serialVersionUID = 8954402501929396184L;
	@Column(name = "company_id")
    private Long companyId;
    @Column(name = "supplier_id")
    private Long supplierId;

    public CompanySupplierId() {
    }

    public CompanySupplierId(Long companyId, Long supplierId) {
        this.companyId = companyId;
        this.supplierId = supplierId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanySupplierId that = (CompanySupplierId) o;
        return Objects.equals(companyId, that.companyId) &&
                Objects.equals(supplierId, that.supplierId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(companyId, supplierId);
    }
}
