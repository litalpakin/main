package com.storenext.assortment.model.composite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserSupplierSubCategoryId implements Serializable {

	private static final long serialVersionUID = -3850777818714737613L;

	@Column(name = "user_id")
	private Long userId;
	@Column(name = "supplier_id")
	private Long supplierId;
	@Column(name = "sub_category_id")
	private Long subcategoryId;

	public UserSupplierSubCategoryId() {
	}

	public UserSupplierSubCategoryId(Long userId, Long supplierId, Long subCategoryId) {
		this.userId = userId;
		this.supplierId = supplierId;
		this.subcategoryId = subCategoryId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}



	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subcategoryId == null) ? 0 : subcategoryId.hashCode());
		result = prime * result + ((supplierId == null) ? 0 : supplierId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserSupplierSubCategoryId other = (UserSupplierSubCategoryId) obj;
		if (subcategoryId == null) {
			if (other.subcategoryId != null)
				return false;
		} else if (!subcategoryId.equals(other.subcategoryId))
			return false;
		if (supplierId == null) {
			if (other.supplierId != null)
				return false;
		} else if (!supplierId.equals(other.supplierId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
}
