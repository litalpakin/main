package com.storenext.assortment.model.data;

import java.util.HashMap;
import java.util.Map;

public class FilterInColumnManagement {
    private String header;
    private String subHeader;
    private Map<String, String> language = new HashMap<>();
    private Boolean doNotDisplay;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getSubHeader() {
        return subHeader;
    }

    public void setSubHeader(String subHeader) {
        this.subHeader = subHeader;
    }

    public Map<String, String> getLanguage() {
        return language;
    }

    public void setLanguage(Map<String, String> language) {
        this.language = language;
    }

    public Boolean getDoNotDisplay() {
        return doNotDisplay;
    }

    public void setDoNotDisplay(Boolean doNotDisplay) {
        this.doNotDisplay = doNotDisplay;
    }

    public void putLanguage(String language, String value) {
        getLanguage().put(language, value);
    }
}
