package com.storenext.assortment.model.data;

import org.modelmapper.ModelMapper;

import com.storenext.assortment.dto.SegmentDto;
import com.storenext.assortment.enums.Language;

public class Segment {
	protected Long brandId;
	protected String brandName;
	protected Long subcategoryId;
	protected String subcategoryName;
	protected Long categoryId;
	protected String categoryName;
	protected Long supplierId;
	protected String supplierName;
	protected Long classId;
	protected String className;
	protected String enBrandName;
	protected String enSubcategoryName;
	protected String enCategoryName;
	protected String enSupplierName;
	protected String enClassName;
	
	public Long getBrandId() {
		return brandId;
	}
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}
	public Long getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public String getSubcategoryName() {
		return subcategoryName;
	}
	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getClassId() {
		return classId;
	}
	public void setClassId(Long classId) {
		this.classId = classId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getEnBrandName() {
		return enBrandName;
	}
	public void setEnBrandName(String enBrandName) {
		this.enBrandName = enBrandName;
	}
	public String getEnSubcategoryName() {
		return enSubcategoryName;
	}
	public void setEnSubcategoryName(String enSubcategoryName) {
		this.enSubcategoryName = enSubcategoryName;
	}
	public String getEnCategoryName() {
		return enCategoryName;
	}
	public void setEnCategoryName(String enCategoryName) {
		this.enCategoryName = enCategoryName;
	}
	public String getEnSupplierName() {
		return enSupplierName;
	}
	public void setEnSupplierName(String enSupplierName) {
		this.enSupplierName = enSupplierName;
	}
	public String getEnClassName() {
		return enClassName;
	}
	public void setEnClassName(String enClassName) {
		this.enClassName = enClassName;
	}

	public SegmentDto getSegmentDto(Language language, ModelMapper modelMapper){
		SegmentDto dto  = modelMapper.map(this, SegmentDto.class);
		if(Language.ENGLISH == language){
			dto.setBrandName(getEnBrandName());
			dto.setCategoryName(getEnCategoryName());
			dto.setClassName(getEnClassName());
			dto.setSubcategoryName(getEnSubcategoryName());
			dto.setSupplierName(getEnSupplierName());
		}
		return dto;
	}
}
