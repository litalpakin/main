package com.storenext.assortment.model.data;

import org.modelmapper.ModelMapper;

import com.storenext.assortment.dto.CatalogFormatItemDto;
import com.storenext.assortment.enums.Language;

public class CatalogFormatItem extends FormatItem{
	protected Long subcategoryId;
	protected Long supplierId;
	public Long getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public SupplierSubCategory getSupplierSubCategory(){
		return new SupplierSubCategory(subcategoryId,null,supplierId,null);
	}
	
	public CatalogFormatItemDto getCatalogFormatItemDto(Language language, ModelMapper modelMapper){
		CatalogFormatItemDto dto  = modelMapper.map(this, CatalogFormatItemDto.class);
		if(Language.ENGLISH == language){
			dto.setItemName(getEnItemName());
		}
		return dto;
	}
}
