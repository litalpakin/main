package com.storenext.assortment.model;

import javax.persistence.Entity;

@Entity
public class LoginFailure extends BaseEntity {

	private static final long serialVersionUID = 2870435951099632535L;
	private Long userId;

	public LoginFailure() {
	}

	public LoginFailure(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
