package com.storenext.assortment.model;

public enum Gender {
	MALE, FEMALE
}
