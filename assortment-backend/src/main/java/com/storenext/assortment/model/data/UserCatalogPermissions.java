package com.storenext.assortment.model.data;

import java.util.HashSet;
import java.util.Set;

public class UserCatalogPermissions {

	Set<Long> brands = new HashSet<>();
	Set<Long> categories = new HashSet<>();
	Set<Long> classes = new HashSet<>();
	Set<Long> subCategories = new HashSet<>();
	Set<Long> suppliers = new HashSet<>();
	public Set<Long> getBrands() {
		return brands;
	}
	public void setBrands(Set<Long> brands) {
		this.brands = brands;
	}
	public Set<Long> getCategories() {
		return categories;
	}
	public void setCategories(Set<Long> categories) {
		this.categories = categories;
	}
	public Set<Long> getClasses() {
		return classes;
	}
	public void setClasses(Set<Long> classes) {
		this.classes = classes;
	}
	public Set<Long> getSubCategories() {
		return subCategories;
	}
	public void setSubCategories(Set<Long> subCategories) {
		this.subCategories = subCategories;
	}
	public Set<Long> getSuppliers() {
		return suppliers;
	}
	public void setSuppliers(Set<Long> suppliers) {
		this.suppliers = suppliers;
	}
}
