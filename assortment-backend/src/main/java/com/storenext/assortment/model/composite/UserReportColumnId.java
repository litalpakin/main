package com.storenext.assortment.model.composite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserReportColumnId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 45258110228754935L;
	@Column(name = "user_report_id")
	private Long userReportId;
	@Column(name = "column_name")
	private String columnName;

	public UserReportColumnId(Long userReportId, String columnName) {
		this.userReportId = userReportId;
		this.columnName = columnName;
	}
	
	public UserReportColumnId() {
	}

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public Long getUserReportId() {
		return userReportId;
	}

	public void setUserReportId(Long userReportId) {
		this.userReportId = userReportId;
	}
	

}
