package com.storenext.assortment.model;

import com.storenext.assortment.utils.LocaleUtils;

public enum CompanyType {
	
    ADMIN("company.type.admin"), MAIN("company.type.main"), SECONDARY("company.type.secondary");
	
	String caption;
	
	public String getCaption() {
		return LocaleUtils.getLocalized(caption);
	}

	CompanyType(String caption){
		this.caption = caption;
	}
}

