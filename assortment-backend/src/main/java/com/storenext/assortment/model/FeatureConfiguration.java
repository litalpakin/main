package com.storenext.assortment.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.storenext.assortment.enums.Feature;

@Entity
public class FeatureConfiguration extends BaseEntity {

	private static final long serialVersionUID = -49155667155159296L;
	@Enumerated(EnumType.STRING)
	private Feature feature;
	private boolean isEnabled;
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
