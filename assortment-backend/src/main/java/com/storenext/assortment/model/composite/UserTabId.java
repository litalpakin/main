package com.storenext.assortment.model.composite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

import com.storenext.assortment.dto.reports.UserReportType;
import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.TabType;

@Embeddable
public class UserTabId implements Serializable {

	private static final long serialVersionUID = -3522840053164880579L;
	@NotNull
	@Column(name = "user_id")
	private Long userId;
	@Column(name = "classification_group")
	@Enumerated(EnumType.STRING)
	private ClassificationGroup classificationGroup;
	@Column(name = "tab_type")
	@NotNull
	@Enumerated(EnumType.STRING)
	private TabType tabType;
	@Column(name = "column_name")
	@NotNull
	private String columnName;
	@Enumerated(EnumType.STRING)
	@NotNull
	private UserReportType page;
	
	public UserTabId(){
	}
	
	

	public UserTabId(Long userId, ClassificationGroup classificationGroup, TabType tabType, String columnName,UserReportType page) {
		this.userId = userId;
		this.classificationGroup = classificationGroup;
		this.tabType = tabType;
		this.columnName = columnName;
		this.page = page;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ClassificationGroup getClassificationGroup() {
		return classificationGroup;
	}

	public void setClassificationGroup(ClassificationGroup classificationGroup) {
		this.classificationGroup = classificationGroup;
	}

	public TabType getTabType() {
		return tabType;
	}

	public void setTabType(TabType tabType) {
		this.tabType = tabType;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classificationGroup == null) ? 0 : classificationGroup.hashCode());
		result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
		result = prime * result + ((tabType == null) ? 0 : tabType.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserTabId other = (UserTabId) obj;
		if (classificationGroup != other.classificationGroup)
			return false;
		if (columnName == null) {
			if (other.columnName != null)
				return false;
		} else if (!columnName.equals(other.columnName))
			return false;
		if (tabType != other.tabType)
			return false;
		if (page != other.page)
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}



	public UserReportType getPage() {
		return page;
	}



	public void setPage(UserReportType page) {
		this.page = page;
	}
}
