package com.storenext.assortment.model.type;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;

import com.storenext.assortment.enums.ClassificationGroup;

public class HibernateSetClassificationGroupClobType extends HibernateCollectionClobType {
	@Override
	public Class<?> returnedClass() {
		return Set.class;
	}

	@Override
	protected Object stringToCollection(String str) {
		if (StringUtils.isEmpty(str)) {
			return Collections.emptyList();
		}
		List<String> strings = Arrays.asList(str.split(DELIMITER));
		Set<ClassificationGroup> items = strings.stream().filter(s -> !s.isEmpty()).map(ClassificationGroup::valueOf).collect(Collectors.toSet());
		return items;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		} else if (!(x instanceof Set) || !(y instanceof Set)) {
			return false;
		} else {
			return x.equals(y);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value instanceof Set ? ((Set) value).stream().collect(Collectors.toSet()) : null;
	}
}
