package com.storenext.assortment.model;

import com.storenext.assortment.enums.TrafficLightsColor;

public interface ItemClassificationInterface {

	public Long getFormatCode();
	public Long getDwItemKey();
	public TrafficLightsColor getItemClassification();
}
