package com.storenext.assortment.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.storenext.assortment.enums.JobDataType;

@Entity
@Table(name = "job_data")
public class JobData extends BaseEntity {
	@NotNull
	@Enumerated(EnumType.STRING)
	private JobDataType jobType;
	@NotNull
	private String jobName;

	public JobDataType getJobType() {
		return jobType;
	}

	public void setJobType(JobDataType jobType) {
		this.jobType = jobType;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
}
