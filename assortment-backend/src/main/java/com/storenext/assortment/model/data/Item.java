package com.storenext.assortment.model.data;

public class Item {
	private Long dwItemKey;
	private String itemId;
	private String itemName;
	private String enItemName;
	
	public Long getDwItemKey() {
		return dwItemKey;
	}
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getEnItemName() {
		return enItemName;
	}
	public void setEnItemName(String enItemName) {
		this.enItemName = enItemName;
	}
}
