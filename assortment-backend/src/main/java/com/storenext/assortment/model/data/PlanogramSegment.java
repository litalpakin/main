package com.storenext.assortment.model.data;

public class PlanogramSegment {
	protected Long formatCode;
	protected String formatName;
	protected String enFormatName;
	protected Long categoryId;
	protected String categoryName;
	protected String enCategoryName;
	protected Long supplierId;
	protected Long dwStoreKey;
	protected String storeName;
	protected Long subcategoryId;

	public Long getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public String getEnFormatName() {
		return enFormatName;
	}

	public void setEnFormatName(String enFormatName) {
		this.enFormatName = enFormatName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getEnCategoryName() {
		return enCategoryName;
	}

	public void setEnCategoryName(String enCategoryName) {
		this.enCategoryName = enCategoryName;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public Long getDwStoreKey() {
		return dwStoreKey;
	}

	public void setDwStoreKey(Long dwStoreKey) {
		this.dwStoreKey = dwStoreKey;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
}
