package com.storenext.assortment.model.security;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class LoginStatistics {

	@Id
	@GeneratedValue
	protected Long id;
	@NotNull
	private Long userId;

	@NotNull
	private String period;
	@NotNull
	private Long counter;

	
	private List<Date> lastLoginDates;
}
