package com.storenext.assortment.model;

import com.storenext.assortment.enums.TrafficLightsColor;

public class DefaultItemClassification implements ItemClassificationInterface{

	Long formatCode;
	Long dwItemKey;
	String itemId;
	Long itemClassificationCode;
	String itemClassificationName;
	Object isItemClassificationEqualForAllFormat;
	
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public Long getDwItemKey() {
		return dwItemKey;
	}
	public void setDwItemKey(Long dwItemKey) { this.dwItemKey = dwItemKey;
	}

	public Long getItemClassificationCode() {
		return itemClassificationCode;
	}
	public void setItemClassificationCode(Long itemClassificationCode) {
		this.itemClassificationCode = itemClassificationCode;
	}
	public String getItemClassificationName() {
		return itemClassificationName;
	}
	public void setItemClassificationName(String itemClassificationName) {
		this.itemClassificationName = itemClassificationName;
	}
	@Override
	public TrafficLightsColor getItemClassification() {
		if(itemClassificationCode == null){
			return null;
		}
		return TrafficLightsColor.getColor(itemClassificationCode.intValue() - 1);
	}
	public Object getIsItemClassificationEqualForAllFormat() {
		return isItemClassificationEqualForAllFormat;
	}
	public void setIsItemClassificationEqualForAllFormat(Object isItemClassificationEqualForAllFormat) {
		this.isItemClassificationEqualForAllFormat = isItemClassificationEqualForAllFormat;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dwItemKey == null) ? 0 : dwItemKey.hashCode());
		result = prime * result + ((formatCode == null) ? 0 : formatCode.hashCode());
		result = prime * result + ((isItemClassificationEqualForAllFormat == null) ? 0
				: isItemClassificationEqualForAllFormat.hashCode());
		result = prime * result + ((itemClassificationCode == null) ? 0 : itemClassificationCode.hashCode());
		result = prime * result + ((itemClassificationName == null) ? 0 : itemClassificationName.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultItemClassification other = (DefaultItemClassification) obj;
		if (dwItemKey == null) {
			if (other.dwItemKey != null)
				return false;
		} else if (!dwItemKey.equals(other.dwItemKey))
			return false;
		if (formatCode == null) {
			if (other.formatCode != null)
				return false;
		} else if (!formatCode.equals(other.formatCode))
			return false;
		if (isItemClassificationEqualForAllFormat == null) {
			if (other.isItemClassificationEqualForAllFormat != null)
				return false;
		} else if (!isItemClassificationEqualForAllFormat.equals(other.isItemClassificationEqualForAllFormat))
			return false;
		if (itemClassificationCode == null) {
			if (other.itemClassificationCode != null)
				return false;
		} else if (!itemClassificationCode.equals(other.itemClassificationCode))
			return false;
		if (itemClassificationName == null) {
			if (other.itemClassificationName != null)
				return false;
		} else if (!itemClassificationName.equals(other.itemClassificationName))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		return true;
	}
	
}
