package com.storenext.assortment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.storenext.assortment.enums.UserFunctionType;

@Entity
@Table(name = "user_function")
public class UserFunction extends BaseEntity {
	private static final long serialVersionUID = 1758775668543005760L;

	@NotNull
	@Column(name = "is_default")
	private boolean isDefault;
	@NotNull
	private String name;
	@NotNull
	@Column(name = "company_type")
	@Enumerated(EnumType.STRING)
	private CompanyType companyType;
	private Boolean fullCatalogSupport;
	private Boolean allStoresSupport;
	private Boolean areaManager;
	private Boolean itemClassificator;
	@Enumerated(EnumType.STRING)
	private UserFunctionType type;
	private Boolean planogramCategory;
	private Boolean planogramStore;
	private Boolean balanceModelAccess;

	
	public Boolean getBalanceModelAccess() {
		return balanceModelAccess;
	}

	public void setBalanceModelAccess(Boolean balanceModelAccess) {
		this.balanceModelAccess = balanceModelAccess;
	}

	public Boolean getItemClassificator() {
		return itemClassificator;
	}

	public void setItemClassificator(Boolean itemClassificator) {
		this.itemClassificator = itemClassificator;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean aDefault) {
		isDefault = aDefault;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getFullCatalogSupport() {
		return fullCatalogSupport;
	}

	public void setFullCatalogSupport(Boolean fullCatalogSupport) {
		this.fullCatalogSupport = fullCatalogSupport;
	}

	public Boolean getAllStoresSupport() {
		return allStoresSupport;
	}

	public void setAllStoresSupport(Boolean allStoresSupport) {
		this.allStoresSupport = allStoresSupport;
	}

	public Boolean getAreaManager() {
		return areaManager;
	}

	public void setAreaManager(Boolean areaManager) {
		this.areaManager = areaManager;
	}

	public UserFunctionType getType() {
		return type;
	}

	public void setType(UserFunctionType type) {
		this.type = type;
	}

	public Boolean getPlanogramCategory() {
		return planogramCategory;
	}

	public void setPlanogramCategory(Boolean planogramCategory) {
		this.planogramCategory = planogramCategory;
	}

	public Boolean getPlanogramStore() {
		return planogramStore;
	}

	public void setPlanogramStore(Boolean planogramStore) {
		this.planogramStore = planogramStore;
	}
}
