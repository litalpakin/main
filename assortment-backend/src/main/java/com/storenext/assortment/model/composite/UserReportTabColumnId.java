package com.storenext.assortment.model.composite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserReportTabColumnId implements Serializable{


	private static final long serialVersionUID = 7516702204168945883L;
	@Column(name = "user_report_tab_id")
	private Long userReportTabId;
	
	@Column(name = "column_name")
	private String columnName;


	public UserReportTabColumnId(Long userReportTabId, String columnName) {
		this.userReportTabId = userReportTabId;
		this.columnName = columnName;
	}
	public UserReportTabColumnId(){
		
	}

	public Long getUserReportTabId() {
		return userReportTabId;
	}
	public void setUserReportTabId(Long userReportTabId) {
		this.userReportTabId = userReportTabId;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
}
