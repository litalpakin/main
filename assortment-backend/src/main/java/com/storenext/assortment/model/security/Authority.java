package com.storenext.assortment.model.security;

public enum Authority {
	ADMIN("Admin"),  USER("User");

	private String caption;

	Authority(String caption) {
		this.caption = caption;
	}

	public boolean atLeast(Authority role) {
		switch (role) {
		case ADMIN:
			return this == role;
		case USER:
			return this == role;
		default:
			throw new IllegalStateException("Unsupported role");

		}
	}

	public String getCaption() {
		return caption;
	}
}