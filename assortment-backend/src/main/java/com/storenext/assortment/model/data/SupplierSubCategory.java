package com.storenext.assortment.model.data;

public class SupplierSubCategory {



	protected Long subcategoryId;
	protected String subcategoryName;
	protected String supplierName;
	protected Long supplierId;
	
	public SupplierSubCategory(Long subcategoryId, String subcategoryName, Long supplierId, String supplierName) {
		this.subcategoryId = subcategoryId;
		this.subcategoryName = subcategoryName;
		this.supplierName = supplierName;
		this.supplierId = supplierId;
	}
	public SupplierSubCategory() {
	}
	public Long getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public String getSubcategoryName() {
		return subcategoryName;
	}
	public void setSubcategoryName(String subcategoryName) {
		this.subcategoryName = subcategoryName;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subcategoryId == null) ? 0 : subcategoryId.hashCode());
		result = prime * result + ((supplierId == null) ? 0 : supplierId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierSubCategory other = (SupplierSubCategory) obj;
		if (subcategoryId == null) {
			if (other.subcategoryId != null)
				return false;
		} else if (!subcategoryId.equals(other.subcategoryId))
			return false;
		if (supplierId == null) {
			if (other.supplierId != null)
				return false;
		} else if (!supplierId.equals(other.supplierId))
			return false;
		return true;
	}

}
