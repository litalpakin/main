package com.storenext.assortment.model.type;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.enums.SubcategoryClassification;

public class HibernateSetSubcategoryClassificationClobType extends HibernateCollectionClobType {
	@Override
	public Class<?> returnedClass() {
		return Set.class;
	}

	@Override
	protected Object stringToCollection(String string) {
		if (StringUtils.isEmpty(string)) {
			return Collections.emptySet();
		}
		return Arrays.stream(string.split(DELIMITER)).map(SubcategoryClassification::valueOf)
				.collect(Collectors.toSet());
	}

	@Override
	public String collectionToString(Object data) {
		if (!(data instanceof Set) && CollectionUtils.isEmpty((Set) data)) {
			return null;
		}
		return (String) ((Set) data).stream().map(value -> "" + value).collect(Collectors.joining(DELIMITER));
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		} else if (!(x instanceof Set) || !(y instanceof Set)) {
			return false;
		} else {
			return x.equals(y);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value instanceof Set ? ((Set) value).stream().collect(Collectors.toSet()) : null;
	}
}
