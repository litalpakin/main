package com.storenext.assortment.model;

import com.storenext.assortment.model.composite.CompanySupplierId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "companies_suppliers")
public class CompanySupplier implements Serializable {

    private static final long serialVersionUID = 9095344190008339610L;
    @EmbeddedId
    private CompanySupplierId id;
    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("companyId")
    private Company company;

    public CompanySupplier() {
    }

    public CompanySupplier(Company company, Long supplierId) {
        this.id = new CompanySupplierId(company.getId(), supplierId);
        this.company = company;
    }

    public CompanySupplierId getId() {
        return id;
    }

    public void setId(CompanySupplierId id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanySupplier that = (CompanySupplier) o;
        return Objects.equals(company, that.company) &&
                Objects.equals(id.getSupplierId(), that.getId().getSupplierId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, company);
    }
}
