package com.storenext.assortment.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "weekly_dashboard")
public class WeeklyDashboard extends BaseEntity {
	@NotNull
	private Integer week;
	@NotNull
	private Integer year;
	@NotNull
	private Long formatCode;
	@OneToMany(mappedBy = "weeklyDashboard", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<WeeklyDashboardItem> items = new HashSet<>();

	public WeeklyDashboard() {
	}

	public WeeklyDashboard(Integer week, Integer year, Long formatCode) {
		this.week = week;
		this.year = year;
		this.formatCode = formatCode;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Long getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public Set<WeeklyDashboardItem> getItems() {
		return items;
	}

	public void setItems(Set<WeeklyDashboardItem> items) {
		this.items = items;
	}

	public void addItem(WeeklyDashboardItem item) {
		this.items.add(item);
	}
}
