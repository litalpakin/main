package com.storenext.assortment.model.data;

import org.modelmapper.ModelMapper;

import com.storenext.assortment.dto.FormatItemDto;
import com.storenext.assortment.enums.Language;

public class FormatItem extends Item {

	private Long formatCode;

	public FormatItem() {}
	
	
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	
	public String getItemName(Language language) {
		if(Language.ENGLISH == language){
			return  super.getEnItemName();
		}else{
			return super.getItemName();
		}
	}

	public FormatItemDto getFormatItemDto(Language language, ModelMapper modelMapper){
		FormatItemDto dto  = modelMapper.map(this, FormatItemDto.class);
		if(Language.ENGLISH == language){
			dto.setItemName(getEnItemName());
		}
		return dto;
	}
}
