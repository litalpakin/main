package com.storenext.assortment.model.data;

public class RetailerSegment extends Segment {
	
	Long snSubcategoryId;

	public Long getSnSubcategoryId() {
		return snSubcategoryId;
	}

	public void setSnSubcategoryId(Long snSubcategoryId) {
		this.snSubcategoryId = snSubcategoryId;
	}

}
