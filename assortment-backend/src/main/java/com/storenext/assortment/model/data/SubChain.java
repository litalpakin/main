package com.storenext.assortment.model.data;

public class SubChain {
	private Long subChainCode;
	private Long sectorCode;
	private Long formatId;
	private String formatName;
	public Long getSubChainCode() {
		return subChainCode;
	}
	public void setSubChainCode(Long subChainCode) {
		this.subChainCode = subChainCode;
	}
	public Long getSectorCode() {
		return sectorCode;
	}
	public void setSectorCode(Long sectorCode) {
		this.sectorCode = sectorCode;
	}
	
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public Long getFormatId() {
		return formatId;
	}
	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}
	

}
