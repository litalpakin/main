package com.storenext.assortment.model.data;

import com.storenext.assortment.enums.SubcategoryClassification;

public class SubCategoryFormatClassification {

	
	private Long subCategoryId;
	private Long formatCode;
	private SubcategoryClassification subcategoryClassification;
	
	
	public Long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(Long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	public SubcategoryClassification getSubcategoryClassification() {
		return subcategoryClassification;
	}
	public void setSubcategoryClassification(SubcategoryClassification subcategoryClassification) {
		this.subcategoryClassification = subcategoryClassification;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formatCode == null) ? 0 : formatCode.hashCode());
		result = prime * result + ((subCategoryId == null) ? 0 : subCategoryId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubCategoryFormatClassification other = (SubCategoryFormatClassification) obj;
		if (formatCode == null) {
			if (other.formatCode != null)
				return false;
		} else if (!formatCode.equals(other.formatCode))
			return false;
		if (subCategoryId == null) {
			if (other.subCategoryId != null)
				return false;
		} else if (!subCategoryId.equals(other.subCategoryId))
			return false;
		return true;
	}
}
