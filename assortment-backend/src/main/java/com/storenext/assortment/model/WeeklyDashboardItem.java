package com.storenext.assortment.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.TrafficLightsColor;

@Entity
@Table(name = "weekly_dashboard_item")
public class WeeklyDashboardItem extends BaseEntity {
	@NotNull
	@ManyToOne
	@JoinColumn(name = "weekly_dashboard_id", nullable = false)
	private WeeklyDashboard weeklyDashboard;
	private Double value;
	@Enumerated(EnumType.STRING)
	private TrafficLightsColor color;
	@NotNull
	@Type(type = "com.storenext.assortment.model.type.HibernateSetClassificationGroupClobType")
	@Column(name = "classification_group", nullable = false)
	private Set<ClassificationGroup> classificationGroups = new HashSet<>();

	public WeeklyDashboardItem() {
	}

	public WeeklyDashboardItem(WeeklyDashboard weeklyDashboard, Double value, TrafficLightsColor color, Collection<ClassificationGroup> classificationGroups) {
		this.weeklyDashboard = weeklyDashboard;
		this.value = value;
		this.color = color;
		this.classificationGroups = new HashSet<>(classificationGroups);
	}

	public WeeklyDashboard getWeeklyDashboard() {
		return weeklyDashboard;
	}

	public void setWeeklyDashboard(WeeklyDashboard weeklyDashboard) {
		this.weeklyDashboard = weeklyDashboard;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public TrafficLightsColor getColor() {
		return color;
	}

	public void setColor(TrafficLightsColor color) {
		this.color = color;
	}

	public Set<ClassificationGroup> getClassificationGroups() {
		return classificationGroups;
	}

	public void setClassificationGroups(Set<ClassificationGroup> classificationGroups) {
		this.classificationGroups = classificationGroups;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		WeeklyDashboardItem that = (WeeklyDashboardItem) o;
		return weeklyDashboard.equals(that.weeklyDashboard) && Objects.equals(value, that.value) && color == that.color && classificationGroups.equals(that.classificationGroups);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), weeklyDashboard, value, color, classificationGroups);
	}
}
