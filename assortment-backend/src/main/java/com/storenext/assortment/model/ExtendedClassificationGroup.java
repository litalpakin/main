package com.storenext.assortment.model;

import com.storenext.assortment.enums.ClassificationGroup;

public class ExtendedClassificationGroup {
	
	private ClassificationGroup classificationGroup;
	private Object isItemClassificationEqualForAllFormat;
	
	
	public ExtendedClassificationGroup(ClassificationGroup classificationGroup,
			Object isItemClassificationEqualForAllFormat) {
		this.classificationGroup = classificationGroup;
		this.isItemClassificationEqualForAllFormat = isItemClassificationEqualForAllFormat;
	}
	public ClassificationGroup getClassificationGroup() {
		return classificationGroup;
	}
	public void setClassificationGroup(ClassificationGroup classificationGroup) {
		this.classificationGroup = classificationGroup;
	}
	public Object getIsItemClassificationEqualForAllFormat() {
		return isItemClassificationEqualForAllFormat;
	}
	public void setIsItemClassificationEqualForAllFormat(Object isItemClassificationEqualForAllFormat) {
		this.isItemClassificationEqualForAllFormat = isItemClassificationEqualForAllFormat;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classificationGroup == null) ? 0 : classificationGroup.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExtendedClassificationGroup other = (ExtendedClassificationGroup) obj;
		if (classificationGroup != other.classificationGroup)
			return false;
		return true;
	}

}
