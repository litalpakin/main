package com.storenext.assortment.model.composite;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class UserUserFunctionId implements Serializable {

	private static final long serialVersionUID = -5385385075936724449L;
	@Column(name = "user_id")
    private Long userId;
    @Column(name = "user_function_id")
    private Long userFunctionId;

    public UserUserFunctionId() {
    }

    public UserUserFunctionId(Long userId, Long userFunctionId) {
        this.userId = userId;
        this.userFunctionId = userFunctionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserFunctionId() {
        return userFunctionId;
    }

    public void setUserFunctionId(Long userFunctionId) {
        this.userFunctionId = userFunctionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserUserFunctionId that = (UserUserFunctionId) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(userFunctionId, that.userFunctionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, userFunctionId);
    }
}
