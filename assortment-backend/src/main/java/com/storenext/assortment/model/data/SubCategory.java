package com.storenext.assortment.model.data;

public class SubCategory extends BaseDataEntity{

	public SubCategory(){}
	
	public SubCategory(Long id, String name) {
		super(id,name);
	}
}
