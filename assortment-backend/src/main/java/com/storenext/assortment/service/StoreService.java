package com.storenext.assortment.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.model.data.CatalogFormatItem;
import com.storenext.assortment.model.data.FormatStore;
import com.storenext.assortment.model.data.Store;

@Service
public class StoreService {

    @Autowired
    SegmentRedshiftService segmentRedshiftService;

    public List<Store> getAll() {
        return segmentRedshiftService.getStores();
    }
    
    
    public Store getStoreById(Long id) {
        List<Store> stores = segmentRedshiftService.getStores().stream().filter(store -> store.getId().equals(id)).collect(Collectors.toList());
        if(stores == null || stores.isEmpty()){
        	return null;
        }
        return stores.get(0); 
    }
    public Set<Store> getStoresByIds(Set<Long> ids) {
    	return segmentRedshiftService.getStores().stream().filter(store ->ids.contains(store.getId())).collect(Collectors.toSet());
    }
    
    public Set<FormatStore> getAllFormats(){
    	return new HashSet<>(segmentRedshiftService.getFormatStoreList());
    }
    public Set<CatalogFormatItem> getAllFormatItems(){
    	return new HashSet<>(segmentRedshiftService.getFormatItemList());
    }
    public List<CatalogFormatItem> getAllOrderedFormatItems(){
    	return segmentRedshiftService.getFormatItemList();
    }
    public Set<FormatStore> getFormatsForStore(Long storeId){
    	return segmentRedshiftService.getFormatStoreList().stream().filter(f->f.getStoreId().equals(storeId)).collect(Collectors.toSet());
    }
    public Set<FormatStore> getFormatsForStores(Set<Long>  storeIds){
    	return segmentRedshiftService.getFormatStoreList().stream().filter(f->storeIds.contains(f.getStoreId())).collect(Collectors.toSet());
    }
    public Set<Long> getFormatsByStores(Set<Long>  storeIds){
    	Set<FormatStore> formatStores = getFormatsForStores(storeIds);
    	Set<Long> formats = formatStores.stream().map(fs->fs.getFormatId()).collect(Collectors.toSet());   
   	 	return formats;
    }
	
    public Set<Long> getStoreIdsForFormats(Set<Long> formatIds){
    	Set<Long> storeIds = new HashSet<>();
    	Set<FormatStore> formats =  segmentRedshiftService.getFormatStoreList().stream().filter(f->formatIds.contains(f.getFormatId())).collect(Collectors.toSet());
    	for(FormatStore format:formats){
    		storeIds.add(format.getStoreId());
    	}
    	return storeIds;
    }
    public Set<Long> getStoreIdsForFormatId(Long formatId){
    	Set<Long> storeIds = new HashSet<>();
    	Set<FormatStore> formats =  segmentRedshiftService.getFormatStoreList().stream().filter(f->f.getFormatId().equals(formatId)).collect(Collectors.toSet());
    	for(FormatStore format:formats){
    		storeIds.add(format.getStoreId());
    	}
    	return storeIds;
    }
}
