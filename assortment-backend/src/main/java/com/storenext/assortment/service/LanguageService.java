package com.storenext.assortment.service;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.Language;

@Service
public class LanguageService {
	@Value("${storenext.default.language:HEBREW}")
	private String defaultLanguageStr ;
	@Value("${storenext.supported.language:HEBREW,ENGLISH}")
	private String supportedLanguageStr;
	
	private Language defaultLanguage;
	private Set<Language> supportedLanguages = new HashSet<>();

	@PostConstruct
	private void init(){
		
		defaultLanguage = Language.valueOf(defaultLanguageStr);
		String[] supportedLanguageStrs = supportedLanguageStr.split(",");
		for(String supportedLanguage:supportedLanguageStrs){
			supportedLanguages.add(Language.valueOf(supportedLanguage));
		}
	}
	public Set<Language> getSupportedLanguages(){
		return supportedLanguages;
	}
	public Language getDefaultLanguage(){
		return defaultLanguage;
	}

}
