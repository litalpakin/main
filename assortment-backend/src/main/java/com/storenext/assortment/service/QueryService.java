package com.storenext.assortment.service;

import java.io.InputStream;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.storenext.assortment.config.RedshiftConfiguration;
import com.storenext.assortment.dto.CatalogData;
import com.storenext.assortment.dto.TableDataDto;
import com.storenext.assortment.dto.UserSelectionFilterDto;
import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.enums.QueryName;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.model.CatalogHierarchy;
import com.storenext.assortment.model.UserFunction;
import com.storenext.assortment.model.UserSupplierSubCategory;
import com.storenext.assortment.model.UserUserFunction;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.cache.CacheCleaner;
import com.storenext.assortment.service.cache.query.QueryCacheService;
import com.storenext.assortment.utils.SecurityUtils;

import io.jsonwebtoken.lang.Collections;

@Service
public class QueryService implements CacheCleaner {
	private static final Logger logger = LoggerFactory.getLogger(QueryService.class);

	private ExecutorService executor = null;
	private Cache<Object, Object> cache = null;

	@Autowired
	private ClusterTransformService clasterTransformService;
	@Autowired
	private StoreTransformService storeTransformService;
	@Qualifier(RedshiftConfiguration.FAST_TEMPLATE_POOL)
	@Autowired
	private List<NamedParameterJdbcOperations> dbOperationsPool;
	@Autowired
	private QueryCacheService queryCacheService;
	@Autowired
	private FeatureConfigurationService featureConfigurationService;
	@Autowired
	private UserService userService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private RegionalItemService regionalItemService;
	@Autowired
	private CatalogCacheService catalogCacheService;


	@Value("${query-cache.expire-after-write:2000}")
	private long queryCachExpireAfterWrite;

	@PostConstruct
	private void init() {
		executor = Executors.newFixedThreadPool(40);
		cache = CacheBuilder.newBuilder().expireAfterWrite(queryCachExpireAfterWrite, TimeUnit.SECONDS).build();

	}

	public Set<String> getSkippedColumns() {
		Set<String> skippedColumns = new HashSet<>();

		if (!featureConfigurationService.isEnabled(Feature.UNITED_BARCODE)) {
			skippedColumns.add("is_united_dw_item_key");
		}
		if (!featureConfigurationService.isEnabled(Feature.ITEM_STATUS_COLUMN)) {
			skippedColumns.add("general_status_key");
		}

		return skippedColumns;
	}

	public String readRedshiftSql(String queryName) {
		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(String.format("redshift/sql/%s.sql", queryName))) {
			return IOUtils.toString(inputStream, "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("Failed to read query: " + queryName, e);
		}
	}

	public String readPostgreSql(String queryName) {
		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(String.format("postgre/sql/%s.sql", queryName))) {
			return IOUtils.toString(inputStream, "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("Failed to read query: " + queryName, e);
		}
	}

	public void updateRequest(String uuid) {
		logger.info("Updated uuid: " + uuid);
		cache.put(uuid, uuid);
	}

	public void removeRequest(String uuid) {
		logger.info("Updated uuid: " + uuid);
		cache.invalidate(uuid);
	}

	public boolean checkRequest(String uuid) {
		if (StringUtils.isEmpty(uuid)) {
			logger.info("Request uuid: is empty");
			return true;
		}
		Object obj = cache.getIfPresent(uuid);
		boolean isExpaired = obj == null;
		if (isExpaired) {
			logger.info("Request uuid: " + uuid + " expired");
		}
		return isExpaired;
	}

	private Pair<SqlRowSet, Long> executeQuery(String sql, QueryName queryName, Map<String, Object> parameters, boolean systemRequest) {
		if (!systemRequest && checkRequest(SecurityUtils.currentRequestId())) {
			return null;
		}
		long start = System.currentTimeMillis();
		SqlRowSet response = getNamedParameterJdbcOperations(queryName).queryForRowSet(sql, Optional.ofNullable(parameters).orElse(new HashMap<>()));
		long queryRuntime = System.currentTimeMillis() - start;
		logger.info("Query " + queryName + " time " + queryRuntime);
		return Pair.of(response, queryRuntime);
	}

	private NamedParameterJdbcOperations getNamedParameterJdbcOperations(QueryName queryName) {
		int index = ThreadLocalRandom.current().nextInt(0, 2);
		return dbOperationsPool.get(index);
	}

	private String logQuery(String sql, Map<String, Object> parameters) {
		logger.info("Sending parameter to query: ");
		String fullQuery = getFullQueryString(sql, parameters);
		logger.info("Query is: " + fullQuery);
		return fullQuery;
	}

	private String getFullQueryString(String sql, Map<String, Object> parameters) {
		String fullQuery = sql;
		if (parameters == null) {
			return fullQuery;
		}

		for (Entry<String, Object> parameter : parameters.entrySet()) {
			String valueString;
			if (parameter.getValue() instanceof Collection) {
				@SuppressWarnings("rawtypes")
				Collection collection = (Collection) parameter.getValue();
				valueString = StringUtils.join(collection, ',');

			} else {
				valueString = "" + parameter.getValue();
			}

			logger.info(parameter.getKey() + " : " + valueString);
			if (parameter.getValue() instanceof String) {
				valueString = "'" + valueString + "'";
			}
			fullQuery = fullQuery.replaceAll(":" + parameter.getKey(), valueString);
		}
		return fullQuery;
	}

	public List<Map<String, Object>> executeQueryAndReturnRows(String sql, QueryName queryName) {
		return executeQueryAndReturnRows(sql, queryName, null, null, true, false);
	}

	public List<Map<String, Object>> executeQueryAndReturnRows(String sql, QueryName queryName, Map<String, Object> parameters) {
		return executeQueryAndReturnRows(sql, queryName, parameters, null, true, false);
	}

	public List<Map<String, Object>> executeQueryAndReturnRows(String sql, QueryName queryName, Map<String, Object> parameters, CatalogData catalogData) {
		return executeQueryAndReturnRows(sql, queryName, parameters, catalogData, false, false);
	}

	public List<Map<String, Object>> executeQueryAndReturnRows(String sql, QueryName queryName, Map<String, Object> parameters, CatalogData catalogData, boolean noFilter) {
		return executeQueryAndReturnRows(sql, queryName, parameters, catalogData, false, noFilter);
	}

	public List<Map<String, Object>> executeQueryAndReturnRows(String sql, QueryName queryName, Map<String, Object> parameters, CatalogData catalogData, boolean systemRequest, boolean noFilter) {
		sql = regionalItemService.applyRegional(sql);
		String fullQuery = logQuery(sql, parameters);
		List<Map<String, Object>> response = getFromCacheIfNotSystem(fullQuery, systemRequest);
		if(response == null) {
			Pair<SqlRowSet, Long> executeQueryPair = executeQuery(sql, queryName, parameters, systemRequest);
			response = rowSetToRows(executeQueryPair.getFirst());
			if(!systemRequest) {
				queryCacheService.add(Pair.of(response, executeQueryPair.getSecond()), queryName, fullQuery, noFilter);
			}
		}

		catalogCacheService.applyValues(userService.getCurrentUser() != null ? userService.getCurrentUser().getLanguage() : null, response,
				catalogData != null ? catalogData.getCatalogType() : null, queryName);

		return response;
	}

	private List<Map<String, Object>> getFromCacheIfNotSystem(String fullQuery, boolean systemRequest) {
		if (systemRequest) {
			return null;
		}

		List<Map<String, Object>> response = queryCacheService.getData(fullQuery);
		if (response != null) {
			logger.info("Returning from cache!");
		}

		return response;
	}

	public void filterColumns(List<Map<String, Object>> rows, Set<String> columnList) {
		for (Map<String, Object> row : rows) {
			row.keySet().removeAll(columnList);
		}
	}

	public void applyClassification(List<Map<String, Object>> data, String columnName, String targetColumnName) {
		for (Map<String, Object> row : data) {
			Object value = row.get(columnName);
			if (value instanceof Integer) {
				ClassificationGroup classification = ClassificationGroup.getClassification((Integer) value);
				if (targetColumnName != null) {
					row.put(targetColumnName, classification);
				} else {
					row.put(columnName, classification);
				}
			}
		}
	}

	public void replaceClassification(List<List<Object>> rows, int classificationColumnIndex) {
		for (List<Object> row : rows) {
			Object value = row.get(classificationColumnIndex);
			if (value instanceof Integer) {
				ClassificationGroup classification = ClassificationGroup.getClassification((Integer) value);
				row.set(classificationColumnIndex, classification);

			}
		}
	}

	private List<Map<String, Object>> rowSetToRows(SqlRowSet dataSet) {
		List<Map<String, Object>> result = new ArrayList<>();
		if (dataSet != null) {
			SqlRowSetMetaData metaData = dataSet.getMetaData();
			List<String> columnNames = new ArrayList<>();
			columnNames.add("");

			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				columnNames.add(metaData.getColumnName(i));
			}
			Set<String> skippedColumns = getSkippedColumns();
			while (dataSet.next()) {
				Map<String, Object> values = new HashMap<>(metaData.getColumnCount());
				for (int i = 1; i <= metaData.getColumnCount(); i++) {
					Object value = dataSet.getObject(i);
					if (value instanceof Clob) {
						try {
							value = ((Clob) value).getSubString(1, (int) ((Clob) value).length());
						} catch (SQLException e) {
							logger.error("", e);
						}
					}
					String columnName = metaData.getColumnName(i);
					if (skippedColumns.contains(columnName)) {
						continue;
					}
					if (columnName != null && columnName.equals("store_name")) {
						value = storeTransformService.map((String) value);
					}
					if (columnName != null && (columnName.equals("format_name") || columnName.equals("english_format_name"))) {
						value = clasterTransformService.map((String) value);
					}
					value = validateRound(value);
					values.put(columnNames.get(i), value);
				}
				result.add(values);
			}

		}
		return result;
	}

	public Object validateRound(Object value) {
		if (value instanceof Double && Math.round((Double) value * 10000) == 0) {
			return 0.;
		} else if (value instanceof Float && Math.round((Float) value * 10000) == 0) {
			return (float) 0.;
		} else {
			return value;
		}
	}

	
	public void executeRedshiftQueryTask(FutureTask<?> futureTask) {
		executor.execute(futureTask);
	}

	public String replacePlaceholdersByCatalogHierarchy(CatalogHierarchy catalogHierarchyLevel, String query) {
		switch (catalogHierarchyLevel) {
			case CLASS:
				query = query.replace("Qty_Attr_Id_levelField", "Qty_Attr_Id_class");
				query = query.replace("Qty_Attr_Id_levelField".toLowerCase(), "Qty_Attr_Id_class");
				query = query.replace("levelFieldPlaceholder", "class_key");

				break;
			case CATEGORY:
				query = query.replace("Qty_Attr_Id_levelField", "Qty_Attr_Id_Category");
				query = query.replace("Qty_Attr_Id_levelField".toLowerCase(), "Qty_Attr_Id_Category");
				query = query.replace("levelFieldPlaceholder", "category_key");
				break;
			case SUB_CATEGORY:
				query = query.replace("Qty_Attr_Id_levelField", "Qty_Attr_Id_Sub_category");
				query = query.replace("Qty_Attr_Id_levelField".toLowerCase(), "Qty_Attr_Id_Sub_category");
				query = query.replace("levelFieldPlaceholder", "sub_category_key");
				break;
			default:
				throw new ArgumentValidationException(catalogHierarchyLevel + " is not supported");
		}
		return query;
	}

	public String generateRequestFilterCatalogPlaceholder(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		String response = "";
		response += getCatalogClause(cAndNRequest, prefix, user);
		return response;
	}

	public String generateRequestFilterPlaceholder(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		String response = "";
		response += getCatalogClause(cAndNRequest, prefix, user);
		response = appendClause(response, getStoresClause(cAndNRequest.getStores(), prefix, user));
		response = createClusterFilter(cAndNRequest.getClusters(), prefix, response);
		return response;
	}

	public String getUserClusterClause(String prefix, User user) {
		if (user.getFunction().getUserFunction().getAllStoresSupport()) {
			return " " + prefix + "format_code is not null";
		} else if (user.getStoreGroup() != null) {
			return getFormatClauseByStores(user.getStoreGroup().getStores(), prefix);
		} else if (user.getStoreId() != null) {
			return getFormatClauseByStores(Stream.of(user.getStoreId()).collect(Collectors.toSet()), prefix);
		}
		return "";
	}

	private String getFormatClauseByStores(Set<Long> storeIds, String prefix) {
		Set<Long> formats = storeService.getFormatsByStores(storeIds);
		if (!formats.isEmpty()) {
			return createInClause(formats, prefix + "format_code");
		} else {
			return " " + prefix + "format_code is null";
		}
	}

	public String createClusterFilter(Set<Long> clusters, String prefix, String response) {
		return appendClause(response, createInClause(clusters, prefix + "format_code"));
	}

	public String generateRequestFilterNoFormatPlaceholder(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		String response = "";
		response += getCatalogClause(cAndNRequest, prefix, user);
		response = appendClause(response, getStoresClause(cAndNRequest.getStores(), prefix, user));
		return response;
	}

	public String generateRequestFilterStoresAndFormat(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		String response = "";
		response = appendClause(response, getStoresClause(cAndNRequest.getStores(), prefix, user));
		response = createClusterFilter(cAndNRequest.getClusters(), prefix, response);
		return response;
	}

	public String getStoreClause(Set<Long> storeIds, String prefix) {
		return createInClause(storeIds, prefix + "dw_store_key");
	}

	public String getStoresClause(Set<Long> storeIds, String prefix, User user) {
		String response = getUserStoreClause(prefix, user);
		if (Collections.isEmpty(storeIds)) {
			return response;
		} else {
			return appendClause(response, getStoreClause(storeIds, prefix));
		}
	}

	public String getUserStoreClause(String prefix, User user) {
		if (user.getFunction().getUserFunction().getAllStoresSupport()) {
			return " " + prefix + "dw_store_key is not null";
		} else if (user.getStoreGroup() != null) {
			return getStoreClause(user.getStoreGroup().getStores(), prefix);
		} else if (user.getStoreId() != null) {
			return getStoreClause(Stream.of(user.getStoreId()).collect(Collectors.toSet()), prefix);
		}
		return "";
	}

	public String getSegmentClause(CatalogData catalogData, String prefix, CatalogType catalogType) {
		String response = "";
		if (catalogData == null || !catalogData.isCatalogDataExists()) {
			return "";
		}
		response = appendClause(response, createInClause(catalogData.getBrands(), prefix + getBrandIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getCategories(), prefix + getCategoryIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getClasses(), prefix + getClassIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getSubCategories(), prefix + getSubCategoryIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getSuppliers(), prefix + getSupplierIdColumn(catalogType)));

		return response;
	}

	public String getCatalogSegmentClause(CatalogData catalogData, String prefix, CatalogType catalogType) {
		String response = "";
		if (catalogData == null || !catalogData.isCatalogDataExists()) {
			return "";
		}
		response = appendClause(response, createInClause(catalogData.getBrands(), prefix + getBrandIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getCategories(), prefix + getCategoryIdColumn(catalogType)));
		response = appendClause(response, createInClause(catalogData.getSubCategories(), prefix + getSubCategoryIdColumn(catalogType)));

		return response;
	}

	public String getSupplierIdColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_supplier_key";
		} else {
			return "supplier_key";
		}
	}

	public String getSupplierNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_supplier_name";
		} else {
			return "supplier_name";
		}
	}

	public String getBrandIdColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_sub_brand_hierarchy_2_key";
		} else {
			return "sub_brand_hierarchy_2_key";
		}
	}

	public String getBrandNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_sub_brand_hierarchy_2_name";
		} else {
			return "sub_brand_hierarchy_2_name";
		}
	}

	public String getSubCategoryNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_sub_category_name";
		} else {
			return "sub_category_name";
		}
	}

	public String getCategoryNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_category_name";
		} else {
			return "category_name";
		}
	}

	public String getClassNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_class_name";
		} else {
			return "class_name";
		}
	}

	public String getItemNameColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_item_name";
		} else {
			return "item_name";
		}
	}

	public String getCategoryIdColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_category_key";
		} else {
			return "category_key";
		}
	}

	public String getClassIdColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_class_key";
		} else {
			return "class_key";
		}
	}

	public String getSubCategoryIdColumn(CatalogType catalogType) {
		if (featureConfigurationService.isRetailerCatalog(catalogType)) {
			return "retailer_sub_category_key";
		} else {
			return "sub_category_key";
		}
	}

	public String getCatalogClause(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		return getCatalogClause(cAndNRequest == null ? null : cAndNRequest.getCatalogData(), prefix, user);
	}

	public String getCatalogClause(CatalogData catalogData, String prefix, User user) {
		String catalogClause = getCatalogClauseByUser(prefix, user);
		if (catalogData == null || !catalogData.isCatalogDataExists()) {
			return catalogClause;
		} else {
			return appendClause(catalogClause, getSegmentClause(catalogData, prefix, catalogData.getCatalogType()));
		}

	}

	public String getCategoryClause(UserSelectionFilterDto cAndNRequest, String prefix, User user) {
		String catalogClause = getCategoryClauseByUser(prefix, user);
		if (cAndNRequest == null || !cAndNRequest.isCatalogDataExists()) {
			return catalogClause;
		} else {
			return appendClause(catalogClause, getCatalogSegmentClause(cAndNRequest.getCatalogData(), prefix, cAndNRequest.getCatalogData().getCatalogType()));
		}

	}

	


	public String getCatalogClauseByUser(String prefix, User user) {
		String catalogClause;
		if (user.getFunction().getUserFunction().getFullCatalogSupport()) {
			catalogClause = prefix + "supplier_key is not null " + " and " + prefix + "sub_category_key is not null ";
		} else {
			Set<Long> suppliers = new HashSet<>();
			Set<Long> subCategories = new HashSet<>();
			for (UserSupplierSubCategory userSupplierSubCategory : user.getSupplierSubCategories()) {
				suppliers.add(userSupplierSubCategory.getId().getSupplierId());
				subCategories.add(userSupplierSubCategory.getId().getSubcategoryId());
			}
			CatalogData catalogData = new CatalogData(suppliers, subCategories, CatalogType.ANALYST);
			catalogClause = getSegmentClause(catalogData, prefix, CatalogType.ANALYST);
		}
		return catalogClause;
	}

	public String getCategoryClauseByUser(String prefix, User user) {
		String catalogClause;
		if (user.getFunction().getUserFunction().getFullCatalogSupport()) {
			catalogClause = prefix + "sub_category_key is not null ";
		} else {
			Set<Long> subCategories = new HashSet<>();
			for (UserSupplierSubCategory userSupplierSubCategory : user.getSupplierSubCategories()) {
				subCategories.add(userSupplierSubCategory.getId().getSubcategoryId());
			}
			CatalogData catalogData = new CatalogData(null, subCategories, CatalogType.ANALYST);
			catalogClause = getCatalogSegmentClause(catalogData, prefix, CatalogType.ANALYST);
		}
		return catalogClause;
	}

	public String createInClause(Set<Long> ids, String property) {
		if (Collections.isEmpty(ids)) {
			return "";
		}
		return " " + property + " in (" + StringUtils.join(ids, ',') + ")";
	}

	public String appendClause(String result, String clause) {
		if (StringUtils.isEmpty(result)) {
			return clause;
		}
		if (StringUtils.isEmpty(clause)) {
			return result;
		}
		return result + " and " + clause;
	}

	public List<String> getColumnNames(List<Map<String, Object>> data) {
		return data.isEmpty() ? new ArrayList<>() : new ArrayList<>(data.get(0).keySet());
	}

	
	
	@Override
	public void cleanCache() {
		cache.invalidateAll();
	}

	public void addRankField(TableDataDto tableDataDto) {
		for (int i = 1; i <= tableDataDto.getRows().size(); i++) {
			tableDataDto.getRows().get(i - 1).add(new Integer(i));
		}
	}

	public int getOrderingIndex(String orderFieldName, List<String> formatedColumnNames) {
		int orderFieldIndex = 0;
		for (String columnName : formatedColumnNames) {
			if (columnName.equalsIgnoreCase(orderFieldName)) {
				break;
			}
			orderFieldIndex++;
		}
		return orderFieldIndex;
	}

	public Set<String> getRetailerCatalogColumns() {
		return new HashSet<>(Arrays.asList("retailer_class_key", "retailer_class_name", "retailer_category_key", "retailer_category_name", "retailer_sub_category_key",
				"retailer_sub_category_name", "retailer_supplier_key", "retailer_supplier_name", "retailer_sub_brand_hierarchy_2_key", "retailer_sub_brand_hierarchy_2_name",
				"retailer_item_key", "retailer_item_name"));
	}

	public boolean userNoFilters(User user) {
		return Optional.ofNullable(user).map(User::getFunction).map(UserUserFunction::getUserFunction)
				.filter(UserFunction::getFullCatalogSupport)
				.filter(UserFunction::getAllStoresSupport)
				.isPresent();
	}
}
