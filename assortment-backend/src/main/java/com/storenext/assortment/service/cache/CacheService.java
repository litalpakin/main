package com.storenext.assortment.service.cache;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.config.RedshiftConfiguration;
import com.storenext.assortment.service.CatalogCacheService;
import com.storenext.assortment.service.StoreTransformService;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;
import com.zaxxer.hikari.util.UtilityElf;

@Service
public class CacheService {

	private final Logger logger = LoggerFactory.getLogger(CacheService.class);
	private Date lastCheck = new Date();
	@Autowired
	private List<CacheCleaner> cacheCleaners;
	@Autowired
	private CacheManager cacheManager;
	@Qualifier(RedshiftConfiguration.FAST_DATASOURCE_NAME)
	@Autowired
	private DataSource datasource;
	@Qualifier(RedshiftConfiguration.FAST_TEMPLATE_NAME)
	@Autowired
	private NamedParameterJdbcOperations fastOperations;
	@Autowired
	private StoreTransformService storeTransformService;
	@Autowired
	private CatalogCacheService catalogCacheService;
	@PostConstruct
	private void init() {
		lastCheck = getLastRefreshDate().get(0);
	}

	@Scheduled(fixedRate = 600000)
	private void cleanCache() {
		logger.info("Checking cache date, last check is " + lastCheck);

		// fetch last date
		List<Date> dates = getLastRefreshDate();
		if (CollectionUtils.isEmpty(dates)) {
			logger.error("update dates are empty");
			return;
		}
		Date updateDate = dates.get(0);
		if (lastCheck.before(updateDate)) {
			logger.info("Cleaning cache....");
			catalogCacheService.cleanCache();
			evictSpringCaches();
			for (CacheCleaner cacheCleaner : cacheCleaners) {
				cacheCleaner.cleanCache();
			}
			connectionPoolRestart((HikariDataSource)datasource);
			storeTransformService.clear();
			lastCheck = updateDate;
		}
	}

	public List<Date> getLastRefreshDate() {
		return fastOperations.query("SELECT max(update_data) as update_data FROM migvan.update_data ",
				Collections.emptyMap(), new RowMapper<Date>() {
					@Override
					public Date mapRow(ResultSet arg0, int arg1) throws SQLException {
						return arg0.getTimestamp("update_data");

					}
				});
	}

	private void evictSpringCaches() {
		cacheManager.getCacheNames().stream().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
	}

	private void connectionPoolRestart(HikariDataSource dataSource) {
		try {
			HikariConfig config = ((HikariPool) dataSource.getHikariPoolMXBean()).config;
			ThreadPoolExecutor assassinExecutor = UtilityElf.createThreadPoolExecutor(config.getMaximumPoolSize(),
					"pool restart", config.getThreadFactory(), new CallerRunsPolicy());
			try {
				while (((HikariPool) dataSource.getHikariPoolMXBean()).getActiveConnections() > 1) {
					Connection conn = ((HikariPool) dataSource.getHikariPoolMXBean()).getConnection();
					conn.abort(assassinExecutor);
					conn.close();
					((HikariPool) dataSource.getHikariPoolMXBean()).softEvictConnections();
				}
				Connection conn = ((HikariPool) dataSource.getHikariPoolMXBean()).getConnection();
				conn.abort(assassinExecutor);
				conn.close();
			} finally {
				assassinExecutor.shutdown();
				assassinExecutor.awaitTermination(10L, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			logger.error("Failed to restart pool");
		}
	}
}
