package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.MAIL_BAD_RESPONSE;
import static com.storenext.assortment.utils.Constants.RESEARCH_AND_STRATEGY;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.utils.LocaleUtils;

@Service
public class MailService {
	private static final Logger logger = LoggerFactory.getLogger(MailService.class);

	@Value("${sendgrid.api.key}")
	private String sendgridApiKey;

	@Value("${sendgrid.email.from}")
	private String emailAdress;

	@Value("${sendgrid.email.name}")
	private String emailAdressName;


	@Value("${sendgrid.generic.template.id:}")
	private String userGenericTemplateId;

	@Value("${sendgrid.block.user.template.id:}")
	private String blockUserTemplateId;

	@Value("${storenext.email.activation-url}")
	private String validationEmailUrl;

	@Value("${storenext.email.userPreferences-url}")
	private String userPreferencesUrl;

	@Value("${storenext.email.reset-url}")
	private String restEmailUrl;

	@Autowired
	private MailTextService mailTextService;
	@Autowired
	private LanguageService languageService;

	@Value("${spring.datasource.jdbc-url:none}")
	private String dbURl;
	@Value("${storenext.base.url}")
	private String baseUrl;


	@PostConstruct
	private void log() {
		logger.info("userPreferencesUrl " + userPreferencesUrl);
		logger.info("restEmailUrl " + restEmailUrl);
		logger.info("validationEmailUrl " + validationEmailUrl);
	}

	public boolean isSendFailed(Response response) {
		boolean success = response.getStatusCode() == HttpStatus.OK.value() || response.getStatusCode() == HttpStatus.ACCEPTED.value();
		return !success;
	}

	public void sendUserActivationMail(User user) {
		if (userNotActive(user)) {
			return;
		}

		String content = mailTextService.getActivationPasswordText(user);
		content = content.replace(":link", validationEmailUrl + "?type=create&token=" + user.getEmailValidationToken());
		content = content.replace(":user", getUser(user));

		Map<String, String> props = new HashMap<>();

		props.put(":content", content);

		Mail mail = prepareEmail(user.getUsername(), mailTextService.getActivationPasswordSubject(user), props, userGenericTemplateId);
		sendMail(null, null, user.getUsername(), mail);
	}

	private String getUser(User user) {
		return user.getFirstName();
	}

	public void sendResetPasswordApprovalMail(User user) {
		if (userNotActive(user)) {
			return;
		}

		String content = mailTextService.getResetPasswordApprovalText(user);
		content = content.replace(":user", getUser(user));
		Map<String, String> props = new HashMap<>();

		props.put(":content", content);
		Mail mail = prepareEmail(user.getUsername(), mailTextService.getResetPasswordApprovalSubject(user), props, userGenericTemplateId);
		sendMail(null, null, user.getUsername(), mail);
	}

	public void sendExternalResetPasswordMail(User user, String token) {
		if (userNotActive(user)) {
			return;
		}

		String content = mailTextService.getExternalResetPasswordText(user);
		content = content.replace(":link", restEmailUrl + "?type=reset&token=" + token);
		content = content.replace(":user", getUser(user));
		Map<String, String> props = new HashMap<>();

		props.put(":content", content);
		Mail mail = prepareEmail(user.getUsername(), mailTextService.getExternalResetPasswordSubject(user), props, userGenericTemplateId);
		sendMail(null, null, user.getUsername(), mail);
	}

	public void sendBlockUserMail(User user) {
		if (userNotActive(user)) {
			return;
		}

		Map<String, String> props = new HashMap<>();
		props.put(":user", getUser(user));

		Mail mail = prepareEmail(user.getUsername(), LocaleUtils.getLocalized(RESEARCH_AND_STRATEGY, user), props, blockUserTemplateId);
		sendMail(null, null, user.getUsername(), mail);
	}

	public void sendReportMail(String mailTo, String subject, String body) {
		Map<String, String> props = new HashMap<>();
		props.put(":content", body);
		Mail mail = prepareEmail(mailTo, subject, props, userGenericTemplateId);
		sendMail(null, null, mailTo, mail);
	}

	public void sendUserWelcomeMail(User user) {
		String content = mailTextService.getUserActivationForUserWithPassword(user);
		content = content.replace(":link", baseUrl);
		content = content.replace(":user", getUser(user));
		Map<String, String> props = new HashMap<>();
		props.put(":content", content);
		Mail mail = prepareEmail(user.getUsername(), mailTextService.getActivationPasswordSubject(user), props, userGenericTemplateId);
		sendMail(null, null, user.getUsername(), mail);
	}

	private Mail prepareEmail(String mailTo, String subject, Map<String, String> properties, String templateId) {
		Email mailFrom = new Email(emailAdress, emailAdressName);
		Email to = new Email(mailTo);
		Mail mail = new Mail(mailFrom, subject, to, new Content("text/html", " "));
		mail.setTemplateId(templateId);

		Personalization personalization = new Personalization();
		personalization.addTo(to);

		properties.keySet().forEach(propKey -> personalization.addSubstitution(propKey, properties.get(propKey)));

		mail.getPersonalization().clear();
		mail.addPersonalization(personalization);

		logger.info("mailTo: {}, content: {}", mailTo, properties.get(":content"));
		return mail;
	}

	private void sendMail(File file, String fileName, String mailTo, Mail mail) {
		SendGrid sendgrid = new SendGrid(sendgridApiKey);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sendgrid.api(request);
			if (isSendFailed(response)) {
				throw new RuntimeException(MAIL_BAD_RESPONSE + response.getBody());
			}
			logger.info("Mail notification sent to " + mailTo);
		} catch (Exception e) {
			logger.error("Cannot send mail", e);
			//throw new GeneralException(CANNOT_SEND_MAIL + e.getMessage());
		}
	}

	private boolean userNotActive(User user) {
		boolean userNotActive = !user.isEnabled();
		if (userNotActive) {
			logger.info("User {} not active.", user.getUsername());
		}

		return userNotActive;
	}
}
