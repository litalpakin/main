package com.storenext.assortment.service;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.security.User;

@Service
public class MailTextService {

	
	public String getExternalResetPasswordText(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return readMailContent("reset_password_iw");
		}else{
			return readMailContent("reset_password_en");
		}
	}
	public String getResetPasswordApprovalText(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return readMailContent("reset_password_approval_iw");
		}else{
			return readMailContent("reset_password_approval_en");
		}
	}
	public String getExternalResetPasswordSubject(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return "RA System – בקשה לאיפוס סיסמה";
		}else{
			return "RA system – Password reset";
		}

	}
	public String getResetPasswordApprovalSubject(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return "RA System – סיסמתך שונתה בהצלחה";
		}else{
			return "RA system – Your password has changed";
		}

	}
	
	public String getActivationPasswordText(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return readMailContent("user_activation_iw");
		}else{
			return readMailContent("user_activation_en");
		}
	}

	public String getUserActivationForUserWithPassword(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return readMailContent("user_activation_for_user_with_password_iw");
		}else{
			return readMailContent("user_activation_for_user_with_password_en");
		}
	}

	public String getActivationPasswordSubject(User user){
		if(user.getLanguage() == null || user.getLanguage() == Language.HEBREW){
			return "Welcome to RA (RoBI Assortment) System";
		}else{
			return "Welcome to RA (RoBI Assortment) System";
		}

	}
	private String readMailContent(String fileName) {
		try (InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(String.format("i18n/%s.properties", fileName))) {
			return IOUtils.toString(inputStream, "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("Failed to read content: " + fileName, e);
		}
	}
}
