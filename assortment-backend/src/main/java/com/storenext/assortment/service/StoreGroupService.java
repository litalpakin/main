package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.ID_IS_PRESENT;
import static com.storenext.assortment.utils.Constants.MANDATORY_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.dto.StoreGroupDto;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.exception.BadRequestException;
import com.storenext.assortment.model.StoreGroup;
import com.storenext.assortment.model.data.Format;
import com.storenext.assortment.model.data.FormatStore;
import com.storenext.assortment.repository.StoreGroupRepository;
import com.storenext.assortment.repository.UserRepository;
import com.storenext.assortment.service.cache.CacheCleaner;
import com.storenext.assortment.utils.LocaleUtils;

@Service
public class StoreGroupService implements CacheCleaner {
    
	@Autowired
	private SegmentRedshiftService segmentRedshiftService;
	@Autowired
    private StoreGroupRepository storeGroupRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private LanguageService languageService;
    private static final Logger logger = LoggerFactory.getLogger(StoreGroupService.class);
    @PostConstruct
    private void init(){
    	createStoreGroupsFromFormats();
    }
   @Transactional
	private void createStoreGroupsFromFormats() {
		logger.info("Updating store groups from cluster");
		List<FormatStore> formatStores = segmentRedshiftService.getFormatStoreList();
    	Map<Long,Set<Long>> formatIdToStores = new HashMap<>();
    	Map<Long,Format> formats = new HashMap<>();
    	//group format data
    	buildFormatsMaps(formatStores, formatIdToStores, formats);
    	List<StoreGroup> storeGroups = getAll();
    	Map<String,StoreGroup> namesToStoreGroup = storeGroups.stream().collect(Collectors.toMap(StoreGroup::getName, Function.identity()));
    	List<StoreGroup> storeGroupsForDeletion = new ArrayList<>();
    	List<StoreGroup> storeGroupsForUpdate = new ArrayList<>();
    	fillUpdateAndDeleteGroups(formatIdToStores, formats, storeGroups, storeGroupsForDeletion, storeGroupsForUpdate, namesToStoreGroup);
    	//now formats contains only new formats
    	createNewStoregroupsFromFormats(formatIdToStores, formats, storeGroupsForUpdate, namesToStoreGroup);
    	storeGroupRepository.save(storeGroupsForUpdate);
    	for(StoreGroup storeGroupForDeletion:storeGroupsForDeletion){
    		userRepository.deleteStoreGroup(storeGroupForDeletion.getId());
    		storeGroupRepository.delete(storeGroupForDeletion.getId());
    	}
	}

	private void fillUpdateAndDeleteGroups(Map<Long, Set<Long>> formatIdToStores, Map<Long, Format> formats,
			List<StoreGroup> storeGroups, List<StoreGroup> storeGroupsForDeletion,
			List<StoreGroup> storeGroupsForUpdate,Map<String,StoreGroup> namesToStoreGroup) {
		for(StoreGroup storeGroup:storeGroups){
    		if(storeGroup.getClusterId() != null){
    			Format format = formats.get(storeGroup.getClusterId());
    			if(format == null ){
    				storeGroupsForDeletion.add(storeGroup);
    			}else{
    				storeGroup.setStores(formatIdToStores.get(storeGroup.getClusterId()));
    				storeGroup.setName(getName(format,namesToStoreGroup));
    				formats.remove(storeGroup.getClusterId());
    				storeGroupsForUpdate.add(storeGroup);
    			}
    		}
    	}
	}

	private void buildFormatsMaps(List<FormatStore> formatStores, Map<Long, Set<Long>> formatIdToStores,
			Map<Long, Format> formats) {
		for(FormatStore formatStore:formatStores){
    		Format format = new Format();
    		modelMapper.map(formatStore,format);
    		
    		Set<Long> stores = formatIdToStores.get(format.getFormatId());
    		if(stores == null){
    			stores = new HashSet<>();
    			formatIdToStores.put(format.getFormatId(), stores);
    			formats.put(format.getFormatId(), format);
    		}
    		stores.add(formatStore.getStoreId());
    	}
	}
	private void createNewStoregroupsFromFormats(Map<Long, Set<Long>> formatIdToStores, Map<Long, Format> formats,
			List<StoreGroup> storeGroupsForUpdate,Map<String,StoreGroup> namesToStoreGroup) {
		for(Entry<Long, Format> format:formats.entrySet()){
    		StoreGroup sg = new StoreGroup();
    		sg.setName(getName(format.getValue(),namesToStoreGroup));
    		sg.setClusterId(format.getKey());
    		sg.setStores(formatIdToStores.get(format.getKey()));
    		storeGroupsForUpdate.add(sg);
    	}
	}
	
	private boolean validateName(String name,Map<String,StoreGroup> namesToStoreGroup,Format format){
		StoreGroup storeGroup = namesToStoreGroup.get(name);
    	if(storeGroup != null && (storeGroup.getClusterId() == null
    			|| !storeGroup.getClusterId().equals(format.getFormatId()))){
    		return true;
    	}else{
    		return false;
    	}
	}
    private String getName(Format format,Map<String,StoreGroup> namesToStoreGroup){
    	int index = 0;
    	String name = getName(format,index);
    	while(validateName(name,namesToStoreGroup,format)){
    		index++;
    		name = getName(format,index);
    	}
    	return name;
    }
    private String getName(Format format,int index){
    	String prefix = LocaleUtils.getLocalized("storeGroup.prefix",null,languageService.getDefaultLanguage().getLocale());
    	String name = prefix + " " + format.getFormatName();
    	if(index > 0){
    		name = name + "(" + index + ")";
    	}
    	return name;
    }
    /**
     * Return all groups.
     *
     * @return List of available groups
     * @see List< ShopGroup >
     */
    public List<StoreGroup> getAll() {
        return storeGroupRepository.findAll();
    }
  
    public StoreGroup getStoreGroupById(Long id) {
        return storeGroupRepository.findOne(id);
    }
    /**
     * Add new group object to DB.
     *
     * @param shopGroupDto (@see ShopGroupDto) object which will be stored to DB.
     * @return created objects
     * @see ShopGroup
     */
    @Transactional
    public StoreGroup add(StoreGroupDto storeGroupDto) {
    	StoreGroup storeGroup = modelMapper.map(storeGroupDto, StoreGroup.class);
        if (storeGroup.getId() != null) {
            throw new ArgumentValidationException(ID_IS_PRESENT);
        }
        storeGroup.setStores(storeGroupDto.getStores()
                .stream().map(it -> it.getId()).collect(Collectors.toSet()));
        return storeGroupRepository.save(storeGroup);
    }

    /**
     * Update existing group object in DB.
     *
     * @param shopGroupDto (@see ShopGroupDto) object which will be updated.
     * @return updated objects
     * @see ShopGroup
     */
    @Transactional
    public StoreGroup update(StoreGroupDto storeGroupDto) {
    	StoreGroup storeGroup = modelMapper.map(storeGroupDto, StoreGroup.class);
        if (storeGroup.getId() == null) {
            throw new ArgumentValidationException(MANDATORY_ID);
        }
        StoreGroup original = storeGroupRepository.findOne(storeGroup.getId());
        if(original.getClusterId() != null){
        	 throw new BadRequestException("Store group can't be edited");
        }
        storeGroup.setStores(storeGroupDto.getStores()
                .stream().map(it -> it.getId()).collect(Collectors.toSet()));
        return storeGroupRepository.save(storeGroup);
    }

    /**
     * Remove group from DB.
     *
     * @param id group id to remove.
     */
    @Transactional
    public void delete(Long id,Long alternativeId) {
    	if(alternativeId == null){
    		 throw new ArgumentValidationException("Alternative Id is mandatory for deletion");	
    	}
    	StoreGroup destinationStoreGroup = getStoreGroupById(alternativeId);
    	if(destinationStoreGroup == null){
   		  throw new ArgumentValidationException("Store Group not found for  Alternative Id " + alternativeId);	
    	}
    	StoreGroup sourceStoreGroup = getStoreGroupById(id);
    	if(sourceStoreGroup == null){
     		  throw new ArgumentValidationException("Store Group not found for Id " + id);	
      	}
    	 if(sourceStoreGroup.getClusterId() != null){
        	 throw new BadRequestException("Store group can't be deleted");
        }
    	userRepository.replaceStoreGroup(sourceStoreGroup, destinationStoreGroup);
        storeGroupRepository.delete(id);
    }

	@Override
	public void cleanCache() {
		init();
	}
}
