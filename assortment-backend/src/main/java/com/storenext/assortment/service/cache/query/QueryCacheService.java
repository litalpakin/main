package com.storenext.assortment.service.cache.query;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.ResourcePools;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.core.spi.service.StatisticsService;
import org.ehcache.core.statistics.CacheStatistics;
import org.ehcache.core.statistics.DefaultStatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.QueryName;
import com.storenext.assortment.service.cache.CacheCleaner;

import io.jsonwebtoken.lang.Collections;

@Service
public class QueryCacheService implements CacheCleaner {
	private final Logger logger = LoggerFactory.getLogger(QueryCacheService.class);

	private static final long HEAP_SIZE = 500L;
	private static final int COUNTER_SIZE = 5;

	private Cache<String, EhcacheValue<List<Map<String, Object>>>> cache = null;
	private Map<Integer, Map<Integer, LinkedHashSet<String>>> frequencyPrioritizedCounter = new HashMap<>();
	private CacheStatistics cacheStatistics;
	private Set<QueryName> supportedQueries = new HashSet<>(Arrays.asList(QueryName.newItemsLaunchePeriodMeasuresGroupFilterSql
			, QueryName.oldItemsMeasuresItemStoreSql
			, QueryName.oldItemsMeasuresGroupFilterSql
			, QueryName.newItemsAfterLaunchePeriodMeasuresGroupFilterSql
			, QueryName.comparativeGraphs1ItemsGroupQtySql
			, QueryName.comparativeGraphs2ItemsGroupSalesSql
			, QueryName.comparativeGraphs3ItemsClassificationQtySql
			, QueryName.comparativeGraphs4ItemsClassificationSalesSql
			, QueryName.comparativeGraphs5ItemsByClassificationSalesSql
			, QueryName.comparativeGraphs6AvailabilityDistributionPenetrationByClassificationSql
			, QueryName.comparativeGraphs7PotentialSalesByClassificationSql
			, QueryName.comparativeGraphs8SalesChainMarketSql
			, QueryName.dashboard1
			, QueryName.dashboard2
			, QueryName.dashboard3
			, QueryName.dashboard4
			, QueryName.dashboard5
			, QueryName.dashboard6
			, QueryName.dashboard7
			, QueryName.dashboard_valid_results, QueryName.itemsDetailedFormatSql,
			QueryName.annualWeeklyTrackingSql,
			QueryName.statisticsWeeklyTrackingSql,
			QueryName.sub_category_balance_model,
			QueryName.unitedBarcodeIndex,
			QueryName.bm_base_dynamic));

	@PostConstruct
	public void init() {
		StatisticsService statisticsService = new DefaultStatisticsService();
		CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
				.using(statisticsService)
				.build();
		cacheManager.init();

		CustomExpiryPolicy<String, EhcacheValue<List<Map<String, Object>>>> expiryPolicy = new CustomExpiryPolicy<>();
		ResourcePools resourcePools = ResourcePoolsBuilder.newResourcePoolsBuilder().heap(HEAP_SIZE, EntryUnit.ENTRIES).build();
		@SuppressWarnings("unchecked")
		Class<EhcacheValue<List<Map<String, Object>>>> myEhcacheValue = (Class<EhcacheValue<List<Map<String, Object>>>>) (Class<?>) EhcacheValue.class;
		CacheConfiguration<String, EhcacheValue<List<Map<String, Object>>>> cacheConfiguration = CacheConfigurationBuilder
				.newCacheConfigurationBuilder(String.class, myEhcacheValue, resourcePools)
				.withExpiry(expiryPolicy)
				.build();

		cache = cacheManager.createCache("queryCache", cacheConfiguration);
		cacheStatistics = statisticsService.getCacheStatistics("queryCache");

		frequencyPrioritizedCounter.put(0, createPrioritizedCounter());
	}

	private Map<Integer, LinkedHashSet<String>> createPrioritizedCounter() {
		Map<Integer, LinkedHashSet<String>> newPrioritizedCounter = new HashMap<>();
		for(int i = 1; i <= COUNTER_SIZE; i++) {
			newPrioritizedCounter.put(i, new LinkedHashSet<>());
		}

		return newPrioritizedCounter;
	}

	public void add(Pair<List<Map<String, Object>>, Long> responseQueryPair, QueryName queryName, String query, boolean noFilter) {
		logger.info("Start");
		List<Map<String, Object>> data = responseQueryPair.getFirst();
		if(Collections.isEmpty(data)) {
			return;
		}

		long queryRuntime = responseQueryPair.getSecond();
		int prioritized = countPrioritized(queryRuntime, queryName, noFilter);
		if (prioritized == COUNTER_SIZE) {
			return;
		}

		if(cacheStatistics.getTierStatistics().get("OnHeap").getMappings() >= HEAP_SIZE) {
			frequencyPrioritizedCounter:
			for (Map<Integer, LinkedHashSet<String>> prioritizedCounter : frequencyPrioritizedCounter.values()) {
				int p = prioritized - COUNTER_SIZE;
				while (p <= 0) {
					Iterator<String> iterator = prioritizedCounter.get(prioritized - p).iterator();
					if(iterator.hasNext()) {
						String firstKey = iterator.next();
						EhcacheValue<List<Map<String, Object>>> first = cache.get(firstKey);
						cache.remove(firstKey);
						iterator.remove();
						if (first == null) {
							logger.info("WARNING! key {}", firstKey);
						} else {
							logger.info("Query name: {} runtime: {} prioritized: {} remove from cache.", first.getQueryName(), first.getQueryRuntime(), first.getPrioritized());
						}

						break frequencyPrioritizedCounter;
					}

					p++;
				}
			}
		}

		cache.put(query, new EhcacheValue<>(data, query, queryName, queryRuntime, prioritized));
		frequencyPrioritizedCounter.get(0).get(prioritized).add(query);
		logger.info("Query name: {} runtime: {} prioritized: {} put in cache.", queryName, queryRuntime, prioritized);
	}

	private int countPrioritized(long queryRuntime, QueryName queryName, boolean noFilter) {
		switch (queryName) {
			case dashboard1:// scoreboard
			case dashboard2:
			case dashboard3:
			case dashboard4:
			case dashboard5:
			case dashboard6:
			case dashboard7:
			case dashboard_valid_results:
			case oldItemsMeasuresItemStoreSql://manage  old+ new items - launch + after launch
			case oldItemsIdItemFormatFilterSql:
			case newItemsAfterLaunchePeriodIdFilterSql:
			case newItemsLaunchePeriodIdFilterSql:
				return noFilter ? 1 : 4;
			case statisticsTabItemFormatClassificationSql://category report
			case measuresTabItemSql:
			case measuresTabSupplierSql:
			case measuresTabBrandSql:
			case clusterTabItemFormatClassificationSql:
				return 4;
		}

		if(queryRuntime < 5_000) {
			return 5;
		}

		return queryRuntime > 10_000 ? 2 : 3;
	}

	public List<Map<String, Object>> getData(String query) {
		EhcacheValue<List<Map<String, Object>>> ehcacheValue = cache.get(query);
		if (ehcacheValue == null) {
			return null;
		}

		int oldFrequency = ehcacheValue.getCounter() / COUNTER_SIZE;
		ehcacheValue.count();
		int newFrequency = ehcacheValue.getCounter() / COUNTER_SIZE;
		if (oldFrequency < newFrequency) {
			frequencyPrioritizedCounter.get(oldFrequency).get(ehcacheValue.getPrioritized()).remove(ehcacheValue.getQuery());
			if (!frequencyPrioritizedCounter.containsKey(newFrequency)) {
				frequencyPrioritizedCounter.put(newFrequency, createPrioritizedCounter());
			}

			logger.info("Query name: {} runtime: {} prioritized: {} new frequency: {}.", ehcacheValue.getQueryName(), ehcacheValue.getQueryRuntime(), ehcacheValue.getPrioritized(), newFrequency);
		} else {
			frequencyPrioritizedCounter.get(newFrequency).get(ehcacheValue.getPrioritized()).remove(ehcacheValue.getQuery());
			logger.info("Query name: {} runtime: {} prioritized: {} offer last.", ehcacheValue.getQueryName(), ehcacheValue.getQueryRuntime(), ehcacheValue.getPrioritized());
		}

		frequencyPrioritizedCounter.get(newFrequency).get(ehcacheValue.getPrioritized()).add(ehcacheValue.getQuery());

		return ehcacheValue.getData();
	}

	public Integer getPrioritized(String query) {
		return Optional.ofNullable(cache.get(query)).map(EhcacheValue::getPrioritized).orElse(null);
	}

	@Override
	public void cleanCache() {
		cache.clear();
	}
}
