package com.storenext.assortment.service.cache.query;

import java.time.Duration;

import com.storenext.assortment.enums.QueryName;

public class EhcacheValue<T> {
	private T data;
	private String query;
	private QueryName queryName;
	private long queryRuntime;
	private int prioritized;
	private int counter = 1;
	private long timeToLive = 60 * 60 * 24 * 7;


	public EhcacheValue(T data, String query, QueryName queryName, long queryRuntime, int prioritized) {
		this.data = data;
		this.query = query;
		this.queryName = queryName;
		this.queryRuntime = queryRuntime;
		this.prioritized = prioritized;
	}

	public T getData() {
		return data;
	}

	public String getQuery() {
		return query;
	}

	public QueryName getQueryName() {
		return queryName;
	}

	public long getQueryRuntime() {
		return queryRuntime;
	}

	public int getPrioritized() {
		return prioritized;
	}

	public Duration getTimeToLiveDuration() {
		return Duration.ofSeconds(timeToLive);
	}

	public void count() {
		this.counter = counter + 1;
	}

	public int getCounter() {
		return counter;
	}
}
