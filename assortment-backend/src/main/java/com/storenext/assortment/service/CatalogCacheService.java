package com.storenext.assortment.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.config.RedshiftConfiguration;
import com.storenext.assortment.data.CatalogMappings;
import com.storenext.assortment.dto.IdNameDto;
import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.enums.Language;
import com.storenext.assortment.enums.QueryName;
import com.storenext.assortment.model.data.ItemSegment;
import com.storenext.assortment.model.data.Store;

@Service
public class CatalogCacheService {

	private static final Logger logger = LoggerFactory.getLogger(CatalogCacheService.class);

	@Qualifier(RedshiftConfiguration.FAST_TEMPLATE_NAME)
	@Autowired
	private NamedParameterJdbcOperations jdbcOperations;
	
	@Autowired
	private FeatureConfigurationService featureConfigurationService;
	
	@Autowired
	private ColumnManipulationService columnManipulationService;
	
	@Autowired
	private ClusterTransformService clasterTransformService;
	
	@Autowired
	private StoreTransformService storeTransformService;
	private Map<Long,String> stores = new HashMap<>();
	private Map<Long,String> enFormats = new HashMap<>();
	private Map<Long,String> heFormats = new HashMap<>();
	private CatalogMappings enAnalistCatalog = null;
	private CatalogMappings heAnalistCatalog = null;
	private CatalogMappings enRetailerCatalog = null;
	private CatalogMappings heRetailerCatalog = null;
	@PostConstruct
	public void init() {
		initCaches();
	}

	public String getStoreName(Long dwStoreKey) {
		return stores.get(dwStoreKey);
	}

	public Map<Long, String> getFormats(Language language) {
		return Language.ENGLISH.equals(language) ? enFormats : heFormats;
	}

	public Set<Long> getFormatsCode() {
		return !heFormats.isEmpty() ? heFormats.keySet() : enFormats.keySet();
	}

	private void initCaches() {
		logger.info("Starting to init caches.....");
		List<ItemSegment> fullSegmentList =  getItemSegmentList();
		logger.info("Full segment list size is " + fullSegmentList.size());

		CatalogMappings enAnalistCatalog = new CatalogMappings();
		CatalogMappings heAnalistCatalog = new CatalogMappings();
		fillCatalogData(fullSegmentList, enAnalistCatalog, heAnalistCatalog);
		this.enAnalistCatalog = enAnalistCatalog;
		this.heAnalistCatalog = heAnalistCatalog;
		logger.info("En Analist Catalog item size is " + enAnalistCatalog.getItems().size());
		logger.info("He Analist Catalog item size is " + heAnalistCatalog.getItems().size());

		if(featureConfigurationService.isEnabled(Feature.RETAILER_CATALOG_TYPE)) {
			CatalogMappings enRetailerCatalog = new CatalogMappings();
			CatalogMappings heRetailerCatalog = new CatalogMappings();
			List<ItemSegment> retailerSegmentList =  getRetailerItemSegmentList();
			fillCatalogData(retailerSegmentList, enRetailerCatalog, heRetailerCatalog);
			this.heRetailerCatalog = heRetailerCatalog;
			this.enRetailerCatalog = enRetailerCatalog;
		}
		logger.info("Loading stores ...");
		loadStores();
		logger.info("Stores size is " + stores.size());
		logger.info("Caches init finished");

	}

	private  void loadStores() {
		 jdbcOperations.query(
			" SELECT distinct store_name,dw_store_key, format_code, format_name,english_format_name "
				+ " FROM migvan.assortment_item_store_catalog "
                , Collections.emptyMap(),new RowMapper<Store>(){
					@Override
					public Store mapRow(ResultSet arg0, int arg1) throws SQLException {
						stores.put(arg0.getLong("dw_store_key"), storeTransformService.map(arg0.getString("store_name")));
						enFormats.put(arg0.getLong("format_code"), clasterTransformService.map(arg0.getString("english_format_name")));
						heFormats.put(arg0.getLong("format_code"), clasterTransformService.map(arg0.getString("format_name")));
						return null;
					}
                });
	}
	
	private void fillCatalogData(List<ItemSegment> segmentList, CatalogMappings enCatalog,
			CatalogMappings heCatalog) {
		segmentList.stream().forEach(s->{
				heCatalog.getBrands().put(s.getBrandId(),s.getBrandName());
				enCatalog.getBrands().put(s.getBrandId(),s.getEnBrandName());
				heCatalog.getCategories().put(s.getCategoryId(), s.getCategoryName());
				enCatalog.getCategories().put(s.getCategoryId(), s.getEnCategoryName());
				heCatalog.getClasses().put(s.getClassId(),s.getClassName());
				enCatalog.getClasses().put(s.getClassId(),s.getEnClassName());
				heCatalog.getItems().put(s.getItemId(), s.getItemName());
				enCatalog.getItems().put(s.getItemId(), s.getEnglishItemName());
				heCatalog.getItemEntities().put(s.getDwItemId(),new IdNameDto(s.getItemId(), s.getItemName(), s.getRetailerItemCode()));
				enCatalog.getItemEntities().put(s.getDwItemId(),new IdNameDto(s.getItemId(), s.getEnglishItemName(), s.getRetailerItemCode()));
				heCatalog.getSubCategories().put(s.getSubcategoryId(), s.getSubcategoryName());
				enCatalog.getSubCategories().put(s.getSubcategoryId(), s.getEnSubcategoryName());
				heCatalog.getSuppliers().put(s.getSupplierId(), s.getSupplierName());
				enCatalog.getSuppliers().put(s.getSupplierId(), s.getEnSupplierName());
			});
	}
	
	public CatalogMappings getEnAnalistCatalog() {
		return enAnalistCatalog;
	}

	public CatalogMappings getHeAnalistCatalog() {
		return heAnalistCatalog;
	}

	public CatalogMappings getEnRetailerCatalog() {
		return enRetailerCatalog;
	}

	public CatalogMappings getHeRetailerCatalog() {
		return heRetailerCatalog;
	}

	public void applyValues(Language language,List<Map<String, Object>> response,CatalogType catalogType,QueryName queryName) {
		if(CollectionUtils.isEmpty(response) || (queryName != null && queryName == QueryName.unitedBarcodeIndex)) {
			 return;
		}
		 CatalogMappings analistCatalog;
		 CatalogMappings retailerCatalog;
		 Map<Long,String> formats;
		 if(language != null && language == Language.ENGLISH) {
			 analistCatalog = enAnalistCatalog;
			 retailerCatalog = enRetailerCatalog;

			 formats = enFormats;
		 }else {
			 analistCatalog = heAnalistCatalog;
			
			 retailerCatalog = heRetailerCatalog;
			 formats = heFormats;
		 }
		for(Map<String, Object> row: response) {
			correctRetailerColumns(retailerCatalog,row);
			addNames(language,row, catalogType, analistCatalog, retailerCatalog,formats);
		}
	}
	private void correctRetailerColumns(CatalogMappings retailerCatalog,Map<String, Object> values)  {
		if(retailerCatalog == null) {
			values.remove("retailer_sub_category_key");
			values.remove("retailer_supplier_key");
			values.remove("retailer_category_key");
			values.remove("retailer_class_key");
			values.remove("retailer_item_name");
			values.remove("retailer_sub_brand_hierarchy_2_key");
		}
	}

	private void addName(String keyColumn,String nameColumn, Map<Long, String> descriptions,Map<String, Object> values) {
		if(values.containsKey(keyColumn)) {
		   Object valueObj = values.get(keyColumn);
		   Long value = null;
		   if(valueObj instanceof Integer) {
			   value  = ((Integer)valueObj).longValue() ;
		   }else {
			   value = (Long)valueObj;
		   }
		   values.put(nameColumn, descriptions.get(value));	
		}
		
	}
	private void addItemId(Map<String, Object> values,CatalogType catalogType,CatalogMappings analistCatalog,CatalogMappings retailerCatalog) {
		if(values.containsKey("item_id")) {
		   Object valueObj = values.get("item_id");
		   String value = "" + valueObj;
		   if( catalogType != null && catalogType == CatalogType.ANALYST) {
			   values.put("item_name", analistCatalog.getItems().get(value));
		   }else if(retailerCatalog != null) {
				values.put("retailer_item_name", retailerCatalog.getItems().get(value));
		   }
		   if( catalogType == null) {
			   values.put("item_name", analistCatalog.getItems().get(value));
		   }
		  
		}
	}

	private void addDwItemKey(Map<String, Object> values, CatalogType catalogType, CatalogMappings analistCatalog, CatalogMappings retailerCatalog) {
		if(values.containsKey("dw_item_key")) {
			Object valueObj = values.get("dw_item_key");
			Long value;
			if(valueObj instanceof Integer) {
				value = ((Integer) valueObj).longValue();
			} else {
				value = (Long) valueObj;
			}

			String code = null;
			if(catalogType != null && catalogType == CatalogType.ANALYST) {
				IdNameDto dto = analistCatalog.getItemEntities().get(value);
				if(dto == null) {
					throw new RuntimeException("Can't find item code for analistCatalog " + value);
				}
				code = dto.getCode();
				addDwItemDtoValues(values, dto, "item_name");
			} else if(retailerCatalog != null) {
				IdNameDto dto = retailerCatalog.getItemEntities().get(value);
				if(dto == null) {
					throw new RuntimeException("Can't find item code for retailerCatalog " + value);
				}
				code = dto.getCode();
				addDwItemDtoValues(values, dto, "retailer_item_name");
			}

			if(catalogType == null) {
				IdNameDto dto = analistCatalog.getItemEntities().get(value);
				if(dto == null) {
					throw new RuntimeException("Can't find item code for analistCatalog " + value);
				}
				code = dto.getCode();
				addDwItemDtoValues(values, dto, "item_name");
			}

			if(featureConfigurationService.isEnabled(Feature.RETAILER_CATALOG_TYPE)) {
				values.put("retailer_item_code", code);
			}
		}
	}



	private void addDwItemDtoValues(Map<String, Object> values, IdNameDto dto, String itemNameColumn) {
		if(dto != null) {
			   values.put(itemNameColumn,dto.getName());
			   values.put("item_id",dto.getId()); 
		   }
	}
	private void addNames(Language language,Map<String, Object> values,CatalogType catalogType,	
			CatalogMappings analistCatalog,CatalogMappings retailerCatalog,Map<Long,String> formats) {
		 
		 
		 addName("sub_category_key","sub_category_name",analistCatalog.getSubCategories(),values);
		 addName("category_key","category_name",analistCatalog.getCategories(),values);
		 addName("class_key","class_name",analistCatalog.getClasses(),values);
		 addName("supplier_key","supplier_name",analistCatalog.getSuppliers(),values);
		 addName("sub_brand_hierarchy_2_key","sub_brand_hierarchy_2_name",analistCatalog.getBrands(),values);
		 if(retailerCatalog != null) {
			 addName("retailer_sub_category_key","retailer_sub_category_name",retailerCatalog.getSubCategories(),values);
			 addName("retailer_category_key","retailer_category_name",retailerCatalog.getCategories(),values);
			 addName("retailer_class_key","retailer_class_name",retailerCatalog.getClasses(),values);
			 addName("retailer_supplier_key","retailer_supplier_name",retailerCatalog.getSuppliers(),values);
			 addName("retailer_sub_brand_hierarchy_2_key","retailer_sub_brand_hierarchy_2_name",retailerCatalog.getBrands(),values);
		 }
		 addName("sub_category_key","sub_category_name",analistCatalog.getSubCategories(),values);
		 addName("dw_store_key","store_name",stores,values);
		 addName("format_code","format_name",formats,values);

		 addItemId(values, catalogType, analistCatalog, retailerCatalog);
		 addDwItemKey(values, catalogType, analistCatalog, retailerCatalog);
			
	}
	
	public void cleanCache() {
		initCaches();
		
	}
	private List<ItemSegment> getBMItemSegmentList() {
        return jdbcOperations.query(
        		  "SELECT  m.sub_brand_hierarchy_2_key, "
      	                + " m.category_key "
      	                + " ,m.supplier_key " 
      	                + " ,m.class_key " 
      	                + " ,m.sub_category_key "
      	                + " ,m.item_id,m.dw_item_key " 
      	                + " FROM migvan.assortment_item_hierarchy_multi_lang m " 
      					+ " inner join (select dw_item_keyfrom migvan.assortment_fact_rank_1_24_shortedgroup by dw_item_key) f "
      					+ " on f.dw_item_key= m.dw_item_key" 
      					+ " group by m.sub_brand_hierarchy_2_key, "
      	                + " m.category_key "
      	                + " ,m.supplier_key " 
      	                + " ,m.class_key " 
      	                + " ,m.sub_category_key "
      	                + " ,m.item_id,m.dw_item_key " 
            
                
                , Collections.emptyMap(),new RowMapper<ItemSegment>(){
					@Override
					public ItemSegment mapRow(ResultSet arg0, int arg1) throws SQLException {
						ItemSegment segment = new ItemSegment();
						segment.setBrandId(arg0.getLong("sub_brand_hierarchy_2_key"));	
						segment.setBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("sub_brand_hierarchy_2_name")));
						segment.setCategoryId(arg0.getLong("category_key"));
						segment.setCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("category_name")));
						segment.setSupplierId(arg0.getLong("supplier_key"));
						segment.setSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("supplier_name")));
						segment.setSubcategoryId(arg0.getLong("sub_category_key"));
						segment.setSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("sub_category_name")));
						segment.setClassId(arg0.getLong("class_key"));
						segment.setClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("class_name")));
						segment.setEnBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_sub_brand_hierarchy_2_name")));
						segment.setEnCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_category_name")));
						segment.setEnClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_class_name")));
						segment.setEnSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_sub_category_name")));
						segment.setEnSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_supplier_name")));
						segment.setItemId(arg0.getString("item_id"));
						segment.setDwItemId(arg0.getLong("dw_item_key"));
						segment.setItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("item_name")));
						segment.setEnglishItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_item_name")));
						return segment;
					}
                });
    }
	private List<ItemSegment> getItemSegmentList() {
	        return jdbcOperations.query(
	                "SELECT  sub_brand_hierarchy_2_key,sub_brand_hierarchy_2_name, "
	                + " category_key, category_name "
	                + " ,supplier_key, supplier_name " 
	                + " ,class_key, class_name " 
	                + " ,sub_category_key, sub_category_name "
	                + " ,item_id,item_name,dw_item_key "
	                + " ,english_sub_category_name, english_category_name "
	                + " ,english_class_name, english_supplier_name "
	                + " ,english_sub_brand_hierarchy_2_name"
	                + " ,english_item_name "  
	                + " FROM migvan.assortment_item_hierarchy_multi_lang " 
	            
	                
	                , Collections.emptyMap(),new RowMapper<ItemSegment>(){
						@Override
						public ItemSegment mapRow(ResultSet arg0, int arg1) throws SQLException {
							ItemSegment segment = new ItemSegment();
							segment.setBrandId(arg0.getLong("sub_brand_hierarchy_2_key"));	
							segment.setBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("sub_brand_hierarchy_2_name")));
							segment.setCategoryId(arg0.getLong("category_key"));
							segment.setCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("category_name")));
							segment.setSupplierId(arg0.getLong("supplier_key"));
							segment.setSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("supplier_name")));
							segment.setSubcategoryId(arg0.getLong("sub_category_key"));
							segment.setSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("sub_category_name")));
							segment.setClassId(arg0.getLong("class_key"));
							segment.setClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("class_name")));
							segment.setEnBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_sub_brand_hierarchy_2_name")));
							segment.setEnCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_category_name")));
							segment.setEnClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_class_name")));
							segment.setEnSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_sub_category_name")));
							segment.setEnSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_supplier_name")));
							segment.setItemId(arg0.getString("item_id"));
							segment.setDwItemId(arg0.getLong("dw_item_key"));
							segment.setItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("item_name")));
							segment.setEnglishItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_item_name")));
							return segment;
						}
	                });
	    }
		private List<ItemSegment> getRetailerItemSegmentList() {
	        return jdbcOperations.query(
	                "SELECT  retailer_sub_brand_hierarchy_2_key,retailer_sub_brand_hierarchy_2_name, "
	                + " retailer_category_key, retailer_category_name "
	                + " ,retailer_supplier_key, retailer_supplier_name " 
	                + " ,retailer_class_key, retailer_class_name " 
	                + " ,retailer_sub_category_key, retailer_sub_category_name "
	                + " ,item_id,dw_item_key,retailer_item_name, retailer_item_code "
	                + " ,english_retailer_sub_category_name, english_retailer_category_name "
	                + " ,english_retailer_class_name, english_retailer_supplier_name "
	                + " ,english_retailer_sub_brand_hierarchy_2_name"
	                + " ,english_retailer_item_name "  
	                + " FROM migvan.assortment_item_hierarchy_multi_lang " 
	            
	                
	                , Collections.emptyMap(),new RowMapper<ItemSegment>(){
						@Override
						public ItemSegment mapRow(ResultSet arg0, int arg1) throws SQLException {
							ItemSegment segment = new ItemSegment();
							segment.setBrandId(arg0.getLong("retailer_sub_brand_hierarchy_2_key"));	
							segment.setBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_sub_brand_hierarchy_2_name")));
							segment.setCategoryId(arg0.getLong("retailer_category_key"));
							segment.setCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_category_name")));
							segment.setSupplierId(arg0.getLong("retailer_supplier_key"));
							segment.setSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_supplier_name")));
							segment.setSubcategoryId(arg0.getLong("retailer_sub_category_key"));
							segment.setSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_sub_category_name")));
							segment.setClassId(arg0.getLong("retailer_class_key"));
							segment.setClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_class_name")));
							segment.setEnBrandName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_sub_brand_hierarchy_2_name")));
							segment.setEnCategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_category_name")));
							segment.setEnClassName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_class_name")));
							segment.setEnSubcategoryName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_sub_category_name")));
							segment.setEnSupplierName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_supplier_name")));
							segment.setItemId(arg0.getString("item_id"));
							segment.setDwItemId(arg0.getLong("dw_item_key"));
							segment.setItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("retailer_item_name")));
							segment.setEnglishItemName(columnManipulationService.applyCatalogNamesTransformation(arg0.getString("english_retailer_item_name")));
							segment.setRetailerItemCode(arg0.getString("retailer_item_code"));
							return segment;
						}
	                });
	    }
}
