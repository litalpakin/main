package com.storenext.assortment.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Service;

import com.storenext.assortment.config.RedshiftConfiguration;
import com.storenext.assortment.dto.SupplierSubCategoryDto;
import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.enums.Language;
import com.storenext.assortment.enums.SubcategoryClassification;
import com.storenext.assortment.model.DefaultItemClassification;
import com.storenext.assortment.model.data.CatalogFormatItem;
import com.storenext.assortment.model.data.Format;
import com.storenext.assortment.model.data.FormatStore;
import com.storenext.assortment.model.data.Occasion;
import com.storenext.assortment.model.data.PlanogramSegment;
import com.storenext.assortment.model.data.RetailerSegment;
import com.storenext.assortment.model.data.Segment;
import com.storenext.assortment.model.data.Store;
import com.storenext.assortment.model.data.SubCategoryFormatClassification;
import com.storenext.assortment.model.data.SubChain;
import com.storenext.assortment.service.cache.CacheCleaner;


@Service
public class SegmentRedshiftService implements CacheCleaner {

	   @Qualifier(RedshiftConfiguration.FAST_TEMPLATE_NAME)
	   @Autowired
	   private NamedParameterJdbcOperations jdbcOperations;
	   private ModelMapper modelMapper = new ModelMapper();

	   @Autowired
	   private EnvironmentControlService environmentControlService;

	   @Autowired
	   private CatalogCacheService catalogCacheService;


	   @Autowired
	   private FeatureConfigurationService featureConfigurationService;
	   public static final String FILTER = "(sub_category_key not in (select sub_category_key from migvan.sub_category_to_filter where catalog_type_code=1 group by sub_category_key)) and (retailer_sub_category_key not in (select sub_category_key from migvan.sub_category_to_filter where catalog_type_code=2 group by sub_category_key))";
	   public static final String REVERT_FILTER = "(sub_category_key in (select sub_category_key from migvan.sub_category_to_filter where catalog_type_code=1 group by sub_category_key)) or (retailer_sub_category_key in (select sub_category_key from migvan.sub_category_to_filter where catalog_type_code=2 group by sub_category_key))";
	  
	  
	   private  List<Segment> retailerSegments = null;
	   private  Set<SupplierSubCategoryDto> notSupportedBMSubcategories = null;
	   private Map<Long,List<Segment>> snIdToretailerSegments = null;
	   private List<CatalogFormatItem> catalogFormatItems = null;
	   private Map<String,Set<CatalogFormatItem>> itemToCatalogFormats = null;
	   @Autowired
	   private ClusterTransformService clasterTransformService;
	   
	   @Autowired
	   private StoreTransformService storeTransformService;
	   @PostConstruct
	   public void init() {
		   loadCatalogFormatItems();
		   loadBMSegments();
	   }
	  
	   public void loadCatalogFormatItems() {
		   catalogFormatItems = fetchFormatItemList();
		   Collections.sort(catalogFormatItems,new Comparator<CatalogFormatItem>() {

			@Override
			public int compare(CatalogFormatItem o1, CatalogFormatItem o2) {
				if(o1.getItemName() == null && o2.getItemName() == null){
					return 0;
				}
				if(o1.getItemName() == null){
					return 1;
				}
				if(o2.getItemName() == null){
					return -1;
				}
				return o1.getItemName().compareTo(o2.getItemName());
			}
			   
		   });
		   itemToCatalogFormats = new HashMap<>();
		   for(CatalogFormatItem catalogFormatItem:catalogFormatItems){
			   Set<CatalogFormatItem> formats = itemToCatalogFormats.get(catalogFormatItem.getItemId());
			   if(formats == null){
				   formats = new HashSet<>();
				   itemToCatalogFormats.put(catalogFormatItem.getItemId(), formats);
			   }
			   formats.add(catalogFormatItem);
		   }
	   }
	   public Set<CatalogFormatItem> getCatalogFormatItems(String itemId){
		 return  itemToCatalogFormats.get(itemId);
	   }
	   @Override
	   public void cleanCache() {
		  loadRetailerSegments();
		  loadCatalogFormatItems();
		  loadBMSegments();
	   }
	   private synchronized void loadBMSegments(){
		   notSupportedBMSubcategories = new HashSet<>(getBMSegmentList());
	   }
	   
	   public Set<SupplierSubCategoryDto> getNotSupportedBMSubcategories(){
		   return notSupportedBMSubcategories;
	   }
	   private synchronized void loadRetailerSegments(){
		   if(featureConfigurationService.isEnabled(Feature.RETAILER_CATALOG_TYPE)) {
			   List<RetailerSegment> retailerSegmentsList = getRetailerSegments();
			   Map<Long,List<Segment>> newSnIdToretailerSegments = new HashMap<>();
			   List<Segment> newRetailerSegments = new ArrayList<>();
			   for(RetailerSegment retailerSegment:retailerSegmentsList){
				   List<Segment> segments = newSnIdToretailerSegments.get(retailerSegment.getSnSubcategoryId());
				   if(segments == null){
					   segments = new ArrayList<>();
					   newSnIdToretailerSegments.put(retailerSegment.getSnSubcategoryId(), segments);
				   }
				   Segment newSegment =  modelMapper.map(retailerSegment, Segment.class);
				   segments.add(newSegment);
				   newRetailerSegments.add(newSegment);
			   }
			   retailerSegments = newRetailerSegments;
			   snIdToretailerSegments = newSnIdToretailerSegments;
		   }

	   }
	   public List<Segment> getFullCustomerSegments(){
		 if(retailerSegments == null){
			 loadRetailerSegments(); 
		 }
		 return retailerSegments;
	   }
	   public Set<Segment> getCustomerSegments(Set<Segment> snSegments){
			 if(snIdToretailerSegments == null){
				 loadRetailerSegments(); 
			 }
			 Set<Segment> response = new HashSet<>();
			 for(Segment snSegment:snSegments){
				List<Segment>  segments = snIdToretailerSegments.get(snSegment.getSubcategoryId());
				if(segments != null){
					response.addAll(segments);
				}
			 }
			 return response;
	  }
	   
	   @Cacheable(value="new_dw_store_key")
	    public List<FormatStore> getNewDwStoreKeys() {
		        return jdbcOperations.query(
		        		"SELECT dw_store_key,store_name,format_code,format_name "
		    	        		+ " FROM migvan.new_dw_store_key_without_format "
		                , Collections.emptyMap(),new RowMapper<FormatStore>(){
							@Override
							public FormatStore mapRow(ResultSet arg0, int arg1) throws SQLException {
								FormatStore format = new FormatStore();
								format.setFormatId(arg0.getLong("format_code"));
								format.setFormatName(clasterTransformService.map(arg0.getString("format_name")));
								format.setStoreId(arg0.getLong("dw_store_key"));
								format.setStoreName(storeTransformService.map(arg0.getString("store_name")));
								return format;
							}
		                });
	    }
	   
	   @Cacheable(value="subChain")
	    public List<SubChain> getSubChains() {
		        return jdbcOperations.query(
		        		"SELECT sub_chain_code,sector_code,format_code,format_name "
		    	        		+ " FROM migvan.new_sub_chain_without_format "
		                , Collections.emptyMap(),new RowMapper<SubChain>(){
							@Override
							public SubChain mapRow(ResultSet arg0, int arg1) throws SQLException {
								SubChain subChain = new SubChain();
								subChain.setFormatId(arg0.getLong("format_code"));
								subChain.setFormatName(clasterTransformService.map(arg0.getString("format_name")));
								subChain.setSectorCode(arg0.getLong("sector_code"));
								subChain.setSubChainCode(arg0.getLong("sub_chain_code"));
								return subChain;
							}
		                });
	    }
	   
	   

		@Cacheable(value="stores")
		public List<Store> getStores() {
			return jdbcOperations.query(
					"SELECT DISTINCT dw_store_key AS id, store_name AS name "
						+ " FROM migvan.assortment_item_store_catalog "
		                + getWhereFilter()
		                
		                , Collections.emptyMap(),new RowMapper<Store>(){
							@Override
							public Store mapRow(ResultSet arg0, int arg1) throws SQLException {
								Store store = new Store();
								
								store.setId(arg0.getLong("id"));
								store.setName(storeTransformService.map(arg0.getString("name")));
		
								return store;
							}
		                });
		}
	
	@Cacheable(value = "segments")
	public List<Segment> getFullSegmentList() {
		return getFullSegmentList(getWhereFilter());
	}
	
	public List<Segment> getFullSegmentListWhereRevertFilter() {
		return getFullSegmentList(getWhereRevertFilter());
	}

	public List<Segment> getFullSegmentList(String whereFilter) {
		return jdbcOperations.query(
				"select t.* from ( SELECT  sub_brand_hierarchy_2_key, "
						+ " category_key, supplier_key, class_key, sub_category_key \n"
						+ "  ,sum (item_store_sales_cy) total_sales \n"
						+ " FROM migvan.assortment_item_store_catalog "
						+ whereFilter
						+ " group by  sub_brand_hierarchy_2_key,category_key,supplier_key,class_key,sub_category_key"
						+ " ) t where t.total_sales > 1 "
				
				, Collections.emptyMap(), new RowMapper<Segment>() {
					@Override
					public Segment mapRow(ResultSet arg0, int arg1) throws SQLException {
						Segment segment = new Segment();
						segment.setBrandId(arg0.getLong("sub_brand_hierarchy_2_key"));
						segment.setBrandName(catalogCacheService.getHeAnalistCatalog().getBrands().get(segment.getBrandId()));
						segment.setCategoryId(arg0.getLong("category_key"));
						segment.setCategoryName(catalogCacheService.getHeAnalistCatalog().getCategories().get(segment.getCategoryId()));
						segment.setSupplierId(arg0.getLong("supplier_key"));
						segment.setSupplierName(catalogCacheService.getHeAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
						segment.setSubcategoryId(arg0.getLong("sub_category_key"));
						segment.setSubcategoryName(catalogCacheService.getHeAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
						segment.setClassId(arg0.getLong("class_key"));
						segment.setClassName(catalogCacheService.getHeAnalistCatalog().getClasses().get(segment.getClassId()));
						segment.setEnBrandName(catalogCacheService.getEnAnalistCatalog().getBrands().get(segment.getBrandId()));
						segment.setEnCategoryName(catalogCacheService.getEnAnalistCatalog().getCategories().get(segment.getCategoryId()));
						segment.setEnClassName(catalogCacheService.getEnAnalistCatalog().getClasses().get(segment.getClassId()));
						segment.setEnSubcategoryName(catalogCacheService.getEnAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
						segment.setEnSupplierName(catalogCacheService.getEnAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
						return segment;
					}
				});
	}
	private List<SupplierSubCategoryDto> getBMSegmentList() {
		return jdbcOperations.query(
				  " select distinct m.sub_category_key, m.supplier_key from migvan.assortment_item_hierarchy_multi_lang m " + 
				  "where m.dw_item_key not in ( " + 
				  " select dw_item_key from migvan.assortment_fact_rank_1_24_shorted  group by dw_item_key) " + 
				  " " 
				, Collections.emptyMap(), new RowMapper<SupplierSubCategoryDto>() {
					@Override
					public SupplierSubCategoryDto mapRow(ResultSet arg0, int arg1) throws SQLException {
						SupplierSubCategoryDto  supplierSubCategoryDto = new SupplierSubCategoryDto();
						supplierSubCategoryDto.setSubcategoryId(arg0.getLong("sub_category_key"));
						supplierSubCategoryDto.setSupplierId(arg0.getLong("supplier_key"));
						return supplierSubCategoryDto;
						
					}
				});
	}
	@Cacheable(value = "planogram")
	public List<PlanogramSegment> getPlanogramSegments() {
		return jdbcOperations.query(
				"select t.* from (select format_code, category_key, supplier_key, dw_store_key, sub_category_key, sum (item_store_sales_cy) total_sales "
						+ " FROM migvan.assortment_item_store_catalog "
						+ getWhereFilter()
						+ " group by  format_code,  category_key, supplier_key, dw_store_key, sub_category_key " + " ) t where t.total_sales > 1 ",
				Collections.emptyMap(), (arg0, arg1) -> {
					PlanogramSegment segment = new PlanogramSegment();
					segment.setFormatCode(arg0.getLong("format_code"));
					segment.setFormatName(catalogCacheService.getFormats(Language.HEBREW).get(segment.getFormatCode()));
					segment.setEnFormatName(catalogCacheService.getFormats(Language.ENGLISH).get(segment.getFormatCode()));
					segment.setCategoryId(arg0.getLong("category_key"));
					segment.setCategoryName(catalogCacheService.getHeAnalistCatalog().getCategories().get(segment.getCategoryId()));
					segment.setEnCategoryName(catalogCacheService.getEnAnalistCatalog().getCategories().get(segment.getCategoryId()));
					segment.setSupplierId(arg0.getLong("supplier_key"));
					segment.setDwStoreKey(arg0.getLong("dw_store_key"));
					segment.setStoreName(catalogCacheService.getStoreName(segment.getDwStoreKey()));
					segment.setSubcategoryId(arg0.getLong("sub_category_key"));

					return segment;
				});
	}

	public List<RetailerSegment> getRetailerSegments() {
		return getRetailerSegments(getWhereFilter());
	}
	
	
	public List<Segment> getFullCustomerSegmentsWhereRevertFilter() {
		return getRetailerSegmentsWhereRevertFilter().stream().map(rs->modelMapper.map(rs, Segment.class)).collect(Collectors.toList());
	}

	public List<RetailerSegment> getRetailerSegmentsWhereRevertFilter() {
		return getRetailerSegments(getWhereRevertFilter());
	}

	
	
	public List<RetailerSegment> getRetailerSegments(String whereFilter) {
		return jdbcOperations.query(
				"select * from (SELECT  retailer_sub_brand_hierarchy_2_key, "
						+ " retailer_category_key,retailer_supplier_key, \n"
						+ " retailer_class_key, retailer_sub_category_key, sub_category_key \n"
						+ "  ,sum (item_store_sales_cy) total_sales \n"
						+ " FROM migvan.assortment_item_store_catalog "
						+ whereFilter
						+ " group by retailer_sub_brand_hierarchy_2_key, "
						+ " retailer_category_key,retailer_supplier_key, \n"
						+ " retailer_class_key, retailer_sub_category_key, sub_category_key \n"
						+ " ) t where total_sales > 1 "
				, Collections.emptyMap(), new RowMapper<RetailerSegment>() {
					@Override
					public RetailerSegment mapRow(ResultSet arg0, int arg1) throws SQLException {
						RetailerSegment segment = new RetailerSegment();
						segment.setBrandId(arg0.getLong("retailer_sub_brand_hierarchy_2_key"));
						segment.setBrandName(catalogCacheService.getHeRetailerCatalog().getBrands().get(segment.getBrandId()));
						segment.setCategoryId(arg0.getLong("retailer_category_key"));
						segment.setCategoryName(catalogCacheService.getHeRetailerCatalog().getCategories().get(segment.getCategoryId()));
						segment.setSupplierId(arg0.getLong("retailer_supplier_key"));
						segment.setSupplierName(catalogCacheService.getHeRetailerCatalog().getSuppliers().get(segment.getSupplierId()));
						segment.setSubcategoryId(arg0.getLong("retailer_sub_category_key"));
						segment.setSubcategoryName(catalogCacheService.getHeRetailerCatalog().getSubCategories().get(segment.getSubcategoryId()));
						segment.setClassId(arg0.getLong("retailer_class_key"));
						segment.setClassName(catalogCacheService.getHeRetailerCatalog().getClasses().get(segment.getClassId()));
						segment.setSnSubcategoryId(arg0.getLong("sub_category_key"));
						segment.setEnBrandName(catalogCacheService.getEnRetailerCatalog().getBrands().get(segment.getBrandId()));
						segment.setEnCategoryName(catalogCacheService.getEnRetailerCatalog().getCategories().get(segment.getCategoryId()));
						segment.setEnClassName(catalogCacheService.getEnRetailerCatalog().getClasses().get(segment.getClassId()));
						segment.setEnSubcategoryName(catalogCacheService.getEnRetailerCatalog().getSubCategories().get(segment.getSubcategoryId()));
						segment.setEnSupplierName(catalogCacheService.getEnRetailerCatalog().getSuppliers().get(segment.getSupplierId()));

						return segment;
					}
				});
	}
	   
	    @Cacheable(value="storemanagementsegments")
	    public List<Segment> getStoreManagementCatalog() {
	        return jdbcOperations.query(
	                "SELECT sub_category_key,  category_key" 
	        		+ " , sub_brand_hierarchy_2_key,  supplier_key"
	        		+ " , class_key " 
	  	        		+ " FROM migvan.assortment_core_growth_star_items_store_indices" 
	        		+ " group by sub_category_key,  category_key" 
	        		+ " ,sub_brand_hierarchy_2_key"
	        		+ " ,supplier_key" 
	        		+ " ,class_key" 
	
	                , Collections.emptyMap(),new RowMapper<Segment>(){
						@Override
						public Segment mapRow(ResultSet arg0, int arg1) throws SQLException {
							Segment segment = new Segment();
							segment.setBrandId(arg0.getLong("sub_brand_hierarchy_2_key"));	
							segment.setBrandName(catalogCacheService.getHeAnalistCatalog().getBrands().get(segment.getBrandId()));
							segment.setCategoryId(arg0.getLong("category_key"));
							segment.setCategoryName(catalogCacheService.getHeAnalistCatalog().getCategories().get(segment.getCategoryId()));
							segment.setSupplierId(arg0.getLong("supplier_key"));
							segment.setSupplierName(catalogCacheService.getHeAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
							segment.setSubcategoryId(arg0.getLong("sub_category_key"));
							segment.setSubcategoryName(catalogCacheService.getHeAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
							segment.setClassId(arg0.getLong("class_key"));
							segment.setClassName(catalogCacheService.getHeAnalistCatalog().getClasses().get(segment.getClassId()));
							segment.setEnBrandName(catalogCacheService.getEnAnalistCatalog().getBrands().get(segment.getBrandId()));
							segment.setEnCategoryName(catalogCacheService.getEnAnalistCatalog().getCategories().get(segment.getCategoryId()));
							segment.setEnClassName(catalogCacheService.getEnAnalistCatalog().getClasses().get(segment.getClassId()));
							segment.setEnSubcategoryName(catalogCacheService.getEnAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
							segment.setEnSupplierName(catalogCacheService.getEnAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
							return segment;
						}
	                });
	    }
	    @Cacheable(value="retailerstoremanagementcatalog")
	    public List<RetailerSegment> getRetailerStoreManagementCatalog() {
	        return jdbcOperations.query(
	                "SELECT sub_category_key,retailer_sub_category_key, retailer_category_key" 
	        		+ " ,retailer_sub_brand_hierarchy_2_key"
	        		+ " ,retailer_supplier_key ,retailer_class_key" 
	        		+ " FROM migvan.assortment_core_growth_star_items_store_indices" 
	        		+ " group by sub_category_key,retailer_sub_category_key,retailer_category_key" 
	        		+ " ,retailer_sub_brand_hierarchy_2_key,retailer_supplier_key" 
	        		+ " ,retailer_class_key " 
	                , Collections.emptyMap(),new RowMapper<RetailerSegment>(){
						@Override
						public RetailerSegment mapRow(ResultSet arg0, int arg1) throws SQLException {
							RetailerSegment segment = new RetailerSegment();
							segment.setBrandId(arg0.getLong("retailer_sub_brand_hierarchy_2_key"));	
							segment.setBrandName(catalogCacheService.getHeRetailerCatalog().getBrands().get(segment.getBrandId()));
							segment.setCategoryId(arg0.getLong("retailer_category_key"));
							segment.setCategoryName(catalogCacheService.getHeRetailerCatalog().getCategories().get(segment.getCategoryId()));
							segment.setSupplierId(arg0.getLong("retailer_supplier_key"));
							segment.setSupplierName(catalogCacheService.getHeRetailerCatalog().getSuppliers().get(segment.getSupplierId()));
							segment.setSubcategoryId(arg0.getLong("retailer_sub_category_key"));
							segment.setSubcategoryName(catalogCacheService.getHeRetailerCatalog().getSubCategories().get(segment.getSubcategoryId()));
							segment.setClassId(arg0.getLong("retailer_class_key"));
							segment.setClassName(catalogCacheService.getHeRetailerCatalog().getClasses().get(segment.getClassId()));
							segment.setSnSubcategoryId(arg0.getLong("sub_category_key"));
							return segment;
						}
	                });
	    }
	    
	   
	    
	    @Cacheable(value="formats")
	    public List<Format> getFormatList() {
	        return jdbcOperations.query(
	               " select format_code, format_name,english_format_name "
	                + " from ( SELECT  format_code, format_name,english_format_name,sum(item_store_sales_cy) total_sales"
	                + " FROM migvan.assortment_item_store_catalog"
	                + getWhereFilter()
	                + " group by   format_code, format_name,english_format_name) t"
	                + " where total_sales > 1"
	                
	               
	                , Collections.emptyMap(),new RowMapper<Format>(){
						@Override
						public Format mapRow(ResultSet arg0, int arg1) throws SQLException {
							Format format = new Format();
							format.setFormatId(arg0.getLong("format_code"));
							format.setFormatName(clasterTransformService.map(arg0.getString("format_name")));
							format.setEnglishFormatName(clasterTransformService.map(arg0.getString("english_format_name")));
							
							return format;
						}
	                });
	    }
	    @Cacheable(value="formatstores")
	    public List<FormatStore> getFormatStoreList() {
	        return jdbcOperations.query(
	               " select dw_store_key,  format_code, format_name, store_name,english_format_name "
	                + " from ( SELECT store_name,dw_store_key, format_code, format_name,english_format_name,sum(item_store_sales_cy) total_sales"
	                + " FROM migvan.assortment_item_store_catalog"
	                + getWhereFilter()
	                + " group by   format_code, format_name,english_format_name, dw_store_key, store_name) t "
	                + " where total_sales > 1"
	                
	               
	                , Collections.emptyMap(),new RowMapper<FormatStore>(){
						@Override
						public FormatStore mapRow(ResultSet arg0, int arg1) throws SQLException {
							FormatStore format = new FormatStore();
							format.setFormatId(arg0.getLong("format_code"));
							format.setFormatName(clasterTransformService.map(arg0.getString("format_name")));
							format.setStoreId(arg0.getLong("dw_store_key"));
							format.setStoreName(storeTransformService.map(arg0.getString("store_name")));
							
							format.setEnglishFormatName(clasterTransformService.map(arg0.getString("english_format_name")));
							
							return format;
						}
	                });
	    }
	   public List<CatalogFormatItem> getFormatItemList() {
		   return catalogFormatItems;
	   }
	    private List<CatalogFormatItem> fetchFormatItemList() {
	        return jdbcOperations.query(
	                "SELECT  dw_item_key,item_id,format_code, supplier_key,sub_category_key FROM "
	                + " ( SELECT dw_item_key, item_id, format_code, supplier_key,sub_category_key, sum(item_store_sales_cy) total_sales"
	                + " FROM migvan.assortment_item_store_catalog " 
	                + getWhereFilter()
	                + " group by  dw_item_key,item_id, format_code, supplier_key,sub_category_key ) t"
	                + " where total_sales > 1"
	        		, Collections.emptyMap(),new RowMapper<CatalogFormatItem>(){
						@Override
						public CatalogFormatItem mapRow(ResultSet arg0, int arg1) throws SQLException {
							CatalogFormatItem format = new CatalogFormatItem();
							format.setDwItemKey(arg0.getLong("dw_item_key"));
							format.setFormatCode(arg0.getLong("format_code"));
							format.setItemId(arg0.getString("item_id"));
							format.setItemName(catalogCacheService.getHeAnalistCatalog().getItems().get(format.getItemId()));
							format.setSubcategoryId(arg0.getLong("sub_category_key"));
							format.setSupplierId(arg0.getLong("supplier_key"));
							format.setEnItemName(catalogCacheService.getEnAnalistCatalog().getItems().get(format.getItemId()));
							
							return format;
						}
	                });
	    }

	   @Cacheable(value="fullItemClassifications")
	   public List<DefaultItemClassification> getFullItemClassifications() {
	        return jdbcOperations.query(
	        		"SELECT dw_item_key, item_id, format_code, item_format_classification_code,"
	        		+ " item_format_classification_name, is_item_classification_equal_for_all_format"
	        		+ " FROM migvan.assortment_item_format_indices where  item_format_top_classification_code <> 5 "
	        		+ getAndFilter()
	                , Collections.emptyMap(),new RowMapper<DefaultItemClassification>(){
						@Override
						public DefaultItemClassification mapRow(ResultSet arg0, int arg1) throws SQLException {
						
							DefaultItemClassification defaultItemClassification = new DefaultItemClassification();
							try{
							defaultItemClassification.setDwItemKey(arg0.getLong("dw_item_key"));
							defaultItemClassification.setFormatCode(arg0.getLong("format_code"));
							defaultItemClassification.setItemId(arg0.getString("item_id"));
							defaultItemClassification.setItemClassificationCode(arg0.getLong("item_format_classification_code"));
							defaultItemClassification.setItemClassificationName(arg0.getString("item_format_classification_name"));
							defaultItemClassification.setIsItemClassificationEqualForAllFormat(arg0.getObject("is_item_classification_equal_for_all_format"));
							}catch(Exception e){
								e.printStackTrace();
							}
							return defaultItemClassification;
						}
	                });
	    }

	   @Cacheable(value="occasions")
	   public List<Occasion> getOccasions(){
		   return jdbcOperations.query(
	                "SELECT distinct occasion_key,occasion_name,english_occasion_name FROM migvan.occasions" 
	        		, Collections.emptyMap(),new RowMapper<Occasion>(){
						@Override
						public Occasion mapRow(ResultSet arg0, int arg1) throws SQLException {
							Occasion dto = new Occasion();
							dto.setName(arg0.getString("occasion_name"));
							dto.setEnglishName(arg0.getString("english_occasion_name"));
							dto.setId((arg0.getObject("occasion_key")));
							return dto;
						}
	                });
	}
	   
	   //  
	   @Cacheable(value="occasionsegments")
	    public List<Segment> getOccasionSegmentList() {
	        return jdbcOperations.query(
	                "SELECT DISTINCT sub_brand_hierarchy_2_key, "
	                + " category_key, \n"
	                + " supplier_key, \n" 
	                + " class_key, \n" 
	                + " sub_category_key \n"
	   	            + " FROM migvan.occasion_item_format_monthly_indices " 
	                + getWhereFilter()
	                , Collections.emptyMap(),new RowMapper<Segment>(){
						@Override
						public Segment mapRow(ResultSet arg0, int arg1) throws SQLException {
							Segment segment = new Segment();
							segment.setBrandId(arg0.getLong("sub_brand_hierarchy_2_key"));	
							segment.setBrandName(catalogCacheService.getHeAnalistCatalog().getBrands().get(segment.getBrandId()));
							segment.setCategoryId(arg0.getLong("category_key"));
							segment.setCategoryName(catalogCacheService.getHeAnalistCatalog().getCategories().get(segment.getCategoryId()));
							segment.setSupplierId(arg0.getLong("supplier_key"));
							segment.setSupplierName(catalogCacheService.getHeAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
							segment.setSubcategoryId(arg0.getLong("sub_category_key"));
							segment.setSubcategoryName(catalogCacheService.getHeAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
							segment.setClassId(arg0.getLong("class_key"));
							segment.setClassName(catalogCacheService.getHeAnalistCatalog().getClasses().get(segment.getClassId()));
							segment.setEnBrandName(catalogCacheService.getEnAnalistCatalog().getBrands().get(segment.getBrandId()));
							segment.setEnCategoryName(catalogCacheService.getEnAnalistCatalog().getCategories().get(segment.getCategoryId()));
							segment.setEnClassName(catalogCacheService.getEnAnalistCatalog().getClasses().get(segment.getClassId()));
							segment.setEnSubcategoryName(catalogCacheService.getEnAnalistCatalog().getSubCategories().get(segment.getSubcategoryId()));
							segment.setEnSupplierName(catalogCacheService.getEnAnalistCatalog().getSuppliers().get(segment.getSupplierId()));
							return segment;
						}
	                });
	    }
	   @Cacheable(value="subCategory_format_classification")
	    public List<SubCategoryFormatClassification> getSubCategoryFormatClassifications() {
		        return jdbcOperations.query(
		        		"SELECT  sub_category_key,format_code,subcat_format_classification_code, "
		        		+ " subcat_format_classification_nameFROM migvan.sub_category_store_classification_indices"
		        		+ " group by  sub_category_key,format_code,subcat_format_classification_code, subcat_format_classification_name"
		                , Collections.emptyMap(),new RowMapper<SubCategoryFormatClassification>(){
							@Override
							public SubCategoryFormatClassification mapRow(ResultSet arg0, int arg1) throws SQLException {
								SubCategoryFormatClassification subCategoryFormatClassification = new SubCategoryFormatClassification();
								subCategoryFormatClassification.setFormatCode(arg0.getLong("format_code"));
								subCategoryFormatClassification.setSubCategoryId(arg0.getLong("sub_category_key"));
								subCategoryFormatClassification.setSubcategoryClassification(SubcategoryClassification.getClassification(arg0.getLong("subcat_format_classification_code")));
		
								return subCategoryFormatClassification;
							}
		                });
	    }

	   public String getAndFilter(){
		   return " and " + FILTER;
	   }
	   public String getWhereFilter(){
		   return " where " + FILTER;
	   }
	
	public String getWhereRevertFilter() {
		return " where " + REVERT_FILTER;
	}
}
