package com.storenext.assortment.service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.dto.DroppedHierarchsDto;
import com.storenext.assortment.dto.SegmentDto;
import com.storenext.assortment.dto.SupplierSubCategoryDto;
import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.data.Brand;
import com.storenext.assortment.model.data.Category;
import com.storenext.assortment.model.data.PlanogramSegment;
import com.storenext.assortment.model.data.RetailerSegment;
import com.storenext.assortment.model.data.Segment;
import com.storenext.assortment.model.data.SubCategory;
import com.storenext.assortment.model.data.Supplier;
import com.storenext.assortment.model.data.SupplierClass;
import com.storenext.assortment.model.data.SupplierSubCategory;
import com.storenext.assortment.model.security.User;

@Service
public class CatalogHierarchyService  {

	@Autowired
	private SegmentRedshiftService segmentRedshiftService;
	@Autowired
	private LanguageService languageService;
	@Autowired
	private FeatureConfigurationService featureConfigurationService;
	
	@Value("${storenext.supported.analystcatalog:classId|class_name,categoryId|category_name,subcategoryId|sub_category_name,supplierId|supplier_name,brandId|sub_brand_hierarchy_2_name}")
	private String analystCatalogStr;
	@Value("${storenext.supported.retailer catalog:classId|retailer_class_name,categoryId|retailer_category_name,subcategoryId|retailer_sub_category_name,supplierId|retailer_supplier_name,brandId|retailer_sub_brand_hierarchy_2_name}")
	private String retailerCatalogStr;
	
	private Map<String,String> analystCatalogColumns;
	private Map<String,String> retailerCatalogColumns;
	private Map<Long, List<PlanogramSegment>> planogramSegmentByFormat;
	private Map<Long, Map<Long, List<PlanogramSegment>>> planogramSegmentByDwStoreAndFormat;
	
	public static final String COLUMN_NAME_PLACEHOLDER = "COLUMN_NAME_PLACEHOLDER";
	public static final String COLUMN_NAME_REGEXP1 = "," + COLUMN_NAME_PLACEHOLDER +",";
	public static final String COLUMN_NAME_REGEXP2 = "[\\s]" + COLUMN_NAME_PLACEHOLDER +"[,]";
	public static final String COLUMN_NAME_REGEXP3 = "[,]" + COLUMN_NAME_PLACEHOLDER +"[\\s]";
	public static final String COLUMN_NAME_REGEXP4 = "[\\s]" + COLUMN_NAME_PLACEHOLDER +"[\\s]";
	public static final String COLUMN_NAME_REGEXP5 = "[.]" + COLUMN_NAME_PLACEHOLDER +"[,]";
	public static final String COLUMN_NAME_REGEXP6 = "[.]" + COLUMN_NAME_PLACEHOLDER +"[\\s]";
	
	private Map<String,String> loadColumnsFromString(String str){
		Map<String,String> response = new HashMap<>();
		if(!StringUtils.isEmpty(str)){
			String[] columns = str.split(",");
			for(String column:columns){
				String[] pair = column.split("\\|");
				response.put(pair[0], pair[1]);
			}
		}
		return response;
	}
	
	@PostConstruct
	private void init(){
		analystCatalogColumns = loadColumnsFromString(analystCatalogStr);
		retailerCatalogColumns = loadColumnsFromString(retailerCatalogStr);
		segmentRedshiftService.getFullSegmentList();
		fillPlanogramSegments();
	}

	private void fillPlanogramSegments() {
		List<PlanogramSegment> tempPlanogramSegments = segmentRedshiftService.getPlanogramSegments();
		planogramSegmentByFormat = tempPlanogramSegments.stream().collect(Collectors.groupingBy(PlanogramSegment::getFormatCode));
		planogramSegmentByDwStoreAndFormat = tempPlanogramSegments.stream()
				.collect(Collectors.groupingBy(PlanogramSegment::getDwStoreKey, Collectors.groupingBy(PlanogramSegment::getFormatCode)));
		tempPlanogramSegments.clear();
	}

	private List<Segment> getBMFullSegmentList(CatalogType catalogType){
		Set<SupplierSubCategoryDto> notSupportedBMSubcategories = segmentRedshiftService.getNotSupportedBMSubcategories();
		List<Segment> segments = getFullSegmentList(catalogType);
		if(CollectionUtils.isEmpty(segments) || CollectionUtils.isEmpty(notSupportedBMSubcategories)) {
			return segments;
		}
		return segments.stream().filter(s -> !notSupportedBMSubcategories.contains(
				new SupplierSubCategoryDto(s.getSupplierId(),s.getSubcategoryId())))
				.collect(Collectors.toList());

	}
	private List<Segment> getFullSegmentList(CatalogType catalogType){
		
		if(featureConfigurationService.isRetailerCatalog(catalogType)){
			return segmentRedshiftService.getFullCustomerSegments();
		}else {
			return segmentRedshiftService.getFullSegmentList();
		}
	}

	public List<DroppedHierarchsDto> getDroppedHierarchies(User user, CatalogType catalogType){
		List<? extends Segment> segments;
		if(featureConfigurationService.isRetailerCatalog(catalogType)){
			segments = segmentRedshiftService.getRetailerSegmentsWhereRevertFilter();
		}else{
			segments = segmentRedshiftService.getFullSegmentListWhereRevertFilter();
		}
		
		Map<String, Map<Long, DroppedHierarchsDto>> droppedHierarchies = new HashMap<>();
		Language language = Optional.ofNullable(user.getLanguage()).orElse(languageService.getDefaultLanguage());
		segments.forEach(s -> {
			putDroppedHierarchs(droppedHierarchies, catalogType, "subcategory", s.getSubcategoryId(), Language.HEBREW.equals(language) ? s.getSubcategoryName() : s.getEnSubcategoryName());
			putDroppedHierarchs(droppedHierarchies, catalogType, "category", s.getCategoryId(), Language.HEBREW.equals(language) ? s.getCategoryName() : s.getEnCategoryName());
			putDroppedHierarchs(droppedHierarchies, catalogType,  "supplier", s.getSupplierId(), Language.HEBREW.equals(language) ? s.getSupplierName() : s.getEnSupplierName());
			putDroppedHierarchs(droppedHierarchies, catalogType, "class", s.getClassId(), Language.HEBREW.equals(language) ? s.getClassName() : s.getEnClassName());
		});

		return droppedHierarchies.values().stream().flatMap(v -> v.values().stream()).collect(Collectors.toList());
	}
	
	private void putDroppedHierarchs(Map<String, Map<Long, DroppedHierarchsDto>> droppedHierarchies, CatalogType catalogType, String catalogLevel, Long code, String name) {
		DroppedHierarchsDto droppedHierarchs = new DroppedHierarchsDto(catalogType, catalogLevel, code, name);
		Map<Long, DroppedHierarchsDto> droppedHierarchiesByLevel = droppedHierarchies.getOrDefault(catalogLevel, new HashMap<>());
		droppedHierarchiesByLevel.put(droppedHierarchs.getCode(),droppedHierarchs);
		droppedHierarchies.put(catalogLevel, droppedHierarchiesByLevel);
	}
	
	public Set<Segment> getCustomerSegments(Set<Segment> snSegments){
		 return segmentRedshiftService.getCustomerSegments(snSegments);
	 }
	public List<Segment> getAllSegments(CatalogType catalogType) {
		return getFullSegmentList(catalogType);
	}
	public List<Segment> getAllSegments(CatalogType catalogType, boolean isBm) {
		if(isBm) {
			return getBMFullSegmentList(catalogType);
		}else {
			return getFullSegmentList(catalogType);
		}
	}
	public Set<SupplierSubCategory> getAllSupplierSubcategories(CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().map(s -> new SupplierSubCategory(s.getSupplierId(), s.getSupplierName(),s.getSubcategoryId(),s.getSubcategoryName())).collect(Collectors.toSet());	
	}
	public Set<SupplierClass> getAllSupplierClasses(CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().map(s -> new SupplierClass(s.getSupplierId(), s.getSupplierName(),s.getClassId(),s.getClassName())).collect(Collectors.toSet());		
	}
	public Set<SupplierClass> getSupplierClassesBySupplierIds(Set<Long> supplierIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId())).map(s -> new SupplierClass(s.getSupplierId(), s.getSupplierName(),s.getClassId(),s.getClassName())).collect(Collectors.toSet());		
	}
	public Set<Segment> getSegmentsBySuppliers(Set<Long> supplierIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.collect(Collectors.toSet());
	}
	public Set<Supplier> getAllSuppliers(CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream()
				.map(p -> new Supplier(p.getSupplierId(), p.getSupplierName())).collect(Collectors.toSet());
	}

	public Set<Supplier> getSuppliers(Set<Long> supplierIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.map(p -> new Supplier(p.getSupplierId(), p.getSupplierName())).collect(Collectors.toSet());
	}

	public Set<Category> getCategoriesBySuppliers(Set<Long> supplierIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.map(s -> new Category(s.getCategoryId(), s.getCategoryName(),s.getSupplierId())).collect(Collectors.toSet());
	}

	public Set<Category> getCategories(Set<Long> categoryIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> categoryIds.contains(s.getCategoryId()))
				.map(s -> new Category(s.getCategoryId(), s.getCategoryName(),s.getSupplierId())).collect(Collectors.toSet());
	}
	public Category getCategoryById(Long categoryId, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s ->s.getCategoryId().equals(categoryId))
				.findAny().map(s -> new Category(s.getCategoryId(), s.getCategoryName(),s.getSupplierId())).get();
	}
	public SubCategory getSubCategoryById(Long subCategoryId, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s ->s.getSubcategoryId().equals(subCategoryId))
				.findAny().map(s -> new SubCategory(s.getSubcategoryId(), s.getCategoryName())).get();
	}
	public com.storenext.assortment.model.data.Class getClassById(Long classId, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s ->s.getClassId().equals(classId))
				.findAny().map(s -> new com.storenext.assortment.model.data.Class(s.getClassId(), s.getClassName())).get();
	}
	public Set<com.storenext.assortment.model.data.Class> getClassesBySuppliers(Set<Long> supplierIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.map(s -> new com.storenext.assortment.model.data.Class(s.getClassId(), s.getClassName()))
				.collect(Collectors.toSet());
	}

	public Set<Category> getCategoryBySuppliers(Set<Long> supplierIds, Set<Long> classIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.filter(s -> classIds.contains(s.getClassId()))
				.map(s -> new Category(s.getCategoryId(), s.getCategoryName(),s.getSupplierId())).collect(Collectors.toSet());
	}

	public Set<SubCategory> getSubCategoryBySuppliers(Set<Long> supplierIds,
			Set<Long> categoryIds, Set<Long> brandIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.filter(s -> categoryIds.contains(s.getCategoryId())).filter(s -> brandIds.contains(s.getBrandId()))
				.map(s -> new SubCategory(s.getSubcategoryId(), s.getSubcategoryName())).collect(Collectors.toSet());
	}

	public Set<Brand> getBrandsCategories(Set<Long> supplierIds, Set<Long> categoryIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> supplierIds.contains(s.getSupplierId()))
				.filter(s -> categoryIds.contains(s.getCategoryId()))
				.map(s -> new Brand(s.getBrandId(), s.getBrandName())).collect(Collectors.toSet());
		
		
	}
	public Set<Brand> getBrandsById(Set<Long> brandIds, CatalogType catalogType) {
		return getFullSegmentList(catalogType).stream().filter(s -> brandIds.contains(s.getBrandId()))
				.map(s -> new Brand(s.getBrandId(), s.getBrandName())).collect(Collectors.toSet());
	}
	
	public Set<SegmentDto> transformSegment(Set<Segment> source,User user, ModelMapper modelMapper){
		if(source == null){
			return null;
		}
		Language language  = user.getLanguage();
		if(language == null){
			language  = languageService.getDefaultLanguage();
		}
		final Language userLanguage  = language;
		return source.stream().map(s->s.getSegmentDto(userLanguage,modelMapper )).collect(Collectors.toSet());
	}
	public List<? extends Segment> getStoreManagementCatalog(CatalogType catalogType){

		if(featureConfigurationService.isRetailerCatalog(catalogType)){
			return segmentRedshiftService.getRetailerStoreManagementCatalog();
		}else{
			return segmentRedshiftService.getStoreManagementCatalog();
		}
	}
	public List<RetailerSegment> getRetailerStoreManagementCatalog(){
		return segmentRedshiftService.getRetailerStoreManagementCatalog();
	}
	public Map<String,String> getAnalystCatalogColumns() {
		return analystCatalogColumns;
	}

	public Map<String,String> getRetailerCatalogColumns() {
		return retailerCatalogColumns;
	}

	public List<PlanogramSegment> getPlanogramCategoryByFormatCode(Long formatCode, Set<Long> categoryIds) {
		List<PlanogramSegment> segments;
		if(formatCode != null) {
			segments = planogramSegmentByFormat.getOrDefault(formatCode, Collections.emptyList());
		} else {
			segments = planogramSegmentByFormat.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		}

		if(CollectionUtils.isEmpty(categoryIds)) {
			return segments;
		}

		return segments.stream().filter(s->categoryIds.contains(s.getCategoryId())).collect(Collectors.toList());
	}

	public List<PlanogramSegment> getPlanogramCategoryByDwStoreKeyAndCategoryId(Long formatCode, Set<Long> categoryIds, Long dwStoreKey) {
		List<PlanogramSegment> segments;
		if(dwStoreKey != null && formatCode != null) {
			segments = planogramSegmentByDwStoreAndFormat.getOrDefault(dwStoreKey, Collections.emptyMap()).getOrDefault(formatCode, Collections.emptyList());
		} else if(dwStoreKey != null) {
			segments = planogramSegmentByDwStoreAndFormat.getOrDefault(dwStoreKey, Collections.emptyMap()).values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		} else {
			segments = planogramSegmentByDwStoreAndFormat.values().stream().flatMap(m->m.values().stream()).flatMap(Collection::stream).collect(Collectors.toList());
		}

		if(CollectionUtils.isEmpty(categoryIds)) {
			return segments;
		}

		return segments.stream().filter(s->categoryIds.contains(s.getCategoryId())).collect(Collectors.toList());
	}
}
