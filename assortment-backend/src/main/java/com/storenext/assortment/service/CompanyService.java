package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_DELETED;
import static com.storenext.assortment.utils.Constants.ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_INACTIVATED;
import static com.storenext.assortment.utils.Constants.COMPANY_DOES_NOT_EXIST;
import static com.storenext.assortment.utils.Constants.COMPANY_ID_PRESENT;
import static com.storenext.assortment.utils.Constants.COULD_NOT_CHANGE_COMPANY_TYPE;
import static com.storenext.assortment.utils.Constants.MANDATORY_ID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.storenext.assortment.dto.CompanyDto;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.repository.CompanyRepository;
import com.storenext.assortment.repository.UserRepository;
import com.storenext.assortment.security.SecurityFunctions;

@Service
public class CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private MailService mailService;

	public Company getCompany(long id) {
		return companyRepository.findOne(id);
	}

	public List<Company> getCompanies(User user) {
		Company currCompany = getCompany(user.getCompanyId());
		if(currCompany.getType() == CompanyType.SECONDARY){
			return Arrays.asList(currCompany);
		}

		if(currCompany.getType() == CompanyType.ADMIN){
			return companyRepository.findAll();
		}

		return new ArrayList<>(getCompanies(Arrays.asList(CompanyType.MAIN,CompanyType.SECONDARY)));
	}
	public Set<Company> getCompanies(List<CompanyType> companyTypes) {
		return companyRepository.findByTypeIn(companyTypes);
	}
	@Transactional
	protected Company save(Company company) {
		Company existentCompany = companyRepository.findByName(company.getName());
		validateUniqueCompanyName(company, existentCompany);
		Company response = companyRepository.save(company);

		if (existentCompany != null && existentCompany.isEnabled() && !company.isEnabled()) {
			List<User> users = userRepository.findByCompanyId(existentCompany.getId());
			for (User user : users) {
				mailService.sendBlockUserMail(user);
			}

		}

		return response;
	}

	private void validateUniqueCompanyName(Company company, Company existentCompany) {
		if (existentCompany != null) {
			if (company.getId() == null || !company.getId().equals(existentCompany.getId())) {
				throw new ArgumentValidationException("Company with the same name already exist");
			}
		}

	}

	@Transactional
	public Company add(CompanyDto companyDto) {
		Company company = modelMapper.map(companyDto, Company.class);
		if (company.getId() != null) {
			throw new ArgumentValidationException(COMPANY_ID_PRESENT);
		}
		return save(company);
	}

	@Transactional
	public Company update(CompanyDto companyDto) {
		Company company = modelMapper.map(companyDto, Company.class);
		if (company.getId() == null) {
			throw new ArgumentValidationException(MANDATORY_ID);
		}

		Company existent = companyRepository.getOne(company.getId());
		if (existent == null) {
			throw new ArgumentValidationException(String.format(COMPANY_DOES_NOT_EXIST, companyDto.getId()));
		}

		if (SecurityFunctions.isAdminCompany(existent) && !Boolean
				.logicalAnd(company.isEnabled(), existent.isEnabled())) {
			throw new ArgumentValidationException(
					String.format(ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_INACTIVATED, companyDto.getId()));
		}

		if (!company.getType().equals(existent.getType())) {
			throw new ArgumentValidationException(String.format(COULD_NOT_CHANGE_COMPANY_TYPE, companyDto.getId()));
		}

		return save(company);
	}

	/**
	 * Remove company from DB, only SECONDARY.
	 *
	 * @param companyId company id to remove.
	 */
	@Transactional
	public void delete(Long companyId) {
		Company existent = companyRepository.getOne(companyId);
		if (existent == null) {
			throw new ArgumentValidationException(String.format(COMPANY_DOES_NOT_EXIST, companyId));
		}
		if (SecurityFunctions.isAdminCompany(existent)) {
			throw new ArgumentValidationException(String.format(ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_DELETED, companyId));
		}

		companyRepository.delete(companyId);
	}

}
