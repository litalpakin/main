package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.CANNOT_REFRESH_TOKEN;
import static com.storenext.assortment.utils.Constants.CANNOT_RETRIEVE_TOKEN;
import static com.storenext.assortment.utils.Constants.UNKNOWN_USER_CRED;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import com.storenext.assortment.model.security.User;
import com.storenext.assortment.security.JwtTokenUtil;

@Service
public class AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;
    @Autowired
    private UserService userDetailsService;

    public String authenticate(String email, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        return generateToken(email);
    }

    public String generateToken(String email) {
		Date creationTimestamp = new Date();
        userService.updateTokenCreated(creationTimestamp.getTime(),email);
        userDetailsService.addLastRequest(email, System.currentTimeMillis());
        return jwtTokenUtil.generateToken(email,creationTimestamp);
	}
    
    public boolean isTokenValid(User user,String currentToken){
    	try {
    		
    		return !jwtTokenUtil.isTokenExpired(user,currentToken);
        } catch (Exception e) {
            return false;
        }
       
    }
    
    public String refreshToken(String currentToken){
        String email = jwtTokenUtil.getEmailFromToken(currentToken);
        if(StringUtils.isBlank(email)) {
            throw new AuthenticationCredentialsNotFoundException(CANNOT_RETRIEVE_TOKEN);
        }
        Optional<User> optionalUserByEmail = userService.getOptionalUserByUsername(email);
        if(!optionalUserByEmail.isPresent()) {
            throw new AuthenticationCredentialsNotFoundException(UNKNOWN_USER_CRED);
        }

        User user = optionalUserByEmail.get();

        if(!jwtTokenUtil.canTokenBeRefreshed(user,currentToken, user.getLastPasswordResetDate())){
            throw new InsufficientAuthenticationException(CANNOT_REFRESH_TOKEN);
        }
        return jwtTokenUtil.refreshToken(currentToken);
    }
}


