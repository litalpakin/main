package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.AUTHORITY_IS_MANDATORY;
import static com.storenext.assortment.utils.Constants.CAN_T_CREATE_USER_FOR_DISABLED_COMPANY;
import static com.storenext.assortment.utils.Constants.CAN_T_UPDATE_USER;
import static com.storenext.assortment.utils.Constants.COMPANY_DOESN_T_EXIST;
import static com.storenext.assortment.utils.Constants.COMPANY_IS_MANDATORY;
import static com.storenext.assortment.utils.Constants.COULD_NOT_MOVE_USER_TO_ANOTHER_COMPANY;
import static com.storenext.assortment.utils.Constants.CURRENT_PASSWORD_CAN_T_BE_EMPTY;
import static com.storenext.assortment.utils.Constants.DO_NOT_HAVE_PERMISSION_TO_USE_SUCH_USER_FUNCTION_ID;
import static com.storenext.assortment.utils.Constants.FIRST_NAME_IS_MANDATORY;
import static com.storenext.assortment.utils.Constants.FUNCTIONS_IS_MANDATORY_FIELD;
import static com.storenext.assortment.utils.Constants.FUNCTION_DOES_NOT_EXIST;
import static com.storenext.assortment.utils.Constants.ID_IS_NOT_SUPPORTED;
import static com.storenext.assortment.utils.Constants.LAST_NAME_IS_MANDATORY;
import static com.storenext.assortment.utils.Constants.MANDATORY_ID;
import static com.storenext.assortment.utils.Constants.NEW_PASSWORD_CAN_T_BE_EMPTY;
import static com.storenext.assortment.utils.Constants.THIS_USER_CAN_T_EDIT_AUTHORITY;
import static com.storenext.assortment.utils.Constants.THIS_USER_CAN_T_EDIT_COMPANY;
import static com.storenext.assortment.utils.Constants.THIS_USER_CAN_T_EDIT_OTHERS;
import static com.storenext.assortment.utils.Constants.THIS_USER_CAN_T_EDIT_USERNAME;
import static com.storenext.assortment.utils.Constants.TOKEN_EXPIRED;
import static com.storenext.assortment.utils.Constants.TOKEN_IS_INVALID;
import static com.storenext.assortment.utils.Constants.USER_CAN_T_BE_DISABLED;
import static com.storenext.assortment.utils.Constants.USER_HAS_NOT_ACCESS_TO_SUPPLIER_S_OR_SUB_CATEGORY_S;
import static com.storenext.assortment.utils.Constants.USER_ID_IS_MANDATORY;
import static com.storenext.assortment.utils.Constants.USER_NOT_FOUND;
import static com.storenext.assortment.utils.Constants.USER_NOT_FOUND_PLEASE_CHECK;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.dto.FormatItemDto;
import com.storenext.assortment.dto.ItemDto;
import com.storenext.assortment.dto.SearchItemsDto;
import com.storenext.assortment.dto.SupplierSubCategoryDto;
import com.storenext.assortment.dto.UserDto;
import com.storenext.assortment.dto.UserFunctionDto;
import com.storenext.assortment.dto.reports.UserReportType;
import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.ClassificationGroupReason;
import com.storenext.assortment.enums.Language;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.exception.BadRequestException;
import com.storenext.assortment.exception.ErrorCodeException;
import com.storenext.assortment.exception.UserAlreadyExistsException;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.StoreGroup;
import com.storenext.assortment.model.UserFunction;
import com.storenext.assortment.model.UserSupplierSubCategory;
import com.storenext.assortment.model.UserUserFunction;
import com.storenext.assortment.model.data.BaseDataEntity;
import com.storenext.assortment.model.data.CatalogFormatItem;
import com.storenext.assortment.model.data.FormatItem;
import com.storenext.assortment.model.data.FormatStore;
import com.storenext.assortment.model.data.PlanogramSegment;
import com.storenext.assortment.model.data.Segment;
import com.storenext.assortment.model.data.Store;
import com.storenext.assortment.model.data.Supplier;
import com.storenext.assortment.model.data.SupplierClass;
import com.storenext.assortment.model.data.SupplierSubCategory;
import com.storenext.assortment.model.security.Authority;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.repository.UserRepository;
import com.storenext.assortment.security.JWTTypeEnum;
import com.storenext.assortment.security.JwtTokenUtil;
import com.storenext.assortment.security.SecurityFunctions;
import com.storenext.assortment.security.UserAuthenticationFailureManager;
import com.storenext.assortment.utils.Constants;
import com.storenext.assortment.utils.LocaleUtils;
import com.storenext.assortment.utils.SecurityUtils;

@Service
public class UserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Resource
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MailService mailService;
	@Autowired
	private LanguageService languageService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	private UserFunctionService userFunctionService;
	@Autowired
	private CompanySupplierService companySupplierService;
	@Autowired
	private CatalogHierarchyService catalogHierarchyService;
	@Autowired
	private StoreGroupService storeGroupService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private SegmentRedshiftService segmentRedshiftService;
	@Autowired
	private UserAuthenticationFailureManager userAuthenticationFailureManager;
	
	@Autowired
	private FeatureConfigurationService featureConfigurationService;

	@Value("${storenext.set.password.token.valid.hours}")
	private Integer setPasswordTokenValidHours;

	private Map<Long,Long> userIdToLastRequestMap = new HashMap<>();
	
	private Long adminUserId;
	private Long lastAdminLogin = System.currentTimeMillis();
	
	@PostConstruct
	private void init(){
		adminUserId = userRepository.findByUsername(Constants.ADMIN_EMAIL).get().getId();
	}
	
	public void addLastRequest(String username,Long lastLogin){
		Optional<User> user = userRepository.findByUsername(username);
		user.ifPresent(u -> addLastRequest(u.getId(),lastLogin));
	}
	public void addLastRequest(Long userId,Long lastLogin){
		if(adminUserId.equals(userId)){
			lastAdminLogin = lastLogin;
		}
		userIdToLastRequestMap.put(userId, lastLogin);
	}
	public Long getLastRequest(Long userId){
		if(adminUserId.equals(userId)){
			return lastAdminLogin;
		}
		return userIdToLastRequestMap.get(userId);
	}
	@Scheduled(fixedRate = 300000)
	@Transactional
	private void updateLastLogin(){
		Map<Long,Long> userIdToLastRequestMapForUpdate = userIdToLastRequestMap;
		userIdToLastRequestMap = new HashMap<>();
		for(Entry<Long, Long> userIdToLastRequest:userIdToLastRequestMapForUpdate.entrySet()){
			try{
			userRepository.updateLastRequest(userIdToLastRequest.getValue(), userIdToLastRequest.getKey());
			}catch(Exception e){
				logger.error("Failed to update last request",e);
			}
		}
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository.findByUsername(username.toLowerCase()).orElseThrow(() -> new UsernameNotFoundException(USER_NOT_FOUND));
	}

	public User getCurrentUser() {
		return SecurityUtils.currentUser();
	}
	public boolean shouldPassOnboarding(User user) {
		boolean shouldPassOnboarding = user.getOnBoarding();
		if(shouldPassOnboarding){
			return shouldPassOnboarding;
		}
		Set<Segment> segments = getSegmentsForUser(user,CatalogType.ANALYST,false);
		if(CollectionUtils.isEmpty(segments)){
			return true;
		}
		Set<Store> stores = getUserStores(user);
		if(CollectionUtils.isEmpty(stores)){
			return true;
		}
		return false;
	}
	public List<User> getAllUsers() {
		User currentUser = getCurrentUser();
		Company currCompany = companyService.getCompany(currentUser.getCompanyId());
		if(currCompany.getType() == CompanyType.SECONDARY){
			return Arrays.asList(currentUser);
		}

		if(currCompany.getType() == CompanyType.ADMIN){
			return new ArrayList<>(userRepository.getAll());
		}
		Set<Company> companies = companyService.getCompanies(Arrays.asList(CompanyType.MAIN,CompanyType.SECONDARY));
		Set<Long> companyIds = companies.stream().map(c->c.getId()).collect(Collectors.toSet());
		return new ArrayList<>(userRepository.findByCompanyIdIn(companyIds));
	}

	public List<User> findByCompanyId(Long companyId) {
		return userRepository.findByCompanyId(companyId);
	}

	public Optional<User> getOptionalUserByUsername(String username) {
		return userRepository.findByUsername(username.toLowerCase());
	}
	
	

	public User getUserById(Long id) {
		User response = userRepository.findById(id);
		User currentUser = getCurrentUser();
		Company currCompany = companyService.getCompany(currentUser.getCompanyId());
		if (currentUser.getAuthority() == Authority.ADMIN) {
			if(currCompany.getType() == CompanyType.ADMIN){
				return response;
			}
			if(currentUser.getCompanyId().equals(response.getCompanyId())){
				return response;
			}
			Company responseCompany = companyService.getCompany(response.getCompanyId());
			if(responseCompany.getType() != CompanyType.ADMIN && currCompany.getType() == CompanyType.MAIN){
				return response;
			}
			throw new AccessDeniedException("User id is " + id + " company type is " + responseCompany.getType());
			
		}
		if (currentUser.getAuthority() == Authority.USER && response.getId().equals(currentUser.getId())) {
			return response;
		}
		throw new AccessDeniedException("");
	}

	public User getUserByIdInternal(Long id) {
		return userRepository.findById(id);
	}

	public Map<String, String> getUserRoles() {
		return Arrays.stream(Authority.values()).collect(Collectors.toMap(Enum::name, Authority::getCaption));
	}

	private UserUserFunction mapToUserFunction(UserFunctionDto functionDto, Company company, final User newUser) {
		if (functionDto == null || functionDto.getId() == null) {
			throw new ArgumentValidationException(FUNCTIONS_IS_MANDATORY_FIELD);
		}

		UserFunction function = userFunctionService.getById(functionDto.getId());
		if(function == null){
			throw new ArgumentValidationException(String.format(FUNCTION_DOES_NOT_EXIST, functionDto.getId()));
		}
		if (!function.getCompanyType().equals(company.getType())) {
			throw new ArgumentValidationException(DO_NOT_HAVE_PERMISSION_TO_USE_SUCH_USER_FUNCTION_ID);
		}
		
		return new UserUserFunction(newUser, function);

	}

	private UserUserFunction mapToUserFunction(UserFunction function, final User newUser) {
		return  new UserUserFunction(newUser, function);
	}

	private Set<UserSupplierSubCategory> mapToUserSupplierSubCategory(List<SupplierSubCategoryDto> supplierSubCategory,
			final User newUser, final Company userCompany) {
		if (supplierSubCategory == null || supplierSubCategory.isEmpty()) {
			return new HashSet<>();
		}
		if(newUser.getFunction().getUserFunction().getFullCatalogSupport()){
			throw new ArgumentValidationException("Can't edit supplierSubCategories if full catalog support is defined");
		}
		Set<Supplier> availableSupplierIds = companySupplierService.getCompanySuppliers(userCompany.getId());

		return supplierSubCategory.stream().map(sc -> {
			// CHECK if supplier and sub category is available for user
			if (!availableSupplierIds.contains(new Supplier(sc.getSupplierId(), null))) {
				throw new ArgumentValidationException(String.format(USER_HAS_NOT_ACCESS_TO_SUPPLIER_S_OR_SUB_CATEGORY_S,
						sc.getSupplierId(), sc.getSubcategoryId()));
			}
			return new UserSupplierSubCategory(newUser, sc.getSupplierId(), sc.getSubcategoryId());
		}).collect(Collectors.toSet());
	}

	
	public User updateUserSupplierSubCategory(User user,List<SupplierSubCategoryDto> supplierSubCategories){
		Company company = companyService.getCompany(user.getCompanyId());
		Set<UserSupplierSubCategory> newSupplierSubCategories = mapToUserSupplierSubCategory(supplierSubCategories, user, company);
		if(user.getSupplierSubCategories() != null){
			user.getSupplierSubCategories().clear();
			user.getSupplierSubCategories().addAll(newSupplierSubCategories);
		}else{
			user.setSupplierSubCategories(newSupplierSubCategories);
		}
		return userRepository.save(user);
	}
	public Store updateUserStore(User user,Long storeId){
		if(user.getFunction().getUserFunction().getAllStoresSupport()){
			throw new ArgumentValidationException("Can't set store to user with all stores support");
		}
		Store store = storeService.getStoreById(storeId);
		if(store == null){
			throw new ArgumentValidationException("Store doesn't exist");
		}
		userRepository.updateStoreId(storeId,user.getId());
		return store;
	}
	public List<SupplierSubCategoryDto> getUserSupplierSubCategories(User user) {
		
		if(user.getFunction().getUserFunction().getFullCatalogSupport()){
			return new ArrayList<>(catalogHierarchyService.getAllSupplierSubcategories(CatalogType.ANALYST).stream().map(s -> new SupplierSubCategoryDto(s.getSupplierId(),s.getSubcategoryId())).collect(Collectors.toSet()));
		}
		if(user.getSupplierSubCategories() == null || user.getSupplierSubCategories().isEmpty()){
			return new ArrayList<>();
		}

		return new ArrayList<>(user.getSupplierSubCategories().stream().map(s -> new SupplierSubCategoryDto(s.getId().getSupplierId(),s.getId().getSubcategoryId())).collect(Collectors.toSet()));
	}
	
	private List<FormatItem> getAvailableFormatItems(User user){
			List<CatalogFormatItem>  formatItems = storeService.getAllOrderedFormatItems();
			return applyAvailableList(user, formatItems);
	}

	

	

	private Language getUserLanguage(User user) {
		Language language  = user.getLanguage();
		if(language == null){
			language  = languageService.getDefaultLanguage();
		}
		final Language userLanguage  = language;
		return userLanguage;
	}
	public List<FormatItem> searchAvailableFormatItems(User user,String text){
		if(StringUtils.isEmpty(text)){
			return getAvailableFormatItems(user);
		}
		String[] textParts = text.split(" ");
		Set<String> lowerCaseTextParts = new HashSet<>();
		for(String textPart:textParts){
			lowerCaseTextParts.add(textPart.toLowerCase());
		}
		List<CatalogFormatItem>  formatItems =
				storeService.getAllOrderedFormatItems().parallelStream().filter(fi->checkTexts(fi.getItemId(),fi.getItemName(user.getLanguage()),lowerCaseTextParts)).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(formatItems)){
			return new ArrayList<>();
		}
		return applyAvailableList(user, formatItems);
	}
	
	private boolean checkTexts(String itemId,String itemName,Set<String> texts){
		if(itemId == null && itemName == null){
			return false;
		}
		for(String text:texts){
			if(((itemId != null && !itemId.contains(text)) || itemId == null) && ((itemName != null && !itemName.toLowerCase().contains(text)
					|| itemName == null))){
				return false;
			}
		}
		return true;
	}
	
	private List<FormatItem> applyAvailableList(User user, List<CatalogFormatItem> formatItems) {
		if(user.getFunction().getUserFunction().getFullCatalogSupport()){
			return formatItems.stream().map(formatItem->modelMapper.map(formatItem, FormatItem.class)).collect(Collectors.toList());
		}
		if(user.getSupplierSubCategories() == null || user.getSupplierSubCategories().isEmpty()){
			return new ArrayList<>();
		}
		HashSet<SupplierSubCategory> supplierSubCategories = new HashSet<>();
		user.getSupplierSubCategories().forEach(s -> supplierSubCategories.add(new SupplierSubCategory(s.getId().getSubcategoryId(),"",s.getId().getSupplierId() ,"")));
		return formatItems.stream().filter(formatItem->supplierSubCategories.contains(formatItem.getSupplierSubCategory()))
				.map(formatItem->modelMapper.map(formatItem, FormatItem.class)).collect(Collectors.toList());
	}
	
	public Set<Store> getUserStores(User user) {
		if(user.getFunction().getUserFunction().getAllStoresSupport()){
			return new HashSet<>(storeService.getAll());
		}
		if(user.getStoreId() == null){
			if(user.getStoreGroup() != null){
				return storeService.getStoresByIds(user.getStoreGroup().getStores());
			}else{
				return new HashSet<>();
			}
		}
		return Stream.of(storeService.getStoreById(user.getStoreId())).collect(Collectors.toSet());
	}
	public Set<Long> getUserStoreRestrictions(User user) {
		if(user.getFunction().getUserFunction().getAllStoresSupport()){
			return new HashSet<>();
		}
		if(user.getStoreId() == null){
			if(user.getStoreGroup() != null){
				return user.getStoreGroup().getStores();
			}else{
				return new HashSet<>();
			}
		}
		return Stream.of(user.getStoreId()).collect(Collectors.toSet());
	}
	public Set<FormatStore> getUserFormatStores(User user) {
		if(user.getFunction().getUserFunction().getAllStoresSupport()){
			return new HashSet<>(storeService.getAllFormats());
		}
		if(user.getStoreId() == null){
			if(user.getStoreGroup() != null){
				return storeService.getFormatsForStores(user.getStoreGroup().getStores());
			}else{
				return new HashSet<>();
			}
		}
		return storeService.getFormatsForStore(user.getStoreId());
	}
	public User registerUser(UserDto userDto) {

		User user = modelMapper.map(userDto, User.class);
		if (userDto.getCompanyId() == null) {
			throw new ArgumentValidationException(COMPANY_IS_MANDATORY);
		}
		user.setUsername(user.getUsername().toLowerCase());
		Company company = companyService.getCompany(user.getCompanyId());
		validateUserRegistration(userDto, user, company);

		user.setEnabled(userDto.isEnabled());
		setGeneratedPasswordAndEmailValidationToken(user);
		user.setFunction(mapToUserFunction(userDto.getFunction(), company, user));
		if(user.getLanguage() == null){
			user.setLanguage(languageService.getDefaultLanguage());
		}
		if(!languageService.getSupportedLanguages().contains(user.getLanguage())){
			throw new ArgumentValidationException("Language isn't supported");
		}
		handleStoreGroup(user);
		// Admin company user automatically connected to user function - only
		// Storenext (default)
		if (company.getType().equals(CompanyType.ADMIN)) {
			user.setFunction(mapToUserFunction(userFunctionService.getDefaultAdminFunctions(), user));
		}
		applyUserFunctionRestrictions(user);
		user = userRepository.save(user);

		mailService.sendUserActivationMail(user);
		return user;
	}
	
	private void setGeneratedPasswordAndEmailValidationToken(User user) {
		// User must set password via email link
		user.setPassword(UUID.randomUUID().toString());
		user.setLastPasswordResetDate(new Date());

		user.setEmailValidationToken(jwtTokenUtil.generateCreatePasswordToken(user.getUsername().toLowerCase(), new Date()));
		user.setEmailValidationTokenCreateDate(new Date());
		user.setInitialPasswordUsed(false);
	}

	public void applyUserFunctionRestrictions(User user){
		UserUserFunction userUserFunction = user.getFunction();
		if(userUserFunction == null){
			throw new ArgumentValidationException("User function is mandatory");
		}
		UserFunction userFunction = userUserFunction.getUserFunction();
		if(userFunction.getFullCatalogSupport()){
			//clear user sub categories if all catalog is relevant
			if(user.getSupplierSubCategories() != null){
				user.getSupplierSubCategories().clear();
			}
		}
		if(userFunction.getAllStoresSupport()){
			//clear stores if all stores are relevant
			user.setStoreGroup(null);
			user.setStoreId(null);
		}
		if(userFunction.getAreaManager() != null && userFunction.getAreaManager()){
			user.setStoreId(null);
		}else{
			user.setStoreGroup(null);
		}
	}
	private void handleStoreGroup(User user) {
		if(user.getStoreGroup() != null){
			if(user.getStoreGroup().getId() == null){
				throw new ArgumentValidationException(MANDATORY_ID);
			}
			StoreGroup storeGroup = storeGroupService.getStoreGroupById(user.getStoreGroup().getId());
			if(storeGroup == null){
				throw new ArgumentValidationException("Store group with id : " + user.getStoreGroup().getId() + " desn't exist");
			}
			user.setStoreGroup(storeGroup);
			user.setStoreId(null);
		}
	}

	private void validateUserRegistration(UserDto userDto, User user, Company company) {
		if (userDto.getFunction() == null) {
			throw new ArgumentValidationException(FUNCTIONS_IS_MANDATORY_FIELD);
		}
		if (getOptionalUserByUsername(user.getUsername()).isPresent()) {
			throw new UserAlreadyExistsException();
		}
		if (userDto.getCompanyId() == null) {
			throw new ArgumentValidationException(COMPANY_IS_MANDATORY);
		}
		if (company == null) {
			throw new ArgumentValidationException(COMPANY_DOESN_T_EXIST);
		}
		if (!company.isEnabled()) {
			throw new ArgumentValidationException(CAN_T_CREATE_USER_FOR_DISABLED_COMPANY);
		}

		if (userDto.getId() != null) {
			throw new ArgumentValidationException(ID_IS_NOT_SUPPORTED);
		}
	}

	public void generateResetPassword(String userEmail) throws UnsupportedEncodingException {

		Optional<User> optUser = userRepository.findByUsername(userEmail);
		if (!optUser.isPresent()) {
			throw new ArgumentValidationException(USER_NOT_FOUND_PLEASE_CHECK);
		}
		User user = optUser.get();
		if (!StringUtils.isEmpty(user.getEmailValidationToken())) {
			logger.info("EmailValidationToken exists for " + userEmail + " sending welcome email");
			user.setEmailValidationTokenCreateDate(new Date());
        	mailService.sendUserActivationMail(user);
        	userRepository.save(user);
		} else {
			String resetPasswordToken = jwtTokenUtil.generateResetPasswordToken(userEmail);
			logger.info("EmailValidationToken doesn't exist for " + userEmail + " sending reset email, token: " + resetPasswordToken);
			
			mailService.sendExternalResetPasswordMail(user,URLEncoder.encode(resetPasswordToken,"UTF-8"));
		}

	}
	
	public User setInitialPassword(String token, String password) {
		logger.info("trying to create password for token " + token);
		User user = getUserByEmailValidationToken(token, true);
		user.setInitialPasswordUsed(true);
		return setPassword(user,password,false);
	}
	
	public void setPassword( Long userId, String password) {
		User response = userRepository.findById(userId);
		boolean sendNotification = true;
		if(response != null && response.getEmailValidationToken() != null){
			sendNotification = false;
		}
		setPassword(response,password,sendNotification);
    }
	private User setPassword(User user, String password, boolean sendNotification) {
		if(user == null){
			throw new UsernameNotFoundException(USER_NOT_FOUND);
		}
		if(StringUtils.isEmpty(password)){
			throw new ArgumentValidationException(NEW_PASSWORD_CAN_T_BE_EMPTY);
		}
		user.setPassword(passwordEncoder.encode(password));
		user.setEmailValidationToken(null);
		user.setEmailValidationTokenCreateDate(null);

		user =  userRepository.save(user);
		if(sendNotification){
			mailService.sendResetPasswordApprovalMail(user);
		}
		return user;
	}

	public User getUserByEmailValidationToken(String token) {
		return getUserByEmailValidationToken(token,true);
	}
	public User getUserByEmailValidationToken(String token,boolean isInitialPassword) {
		Optional<User> optionalUser = userRepository.findByEmailValidationToken(token);
		if(!optionalUser.isPresent()){
			String username = jwtTokenUtil.getUsernameCreatePasswordToken(token);
			if(StringUtils.isEmpty(username)){
				throw new BadRequestException(TOKEN_IS_INVALID);
			}
			optionalUser =  userRepository.findByUsername(username);
			if(!optionalUser.isPresent()){
				throw new BadRequestException(TOKEN_IS_INVALID);
			}
			User user = optionalUser.get();
			if(user.isInitialPasswordUsed()){
				throw new ErrorCodeException(442,LocaleUtils.getLocalized("error.password_already_created",null,user.getLanguage().getLocale()));
			}
		}

		User user = optionalUser.get();
		if (isInitialPassword && DateUtils.addHours(user.getEmailValidationTokenCreateDate(), setPasswordTokenValidHours)
				.before(new Date())) {
			throw new BadRequestException(TOKEN_EXPIRED);
		}

		if(!isInitialPassword && !user.isInitialPasswordUsed()){
			throw new ErrorCodeException(441,LocaleUtils.getLocalized("error.welcome",null,user.getLanguage().getLocale()));
		}

		return user;
	}

	public boolean isResetPasswordTokenValid(String token){
		try{
			User user = checkResetPasswordToken(token);
			return user != null;
		}catch(Exception e){
			return false;
		}
		
	}
	private User checkResetPasswordToken(String token){
		if (!jwtTokenUtil.isTokenOfType(token, JWTTypeEnum.RESET_PASSWORD)) {
			throw new RuntimeException(TOKEN_EXPIRED);
		}
		final String email = jwtTokenUtil.getEmailFromToken(token);
		User user = userRepository.findByUsername(email).get();
		if (jwtTokenUtil.isTokenExpired(user,token)) {
			throw new ArgumentValidationException(TOKEN_EXPIRED);
		}
		return user;
	}
	public User resetPassword(String token, String password) {

		logger.info("trying to create password for token " + token);
		User user = checkResetPasswordToken(token);

		user.setPassword(passwordEncoder.encode(password));
		user.setEmailValidationToken(null);
		user.setEmailValidationTokenCreateDate(null);
		
		user = userRepository.save(user);
		mailService.sendResetPasswordApprovalMail(user);
		userAuthenticationFailureManager.handleSuccessfullLogin(user.getId());
		return user;

	}

	public User changePassword(String currPassword, String newPassword) {
		if (StringUtils.isEmpty(currPassword)) {
			throw new ArgumentValidationException(CURRENT_PASSWORD_CAN_T_BE_EMPTY);
		}
		if (StringUtils.isEmpty(newPassword)) {
			throw new ArgumentValidationException(NEW_PASSWORD_CAN_T_BE_EMPTY);
		}
		User currUser = getCurrentUser();
		User user = userRepository.findByUsername(currUser.getUsername()).get();

		if (!passwordEncoder.matches(currPassword, user.getPassword())) {
			throw new ArgumentValidationException(LocaleUtils.getLocalized("errors.invalid.password"));
		}
		return setPassword(user,newPassword,true);

	}

	public User updateUser(UserDto userDto) {
		User user = modelMapper.map(userDto, User.class);
		if (user.getCompanyId() == null) {
			throw new ArgumentValidationException(COMPANY_IS_MANDATORY);
		}

		User existingUser = userRepository.findById(userDto.getId());
		boolean existingUserNotEnabled = !existingUser.isEnabled();
		Company company = companyService.getCompany(user.getCompanyId());

		User userForsave = SerializationUtils.clone(validateAndCopyUpdateUser(user, company, existingUser));
		if (getCurrentUser().getAuthority() == Authority.ADMIN) {
			if (existingUser.getFunction() != null && existingUser.getFunction().getUserFunction().getAreaManager() != null &&
					existingUser.getFunction().getUserFunction().getAreaManager() && user.getStoreId() != null) {
				user.setStoreGroup(null);
				userForsave.setStoreGroup(null);
			}

			userForsave.setFunction(mapToUserFunction(userDto.getFunction(), company, userForsave));
		}

		userForsave.setLanguage(user.getLanguage());
		if (!languageService.getSupportedLanguages().contains(user.getLanguage())) {
			throw new ArgumentValidationException("Language isn't supported");
		}

		userForsave.setStoreGroup(user.getStoreGroup());
		handleStoreGroup(userForsave);
		if (user.getStoreId() != null && userForsave.getStoreId() != null) {
			userForsave.setStoreId(user.getStoreId());
		}

		applyUserFunctionRestrictions(userForsave);
		User updatedUser = userRepository.save(userForsave);
		if (existingUserNotEnabled && updatedUser.isEnabled()) {
			if (updatedUser.getEmailValidationToken() == null) {
				mailService.sendUserWelcomeMail(updatedUser);
			}else {
				setGeneratedPasswordAndEmailValidationToken(updatedUser);
				updatedUser = userRepository.save(userForsave);
				mailService.sendUserActivationMail(updatedUser);
			}
		}

		return updatedUser;
	}

	private User validateAndCopyUpdateUser(User userForUpdate, Company company, User existingUser) {
		if (userForUpdate.getId() == null) {
			throw new ArgumentValidationException(USER_ID_IS_MANDATORY);
		}
		if (userForUpdate.getCompanyId() == null) {
			throw new ArgumentValidationException(COMPANY_IS_MANDATORY);
		}
		if (existingUser == null) {
			throw new UsernameNotFoundException(CAN_T_UPDATE_USER + userForUpdate.getUsername());
		}
		if (!existingUser.getCompanyId().equals(userForUpdate.getCompanyId())) {
			throw new ArgumentValidationException(COULD_NOT_MOVE_USER_TO_ANOTHER_COMPANY);
		}
		if (company == null) {
			throw new ArgumentValidationException(COMPANY_DOESN_T_EXIST);
		}

		User currentUser = getCurrentUser();
		if (StringUtils.isEmpty(currentUser.getFirstName())) {
			throw new ArgumentValidationException(FIRST_NAME_IS_MANDATORY);
		}
		if (StringUtils.isEmpty(currentUser.getLastName())) {
			throw new ArgumentValidationException(LAST_NAME_IS_MANDATORY);
		}
		if (currentUser.getId().equals(userForUpdate.getId()) && !userForUpdate.isEnabled()) {
			throw new UnsupportedOperationException(USER_CAN_T_BE_DISABLED);
		}
		if (currentUser.getAuthority() == Authority.USER) {
			validateNonEditableFieldsByUser(userForUpdate, currentUser);
			copyFieldsEditableByUser(userForUpdate, existingUser);
		}
		if (currentUser.getAuthority() == Authority.ADMIN) {
			if (!existingUser.getUsername().equals(userForUpdate.getUsername())) {
				throw new ArgumentValidationException(THIS_USER_CAN_T_EDIT_USERNAME);
			}
			copyFieldsEditableByCompanyAdmin(userForUpdate, existingUser);
			copyFieldsEditableByAdmin(userForUpdate, existingUser);
		}
		return existingUser;
	}

	private void validateNonEditableFieldsByUser(User userForUpdate, User currentUser) {
		if (!currentUser.getId().equals(userForUpdate.getId())) {
			throw new ArgumentValidationException(THIS_USER_CAN_T_EDIT_OTHERS);
		}
		validateNonEditableFieldsByNonAdmin(userForUpdate, currentUser);
		if (!currentUser.getAuthority().equals(userForUpdate.getAuthority())) {
			throw new ArgumentValidationException(THIS_USER_CAN_T_EDIT_AUTHORITY);
		}
	}

	private void validateNonEditableFieldsByNonAdmin(User userForUpdate, User currentUser) {
		if (!currentUser.getCompanyId().equals(userForUpdate.getCompanyId())) {
			throw new ArgumentValidationException(THIS_USER_CAN_T_EDIT_COMPANY);
		}
		if (userForUpdate.getAuthority() == null) {
			throw new ArgumentValidationException(AUTHORITY_IS_MANDATORY);
		}
	}

	private void copyFieldsEditableByAdmin(User userForUpdate, User existingUser) {
		copyFieldsEditableByCompanyAdmin(userForUpdate, existingUser);
		existingUser.setAuthority(userForUpdate.getAuthority());
	}

	private void copyFieldsEditableByCompanyAdmin(User userForUpdate, User existingUser) {
		copyFieldsEditableByUser(userForUpdate, existingUser);
		// company admin can't disable himself
		User currentUser = getCurrentUser();
		if (!userForUpdate.getId().equals(currentUser.getId())) {
			existingUser.setEnabled(userForUpdate.isEnabled());
		}
	}

	private void copyFieldsEditableByUser(User userForUpdate, User existingUser) {
		// merge
		existingUser.setFirstName(userForUpdate.getFirstName());
		existingUser.setLastName(userForUpdate.getLastName());
		existingUser.setStoreGroup(userForUpdate.getStoreGroup());
		existingUser.setGender(userForUpdate.getGender());
		existingUser.setDateOfBirth(userForUpdate.getDateOfBirth());
	}

	public Set<Segment> getAvailableSegments(User user) {
		Company company = companyService.getCompany(user.getCompanyId());
		if (SecurityFunctions.isAdminCompany(company) || user.getFunction().getUserFunction().getFullCatalogSupport()) {
			return new HashSet<>(catalogHierarchyService.getAllSegments(CatalogType.ANALYST));
		} else {
			Set<Supplier> companySuppliers = companySupplierService.getCompanySuppliers(company);
			Set<Long> supplierIds = companySuppliers.stream().map(element -> element.getId())
					.collect(Collectors.toSet());
			return catalogHierarchyService.getSegmentsBySuppliers(supplierIds,CatalogType.ANALYST);
		}

	}

	public List<PlanogramSegment> getPlanogramCategoryByFormatCode(User user, Long formatCode, Set<Long> categoryIds) {
		List<PlanogramSegment> segments = catalogHierarchyService.getPlanogramCategoryByFormatCode(formatCode, categoryIds);
		if(CollectionUtils.isEmpty(segments)) {
			return segments;
		}

		segments = filterUserAccessToPlanogramSegments(user, segments);

		Set<Long> uniqueCategories = new HashSet<>();
		return segments.stream().filter(s->{
			if(uniqueCategories.contains(s.getCategoryId())) {
				return false;
			}

			uniqueCategories.add(s.getCategoryId());
			return true;
		}).collect(Collectors.toList());
	}

	public List<PlanogramSegment> getPlanogramCategoryByStoreDwAndCategoryId(User user, Long formatCode, Set<Long> categoryIds, Long dwStoreKey) {
		List<PlanogramSegment> segments = catalogHierarchyService.getPlanogramCategoryByDwStoreKeyAndCategoryId(formatCode, categoryIds, dwStoreKey);
		if(CollectionUtils.isEmpty(segments)) {
			return segments;
		}

		segments = filterUserAccessToPlanogramSegments(user, segments);
		Map<Long,Set<Long>> uniquedwStoreKeyCategories = new HashMap<>();
		return segments.stream().filter(s->{
			Set<Long> uniqueCategories = uniquedwStoreKeyCategories.computeIfAbsent(dwStoreKey, v->new HashSet<>());
			if(uniqueCategories.contains(s.getCategoryId())) {
				return false;
			}

			uniqueCategories.add(s.getCategoryId());
			return true;
		}).collect(Collectors.toList());
	}

	private List<PlanogramSegment> filterUserAccessToPlanogramSegments(User user, List<PlanogramSegment> segments) {
		Company company = companyService.getCompany(user.getCompanyId());
		if (!user.getFunction().getUserFunction().getFullCatalogSupport()) {
			Set<Long> clusters;
			Set<Long> stores;
			if (!user.getFunction().getUserFunction().getAllStoresSupport()) {
				if (user.getStoreGroup() != null) {
					stores = user.getStoreGroup().getStores();
					clusters = Collections.singleton(user.getStoreGroup().getClusterId());
				} else {
					stores = Collections.singleton(user.getStoreId());
					clusters = storeService.getFormatsByStores(stores);
				}
			} else {
				clusters = null;
				stores = null;
			}

			Set<Long> supplierIds = companySupplierService.getCompanySuppliers(company, false).stream()
					.map(BaseDataEntity::getId).collect(Collectors.toSet());
			Set<Long> subCategoriesIds = new HashSet<>();
			for (UserSupplierSubCategory userSupplierSubCategory : user.getSupplierSubCategories()) {
				supplierIds.add(userSupplierSubCategory.getId().getSupplierId());
				subCategoriesIds.add(userSupplierSubCategory.getId().getSubcategoryId());
			}

			segments = segments.stream()
					.filter(s->planogramSegmentPredicate(clusters, stores, supplierIds, subCategoriesIds, s))
					.collect(Collectors.toList());
		}

		return segments;
	}

	private boolean planogramSegmentPredicate(Set<Long> clusters, Set<Long> stores, Set<Long> supplierIds,
			Set<Long> subCategoriesIds, PlanogramSegment s) {
		if (clusters != null && !clusters.contains(s.getFormatCode())) {
			return false;
		}

		if (stores != null && !stores.contains(s.getDwStoreKey())) {
			return false;
		}

		if (!supplierIds.contains(s.getSupplierId())) {
			return false;
		}

		if (!subCategoriesIds.contains(s.getSubcategoryId())) {
			return false;
		}

		return true;
	}

	public Set<SupplierClass> getUserClasses(User user, Set<Long> supplierIds) {
		Company company = companyService.getCompany(user.getCompanyId());
		if (SecurityFunctions.isAdminCompany(company) || user.getFunction().getUserFunction().getFullCatalogSupport()) {
			if (CollectionUtils.isEmpty(supplierIds)) {
				return catalogHierarchyService.getAllSupplierClasses(CatalogType.ANALYST);
			} else {
				return catalogHierarchyService.getSupplierClassesBySupplierIds(supplierIds,CatalogType.ANALYST);
			}
		} else {
			Set<Supplier> companySuppliers = companySupplierService.getCompanySuppliers(company);
			Set<Long> companySupplierIds = companySuppliers.stream().map(element -> element.getId())
					.collect(Collectors.toSet());

			if (!CollectionUtils.isEmpty(supplierIds)) {
				supplierIds.retainAll(companySupplierIds);
			} else {
				supplierIds = companySupplierIds;
			}
			return catalogHierarchyService.getSupplierClassesBySupplierIds(supplierIds,CatalogType.ANALYST);
		}

	}
	@Transactional
	public void updateTokenCreated( Long tokenCreated, String username) {
		userRepository.updateTokenCreated(tokenCreated,username);
    }
	public Set<Segment> getSegmentsForUser(User user,CatalogType catalogType, boolean isBm){
		
		if (user.getFunction().getUserFunction().getFullCatalogSupport()) {
			return new HashSet<>(catalogHierarchyService.getAllSegments(catalogType));
		}
		SupplierSubCategoryDto[] array = modelMapper.map(user.getSupplierSubCategories(), SupplierSubCategoryDto[].class);
		Set<SupplierSubCategoryDto> userCatalogHierarchyDto = new HashSet<>(Arrays.asList(array));
		
		Set<Segment> segments = null;
		CatalogType filterCatalog = CatalogType.ANALYST;
		catalogHierarchyService.getAllSegments(filterCatalog,isBm).stream().filter(s -> userCatalogHierarchyDto.contains(
				new SupplierSubCategoryDto(s.getSupplierId(),s.getSubcategoryId())))
				.collect(Collectors.toSet());
		if(!featureConfigurationService.isRetailerCatalog(catalogType)){
			return segments;
		}
		return catalogHierarchyService.getCustomerSegments(segments);
	}
	
	public Set<Segment> getStoreManagementSegmentsForUser(User user,CatalogType catalogType){
		
		if (user.getFunction().getUserFunction().getFullCatalogSupport()) {
			return new HashSet<>(catalogHierarchyService.getStoreManagementCatalog(catalogType));
		}
		Set<Long> subcategoryIds = new HashSet<>();
		user.getSupplierSubCategories().forEach(s->subcategoryIds.add(s.getId().getSubcategoryId()));
		if(!featureConfigurationService.isRetailerCatalog(catalogType)){
			Set<Segment> segments = catalogHierarchyService.getStoreManagementCatalog(CatalogType.ANALYST).stream().filter(s -> subcategoryIds.contains(
					s.getSubcategoryId())).collect(Collectors.toSet());
				return segments;
		}else{
			Set<Segment> segments = catalogHierarchyService.getRetailerStoreManagementCatalog().stream().filter(s -> subcategoryIds.contains(
					s.getSnSubcategoryId())).collect(Collectors.toSet());
				return segments;
		}
	
	}
	
	public Set<Long> getUserFormats(User user){
		if (user.getFunction().getUserFunction().getAllStoresSupport()) {
			return segmentRedshiftService.getFormatList().stream().map(f->f.getFormatId()).collect(Collectors.toSet());
		} else if(user.getStoreGroup() != null){
			return storeService.getFormatsByStores(user.getStoreGroup().getStores());
		}else if(user.getStoreId() != null){
			return storeService.getFormatsByStores(Stream.of(user.getStoreId()).collect(Collectors.toSet()));
		}
		return new HashSet<>();

	}
}
