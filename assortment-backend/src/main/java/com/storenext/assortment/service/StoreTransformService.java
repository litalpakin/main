package com.storenext.assortment.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.Feature;

@Service
public class StoreTransformService extends CacheTransformService{
	
	@Autowired
	FeatureConfigurationService featureConfigurationService;
	@Override
	public String map(String name){
		if(!featureConfigurationService.isEnabled(Feature.RETAILER_STORES_DATA)){
			return super.map(name);
		}else{
			return name;
		}
	}
}
