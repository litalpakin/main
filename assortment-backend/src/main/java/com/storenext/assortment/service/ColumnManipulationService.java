package com.storenext.assortment.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.data.ColumnSort;
import com.storenext.assortment.dto.TableDataDto;
import com.storenext.assortment.enums.Feature;

@Service
public class ColumnManipulationService {

	private Set<String> columnListForAdaptation = new HashSet<>();
	@Autowired
	private FeatureConfigurationService featureConfigurationService;
	
	@PostConstruct
	private void init(){
		columnListForAdaptation.add("item_name");
		columnListForAdaptation.add("supplier_name");
		columnListForAdaptation.add("sub_brand_hierarchy_2_name");
	}
	
	public void adaptOrderColumnsByRetailer(List<ColumnSort> columnSorts, TableDataDto tableDataDto){
		for(ColumnSort columnSort:columnSorts){
			if(columnListForAdaptation.contains(columnSort.getColumnName()) && !tableDataDto.getColumnNames().contains(columnSort.getColumnName())){
				columnSort.setColumnName("retailer_" + columnSort.getColumnName());
			}
		}
	}

	public void applyCatalogNamesTransformation(List<Map<String, Object>> data, String columnName) {
		if(!CollectionUtils.isEmpty(data)) {
			data.stream().filter(rows->rows.containsKey(columnName)).forEach(rows->rows.replace(columnName, applyCatalogNamesTransformation((String) rows.get(columnName))));
		}
	}

	public String applyCatalogNamesTransformation(String value) {
		if(StringUtils.isEmpty(value)) {
			return value;
		}
		if(!featureConfigurationService.isEnabled(Feature.RETAILER_STORES_DATA)) {
			value = value.replace("יינות ביתן", "");
			value = value.replace("ביתן", "");
			value = value.replace(" מגה", "");
		}
		return value;
	}
}
