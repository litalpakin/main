package com.storenext.assortment.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanySupplier;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.data.Supplier;
import com.storenext.assortment.repository.CompanyRepository;
import com.storenext.assortment.repository.CompanySupplierRepository;
import com.storenext.assortment.repository.UserSupplierSubCategoryRepository;
import com.storenext.assortment.security.SecurityFunctions;

@Service
public class CompanySupplierService {

	@Autowired
	private CompanySupplierRepository repository;
	@Autowired
	private  CatalogHierarchyService catalogHierarchyService;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private UserSupplierSubCategoryRepository userSupplierSubCategoryRepository;
	@Autowired
	private UserService userService;

	/**
	 * Return all suppliers for company.
	 *
	 * @return List of available suppliers
	 * @see List<Supplier>
	 */
	public Set<Supplier> getCompanySuppliers(Long companyId) {
		Company company = companyRepository.findOne(companyId);
		return getCompanySuppliers(company);

	}

	public Set<Supplier> getCompanySuppliers(Company company) {
		return getCompanySuppliers(company, true);
	}

	public Set<Supplier> getCompanySuppliers(Company company, boolean checkIsAdminCompany) {
		if (checkIsAdminCompany && SecurityFunctions.isAdminCompany(company)) {
			return catalogHierarchyService.getAllSuppliers(CatalogType.ANALYST);
		} else {
			List<CompanySupplier> companySuppliers = repository.findByCompanyId(company.getId());
			Set<Long> supplierIds = companySuppliers.stream().map(s->s.getId().getSupplierId())
					.collect(Collectors.toSet());
			return catalogHierarchyService.getSuppliers(supplierIds, CatalogType.ANALYST);
		}
	}
	
	/**
	 * Attach rows to link company and suppliers (add/update/delete suppliers to company).
	 *
	 * @return created objects
	 * @see CompanySupplier
	 */
	@Transactional
	public Set<Supplier> updateSupplierToCompany(
			Long companyId, List<Long> supplierIds) {
		Company original = companyRepository.findOne(companyId);
		Company company = SerializationUtils.clone(original);
		List<Long> removeSupplierIds = original.getSuppliers()
				.stream().map(s -> s.getId().getSupplierId())
				.filter(it -> !supplierIds.contains(it))
				.collect(Collectors.toList());
		company.setSuppliers(
				supplierIds.stream().map(it -> new CompanySupplier(company, it)).collect(Collectors.toList()));


		companyRepository.save(company);
		//Remove user - supplier links
		if (company.getType().equals(CompanyType.SECONDARY) && removeSupplierIds.size() > 0) {
			List<Long> userIds = userService
					.findByCompanyId(company.getId()).stream().map(u -> u.getId()).collect(Collectors.toList());
			removeSupplierIds.forEach(supId -> {
				userSupplierSubCategoryRepository.deleteUserSupplierLinks(supId, userIds);
			});
		}
		return getCompanySuppliers(company);
	}
}
