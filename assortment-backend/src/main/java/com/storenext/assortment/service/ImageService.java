package com.storenext.assortment.service;

import java.io.File;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.storenext.assortment.exception.FileUploadException;

import static com.storenext.assortment.utils.Constants.CANNOT_UPLOAD_IMAGE;

@Service
public class ImageService {
	
	private final Logger logger = LoggerFactory.getLogger(ImageService.class);
	
	@Autowired
	private Cloudinary cloudinary;
	
	@SuppressWarnings("unchecked")
	public String uploadImage(MultipartFile file) {
		try {
			File tempFile = File.createTempFile("img-", ".tmp");
			FileUtils.copyInputStreamToFile(file.getInputStream(), tempFile);
			Map<String, String> upload = cloudinary.uploader().upload(tempFile, ObjectUtils.emptyMap());
			return upload.get("secure_url");
		}catch(Exception ex) {
			logger.error(CANNOT_UPLOAD_IMAGE + ex.getMessage(), ex);
			throw new FileUploadException();
		}finally{
			
		}
	}

}
