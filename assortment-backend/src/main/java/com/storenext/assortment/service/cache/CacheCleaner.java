package com.storenext.assortment.service.cache;

public interface CacheCleaner {

	void cleanCache();
}
