package com.storenext.assortment.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.storenext.assortment.dto.TableDataDto;
import com.storenext.assortment.dto.TableDataWithSummaryDto;

@Service
public class ColumnTransformationService {
	private static final Logger logger = LoggerFactory.getLogger(ColumnTransformationService.class);
	@Value("${storenext.column.transformation:Yeynot_Bitan_Conversion}")
	private String outputColumnName;
	private Map<String, String> columnMapping = new HashMap<>();
	private static final String inputColumnName = "Parameter_To_Convert";

	@PostConstruct
	private void init() {

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("converting_parameters_per_environment.xlsx")) {
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
			XSSFSheet worksheet = workbook.getSheetAt(0);
			int inputColumnIndex = -1;
			int outputColumnIndex = -1;
			XSSFRow row = worksheet.getRow(0);
			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
				XSSFCell cell = row.getCell(i);

				if (cell.getStringCellValue().equals(inputColumnName)) {
					inputColumnIndex = i;
				}
				if (cell.getStringCellValue().equals(outputColumnName)) {
					outputColumnIndex = i;
				}
				if (outputColumnIndex > 0 && inputColumnIndex > 0) {
					break;
				}
			}
			if (outputColumnIndex < 0) {
				throw new RuntimeException("Failed to find output columns");
			}
			if (inputColumnIndex < 0) {
				throw new RuntimeException("Failed to find input columns");
			}
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				XSSFRow currRow = worksheet.getRow(i);

				XSSFCell inputCell = currRow.getCell(inputColumnIndex);
				XSSFCell outputCell = currRow.getCell(outputColumnIndex);
				if (!StringUtils.isEmpty(inputCell.getStringCellValue())) {
					String inputColumnName = inputCell.getStringCellValue();
					if (!StringUtils.isEmpty(outputCell.getStringCellValue()) && !outputCell.getStringCellValue().equals("-")) {
						logger.info("Adding mapping " + inputColumnName + " : " + outputCell.getStringCellValue());
						columnMapping.put(inputColumnName, outputCell.getStringCellValue());
					}
				}
			}

		} catch (Exception e) {
			throw new RuntimeException("Failed to read column name file ", e);
		}
		logger.info("Loaded values: " + columnMapping.size());

	}

	public void applyColumnTransformation(TableDataDto dto) {
		applyColumnTransformation(dto, null);

	}

	public void applyColumnTransformation(TableDataDto dto, Set<String> columnsForDeletion) {
		if (dto != null && !CollectionUtils.isEmpty(dto.getColumnNames())) {
			List<Integer> columnIndexesForDeletion = new ArrayList<>();
			for (int i = 0; i < dto.getColumnNames().size(); i++) {
				if (!CollectionUtils.isEmpty(columnsForDeletion) && columnsForDeletion.contains(dto.getColumnNames().get(i))) {
					columnIndexesForDeletion.add(i);
				} else if (!CollectionUtils.isEmpty(columnMapping) && columnMapping.containsKey(dto.getColumnNames().get(i))) {
					dto.getColumnNames().set(i, columnMapping.get(dto.getColumnNames().get(i)));
				}

			}
			if (!CollectionUtils.isEmpty(columnIndexesForDeletion)) {
				Collections.sort(columnIndexesForDeletion);
				Collections.reverse(columnIndexesForDeletion);
				for (Integer columnIndexForDeletion : columnIndexesForDeletion) {
					dto.getColumnNames().remove(columnIndexForDeletion.intValue());
				}
				for (List<Object> row : dto.getRows()) {
					for (Integer columnIndexForDeletion : columnIndexesForDeletion) {
						row.remove(columnIndexForDeletion.intValue());
					}
				}
			}
		}
		if (dto instanceof TableDataWithSummaryDto) {
			TableDataWithSummaryDto summaryDto = (TableDataWithSummaryDto) dto;
			applyColumnTransformation(summaryDto.getSummary(), columnsForDeletion);
		}
	}

	public String applyColumnTransformation(String str) {
		return columnMapping.getOrDefault(str, str);
	}
}
