package com.storenext.assortment.service;

import java.util.HashMap;
import java.util.Map;

public class CacheTransformService {
	Map<String,String> map = new HashMap<>();
	
	protected String map(String storeName){
		if(storeName == null){
			return storeName;
		}
		String key = map.get(storeName);
		if(key == null){
			key = "" + map.size(); 
			map.put(storeName, key);
		}
		return key;
	}
	public void clear(){
		map.clear();
	}
}
