package com.storenext.assortment.service;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.exception.BadRequestException;
import com.storenext.assortment.model.FeatureConfiguration;
import com.storenext.assortment.repository.FeatureConfigurationRepository;
import com.storenext.assortment.utils.LocaleUtils;

@Service
public class FeatureConfigurationService {

	@Autowired
	FeatureConfigurationRepository featureConfigurationRepository;
	
	Map<Feature,FeatureConfiguration> featureConfigurations;
	@PostConstruct
	private void init(){
		featureConfigurations = featureConfigurationRepository.findAll().stream().collect(Collectors.toMap(FeatureConfiguration::getFeature, Function.identity()));
	}
	
	public boolean isEnabled(Feature feature){
		FeatureConfiguration featureConfiguration = featureConfigurations.get(feature);
		if(featureConfiguration != null){
			return featureConfiguration.isEnabled();
		}
		return true;
	}
	
	public Collection<FeatureConfiguration> getFeatureConfigurations(){
		return featureConfigurations.values();
	}
	
	
	public void updateFeatureConfiguration( boolean isEnabled, Feature feature){
		featureConfigurationRepository.updateFeatureConfiguration(isEnabled, feature);
		FeatureConfiguration featureConfiguration = featureConfigurations.get(feature);
		if(featureConfiguration != null){
			featureConfiguration.setEnabled(isEnabled);
		}
	}
	public boolean isRetailerCatalog(CatalogType catalogType){
		if(!isEnabled(Feature.RETAILER_CATALOG_TYPE) && catalogType == CatalogType.RETAILER){
			throw new BadRequestException(LocaleUtils.getLocalized("error.request_catalog"));
		}
		return catalogType == CatalogType.RETAILER;
		
	}


}
