package com.storenext.assortment.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.storenext.assortment.dto.TableDataDto;

@Service
public class FormatDataService {

	
	public String formatColumnName(String name){
		if(name == null){
			return null;
		}
		return name.replaceAll("_\\d+$", "");
	}
	
	
	public List<String> formatColumnNames(List<String> columnNames){
		return columnNames.stream().map(s -> formatColumnName(s)).collect(Collectors.toList());
	}
	public int findColumnIndex(String orderFieldName, List<String> formatedColumnNames) {
		int orderFieldIndex = 0;
		for(String columnName:formatedColumnNames){
			if(columnName.equalsIgnoreCase(orderFieldName)){
				break;
			}
			orderFieldIndex++;
		}
		if(orderFieldIndex == formatedColumnNames.size()){
			return -1;
		}
		return orderFieldIndex;
	}
	public void addRankField(TableDataDto tableDataDto, int start) {
		for(int i = start; i <= tableDataDto.getRows().size() + start -1; i++){
			tableDataDto.getRows().get(i-start).add(new Integer(i));
		}
	}

}
