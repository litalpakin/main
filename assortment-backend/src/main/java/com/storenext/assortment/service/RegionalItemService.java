package com.storenext.assortment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.storenext.assortment.enums.Feature;

@Service
public class RegionalItemService {

	@Autowired
	private FeatureConfigurationService featureConfigurationService;
	
	private final String REGIONAL_ITEMS_PLACEHOLDER = "{REGIONAL_ITEMS_PLACEHOLDER}";
	private final String REGIONAL_ITEMS_CY_PLACEHOLDER = "{REGIONAL_ITEMS_CY_PLACEHOLDER}";
	private final String REGIONAL_ITEMS_MONTH_PLACEHOLDER = "{REGIONAL_ITEMS_MONTH_PLACEHOLDER}";
	
	
	public String applyRegional(String sql){
		
		if(featureConfigurationService.isEnabled(Feature.REGIONAL_ITEMS)){
			sql= sql.replace(REGIONAL_ITEMS_CY_PLACEHOLDER, ",max(regional_optimal_gap_item_format_priority_cy) regional_optimal_gap_item_format_priority_cy ");
			sql= sql.replace(REGIONAL_ITEMS_MONTH_PLACEHOLDER, ",max(regional_optimal_gap_item_format_priority_3months) regional_optimal_gap_item_format_priority_3months ");
			sql= sql.replace(REGIONAL_ITEMS_PLACEHOLDER, ",is_regional_item_format");
			
		}else{
			sql= sql.replace(REGIONAL_ITEMS_PLACEHOLDER, "");
			sql= sql.replace(REGIONAL_ITEMS_MONTH_PLACEHOLDER, "");
			sql= sql.replace(REGIONAL_ITEMS_CY_PLACEHOLDER,"");
			
		}
		return sql;
	}
}
