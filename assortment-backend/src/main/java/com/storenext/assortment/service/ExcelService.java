package com.storenext.assortment.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.storenext.assortment.dto.TableDataDto;

@Service
public class ExcelService {

	public TableDataDto loadTab(String fileName,String tabName){
		TableDataDto response = new TableDataDto();
		try (InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(fileName)) {
			
			XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
		    XSSFSheet worksheet = workbook.getSheet(tabName);
			List<String> columns = new ArrayList<String>();
		    XSSFRow row = worksheet.getRow(0);
		    for(int i=0;i<row.getPhysicalNumberOfCells() ;i++) {
		    	XSSFCell cell =	row.getCell(i);
		    	
		        if(!StringUtils.isEmpty(cell.getStringCellValue())){
		        	columns.add(cell.getStringCellValue());
		        }else{
		        	break;
		        }
		    }
		    response.setColumnNames(columns);;
		    List<List<Object>> rows= new ArrayList<>();
		    for(int rowIndex=1;rowIndex<worksheet.getPhysicalNumberOfRows() ;rowIndex++) {
		    	List<Object> rowValues = new ArrayList<>();
		    	XSSFRow currRow = worksheet.getRow(rowIndex);
		    	for(int columnIndex = 0; columnIndex < columns.size();columnIndex++){
		    		XSSFCell inputCell = currRow.getCell(columnIndex);
		    		switch(inputCell.getCellType()){
		    		case Cell.CELL_TYPE_NUMERIC:
		    			rowValues.add(inputCell.getNumericCellValue());
		    			break;
		    		case Cell.CELL_TYPE_STRING:
		    			rowValues.add(inputCell.getStringCellValue());
		    			break;
		    		case Cell.CELL_TYPE_BOOLEAN:
		    			rowValues.add(inputCell.getBooleanCellValue());

		    		}
		    	}
		    	rows.add(rowValues);  
		    }
		    response.setRows(rows);
	
		} catch (Exception e) {
			throw new RuntimeException("Failed to read column name file " , e);
		}
		return response;
	}
}
