package com.storenext.assortment.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EnvironmentControlService {

	
	private static final String YB_ENVIRONMENT = "Yeynot_Bitan_Conversion";
	private static final String DEMO_ENVIRONMENT = "DEMO_Conversion";
	private static final String VICTORY_ENVIRONMENT = "Victory_Conversion";
	private static final String PRESALE_ENVIRONMENT = "PRESALE_Conversion";
	
	@Value("${storenext.column.transformation:Yeynot_Bitan_Conversion}")
	private String currEnvironment;
	
	
	public boolean isYBEnvironment(){
		return currEnvironment.equals(YB_ENVIRONMENT);
	}
	public boolean isDemoEnvironment(){
		return currEnvironment.equals(DEMO_ENVIRONMENT);
	}
	public boolean isVictoryEnvironment(){
		return currEnvironment.equals(VICTORY_ENVIRONMENT);
	}
	public boolean isPresaleEnvironment(){
		return currEnvironment.equals(PRESALE_ENVIRONMENT);
	}
	public String getCurrEnvironment() {
		return currEnvironment;
	}
	public void setCurrEnvironment(String currEnvironment) {
		this.currEnvironment = currEnvironment;
	}

}
