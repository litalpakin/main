package com.storenext.assortment.service;

import static com.storenext.assortment.utils.Constants.FUNCTION_DOES_NOT_EXIST;
import static com.storenext.assortment.utils.Constants.ID_IS_PRESENT;
import static com.storenext.assortment.utils.Constants.MANDATORY_ID;
import static com.storenext.assortment.utils.Constants.NOT_ENOUGH_PERMISSION;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.ws.rs.ForbiddenException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.storenext.assortment.dto.UserFunctionDto;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.UserFunction;
import com.storenext.assortment.model.UserUserFunction;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.repository.UserFunctionRepository;
import com.storenext.assortment.repository.UserUserFunctionRepository;

@Service
public class UserFunctionService {
	@Autowired
	private UserFunctionRepository userFunctionRepository;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserUserFunctionRepository userUserFunctionRepository;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserService userService;
	/**
	 * Return all functions.
	 *
	 * @return List of available functions for current user
	 * @see List< UserFunction >
	 */
	public List<UserFunction> getAll(User user) {
		Company company = companyService.getCompany(user.getCompanyId());
		if (!company.getType().equals(CompanyType.ADMIN)) {
			return userFunctionRepository.findByCompanyType(company.getType());
		} else {
			return userFunctionRepository.findAll();
		}
	}

	/**
	 * Return default admin functions.
	 *
	 * @return List of default admin  functions
	 * @see List< UserFunction >
	 */
	public UserFunction getDefaultAdminFunctions() {
		return userFunctionRepository.getDefaultAdminFunction();
	}

	/**
	 * Return function.
	 *
	 * @param id entity id in db.
	 * @return function object
	 * @see UserFunction
	 */
	public UserFunction getById(Long id) {
		return userFunctionRepository.findOne(id);
	}

	/**
	 * Add new function object to DB.
	 *
	 * @param userFunctionDto (@see UserFunctionDto) object which will be stored to DB.
	 * @return created objects
	 * @see UserFunction
	 */
	@Transactional
	public UserFunction add(UserFunctionDto userFunctionDto) {
		UserFunction userFunction = modelMapper.map(userFunctionDto, UserFunction.class);
		if (userFunction.getId() != null) {
			throw new ArgumentValidationException(ID_IS_PRESENT);
		}

		return userFunctionRepository.save(userFunction);
	}

	/**
	 * Update existing function object in DB.
	 *
	 * @param userFunctionDto (@see UserFunctionDto) object which will be updated.
	 * @return updated objects
	 * @see UserFunction
	 */
	@Transactional
	public UserFunction update(UserFunctionDto userFunctionDto) {
		UserFunction userFunction = modelMapper.map(userFunctionDto, UserFunction.class);
		if (userFunction.getId() == null) {
			throw new ArgumentValidationException(MANDATORY_ID);
		}

		UserFunction exist = userFunctionRepository.findOne(userFunction.getId());
		if (Objects.isNull(exist)) {
			throw new ArgumentValidationException(String.format(FUNCTION_DOES_NOT_EXIST, userFunction.getId()));
		}

		if (exist.getType() == null) {
			if (exist.isDefault()) {
				throw new ForbiddenException(NOT_ENOUGH_PERMISSION);
			}
			
			exist.setName(userFunction.getName());
		}
		
		exist.setItemClassificator(userFunctionDto.getItemClassificator());
		exist.setPlanogramCategory(userFunctionDto.getPlanogramCategory());
		exist.setPlanogramStore(userFunctionDto.getPlanogramStore());

		return userFunctionRepository.save(exist);
	}

	/**
	 * Remove function from DB.
	 *
	 * @param functionId function id to remove.
	 * @param substituteId function id which will be substituted instead removed one.
	 */
	@Transactional
	public void delete(Long functionId, Long substituteId) {
		UserFunction existUserFunction = userFunctionRepository.findOne(functionId);
		if (existUserFunction.isDefault()) {
			throw new ForbiddenException(NOT_ENOUGH_PERMISSION);
		}
		if (!Objects.isNull(substituteId)) {
			UserFunction newUserFunction = userFunctionRepository.findOne(substituteId);
			if (Objects.isNull(newUserFunction)) {
				throw new ArgumentValidationException(String.format(FUNCTION_DOES_NOT_EXIST, substituteId));
			}
			if(newUserFunction.getCompanyType() != existUserFunction.getCompanyType()){
				throw new ArgumentValidationException("Can't replace by function with diffrent company type");
			}
			
			Set<UserUserFunction> users = userUserFunctionRepository.findByUserFunctionId(functionId);
			for(UserUserFunction userUserFunction:users){
				userUserFunction.setUserFunction(newUserFunction);
				userUserFunction.getId().setUserFunctionId(newUserFunction.getId());
				userUserFunction = userUserFunctionRepository.save(userUserFunction);
				User user = userUserFunction.getUser();
				userService.applyUserFunctionRestrictions(user);
				userUserFunctionRepository.save(userUserFunction);
			}
			
			userUserFunctionRepository.updateFunctionId(functionId, substituteId);
			
		}
		userFunctionRepository.delete(functionId);
	}
	
	
}
