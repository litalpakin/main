package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

public class UsernameDto {
	@NotNull
	private String username;
	
	public UsernameDto(){
		
	}
	
	public UsernameDto(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
