package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class StatusReportTabDto {

	private Long id;
	private String name;
	private List<GraphDataDto> graphs = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<GraphDataDto> getGraphs() {
		return graphs;
	}
	public void setGraphs(List<GraphDataDto> graphs) {
		this.graphs = graphs;
	}
	
}
