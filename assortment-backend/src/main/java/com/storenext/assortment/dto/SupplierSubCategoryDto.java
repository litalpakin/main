package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

public class SupplierSubCategoryDto {
	

	@NotNull
	private Long supplierId;
	@NotNull
	private Long subcategoryId;

	public SupplierSubCategoryDto() {
	}

	public SupplierSubCategoryDto(Long supplierId, Long subCategoryId) {
		this.supplierId = supplierId;
		this.subcategoryId = subCategoryId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}


	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subcategoryId == null) ? 0 : subcategoryId.hashCode());
		result = prime * result + ((supplierId == null) ? 0 : supplierId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierSubCategoryDto other = (SupplierSubCategoryDto) obj;
		if (subcategoryId == null) {
			if (other.subcategoryId != null)
				return false;
		} else if (!subcategoryId.equals(other.subcategoryId))
			return false;
		if (supplierId == null) {
			if (other.supplierId != null)
				return false;
		} else if (!supplierId.equals(other.supplierId))
			return false;
		return true;
	}

	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
}
