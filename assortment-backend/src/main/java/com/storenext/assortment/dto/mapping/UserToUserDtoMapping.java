package com.storenext.assortment.dto.mapping;

import java.time.Instant;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.UserDto;
import com.storenext.assortment.dto.UserFunctionDto;
import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.UserUserFunction;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.LanguageService;

public class UserToUserDtoMapping extends PropertyMap<User, UserDto> {
	private LanguageService languageService;

	public UserToUserDtoMapping(LanguageService languageService) {
		this.languageService = languageService;
	}

	@Override
	protected void configure() {
		using(new AbstractConverter<UserUserFunction, UserFunctionDto>() {
			@Override
			protected UserFunctionDto convert(UserUserFunction source) {
				UserFunctionDto dto = new UserFunctionDto();
				String name;
				if (source.getUserFunction().getType() == null) {
					name = source.getUserFunction().getName();
				} else {
					name = source.getUserFunction().getType().getName(languageService.getDefaultLanguage());
				}

				dto.setName(name);
				dto.setCompanyType(source.getUserFunction().getCompanyType());
				dto.setId(source.getUserFunction().getId());
				dto.setAllStoresSupport(source.getUserFunction().getAllStoresSupport());
				dto.setAreaManager(source.getUserFunction().getAreaManager());
				dto.setFullCatalogSupport(source.getUserFunction().getFullCatalogSupport());
				dto.setItemClassificator(source.getUserFunction().getItemClassificator());
				dto.setPlanogramCategory(source.getUserFunction().getPlanogramCategory());
				dto.setPlanogramStore(source.getUserFunction().getPlanogramStore());
				return dto;
			}
		}).map(source.getFunction()).setFunction(null);
		using(new AbstractConverter<Language, Language>() {
			@Override
			protected Language convert(Language source) {
				if (source == null) {
					return languageService.getDefaultLanguage();
				}
				return source;
			}
		}).map(source.getLanguage()).setLanguage(null);
		using(new AbstractConverter<Long, Date>() {
			@Override
			protected Date convert(Long source) {
				if (source == null) {
					return null;
				}

				return Date.from(Instant.ofEpochMilli(source));
			}
		}).map(source.getLastRequest()).setLastRequestDate(null);
	}
}
