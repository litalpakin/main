package com.storenext.assortment.dto;

import com.storenext.assortment.dto.reports.UserReportType;

public class UserReportIdNameDto extends IdNameDto{
	private UserReportType type;

	public UserReportType getType() {
		return type;
	}

	public void setType(UserReportType type) {
		this.type = type;
	}
}
