package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.enums.Feature;

public class FeatureConfigurationDto {
	@NotNull
	private Feature feature;
	@NotNull
	private boolean isEnabled;
	public Feature getFeature() {
		return feature;
	}
	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	public boolean isEnabled() {
		return isEnabled;
	}
	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}
