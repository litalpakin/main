package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.enums.UserFunctionType;
import com.storenext.assortment.model.CompanyType;

public class UserFunctionDto {
	private Long id;
	@NotNull
	private String name;
	@NotNull
	private CompanyType companyType;
	@NotNull
	private Boolean fullCatalogSupport = false;
	@NotNull
	private Boolean allStoresSupport = false;
	@NotNull
	private Boolean areaManager = false;
	private Boolean itemClassificator = false;
	private Boolean planogramCategory = false;
	private Boolean planogramStore = false;
	private UserFunctionType type;
	private Boolean balanceModelAccess = false;

	public Boolean getBalanceModelAccess() {
		return balanceModelAccess;
	}

	public void setBalanceModelAccess(Boolean balanceModelAccess) {
		this.balanceModelAccess = balanceModelAccess;
	}
	public Boolean getItemClassificator() {
		return itemClassificator;
	}

	public void setItemClassificator(Boolean itemClassificator) {
		this.itemClassificator = itemClassificator;
	}

	public UserFunctionDto() {
	}

	public UserFunctionDto(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CompanyType getCompanyType() {
		return companyType;
	}

	public void setCompanyType(CompanyType companyType) {
		this.companyType = companyType;
	}

	public Boolean getFullCatalogSupport() {
		return fullCatalogSupport;
	}

	public void setFullCatalogSupport(Boolean fullCatalogSupport) {
		this.fullCatalogSupport = fullCatalogSupport;
	}

	public Boolean getAllStoresSupport() {
		return allStoresSupport;
	}

	public void setAllStoresSupport(Boolean allStoresSupport) {
		this.allStoresSupport = allStoresSupport;
	}

	public Boolean getAreaManager() {
		return areaManager;
	}

	public void setAreaManager(Boolean areaManager) {
		this.areaManager = areaManager;
	}

	public UserFunctionType getType() {
		return type;
	}

	public void setType(UserFunctionType type) {
		this.type = type;
	}

	public Boolean getPlanogramCategory() {
		return planogramCategory;
	}

	public void setPlanogramCategory(Boolean planogramCategory) {
		this.planogramCategory = planogramCategory;
	}

	public Boolean getPlanogramStore() {
		return planogramStore;
	}

	public void setPlanogramStore(Boolean planogramStore) {
		this.planogramStore = planogramStore;
	}
}
