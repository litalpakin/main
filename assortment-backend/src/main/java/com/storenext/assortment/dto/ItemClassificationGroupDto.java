package com.storenext.assortment.dto;

import java.util.Set;

import com.storenext.assortment.model.ExtendedClassificationGroup;

public class ItemClassificationGroupDto {
	

	private Long dwKey;
	private Set<ExtendedClassificationGroup> classificationGroups;
	
	public ItemClassificationGroupDto(Long dwKey, Set<ExtendedClassificationGroup> classificationGroups) {
		this.dwKey = dwKey;
		this.classificationGroups = classificationGroups;
	}
	
	public ItemClassificationGroupDto(){
		
	}
	public Long getDwKey() {
		return dwKey;
	}
	public void setDwKey(Long dwKey) {
		this.dwKey = dwKey;
	}
	public Set<ExtendedClassificationGroup> getClassificationGroups() {
		return classificationGroups;
	}
	public void setClassificationGroups(Set<ExtendedClassificationGroup> classificationGroups) {
		this.classificationGroups = classificationGroups;
	}

}
