package com.storenext.assortment.dto.mapping;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.AbstractConverter;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.StoreDto;
import com.storenext.assortment.dto.StoreGroupDto;
import com.storenext.assortment.model.StoreGroup;

public class StoreGroupDtoToStoreGroupMapping extends PropertyMap<StoreGroupDto,StoreGroup>{


	public StoreGroupDtoToStoreGroupMapping(){
	}
	@Override
	protected void configure() {
		using(new AbstractConverter<Set<StoreDto>,Set<Long>>(){

			@Override
			protected Set<Long> convert(Set<StoreDto> source) {
				if(source != null && !source.isEmpty()){
					return source.stream().map(sd -> sd.getId()).collect(Collectors.toSet());
				}
				return new HashSet<>();
			}
		}).map(source.getStores()).setStores(null);
		
	}

}
