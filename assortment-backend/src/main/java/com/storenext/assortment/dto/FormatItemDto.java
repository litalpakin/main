package com.storenext.assortment.dto;

public class FormatItemDto extends ItemDto {

	private Long formatCode;
	
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((formatCode == null) ? 0 : formatCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormatItemDto other = (FormatItemDto) obj;
		if (formatCode == null) {
			if (other.formatCode != null)
				return false;
		} else if (!formatCode.equals(other.formatCode))
			return false;
		return true;
	}
	
}
