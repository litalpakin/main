package com.storenext.assortment.dto;

import com.storenext.assortment.enums.ClassificationGroup;

public class FormatItemClassificationDto {
	private Long formatId;
	private String formatName;
	private ClassificationGroup itemFormatClassification;
	private Object isItemClassificationEqualForAllFormat;
	
	public Long getFormatId() {
		return formatId;
	}
	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	public ClassificationGroup getItemFormatClassification() {
		return itemFormatClassification;
	}
	public void setItemFormatClassification(ClassificationGroup itemFormatClassification) {
		this.itemFormatClassification = itemFormatClassification;
	}
	public Object getIsItemClassificationEqualForAllFormat() {
		return isItemClassificationEqualForAllFormat;
	}
	public void setIsItemClassificationEqualForAllFormat(Object isItemClassificationEqualForAllFormat) {
		this.isItemClassificationEqualForAllFormat = isItemClassificationEqualForAllFormat;
	}
	
}
