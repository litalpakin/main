package com.storenext.assortment.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class ChangePasswordRequest {

	@NotEmpty
	private String newPassword;
	
	@NotEmpty
	private String currentPassword;

	public ChangePasswordRequest() {
	}

	public ChangePasswordRequest(String newPassword, String currentPassword) {
		this.newPassword = newPassword;
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
}
