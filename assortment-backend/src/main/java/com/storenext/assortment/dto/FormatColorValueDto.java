package com.storenext.assortment.dto;

import com.storenext.assortment.enums.TrafficLightsColor;

public class FormatColorValueDto {
	private Integer formatId;
	private String formatName;
	private Object value;
	private TrafficLightsColor trafficLightsColor;
	
	public Integer getFormatId() {
		return formatId;
	}
	public void setFormatId(Integer formatId) {
		this.formatId = formatId;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public TrafficLightsColor getTrafficLightsColor() {
		return trafficLightsColor;
	}
	public void setTrafficLightsColor(TrafficLightsColor trafficLightsColor) {
		this.trafficLightsColor = trafficLightsColor;
	}
	public String getFormatName() {
		return formatName;
	}
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
}
