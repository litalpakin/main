package com.storenext.assortment.dto;

public class CatalogFormatItemDto extends FormatItemDto {
	protected Long subcategoryId;
	protected Long supplierId;

	public Long getSubcategoryId() {
		return subcategoryId;
	}

	public void setSubcategoryId(Long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

}
