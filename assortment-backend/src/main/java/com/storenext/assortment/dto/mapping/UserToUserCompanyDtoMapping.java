package com.storenext.assortment.dto.mapping;

import java.util.function.Function;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.UserCompanyDto;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.security.User;

public class UserToUserCompanyDtoMapping extends PropertyMap<User,UserCompanyDto> {
	private ComapanyIdToComapnyDtoConverter comapanyIdToComapnyDtoConverter = null;

	public UserToUserCompanyDtoMapping(Function<Long,Company> getCompanyByIdFunction,ModelMapper modelMapper){
		comapanyIdToComapnyDtoConverter = new ComapanyIdToComapnyDtoConverter(getCompanyByIdFunction,modelMapper);
	}
	@Override
    protected void configure() {
        using(comapanyIdToComapnyDtoConverter).map(source.getCompanyId()).setCompany(null);
    }

}



