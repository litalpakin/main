package com.storenext.assortment.dto;

import java.util.List;

import com.storenext.assortment.model.data.FormatStore;
import com.storenext.assortment.model.data.SubChain;

public class NewStoresDto {
	private List<SubChain> subChains;
	private List<FormatStore> newStores;
	public List<SubChain> getSubChains() {
		return subChains;
	}
	public void setSubChains(List<SubChain> subChains) {
		this.subChains = subChains;
	}
	public List<FormatStore> getNewStores() {
		return newStores;
	}
	public void setNewStores(List<FormatStore> newStores) {
		this.newStores = newStores;
	}
}
