package com.storenext.assortment.dto;

import java.util.Set;

public class ItemIdDto {
	private Integer serialNumber;
	private String classificationGroup;
	private Integer apiPart;
	private String idCode;
	private Set<Integer> idCodeList;
	private Integer parameterOrderInPart;
	private String parameterName;
	private String comments;

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getClassificationGroup() {
		return classificationGroup;
	}

	public void setClassificationGroup(String classificationGroup) {
		this.classificationGroup = classificationGroup;
	}

	public Integer getApiPart() {
		return apiPart;
	}

	public void setApiPart(Integer apiPart) {
		this.apiPart = apiPart;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public Set<Integer> getIdCodeList() {
		return idCodeList;
	}

	public void setIdCodeList(Set<Integer> idCodeList) {
		this.idCodeList = idCodeList;
	}

	public Integer getParameterOrderInPart() {
		return parameterOrderInPart;
	}

	public void setParameterOrderInPart(Integer parameterOrderInPart) {
		this.parameterOrderInPart = parameterOrderInPart;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}
