package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TableManagementDto {
	private Integer serialNumber;
	private String tableName;
	private List<String> searchOn = new ArrayList<>();
	private Map<String, String> textInSearch = new HashMap<>();
	private List<String> initialOrder = new ArrayList<>();
	private Boolean rowOpensItemId;
	private Boolean columnManagementButton;
	private Boolean advancedFilterButton;
	private Boolean dotsInRows;
	private Boolean displayDocsColumn;
	private List<Integer> connectedTabs = new ArrayList<>();
	private Boolean connectedSearchField;
	private Boolean connectedSort;
	private Boolean connectedAdvancedFilters;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<String> getSearchOn() {
		return searchOn;
	}

	public void setSearchOn(List<String> searchOn) {
		this.searchOn = searchOn;
	}

	public List<String> getInitialOrder() {
		return initialOrder;
	}

	public void setInitialOrder(List<String> initialOrder) {
		this.initialOrder = initialOrder;
	}

	public Integer getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(Integer serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Map<String, String> getTextInSearch() {
		return textInSearch;
	}

	public void setTextInSearch(Map<String, String> textInSearch) {
		this.textInSearch = textInSearch;
	}

	public void putTextInSearch(String key, String value) {
		getTextInSearch().put(key, value);
	}

	public Boolean getRowOpensItemId() {
		return rowOpensItemId;
	}

	public void setRowOpensItemId(Boolean rowOpensItemId) {
		this.rowOpensItemId = rowOpensItemId;
	}

	public Boolean getColumnManagementButton() {
		return columnManagementButton;
	}

	public void setColumnManagementButton(Boolean columnManagementButton) {
		this.columnManagementButton = columnManagementButton;
	}

	public Boolean getAdvancedFilterButton() {
		return advancedFilterButton;
	}

	public void setAdvancedFilterButton(Boolean advancedFilterButton) {
		this.advancedFilterButton = advancedFilterButton;
	}

	public Boolean getDotsInRows() {
		return dotsInRows;
	}

	public void setDotsInRows(Boolean dotsInRows) {
		this.dotsInRows = dotsInRows;
	}

	public Boolean getDisplayDocsColumn() {
		return displayDocsColumn;
	}

	public void setDisplayDocsColumn(Boolean displayDocsColumn) {
		this.displayDocsColumn = displayDocsColumn;
	}

	public List<Integer> getConnectedTabs() {
		return connectedTabs;
	}

	public void setConnectedTabs(List<Integer> connectedTabs) {
		this.connectedTabs = connectedTabs;
	}

	public void addConnectedTab(Integer connectedTab) {
		connectedTabs.add(connectedTab);
	}

	public Boolean getConnectedSearchField() {
		return connectedSearchField;
	}
	
	public void setConnectedSearchField(Boolean connectedSearchField) {
		this.connectedSearchField = connectedSearchField;
	}

	public Boolean getConnectedSort() {
		return connectedSort;
	}

	public void setConnectedSort(Boolean connectedSort) {
		this.connectedSort = connectedSort;
	}

	public Boolean getConnectedAdvancedFilters() {
		return connectedAdvancedFilters;
	}

	public void setConnectedAdvancedFilters(Boolean connectedAdvancedFilters) {
		this.connectedAdvancedFilters = connectedAdvancedFilters;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		TableManagementDto that = (TableManagementDto) o;
		return Objects.equals(serialNumber, that.serialNumber) &&
				Objects.equals(tableName, that.tableName) &&
				Objects.equals(searchOn, that.searchOn) &&
				Objects.equals(textInSearch, that.textInSearch) &&
				Objects.equals(initialOrder, that.initialOrder) &&
				Objects.equals(rowOpensItemId, that.rowOpensItemId) &&
				Objects.equals(columnManagementButton, that.columnManagementButton) &&
				Objects.equals(advancedFilterButton, that.advancedFilterButton) &&
				Objects.equals(dotsInRows, that.dotsInRows) &&
				Objects.equals(displayDocsColumn, that.displayDocsColumn);
	}

	@Override
	public int hashCode() {
		return Objects.hash(serialNumber, tableName, searchOn, textInSearch, initialOrder, rowOpensItemId, columnManagementButton, advancedFilterButton, dotsInRows, displayDocsColumn);
	}
}
