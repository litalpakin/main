package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

public class UserWithPasswordDto extends UsernameDto {

	@NotNull
    private String password;

    public UserWithPasswordDto() {}

    public UserWithPasswordDto(String username, String password) {
        super(username);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
