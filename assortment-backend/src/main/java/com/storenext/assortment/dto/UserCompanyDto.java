package com.storenext.assortment.dto;

public class UserCompanyDto extends UserDto{
	CompanyDto company;

	public CompanyDto getCompany() {
		return company;
	}

	public void setCompany(CompanyDto company) {
		this.company = company;
	}
	
}
