package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class GraphDataLineDto {

	String label;
	List<Double> data = new ArrayList<>();
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<Double> getData() {
		return data;
	}
	public void setData(List<Double> data) {
		this.data = data;
	}

	
}
