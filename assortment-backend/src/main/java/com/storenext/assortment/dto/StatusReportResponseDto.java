package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class StatusReportResponseDto {

	List<StatusReportTabDto> tabs = new ArrayList<>();

	public List<StatusReportTabDto> getTabs() {
		return tabs;
	}

	public void setTabs(List<StatusReportTabDto> tabs) {
		this.tabs = tabs;
	}

	
}
