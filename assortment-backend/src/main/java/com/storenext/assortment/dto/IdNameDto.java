package com.storenext.assortment.dto;

public class IdNameDto {
	
	Object id;
	String name;
	String code;
	
	public IdNameDto(Object id, String name, String code) {
		this.id = id;
		this.name = name;
		this.code = code;
	}

	public IdNameDto() {
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
