package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.model.CompanyType;

public class CompanyDto {

	@NotNull
	private String name;
	@NotNull
	private Boolean enabled;
	
	private Long id;
	
	private String logoUrl;
	
	private CompanyType type;

	public CompanyType getType() {
		return type;
	}
	public void setType(CompanyType type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

}
