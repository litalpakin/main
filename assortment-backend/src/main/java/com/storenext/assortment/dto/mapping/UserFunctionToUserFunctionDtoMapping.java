package com.storenext.assortment.dto.mapping;

import org.modelmapper.AbstractConverter;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.UserFunctionDto;
import com.storenext.assortment.model.UserFunction;
import com.storenext.assortment.service.LanguageService;

public class UserFunctionToUserFunctionDtoMapping extends PropertyMap<UserFunction, UserFunctionDto> {
	private LanguageService languageService;

	public UserFunctionToUserFunctionDtoMapping() {
	}

	public UserFunctionToUserFunctionDtoMapping(LanguageService languageService) {
		this.languageService = languageService;
	}

	@Override
	protected void configure() {
		using(new AbstractConverter<UserFunction, String>(){
			@Override
			protected String convert(UserFunction source) {
				String name;
				if (source.getType()==null) {
					name = source.getName();
				} else {
					name = source.getType().getName(languageService.getDefaultLanguage());
				}

				destination.setName(name);
				return name;
			}
		}).map(source).setName(null);
	}
}
