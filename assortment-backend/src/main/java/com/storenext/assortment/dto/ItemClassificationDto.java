package com.storenext.assortment.dto;

import com.storenext.assortment.enums.TrafficLightsColor;

public class ItemClassificationDto extends ItemDetailsRequestDto{

	private TrafficLightsColor itemClassifiction;

	public TrafficLightsColor getItemClassifiction() {
		return itemClassifiction;
	}

	public void setItemClassifiction(TrafficLightsColor itemClassifiction) {
		this.itemClassifiction = itemClassifiction;
	}
}
