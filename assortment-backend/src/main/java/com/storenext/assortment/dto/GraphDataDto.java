package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class GraphDataDto {

	private List<GraphDataLineDto> graph = new ArrayList<>();
	private List<String> labels = new ArrayList<>();
	
	public List<GraphDataLineDto> getGraph() {
		return graph;
	}
	public void setGraph(List<GraphDataLineDto> graph) {
		this.graph = graph;
	}
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

}
