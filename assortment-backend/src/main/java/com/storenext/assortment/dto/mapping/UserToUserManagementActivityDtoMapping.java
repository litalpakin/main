package com.storenext.assortment.dto.mapping;

import java.time.Instant;
import java.util.Date;

import org.modelmapper.AbstractConverter;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.UserManagementActivityDto;
import com.storenext.assortment.model.security.User;

public class UserToUserManagementActivityDtoMapping extends PropertyMap<User, UserManagementActivityDto> {
	@Override
	protected void configure() {
		using(new AbstractConverter<Long, Date>() {
			@Override
			protected Date convert(Long source) {
				if (source == null) {
					return null;
				}

				return Date.from(Instant.ofEpochMilli(source));
			}
		}).map(source.getLastRequest()).setLastRequestDate(null);
	}
}
