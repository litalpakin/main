package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class DeepDiveTab {

	String reportType;
	String name;
	List<List<Object>> rows = new ArrayList<>(); 
	List<String> columnNames = new ArrayList<>();
	
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public List<List<Object>> getRows() {
		return rows;
	}
	public void setRows(List<List<Object>> rows) {
		this.rows = rows;
	}
	public List<String> getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
