package com.storenext.assortment.dto;

import com.storenext.assortment.enums.TrafficLightsColor;

public class TableDataCell {


	private Object value;
	private TrafficLightsColor trafficLightsColor;
	
	public TableDataCell(){
		
	}
	
	public TableDataCell(Object value, TrafficLightsColor trafficLightsColor) {
		this.value = value;
		this.trafficLightsColor = trafficLightsColor;
	}
	
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public TrafficLightsColor getTrafficLightsColor() {
		return trafficLightsColor;
	}
	public void setTrafficLightsColor(TrafficLightsColor trafficLightsColor) {
		this.trafficLightsColor = trafficLightsColor;
	}
}
