package com.storenext.assortment.dto.mapping;

import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.AbstractConverter;
import org.modelmapper.PropertyMap;

import com.storenext.assortment.dto.StoreDto;
import com.storenext.assortment.dto.StoreGroupDto;
import com.storenext.assortment.model.StoreGroup;
import com.storenext.assortment.service.StoreService;

public class StoreGroupToStoreGroupDtoMapping extends PropertyMap<StoreGroup,StoreGroupDto>{

	StoreService storeService;
	
	public StoreGroupToStoreGroupDtoMapping(StoreService storeService){
		this.storeService = storeService;
	}
	@Override
	protected void configure() {
		using(new AbstractConverter<Set<Long>, Set<StoreDto>>(){

			@Override
			protected Set<StoreDto> convert(Set<Long> source) {
				return storeService.getAll().stream().filter(sg -> source.contains(sg.getId())).map(sg -> new StoreDto(sg.getId(),sg.getName())).collect(Collectors.toSet());
			}
		}).map(source.getStores()).setStores(null);
		
	}

}
