package com.storenext.assortment.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class PasswordSetRequest {
	
	@NotEmpty
	private String token;
	
	@NotEmpty
	private String password;

	public PasswordSetRequest() {
	}

	public PasswordSetRequest(String token, String password) {
		this.token = token;
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "PasswordSetRequest [token=" + token + ", password=" + password + "]";
	}
	
	

}
