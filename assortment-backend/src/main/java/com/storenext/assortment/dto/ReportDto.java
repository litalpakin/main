package com.storenext.assortment.dto;

import com.storenext.assortment.model.ReportType;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class ReportDto {
	@NotNull
	private String name;
	private String description;
	@NotNull
	private ReportType reportType;
	private List<UserFunctionDto> userFunctions = new ArrayList<>();
	
	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UserFunctionDto> getUserFunctions() {
		return userFunctions;
	}

	public void setUserFunctions(List<UserFunctionDto> userFunctions) {
		this.userFunctions = userFunctions;
	}
}
