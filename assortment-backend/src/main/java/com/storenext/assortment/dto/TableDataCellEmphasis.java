package com.storenext.assortment.dto;

public class TableDataCellEmphasis extends TableDataCell {
	private Boolean emphasis;

	public TableDataCellEmphasis() {
	}

	public TableDataCellEmphasis(Boolean emphasis) {
		this.emphasis = emphasis != null && emphasis;
	}

	public Boolean isEmphasis() {
		return emphasis;
	}

	public void setEmphasis(Boolean emphasis) {
		this.emphasis = emphasis;
	}
}
