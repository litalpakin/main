package com.storenext.assortment.dto;

import java.util.Date;

import javax.validation.constraints.Size;

import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.Gender;
import com.storenext.assortment.model.security.Authority;

public class UserDto extends UsernameDto{

	private Long id;
	@Size(min = 2, max = 50, message = "First Name should be between 2 and 50 characters.")
	private String firstName;

	@Size(min = 2, max = 50, message = "Last Name should be between 2 and 50 characters.")
	private String lastName;


	private Date dateOfBirth;

	private Gender gender;

	private Date lastPasswordResetDate;

	private Authority authority;

	private Long companyId;

	private boolean enabled;

	private UserFunctionDto function;

	private StoreGroupDto storeGroup;
	private Date lastRequestDate;
	
	private Language language;
	
	public UserDto() {
	}

	public UserDto(String firstName, String lastName, String username) {
		super(username);
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public UserDto(String firstName, String lastName, String username, Date dob, Gender sex,
			Date lastPasswordResetDate, Authority authority, Long companyId, boolean enabled,
			UserFunctionDto function, StoreGroupDto shopGroup) {
		this(firstName,lastName, username);
		this.dateOfBirth = dob;
		this.gender = sex;
		this.lastPasswordResetDate = lastPasswordResetDate;
		this.authority = authority;
		this.companyId = companyId;
		this.enabled = enabled;
		this.function = function;
		this.storeGroup = shopGroup;
	}

	public StoreGroupDto getStoreGroup() {
		return storeGroup;
	}

	public void setStoreGroup(StoreGroupDto storeGroup) {
		this.storeGroup = storeGroup;
	}

	public UserFunctionDto getFunction() {
		return function;
	}

	public void setFunction(UserFunctionDto function) {
		this.function = function;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastPasswordResetDate() {
		return lastPasswordResetDate;
	}

	public void setLastPasswordResetDate(Date lastPasswordResetDate) {
		this.lastPasswordResetDate = lastPasswordResetDate;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}


	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Date getLastRequestDate() {
		return lastRequestDate;
	}

	public void setLastRequestDate(Date lastRequestDate) {
		this.lastRequestDate = lastRequestDate;
	}
}
