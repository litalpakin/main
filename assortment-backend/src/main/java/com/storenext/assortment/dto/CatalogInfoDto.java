package com.storenext.assortment.dto;

import java.util.Map;

import com.storenext.assortment.enums.CatalogType;

public class CatalogInfoDto {
	
	private String name;
	private Map<String,String> catalogHierarchy;
	private CatalogType catalogType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String,String> getCatalogHierarchy() {
		return catalogHierarchy;
	}
	public void setCatalogHierarchy(Map<String,String> catalogHierarchy) {
		this.catalogHierarchy = catalogHierarchy;
	}
	public CatalogType getCatalogType() {
		return catalogType;
	}
	public void setCatalogType(CatalogType catalogType) {
		this.catalogType = catalogType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((catalogHierarchy == null) ? 0 : catalogHierarchy.hashCode());
		result = prime * result + ((catalogType == null) ? 0 : catalogType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatalogInfoDto other = (CatalogInfoDto) obj;
		if (catalogHierarchy == null) {
			if (other.catalogHierarchy != null)
				return false;
		} else if (!catalogHierarchy.equals(other.catalogHierarchy))
			return false;
		if (catalogType != other.catalogType)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
