package com.storenext.assortment.dto.mapping;

import java.util.function.Function;

import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;

import com.storenext.assortment.dto.CompanyDto;
import com.storenext.assortment.model.Company;

public class ComapanyIdToComapnyDtoConverter  extends AbstractConverter<Long,CompanyDto>{

    Function<Long,Company> getCompanyByIdFunction;
    ModelMapper modelMapper;
     
	public ComapanyIdToComapnyDtoConverter(Function<Long,Company> getCompanyByIdFunction,ModelMapper modelMapper){
		this.getCompanyByIdFunction = getCompanyByIdFunction;
		this.modelMapper = modelMapper;
	}

	@Override
	protected CompanyDto convert(Long source) {
		if(source == null){
			return null;
		}
		Company company = getCompanyByIdFunction.apply(source);
		if(company == null){
			return null;
		}
		return modelMapper.map(company, CompanyDto.class);
	}

	

	
	
}
