package com.storenext.assortment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.storenext.assortment.rest.RestExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResponseErrorDto {

	private List<String> errors;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String failureCount;

	private String checkSql(String errorMessage){
		if(errorMessage != null && errorMessage.toLowerCase().contains("select") && 
				errorMessage.toLowerCase().contains("from")){
			return RestExceptionHandler.INTERNAL_SERVER_ERROR;
		}else{
			return errorMessage;
		}
	}
	public ResponseErrorDto() {
	}

	public ResponseErrorDto(String message) {
		setError(message);
	}

	public ResponseErrorDto(String message, String failureCount) {
		setError(message);
		setFailureCount(failureCount);
	}
	
	public void setError(String error) {
		errors = Arrays.asList(checkSql(error));
	}
	public void setErrors(List<String> errors) {
		List<String> checkedErrors = new ArrayList<>();
		if(errors != null){
			errors.stream().forEach(err->checkedErrors.add(checkSql(err)));
		}
		this.errors = checkedErrors;
	}
	public List<String> getErrors() {
		return errors;
	}

	public String getFailureCount() {
		return failureCount;
	}

	public void setFailureCount(String failureCount) {
		this.failureCount = failureCount;
	}
}
