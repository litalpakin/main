package com.storenext.assortment.dto;

import com.storenext.assortment.enums.CatalogType;

public class DroppedHierarchsDto {
	private CatalogType catalogType;
	private String catalogLevel;
	private Long code;
	private String name;
	
	public DroppedHierarchsDto() {
	}
	
	public DroppedHierarchsDto(CatalogType catalogType, String catalogLevel, Long code, String name) {
		this.catalogType = catalogType;
		this.catalogLevel = catalogLevel;
		this.code = code;
		this.name = name;
	}
	
	public CatalogType getCatalogType() {
		return catalogType;
	}
	
	public void setCatalogType(CatalogType catalogType) {
		this.catalogType = catalogType;
	}
	
	public String getCatalogLevel() {
		return catalogLevel;
	}
	
	public void setCatalogLevel(String catalogLevel) {
		this.catalogLevel = catalogLevel;
	}
	
	public Long getCode() {
		return code;
	}
	
	public void setCode(Long code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
