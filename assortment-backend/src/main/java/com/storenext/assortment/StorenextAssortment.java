package com.storenext.assortment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StorenextAssortment {
	
	public static void main(String[] args) {
		SpringApplication.run(StorenextAssortment.class, args);
	}

}
