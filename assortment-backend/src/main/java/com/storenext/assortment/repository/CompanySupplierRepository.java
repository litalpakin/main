package com.storenext.assortment.repository;

import com.storenext.assortment.model.CompanySupplier;
import com.storenext.assortment.model.composite.CompanySupplierId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompanySupplierRepository extends JpaRepository<CompanySupplier, CompanySupplierId> {
    List<CompanySupplier> findByCompanyId(Long companyId);
}
