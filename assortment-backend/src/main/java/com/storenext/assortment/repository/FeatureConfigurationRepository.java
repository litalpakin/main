package com.storenext.assortment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.model.FeatureConfiguration;

public interface FeatureConfigurationRepository extends JpaRepository<FeatureConfiguration, Long> {
	@Modifying
	@Transactional
	@Query("update FeatureConfiguration fc set fc.isEnabled = :isEnabled where  fc.feature = :feature")
	void updateFeatureConfiguration(@Param("isEnabled") boolean isEnabled,@Param("feature") Feature feature);
}
