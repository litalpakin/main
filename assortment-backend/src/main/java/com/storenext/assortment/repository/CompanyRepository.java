package com.storenext.assortment.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;

public interface CompanyRepository extends JpaRepository<Company, Long>{
	   Company findByName(String name);
	   
	   Set<Company> findByTypeIn(List<CompanyType> type);
}
