package com.storenext.assortment.repository;

import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.UserFunction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface UserFunctionRepository extends JpaRepository<UserFunction, Long> {
	@Query("select uf from UserFunction uf where uf.companyType = 'ADMIN' and uf.isDefault = true")
	UserFunction getDefaultAdminFunction();
	
	@Query("select uf from UserUserFunction uf where uf.id in (:ids)")
	Set<UserFunction> getUserFunctionsByIds(@Param("ids") Set<Long> ids);

	List<UserFunction> findByCompanyType(CompanyType companyType);
	
	@Modifying
	@Query("update UserUserFunction set id.userFunctionId = :subId where id.userFunctionId = :id")
	void updateFunctionId(@Param("id") Long id, @Param("subId") Long subId);
}
