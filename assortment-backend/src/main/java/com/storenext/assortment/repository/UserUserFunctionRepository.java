package com.storenext.assortment.repository;

import com.storenext.assortment.model.UserUserFunction;
import com.storenext.assortment.model.composite.UserUserFunctionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface UserUserFunctionRepository extends JpaRepository<UserUserFunction, UserUserFunctionId> {

	@Modifying
	@Query("update UserUserFunction set id.userFunctionId = :subId where id.userFunctionId = :id")
	void updateFunctionId(@Param("id") Long id, @Param("subId") Long subId);
	@Query("select uuf.id.userFunctionId from UserUserFunction uuf where uuf.id.userId = :userId")
	List<Long> findByUserId(@Param("userId") Long userId);
	
	@Query("select uuf from UserUserFunction uuf where uuf.userFunction.id = :userFunctionId")
	Set<UserUserFunction> findByUserFunctionId(@Param("userFunctionId") Long userFunctionId);
	
}
