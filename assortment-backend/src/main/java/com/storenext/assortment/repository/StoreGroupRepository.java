package com.storenext.assortment.repository;

import com.storenext.assortment.model.StoreGroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreGroupRepository extends JpaRepository<StoreGroup, Long>{
	
	StoreGroup findByName(String name);
}
