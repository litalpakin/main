package com.storenext.assortment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.storenext.assortment.model.UserSupplierSubCategory;
import com.storenext.assortment.model.composite.UserSupplierSubCategoryId;

public interface UserSupplierSubCategoryRepository extends JpaRepository<UserSupplierSubCategory, UserSupplierSubCategoryId> {

	@Modifying
	@Query("DELETE FROM UserSupplierSubCategory ussc WHERE ussc.id.supplierId = :supplierId AND ussc.id.userId in :userIds")
	void deleteUserSupplierLinks(@Param("supplierId") Long supplierId, @Param("userIds") List<Long> userIds);
}
