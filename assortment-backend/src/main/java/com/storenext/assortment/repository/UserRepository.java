package com.storenext.assortment.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.StoreGroup;
import com.storenext.assortment.model.security.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String user);

	Optional<User> findByEmailValidationToken(String token);

	@Query("select u from User u left outer join fetch u.function left outer join fetch u.supplierSubCategories left outer join fetch u.storeGroup where u.id = :userId")
	User findById(@Param("userId") Long userId);

	List<User> findByCompanyId(Long companyId);

	@Query("select u from User u left outer join fetch u.function left outer join fetch u.supplierSubCategories left outer join fetch u.storeGroup where u.companyId in (:companyIds)")
	Set<User> findByCompanyIdIn(@Param("companyIds") Set<Long> companyIds);

	@Query("select u from User u left outer join fetch u.function left outer join fetch u.supplierSubCategories left outer join fetch u.storeGroup")
	Set<User> getAll();

	@Modifying
	@Query("update User u set u.storeGroup = :destinationStoreGroup where u.storeGroup = :sourceStoreGroup")
	void replaceStoreGroup(@Param("sourceStoreGroup") StoreGroup sourceStoreGroup, @Param("destinationStoreGroup") StoreGroup destinationStoreGroup);

	@Modifying
	@Query("update User u set u.storeGroup = null where u.storeGroup.id = :sourceStoreGroupId")
	@Transactional
	void deleteStoreGroup(@Param("sourceStoreGroupId") Long sourceStoreGroupId);

	@Modifying
	@Query("update User u set u.tokenCreated = :tokenCreated, u.lastRequest = :tokenCreated where u.username = :username")
	void updateTokenCreated(@Param("tokenCreated") Long tokenCreated, @Param("username") String username);

	@Modifying
	@Transactional
	@Query("update User u set u.lastRequest = :lastRequest where u.id = :userId")
	void updateLastRequest(@Param("lastRequest") Long lastRequest, @Param("userId") Long userId);

	@Modifying
	@Transactional
	@Query("update User u set u.storeId = :storeId, u.storeGroup = null where u.id = :userId")
	void updateStoreId(@Param("storeId") Long storeId, @Param("userId") Long userId);

	@Query("select u from User u where u.function.userFunction.allStoresSupport = :allStoresSupport and u.function.userFunction.areaManager = :areaManager " +
			"and u.function.userFunction.companyType = :companyType and u.function.userFunction.fullCatalogSupport = :fullCatalogSupport " +
			"and u.function.userFunction.itemClassificator = :itemClassificator ")
	Set<User> findUsersByUserFunctionProperties(@Param("allStoresSupport") boolean allStoresSupport, @Param("areaManager") boolean areaManager,
												@Param("companyType") CompanyType companyType, @Param("fullCatalogSupport") boolean fullCatalogSupport,
												@Param("itemClassificator") boolean itemClassificator);

	@Query("select u from User u where u.username in (:userNames) ")
	Set<User> findUsersByUserNames(@Param("userNames") Set<String> usernames);
}