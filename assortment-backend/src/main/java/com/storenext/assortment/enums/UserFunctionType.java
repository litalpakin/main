package com.storenext.assortment.enums;

public enum UserFunctionType {
	HEADQUARTERS_MANAGER("Headquarters Manager", "מנהל מטה"),
	CATALOG_MANAGER("Catalog Manager", "מנהל קטגוריה"),
	REGION_MANAGER("Region Manager", "מנהל אזור"),
	STORE_MANAGER("Store Manager", "מנהל סניף"),
	CATALOG_REGION_MANAGER("Catalog Region Manager", "מנהל קטלוג-אזור");
	
	private String enName;
	private String heName;
	
	UserFunctionType(String enName, String heName) {
		this.enName = enName;
		this.heName = heName;
	}
	
	public String getName(Language language) {
		return language == null || Language.HEBREW.equals(language) ? heName : enName;
	}
}
