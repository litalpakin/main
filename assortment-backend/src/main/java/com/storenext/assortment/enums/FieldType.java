package com.storenext.assortment.enums;

public enum FieldType {
	INTEGER,
	PERCENT,
	MONEY,
	TEXT,
	TEXT_BY_NUMBER,
	QTY
}
