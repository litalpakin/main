package com.storenext.assortment.enums;

import java.util.Arrays;

import com.storenext.assortment.dto.reports.UserReportType;

public enum ClassificationGroupFilter {
	UNDEFINED(400, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE, UserReportType.ITEM_ID, UserReportType.ITEM_TABLE_PAGE, UserReportType.ITEM_IN_STORES_REPORT_PAGE}),
	STAR_LAUNCH_PERIOD(406, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE}),
	INTERMEDIATE_LAUNCH_PERIOD(407, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE}),
	INSIGNIFICANT_LAUNCH_PERIOD(408, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE}),
	OCCASION_ITEMS(424, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE, UserReportType.ITEM_ID, UserReportType.ITEM_TABLE_PAGE, UserReportType.ITEM_IN_STORES_REPORT_PAGE}),
	NON_SELL_ITEMS(425, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE, UserReportType.ITEM_ID, UserReportType.ITEM_TABLE_PAGE, UserReportType.ITEM_IN_STORES_REPORT_PAGE}),
	NEWBORN_ITEMS(426, new UserReportType[]{UserReportType.ALTERNATIVE_COST_PAGE, UserReportType.ITEM_ID, UserReportType.ITEM_TABLE_PAGE, UserReportType.ITEM_IN_STORES_REPORT_PAGE});

	private int statusCode;
	private UserReportType[] userReportTypes;

	ClassificationGroupFilter(int statusCode, UserReportType[] userReportTypes) {
		this.statusCode = statusCode;
		this.userReportTypes = userReportTypes;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public UserReportType[] getUserReportTypes() {
		return userReportTypes;
	}

	public static ClassificationGroupFilter valueOf(ClassificationGroup classificationGroup, UserReportType userReportType) {
		return Arrays.stream(ClassificationGroupFilter.values()).filter(f -> f.name().equals(classificationGroup.name())).filter(f -> containsUserReportType(f, userReportType))
				.findFirst().orElse(null);
	}
	
	private static boolean containsUserReportType(ClassificationGroupFilter classificationGroupFilter, UserReportType userReportType) {
		for (UserReportType urt : classificationGroupFilter.getUserReportTypes()) {
			if (urt.equals(userReportType)) {
				return true;
			}
		}
		return false;
	}
}
