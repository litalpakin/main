package com.storenext.assortment.enums;

public enum CalculationType {
	SUM,
	MAX
}
