package com.storenext.assortment.enums;

public enum ShelfType {
	THREE_TIMES_A_DAY(18.),
	TWICE_A_DAY(12.),
	ONCE_A_DAY(6.),
	EVERY_TWO_DAYS(3.),
	EVERY_THREE_DAYS(2),
	EVERY_FOUR_DAYS(1.5),
	EVERY_FIVE_DAYS(1.2),
	ONCE_A_WEEK(1.),
	ONCE_A_TEN_DAYS(0.75),
	ONCE_IN_TWO_WEEKS(0.5);

	private double value;

	ShelfType(double value) {
		this.value = value;
	}

	public String getValue() {
		return Double.toString(value);
	}
}
