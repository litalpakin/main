package com.storenext.assortment.enums;

import java.util.Locale;

import org.springframework.util.StringUtils;

public enum Language {
	ENGLISH(Locale.ENGLISH),HEBREW(StringUtils.parseLocaleString("iw"));

	Locale locale;
	
	Language(Locale locale){
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
