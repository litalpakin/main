package com.storenext.assortment.enums;

public enum ClassificationGroupType {
	SEASONAL,REGULAR,BABY_ITEMS,TEENAGER_ITEMS;
}
