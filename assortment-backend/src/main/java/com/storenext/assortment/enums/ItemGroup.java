package com.storenext.assortment.enums;

public enum ItemGroup {
	OLD(2),NEW(1);
	
	int classification;
	
	public int getClassification() {
		return classification;
	}
	private ItemGroup(int classification) {
		this.classification = classification;
	}
	public static ItemGroup getByClassification(Integer classification) {
		if(classification == null){
			return null;
		}
		for(ItemGroup value:ItemGroup.values()){
			if(value.getClassification() == classification.intValue()){
				return value;
			}
		}
		return null;
	}
	
}
