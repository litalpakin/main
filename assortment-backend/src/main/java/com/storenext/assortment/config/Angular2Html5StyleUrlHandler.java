package com.storenext.assortment.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Angular2Html5StyleUrlHandler {

    @RequestMapping({"/","/login/**", "/onboard"})
    public String index() {
        return "forward:/index.html";
    }
}
