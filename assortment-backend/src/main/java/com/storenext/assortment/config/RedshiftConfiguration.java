package com.storenext.assortment.config;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration public class RedshiftConfiguration {

	public static final String FAST_TEMPLATE_POOL = "fastJDBCTemplatePool";
	public static final String FAST_TEMPLATE_NAME = "fastJDBCTemplate";
	public static final String FAST_DATASOURCE_NAME = "fastDatasource";
	
	@Bean(FAST_DATASOURCE_NAME)
	@ConfigurationProperties(prefix = "fast.datasource")
	public DataSource posgreDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(FAST_TEMPLATE_NAME)
	public NamedParameterJdbcOperations posgreTemplate(@Qualifier(FAST_DATASOURCE_NAME) DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean(FAST_TEMPLATE_POOL)
	public List<NamedParameterJdbcOperations> posgreTemplatePool(@Qualifier(FAST_DATASOURCE_NAME) DataSource dataSource) {
		List<NamedParameterJdbcOperations> response = new ArrayList<>();
		for(int i = 0; i < 10; i++){
			response.add(new NamedParameterJdbcTemplate(dataSource));
		}
		
		return response;
	}
}
