package com.storenext.assortment.config;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.storenext.assortment.security.JwtAuthenticationEntryPoint;
import com.storenext.assortment.security.JwtAuthenticationTokenFilter;
import com.storenext.assortment.security.StorenextAuthenticationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableScheduling
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	
	@Inject
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder, StorenextAuthenticationProvider authProvider) throws Exception {
		authenticationManagerBuilder.authenticationProvider(authProvider);
    }
    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        return new JwtAuthenticationTokenFilter();
    }
    

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {


        httpSecurity
                // we don't need CSRF because our token is invulnerable
                .csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers(
                        HttpMethod.GET,
                        "/",
                        "/assets/**",
                        "/health",
                        "/favicon.ico",
                        "/*.html",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.png",
                        "/**/*.ttf",
                        "/**/*.woff",
                        "/**/*.eot",
                        "/**/*.woff2",
                        "/**/*.svg",
                        "/**/*.gif",                  
                        "/**/*.js",
                        "/images/**",
                        "/fonts/**",
                        "/swagger-resources/**",
                        "/webjars/springfox-swagger-ui/**",
                        "/v2/api-docs",
                        "/configuration/**",
                        "/health",
                        "/login/**"
                        
                ).permitAll().antMatchers(HttpMethod.OPTIONS,
                        "/**"
                ).permitAll()
                .antMatchers("/rest/api/v1/auth/**").permitAll()
                .antMatchers("/rest/api/v1/query/ping/**").permitAll()
                .antMatchers(HttpMethod.POST,"/rest/api/v1/user/create-password").permitAll()
                .antMatchers(HttpMethod.GET,"/rest/api/v1/user/create-password/token").permitAll()
                .antMatchers(HttpMethod.POST,"/rest/api/v1/user/create-password/info").permitAll() 
                .antMatchers(HttpMethod.POST,"/rest/api/v1/user/reset-password").permitAll()
                .antMatchers(HttpMethod.GET,"/rest/api/v1/user/reset-password/token").permitAll()
				.antMatchers(HttpMethod.GET,"/rest/api/v1/user/message").permitAll()
                .antMatchers(HttpMethod.GET,"/rest/api/v1/enviroment/defaultLanguage").permitAll()
                .antMatchers(HttpMethod.GET,"/rest/api/v1/enviroment/underConstruction").permitAll()
                .antMatchers(HttpMethod.GET,"/rest/api/v1/enviroment/supportedLanguages").permitAll().anyRequest().authenticated();
     
     
       
      
        // Custom JWT based security filter
        httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

        // disable page caching
        httpSecurity.headers().cacheControl().disable();
    }

}
