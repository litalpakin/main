package com.storenext.assortment.config;

import javax.servlet.Filter;
import javax.sql.DataSource;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.ziplet.filter.compression.CompressingFilter;
import com.storenext.assortment.dto.UserDto;
import com.storenext.assortment.dto.mapping.StoreGroupDtoToStoreGroupMapping;
import com.storenext.assortment.dto.mapping.StoreGroupToStoreGroupDtoMapping;
import com.storenext.assortment.dto.mapping.UserFunctionToUserFunctionDtoMapping;
import com.storenext.assortment.dto.mapping.UserToUserCompanyDtoMapping;
import com.storenext.assortment.dto.mapping.UserToUserDtoMapping;
import com.storenext.assortment.dto.mapping.UserToUserManagementActivityDtoMapping;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.CompanyService;
import com.storenext.assortment.service.LanguageService;
import com.storenext.assortment.service.StoreService;

@Configuration
@EnableAsync
@EnableCaching
@EnableAutoConfiguration
public class BeanConfiguration {

    @Autowired
    private CompanyService companyService;

    @Value("${cloudinary.cloud-name}")
    private String cloudinaryCloudName;

    @Value("${cloudinary.key}")
    private String cloudinaryKey;

    @Value("${cloudinary.secret}")
    private String cloudinarySecret;

    @Primary
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper modelMapper = new ObjectMapper();
        modelMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return modelMapper;
    }

    public Company getCompanyById(Long id) {
        return companyService.getCompany(id);
    }
    @Bean
    public Filter compressingFilter() {
        CompressingFilter compressingFilter = new CompressingFilter();
        return compressingFilter;
    }

    @Bean
    public ModelMapper modelMapper(StoreService storeService, LanguageService languageService) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.addMappings(new UserToUserDtoMapping(languageService));
        modelMapper.addMappings(new UserToUserCompanyDtoMapping(this::getCompanyById, modelMapper)).includeBase(User.class, UserDto.class);
        modelMapper.addMappings(new StoreGroupDtoToStoreGroupMapping());
        modelMapper.addMappings(new StoreGroupToStoreGroupDtoMapping(storeService));
        modelMapper.addMappings(new UserFunctionToUserFunctionDtoMapping(languageService));
        modelMapper.addMappings(new UserToUserManagementActivityDtoMapping());

        return modelMapper;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Primary
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

    @Bean
    public Cloudinary cloudinary() throws Exception {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudinaryCloudName,
                "api_key", cloudinaryKey,
                "api_secret", cloudinarySecret));
        return cloudinary;
    }
    
}
