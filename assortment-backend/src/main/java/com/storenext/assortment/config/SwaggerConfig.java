package com.storenext.assortment.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {        
	
	@Value("${storenext.version}")
	private String backendVersion;
	
	@Value("${storenext.base.url}")
	private String serverUrl;
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)
          .ignoredParameterTypes(ApiIgnore.class)
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.storenext.assortment.rest"))
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(new ApiInfo("Storenext portal server REST API","", backendVersion,"Terms of service",new Contact("Storenext", "", "admin@storenext.com"), "Storenext", serverUrl))
          ;           
    }

    @Bean
    public SecurityConfiguration security() {
        return new springfox.documentation.swagger.web.SecurityConfiguration(
                null, null, null,
                null,
                null,
                ApiKeyVehicle.HEADER,
                "Authorization",
                null);
    }

}