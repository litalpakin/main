package com.storenext.assortment.data;

public class SummaryDataColumn {
	private String columnName;
	private String summaryColumnName;
	private int columnIndex = -1;
	private int summaryColumnIndex = -1;
	private Object value;
	
	public SummaryDataColumn(String columnName,String summaryColumnName){
		this.columnName = columnName;
		this.summaryColumnName = summaryColumnName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public int getColumnIndex() {
		return columnIndex;
	}
	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getSummaryColumnName() {
		return summaryColumnName;
	}
	public void setSummaryColumnName(String summaryColumnName) {
		this.summaryColumnName = summaryColumnName;
	}
	public int getSummaryColumnIndex() {
		return summaryColumnIndex;
	}
	public void setSummaryColumnIndex(int summaryColumnIndex) {
		this.summaryColumnIndex = summaryColumnIndex;
	}
}
