package com.storenext.assortment.data;

public class ColumnSort {
	private String columnName;
	private int sortFactor;
	private boolean isNullLast = false;
	
	public ColumnSort(String columnName, final int sortFactor,boolean isNullLast) {
		this.columnName = columnName;
		this.sortFactor = sortFactor;
		if(!isNullLast){
			this.sortFactor = sortFactor*2;
		}
		this.isNullLast = isNullLast;
	}

	public ColumnSort(String columnName, final int sortFactor) {
		this.columnName = columnName;
		this.sortFactor = sortFactor;
	}

	public String getColumnName() {
		return columnName;
	}

	public int getSortFactor() {
		return sortFactor;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public boolean isNullLast() {
		return isNullLast;
	}
}
