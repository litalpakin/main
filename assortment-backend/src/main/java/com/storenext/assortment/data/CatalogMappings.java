package com.storenext.assortment.data;

import java.util.HashMap;
import java.util.Map;

import com.storenext.assortment.dto.IdNameDto;

public class CatalogMappings {
	Map<Long,String> classes = new HashMap<>();
	Map<Long,String> categories = new HashMap<>();
	Map<Long,String> subCategories = new HashMap<>();
	Map<Long,String> suppliers = new HashMap<>();
	Map<Long,String> brands = new HashMap<>();
	Map<String,String> items = new HashMap<>();
	Map<Long,IdNameDto> itemEntities = new HashMap<>();
	
	
	public Map<Long, String> getClasses() {
		return classes;
	}

	public Map<Long, String> getCategories() {
		return categories;
	}

	public Map<Long, IdNameDto> getItemEntities() {
		return itemEntities;
	}

	public Map<Long, String> getSubCategories() {
		return subCategories;
	}

	public Map<Long, String> getSuppliers() {
		return suppliers;
	}

	public Map<Long, String> getBrands() {
		return brands;
	}

	public Map<String, String> getItems() {
		return items;
	}


	
}
