package com.storenext.assortment.exception;

public class UserAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = -3057482175291875263L;

	public UserAlreadyExistsException() {
        super("User already exists in the system.");
    }
}
