package com.storenext.assortment.exception;

public class ArgumentValidationException extends RuntimeException {
	
    private static final long serialVersionUID = -5807240081670917953L;

	public ArgumentValidationException(String message) {
		super(message);
    }	
}
