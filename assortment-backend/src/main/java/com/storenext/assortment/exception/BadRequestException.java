package com.storenext.assortment.exception;

public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = -7036648966731639433L;
	
	public BadRequestException() {
        super("Bad request");
    }
	public BadRequestException(String message) {
        super(message);
    }
}
