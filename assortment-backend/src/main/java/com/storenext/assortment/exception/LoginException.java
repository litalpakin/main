package com.storenext.assortment.exception;

public class LoginException extends RuntimeException {
	private static final long serialVersionUID = -3057482175291875263L;
	
	public LoginException(String message) {
	    super(message);
	}

}
