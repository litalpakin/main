package com.storenext.assortment.exception;

public class GeneralException extends RuntimeException {

	private static final long serialVersionUID = -3057482175291875263L;

	public GeneralException() {
        super("General exception");
    }
	
	public GeneralException(String message) {
		super(message);
	}
}
