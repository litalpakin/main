package com.storenext.assortment.exception;

public class InsufficientDataException extends BadRequestException{


	/**
	 * 
	 */
	private static final long serialVersionUID = 2462532106398197368L;

	public InsufficientDataException(String message) {
        super(message);
    }
	
}
