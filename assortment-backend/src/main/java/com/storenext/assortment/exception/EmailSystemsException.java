package com.storenext.assortment.exception;

public class EmailSystemsException extends RuntimeException {
	public EmailSystemsException(String message) {
		super(message);
	}

	public EmailSystemsException(String message, Exception e) {
		super(message, e);
	}
}
