package com.storenext.assortment.exception;

public class CreatePasswordInvalidException extends RuntimeException {

	private static final long serialVersionUID = 2748967096897908896L;

	public CreatePasswordInvalidException(String message) {
        super(message);
    }
}