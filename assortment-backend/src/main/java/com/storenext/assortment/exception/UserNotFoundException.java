package com.storenext.assortment.exception;

public class UserNotFoundException extends BadRequestException{

	private static final long serialVersionUID = 8221187801424672658L;

	public UserNotFoundException() {
        super("User not found.");
    }
	
}
