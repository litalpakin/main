package com.storenext.assortment.exception;

public class ErrorCodeException extends RuntimeException{
	
	

	private int errorCode;

	public ErrorCodeException(int errorCode,String errorMessge) {
		super(errorMessge);
		this.errorCode = errorCode;
	}
	
	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	

}
