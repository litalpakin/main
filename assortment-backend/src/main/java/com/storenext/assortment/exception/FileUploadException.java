package com.storenext.assortment.exception;

public class FileUploadException extends RuntimeException {
	
	private static final long serialVersionUID = 435114975892576291L;

	public FileUploadException() {
		super("Error uploading file");
    }	
}
