package com.storenext.assortment.rest;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.storenext.assortment.dto.UserManagementActivityDto;
import com.storenext.assortment.service.UserService;

@RestController
@RequestMapping("/rest/api/v1/user/management")
public class UserManagementController {
	@Autowired
	private UserService userService;
	@Autowired
	private ModelMapper modelMapper;

	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("hasAnyAuthority('ADMIN')")
	public List<UserManagementActivityDto> getUserManagement() {
		return userService.getAllUsers().stream().map(u -> modelMapper.map(u, UserManagementActivityDto.class)).collect(Collectors.toList());
	}
}
