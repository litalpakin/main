package com.storenext.assortment.rest;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.storenext.assortment.dto.UserFunctionDto;
import com.storenext.assortment.service.UserFunctionService;
import com.storenext.assortment.service.UserService;

@RestController
@RequestMapping(UserFunctionController.URL)
public class UserFunctionController {
	public static final String URL = "/rest/api/v1/user_function";
	@Autowired
	private UserFunctionService userFunctionService;
	@Autowired
	private UserService userService;
	@Autowired
	private ModelMapper modelMapper;

	@RequestMapping(method = RequestMethod.GET)
	@PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAdminPermissions()")
	public UserFunctionDto[] getFunctions() {
		return modelMapper.map(userFunctionService.getAll(userService.getCurrentUser()), UserFunctionDto[].class);
	}

	@RequestMapping(method = RequestMethod.POST)
	@PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAdminPermissions()")
	public UserFunctionDto addFunction(@RequestBody @Valid UserFunctionDto userFunctionDto) {
		return modelMapper.map(userFunctionService.add(userFunctionDto), UserFunctionDto.class);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAdminPermissions()")
	public UserFunctionDto updateFunction(@RequestBody @Valid UserFunctionDto userFunctionDto) {
		return modelMapper.map(userFunctionService.update(userFunctionDto), UserFunctionDto.class);
	}

	@RequestMapping(value = { "/{id}", "/{id}/{subId}" }, method = RequestMethod.DELETE)
	@PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAdminPermissions()")
	public Boolean deleteFunction(@PathVariable Long id, @PathVariable(required = false) Long subId) {
		userFunctionService.delete(id, subId);
		return Boolean.TRUE;
	}
}
