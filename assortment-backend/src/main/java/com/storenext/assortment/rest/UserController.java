package com.storenext.assortment.rest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.storenext.assortment.dto.ChangePasswordRequest;
import com.storenext.assortment.dto.DroppedHierarchsDto;
import com.storenext.assortment.dto.PasswordSetRequest;
import com.storenext.assortment.dto.SegmentDto;
import com.storenext.assortment.dto.UserCompanyDto;
import com.storenext.assortment.dto.UserDto;
import com.storenext.assortment.enums.CatalogType;
import com.storenext.assortment.exception.ErrorCodeException;
import com.storenext.assortment.exception.UserNotFoundException;
import com.storenext.assortment.model.data.SupplierClass;
import com.storenext.assortment.model.security.Authority;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.CatalogHierarchyService;
import com.storenext.assortment.service.UserService;

@RestController
@RequestMapping(UserController.URL)
public class UserController {
    public static final String URL = "/rest/api/v1/user";
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private CatalogHierarchyService catalogHierarchyService;
    @Autowired
    private ModelMapper modelMapper;


    @GetMapping
    public UserDto getUsers() {
        return modelMapper.map(userService.getCurrentUser(), UserDto.class);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public UserCompanyDto getUser(@PathVariable Long id) {
        User user = getUserById(id);
        UserCompanyDto dto = modelMapper.map(user, UserCompanyDto.class);
        return dto;
    }

    private User getUserById(Long id) {
        User user = userService.getUserById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @RequestMapping(value = "/{id}/{password}", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void setPassword(@PathVariable Long id, @PathVariable String password) {
        userService.setPassword(id, password);
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public UserCompanyDto getCurrentUser() {
        UserCompanyDto dto = modelMapper.map(userService.getCurrentUser(), UserCompanyDto.class);
        return dto;
    }

    @RequestMapping(value = "/shouldPassOnboarding", method = RequestMethod.GET)
    public Boolean shouldPassOnboarding() {
        return userService.shouldPassOnboarding(userService.getCurrentUser());
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public UserDto[] getAllUsers() {
        User currentUser = userService.getCurrentUser();
        if (currentUser.getAuthority() == Authority.ADMIN) {
            return modelMapper.map(userService.getAllUsers(), UserDto[].class);
        }
        return null;
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN') && @securityFunction.hasAccessToCompanyUser(#dto.companyId)")
    public UserDto createUser(@RequestBody @Valid UserDto dto) {
        return modelMapper.map(userService.registerUser(dto), UserDto.class);
    }

    @PutMapping
    @PreAuthorize("@securityFunction.hasAccessToCompanyUser(#dto.companyId)")
    public UserDto updateUser(@RequestBody @Valid UserDto dto) {
        return modelMapper.map(userService.updateUser(dto), UserDto.class);
    }

    @RequestMapping(value = "create-password", method = RequestMethod.POST)
    public UserDto createPassword(@RequestBody @Valid PasswordSetRequest passwordSetRequest) {
        return modelMapper.map(userService.setInitialPassword(passwordSetRequest.getToken(), passwordSetRequest.getPassword()), UserDto.class);
    }

    @RequestMapping(value = "/create-password/info", method = RequestMethod.POST)
    public UserCompanyDto getUserInfo(@RequestParam(value = "token") String token) {
        try {
            User user = userService.getUserByEmailValidationToken(token);
            if (user != null) {
                return modelMapper.map(user, UserCompanyDto.class);
            }
        } catch (Exception e) {
            logger.error("", e);
        }
        return null;
    }

    @RequestMapping(value = "create-password/token", method = RequestMethod.GET)
    public Boolean isCreatePasswordTokenValid(@RequestParam(value = "token") String token) {
        try {
            User user = userService.getUserByEmailValidationToken(token);
            if (user != null) {
                return true;
            }

        } catch (ErrorCodeException e) {
            throw e;
        } catch (Exception e) {
            logger.error("", e);
        }
        return false;
    }

    @RequestMapping(value = "reset-password", method = RequestMethod.POST)
    public UserDto resetPassword(@RequestBody @Valid PasswordSetRequest passwordSetRequest) {
        User user = userService.resetPassword(passwordSetRequest.getToken(), passwordSetRequest.getPassword());
        if (user != null) {
            return modelMapper.map(user, UserDto.class);
        }
        return null;
    }

    @RequestMapping(value = "/reset-password/token", method = RequestMethod.GET)
    public Boolean isResetPasswordValid(@RequestParam(value = "token") String token) {
        return userService.isResetPasswordTokenValid(token);
    }

    @RequestMapping(value = "change-password", method = RequestMethod.POST)
    public UserCompanyDto changePassword(@RequestBody @Valid ChangePasswordRequest passwordSetRequest) {

        User user = userService.changePassword(passwordSetRequest.getCurrentPassword(), passwordSetRequest.getNewPassword());
        if (user != null) {
            return modelMapper.map(user, UserCompanyDto.class);
        }
        return null;
    }

    @RequestMapping(value = "/available/roles", method = RequestMethod.GET)
    public Map<String, String> getUserRoles() {
        return Arrays.stream(Authority.values())
                .collect(Collectors.toMap(
                        Enum::name,
                        Authority::getCaption));
    }

    @RequestMapping(value = "/segments", method = RequestMethod.GET)
    public Set<SegmentDto> getCurrentUserSegments(@RequestParam(value = "catalogType") CatalogType catalogType) {
        return getSegmentsForUser(userService.getCurrentUser(), catalogType);
    }

    @RequestMapping(value = "/available/segments", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Set<SegmentDto> getCurrentUserAvailableSegments() {
        return catalogHierarchyService.transformSegment(userService.getAvailableSegments(userService.getCurrentUser()), userService.getCurrentUser(), modelMapper);
    }

    private Set<SegmentDto> getSegmentsForUser(User user, CatalogType catalogType) {
        return catalogHierarchyService.transformSegment(userService.getSegmentsForUser(user, catalogType,false), user, modelMapper);
    }

    @RequestMapping(value = "/{id}/segments", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Set<SegmentDto> getUserSegments(@PathVariable Long id, @RequestParam(value = "catalogType") CatalogType catalogType) {
        User user = getUserById(id);
        return getSegmentsForUser(user, catalogType);
    }

    @RequestMapping(value = "/{id}/available/segments", method = RequestMethod.GET)
    public Set<SegmentDto> getAvailableSuppliers(@PathVariable Long id) {
        if (!userService.getCurrentUser().getId().equals(id)) {
            if (!userService.getCurrentUser().getAuthority().equals(Authority.ADMIN)) {
                throw new AccessDeniedException("");
            }
        }

        User user = getUserById(id);

        return catalogHierarchyService.transformSegment(userService.getAvailableSegments(user), user, modelMapper);
    }

    @RequestMapping(value = "/available/supplier", method = RequestMethod.GET)
    public Set<SegmentDto> getAvailableSuppliersForCurrent() {
        return catalogHierarchyService.transformSegment(userService.getAvailableSegments(userService.getCurrentUser()), userService.getCurrentUser(), modelMapper);
    }

    @RequestMapping(value = "/available/class", method = RequestMethod.GET)
    public Set<SupplierClass> getClassesForCurrent(@RequestParam(value = "supplierIds", required = false) Set<Long> supplierIds) {
        return userService.getUserClasses(userService.getCurrentUser(), supplierIds);
    }

    @RequestMapping(value = "/{id}/available/class", method = RequestMethod.GET)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public Set<SupplierClass> getClasses(@PathVariable Long id, @RequestParam(value = "supplierIds", required = false) Set<Long> supplierIds) {
        User user = getUserById(id);
        return userService.getUserClasses(user, supplierIds);
    }


    @RequestMapping(value = "/storemanagementsegments", method = RequestMethod.GET)
    public Set<SegmentDto> getCurrentUserStoreManagementSegments(@RequestParam(value = "catalogType") CatalogType catalogType) {
        User currentUser = userService.getCurrentUser();
        return catalogHierarchyService.transformSegment(userService.getStoreManagementSegmentsForUser(currentUser, catalogType), currentUser, modelMapper);
    }

    @GetMapping("/hierarchs/dropped")
    public List<DroppedHierarchsDto> getDroppedHierarchies(@RequestParam(value = "catalogType") CatalogType catalogType) {
        return catalogHierarchyService.getDroppedHierarchies(userService.getCurrentUser(), catalogType);
    }
}
