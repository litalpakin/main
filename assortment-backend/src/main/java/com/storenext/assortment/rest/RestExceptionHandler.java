package com.storenext.assortment.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.storenext.assortment.dto.ResponseErrorDto;
import com.storenext.assortment.exception.ArgumentValidationException;
import com.storenext.assortment.exception.BadRequestException;
import com.storenext.assortment.exception.CreatePasswordInvalidException;
import com.storenext.assortment.exception.ErrorCodeException;
import com.storenext.assortment.exception.LoginException;
import com.storenext.assortment.security.LoginFailureException;
import com.storenext.assortment.utils.Constants;
import com.storenext.assortment.utils.LocaleUtils;





@ControllerAdvice
public class RestExceptionHandler {

	public static final String INTERNAL_SERVER_ERROR = "Internal server error";
	private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

	private ResponseErrorDto buildResponse(Exception exception) {
		List<String> errors = new ArrayList<String>();
		errors.add(ExceptionUtils.getMessage(exception));
		ResponseErrorDto response = new ResponseErrorDto();
		response.setErrors(errors);
		return response;
	}

	
	private ResponseErrorDto buildMessageResponse(Exception exception) {
	
		return new ResponseErrorDto(exception.getMessage());
	}
	private ResponseErrorDto buildMessageResponse(String message) {
		return new ResponseErrorDto(message);
	}
	private ResponseErrorDto buildMessageResponse(String message, String aditionalInfo) {
		return new ResponseErrorDto(message, aditionalInfo);
	}
	@ExceptionHandler({BadCredentialsException.class})
	public ResponseEntity<ResponseErrorDto> handleArgumentValidationException(Exception exception) {
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(buildMessageResponse(LocaleUtils.getLocalized("errors.BAD_CREDENTIALS_EXCEPTION")));
	}
	@ExceptionHandler({UsernameNotFoundException.class})
	public ResponseEntity<ResponseErrorDto> handleUsernameNotFoundException(UsernameNotFoundException exception) {
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(buildMessageResponse(LocaleUtils.getLocalized("errors.USERNAME_NOT_FOUND_EXCEPTION")));
	}
	@ExceptionHandler({LoginFailureException.class})
	public ResponseEntity<ResponseErrorDto> handleLoginFailureException(LoginFailureException exception) {
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
				.body(buildMessageResponse(LocaleUtils.getLocalized("errors.LOGIN_FAILURE_EXCEPTION"),
					String.valueOf(exception.getAttemptCount())));
	}
	
	@ExceptionHandler(BadSqlGrammarException.class)
	public ResponseEntity<ResponseErrorDto> handleBadSqlGrammarException(BadSqlGrammarException exception) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(buildMessageResponse(INTERNAL_SERVER_ERROR));
	}
	@ExceptionHandler(UnsupportedOperationException.class)
	public ResponseEntity<ResponseErrorDto> handleUnsupportedOperationException(UnsupportedOperationException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildMessageResponse(LocaleUtils.getLocalized("errors.UNSUPPORTED_OPERATION_EXCEPTION")));
	}
	
	@ExceptionHandler(ArgumentValidationException.class)
	public ResponseEntity<ResponseErrorDto> handleArgumentValidationException(ArgumentValidationException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildMessageResponse(exception));
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ResponseErrorDto> handleArgumentValidationException(MethodArgumentNotValidException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildMessageResponse(exception));
	}	
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<ResponseErrorDto> handleBadRequestException(BadRequestException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildMessageResponse(exception));
	}
	
	@ExceptionHandler(ErrorCodeException.class)
	public ResponseEntity<ResponseErrorDto> handleErrorCodeException(ErrorCodeException exception) {
		return ResponseEntity.status(exception.getErrorCode()).body(buildMessageResponse(exception));
	}
	@ExceptionHandler(CreatePasswordInvalidException.class)
	public ResponseEntity<ResponseErrorDto> handleCreatePasswordInvalidException(CreatePasswordInvalidException exception) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(buildMessageResponse(exception));
	}
	
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public  ResponseEntity<ResponseErrorDto> accessDesignException(AccessDeniedException exception) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(buildResponse(exception));
    }
    @ExceptionHandler(DisabledException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public  ResponseEntity<ResponseErrorDto> authenticationException(DisabledException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(buildMessageResponse(LocaleUtils.getLocalized(Constants.USER_DISABLED_ERROR)));
    }
    @ExceptionHandler({LoginException.class})
	public ResponseEntity<ResponseErrorDto> handleLoginException(LoginException exception) {
		return ResponseEntity.status(HttpStatus.FORBIDDEN)
				.body(buildMessageResponse(exception.getLocalizedMessage()));
	}
	@ExceptionHandler({ Throwable.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ResponseErrorDto> handleGeneralException(Exception e, HttpServletRequest request) {
		logger.error(e.getMessage(), e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(buildResponse(e));
	}


}
