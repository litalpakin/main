package com.storenext.assortment.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.storenext.assortment.service.ImageService;

@RestController
@RequestMapping("rest/api/v1/image")
public class ImageRestController {
	
	@Autowired
	private ImageService imageService;

	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN') && @securityFunction.hasAdminPermissions()")
    public String uploadImage(@RequestParam("file") MultipartFile file) {
    	return imageService.uploadImage(file);
    }

   

}
