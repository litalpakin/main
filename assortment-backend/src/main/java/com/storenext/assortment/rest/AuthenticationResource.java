package com.storenext.assortment.rest;

import java.io.UnsupportedEncodingException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.storenext.assortment.dto.UserWithPasswordDto;
import com.storenext.assortment.dto.UsernameDto;
import com.storenext.assortment.security.JwtAuthenticationResponse;
import com.storenext.assortment.security.JwtTokenUtil;
import com.storenext.assortment.service.AuthenticationService;
import com.storenext.assortment.service.UserService;

@RestController
@RequestMapping("/rest/api/v1/auth")
public class AuthenticationResource {

    @Autowired
    private AuthenticationService authenticationService;
    
    @Autowired
    private UserService userService;
    

    @RequestMapping(value = "token", method = RequestMethod.POST)
    
    public ResponseEntity<JwtAuthenticationResponse> createAuthenticationToken(@RequestBody  @Valid UserWithPasswordDto userDto) throws AuthenticationException {
        String token = authenticationService.authenticate(userDto.getUsername(), userDto.getPassword());
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }
    
    @RequestMapping(value = "/loginas/token", method = RequestMethod.POST)
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<JwtAuthenticationResponse> createUserToken(@RequestBody  @Valid UsernameDto userDto) throws AuthenticationException {
    	String token = authenticationService.generateToken(userDto.getUsername());
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }
    @RequestMapping(value = "token/valid", method = RequestMethod.GET)
    public Boolean isValid(@RequestParam(value = "currentToken")  String currentToken) {
        return authenticationService.isTokenValid(null,currentToken);
    }

    @RequestMapping(value = "refresh", method = RequestMethod.GET)
    public ResponseEntity<JwtAuthenticationResponse> refreshAndGetAuthenticationToken(@RequestHeader(JwtTokenUtil.TOKEN_HEADER) String currentToken) {
        String newToken = authenticationService.refreshToken(currentToken);
        return ResponseEntity.ok(new JwtAuthenticationResponse(newToken));
    }
    
    @RequestMapping(value = "genereate/reset-password", method = RequestMethod.GET)
	public void generateResetPassword(@RequestParam(value = "userEmail") String userEmail) throws UnsupportedEncodingException {
		userService.generateResetPassword(userEmail);
	}

}
