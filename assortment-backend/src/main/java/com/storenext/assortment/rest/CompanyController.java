package com.storenext.assortment.rest;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.storenext.assortment.dto.CompanyDto;
import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.service.CompanyService;
import com.storenext.assortment.service.UserService;

@RestController
@RequestMapping(CompanyController.URL)
public class CompanyController {

    public static final String URL ="/rest/api/v1/company" ;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public CompanyDto getCurrentCompany() {
        Company company = companyService.getCompany(userService.getCurrentUser().getCompanyId());
        return modelMapper.map(company, CompanyDto.class);
    }


    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN')")
    public CompanyDto[] getCompanies() {
        return modelMapper.map(companyService.getCompanies(userService.getCurrentUser()), CompanyDto[].class);
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAccessToCompany(#companyDto.type)")
    public CompanyDto addCompany(@RequestBody @Valid CompanyDto companyDto) {
        return modelMapper.map(companyService.add(companyDto), CompanyDto.class);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ADMIN') && @securityFunction.hasAccessToCompany(#companyDto.type)")
    public CompanyDto updateCompany(@PathVariable Long id, @RequestBody @Valid CompanyDto companyDto) {
        companyDto.setId(id);
        return modelMapper.map(companyService.update(companyDto), CompanyDto.class);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ADMIN')")
    public Boolean deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
        return Boolean.TRUE;
    }

    @RequestMapping(value = "/available/types", method = RequestMethod.GET)
	public Map<String,String> getCompanyRoles() {
		return Arrays.stream(CompanyType.values())
				.collect(Collectors.toMap(
                        Enum::name,
                        CompanyType::getCaption));
	}
}
