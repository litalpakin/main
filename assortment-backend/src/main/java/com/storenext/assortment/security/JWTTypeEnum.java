package com.storenext.assortment.security;

public enum JWTTypeEnum {
    AUTH,
    RESET_PASSWORD,
    CREATE_PASSWORD
}
