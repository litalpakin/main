package com.storenext.assortment.security;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.storenext.assortment.model.security.User;
import com.storenext.assortment.utils.Constants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {

    private static final int RESET_PASSWORD_EXPIRATION = 48 * 60 * 60 * 1000;
	private static final int ADMIN_TOKEN_VALID_PERIOD = 4 * 60 * 60 * 1000;
	private static final int PASSWORD_RESET_DATE_EPSILON = 1000;
    private final Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class);
	private static final long serialVersionUID = -3301605591108950415L;

    static final String CLAIM_KEY_TOKEN_TYPE = "type";
    static final String CLAIM_KEY_USER_EMAIL = "sub";
    static final String CLAIM_KEY_CREATED = "created";

    static public final String TOKEN_HEADER = "Authorization";

   
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;
   
    public String getEmailFromToken(String token) {
        String email;
        try {
            final Claims claims = getClaimsFromToken(token);
            email = claims.getSubject();
            if(email != null){
            	email = email.toLowerCase();
            }
        } catch (Exception e) {
            logger.debug("Can't get email for token: {}", token, e);
            email = null;
        }
        return email;
    }

    public Date getCreatedDateFromToken(String token) {
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        } catch (Exception e) {
            logger.debug("Can't get created date for token: {}", token, e);
            created = null;
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            logger.debug("Can't get expiration date for token: {}", token, e);
            expiration = null;
        }
        return expiration;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            logger.debug("Can't get claims for token: {}", token, e);
            claims = null;
        }
        return claims;
    }

    private Date generateExpirationDate(Map<String, Object> claims) {
        Date date;
        if (Constants.ADMIN_EMAIL.equals(claims.get(CLAIM_KEY_USER_EMAIL))){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.MILLISECOND, 999);
            date = calendar.getTime();
        } else {
            date = new Date(System.currentTimeMillis() + expiration * 1000);
        }
        return date;
    }
    
    public Boolean isTokenExpired(User user, String token) {
        Date date;
        if(Constants.ADMIN_EMAIL.equals(user.getUsername()) && user.getLastRequest() != null) {
            date = new Date(user.getLastRequest() + ADMIN_TOKEN_VALID_PERIOD);
            logger.debug("validate token. last request: {}, expiration date: {}", new Date(user.getLastRequest()), date);
        } else {
            date = getExpirationDateFromToken(token);
            logger.debug("validate token. expiration date from token: {}", date);
        }
        return date.before(new Date());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
    	if(lastPasswordReset == null) {
            logger.debug("validate token. last password reset date null");
    		return false;
    	}
    	lastPasswordReset.setTime(lastPasswordReset.getTime() - PASSWORD_RESET_DATE_EPSILON);
        logger.debug("validate token. created date : {} last password reset date: {}", created, lastPasswordReset);
        return !created.after(lastPasswordReset);
    }



    public String generateToken(String username,Date date) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USER_EMAIL, username);
        claims.put(CLAIM_KEY_TOKEN_TYPE, JWTTypeEnum.AUTH);
        claims.put(CLAIM_KEY_CREATED, date);

        return generateToken(claims);
    }
    
    public String generateCreatePasswordToken(String username,Date date) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USER_EMAIL, username);
        claims.put(CLAIM_KEY_TOKEN_TYPE, JWTTypeEnum.CREATE_PASSWORD);
        claims.put(CLAIM_KEY_CREATED, date);

        return generateTokenNoExperation(claims);
    }
    
    public String getUsernameCreatePasswordToken(String token) {
        String  username = null;
        try {
            final Claims claims = getClaimsFromToken(token);
            String type = (String)claims.get(CLAIM_KEY_TOKEN_TYPE);
            if( type != null){
            	username = (String) claims.get(CLAIM_KEY_USER_EMAIL);
            }
        } catch (Exception e) {
            logger.error("Failed to extract create password token",e);
        }
        return username;
    }
    String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate(claims))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    String generateTokenNoExperation(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    String generateResetToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + RESET_PASSWORD_EXPIRATION))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
    public Boolean canTokenBeRefreshed(User user,String token, Date lastPasswordReset) {
        final Date created = getCreatedDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && !isTokenExpired(user,token) && isTokenOfType(token, JWTTypeEnum.AUTH);
    }

    public Boolean canTokenBeUsedForPasswordReset(User user,String token, Date lastPasswordReset) {
        final Date created = getCreatedDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && !isTokenExpired(user,token) && isTokenOfType(token, JWTTypeEnum.RESET_PASSWORD);
    }

    public String refreshToken(String token) {
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }
    
    public Boolean validateToken(String token, User user) {
        final String email = getEmailFromToken(token);
        logger.debug("validate token. username: {} email from token: {}", user.getUsername(), email);
        final Date created = getCreatedDateFromToken(token);
        return user.getUsername().equals(email)
                && checkTokenCreatedDate(token, user)
                && !isTokenExpired(user, token)
                && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordResetDate());
    }
    
    public boolean checkTokenCreatedDate(String token, User user) {
        if(Constants.ADMIN_EMAIL.equalsIgnoreCase(user.getUsername())) {
            return true;
        }
        
        final Date createdDateFromToken = getCreatedDateFromToken(token);
        final Date tokenCreated = new Date(user.getTokenCreated());
        logger.debug("validate token. token created: {} created date from token: {}", user.getTokenCreated(), createdDateFromToken);
        return createdDateFromToken.equals(tokenCreated);
    }
    
    public boolean isTokenOfType(String token, JWTTypeEnum type) {
        try {
            final Claims claims = getClaimsFromToken(token);
            return type.equals(JWTTypeEnum.valueOf((String)claims.get(CLAIM_KEY_TOKEN_TYPE)));
        } catch (Exception e) {
            logger.debug("Can't get type of token for token: {}", token, e);
            return false;
        }
    }


    public String generateResetPasswordToken(String email) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USER_EMAIL, email);
        claims.put(CLAIM_KEY_TOKEN_TYPE, JWTTypeEnum.RESET_PASSWORD);
        claims.put(CLAIM_KEY_CREATED, new Date());

        return generateResetToken(claims);
    }
}