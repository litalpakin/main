package com.storenext.assortment.security;

public class LoginFailureException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6487448344569774876L;
	private Long attemptCount;

	public LoginFailureException(Long attemptCount, String message, Throwable cause) {
		super(message, cause);
		this.attemptCount = attemptCount;
	}

	public Long getAttemptCount() {
		return attemptCount;
	}

	public void setAttemptCount(Long attemptCount) {
		this.attemptCount = attemptCount;
	}
}
