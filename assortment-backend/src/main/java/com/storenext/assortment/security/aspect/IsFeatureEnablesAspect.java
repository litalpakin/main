package com.storenext.assortment.security.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.service.FeatureConfigurationService;

@Aspect
@Component
public class IsFeatureEnablesAspect {
	@Autowired
	private FeatureConfigurationService featureConfigurationService;

	@Pointcut("@annotation(com.storenext.assortment.security.aspect.IsFeatureEnables)")
	private void isFeatureEnabled() {
	}

	@Around("com.storenext.assortment.security.aspect.IsFeatureEnablesAspect.isFeatureEnabled()")
	public Object isFeatureEnabled(ProceedingJoinPoint call) throws Throwable {
		for (Feature feature : ((MethodSignature) call.getSignature()).getMethod().getAnnotation(IsFeatureEnables.class).value()) {
			if (!featureConfigurationService.isEnabled(feature)) {
				return null;
			}
		}

		return call.proceed();
	}
}
