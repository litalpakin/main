package com.storenext.assortment.security;

import static com.storenext.assortment.utils.Constants.COMPANY_DISABLED_ERROR;
import static com.storenext.assortment.utils.Constants.TOKEN_IS_INVALID;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.storenext.assortment.enums.Feature;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.FeatureConfigurationService;
import com.storenext.assortment.service.QueryService;
import com.storenext.assortment.service.UserService;
import com.storenext.assortment.utils.Constants;
import com.storenext.assortment.utils.LocaleUtils;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private UserService userDetailsService;
    @Autowired
    private StorenextAuthenticationProvider storenextAuthenticationProvider;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private QueryService queryService;
    @Autowired
    private UserAuthenticationFailureManager userAuthenticationFailureManager;
    @Autowired
    private FeatureConfigurationService featureConfigurationService;
    
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
		if(!StringUtils.isEmpty(request.getRequestURI()) && request.getRequestURI().contains("query/ping")) {
			chain.doFilter(request, response);
			return;
		}
		
		String authToken = request.getHeader(JwtTokenUtil.TOKEN_HEADER);
		String reportId = request.getHeader("ReportId");
		if(reportId == null) {
			reportId = UUID.randomUUID().toString();
		}
		
		String email = jwtTokenUtil.getEmailFromToken(authToken);
		
		logger.debug("checking authentication for user " + email);
		
		if(SecurityContextHolder.getContext().getAuthentication() == null && jwtTokenUtil.isTokenOfType(authToken, JWTTypeEnum.AUTH)) {
			if(featureConfigurationService.isEnabled(Feature.UNDER_CONSTRUCTION) && !Constants.ADMIN_EMAIL.equals(email)) {
				response.sendError(HttpServletResponse.SC_OK, LocaleUtils.getLocalized(Constants.UNDER_CONSTRUCTION_MESSAGE));
				return;
			}
			
			User user = (User) this.userDetailsService.loadUserByUsername(email);
			Long lastRequest = userDetailsService.getLastRequest(user.getId());
			if(lastRequest != null) {
				user.setLastRequest(lastRequest);
			}
			
			if(!jwtTokenUtil.validateToken(authToken, user)){
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, LocaleUtils.getLocalized(TOKEN_IS_INVALID));
				return;
			}
			
			if(!storenextAuthenticationProvider.validateCompany(user.getCompanyId())) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, LocaleUtils.getLocalized(COMPANY_DISABLED_ERROR));
				return;
			}
			
			if(userAuthenticationFailureManager.isMaxFailureCountReached(user.getId())) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, LocaleUtils.getLocalized(LocaleUtils.USERLOGIN_LOCKED) + " " + email);
				return;
			}
			
			userAuthenticationFailureManager.handleSuccessfullLogin(user.getId());
			UsernamePasswordCustomAuthenticationToken authentication = new UsernamePasswordCustomAuthenticationToken(user, null, user.getAuthorities());
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			authentication.setSessionId(reportId);
			queryService.updateRequest(reportId);
			logger.debug("authenticated user " + email + ", setting security context");
			userDetailsService.addLastRequest(user.getId(), System.currentTimeMillis());
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}
		
		chain.doFilter(request, response);
	}
}