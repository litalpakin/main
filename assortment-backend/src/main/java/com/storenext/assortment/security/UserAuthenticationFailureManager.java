package com.storenext.assortment.security;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.storenext.assortment.exception.LoginException;
import com.storenext.assortment.utils.LocaleUtils;

@Service
public class UserAuthenticationFailureManager {
	
	@Value("${storenext.login-failure.attempt:6}")
	private Integer loginFailureAttempt;
	@Value("${storenext.login-failure.time-window:10}")
	private Integer timeWindow;
	
	private Cache<Object,Object> cache = null;
	
	@PostConstruct
	private void init(){
		cache = CacheBuilder.newBuilder().expireAfterWrite(timeWindow, TimeUnit.MINUTES).build();
	}
	public boolean isMaxFailureCountReached(Long userId){
		Integer count = (Integer) cache.getIfPresent(userId);
		if(count == null){
			return false;
		}
		return count >= loginFailureAttempt;
	}
	
	public void handleSuccessfullLogin(Long userId){
		cache.invalidate(userId);
	}
	public void addFailureAndThrowAdjustedError (AuthenticationException ae, Long userId){
		Integer count = (Integer) cache.getIfPresent(userId);
		if(count == null || count < loginFailureAttempt ){
				if(count == null){
					count = 0;
				}
				count++;
				cache.put(userId, count);
		}
		if(count+2 == loginFailureAttempt){
				throw new LoginException(LocaleUtils.getLocalized(LocaleUtils.USERLOGIN_BE_LOCKED2));
		} else
		if(count+1 == loginFailureAttempt){
			throw new LoginException(LocaleUtils.getLocalized(LocaleUtils.USERLOGIN_BE_LOCKED1));
		} else
		if (count >= loginFailureAttempt) {
				throw new LoginFailureException((long) count, ae.getMessage(), ae.getCause());
		} else {
		 	throw ae;
		}
	}
}
