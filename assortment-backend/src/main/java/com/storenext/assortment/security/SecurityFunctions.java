package com.storenext.assortment.security;

import static com.storenext.assortment.utils.Constants.ADMIN_COMPANIES;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.CompanyType;
import com.storenext.assortment.model.security.Authority;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.CompanyService;
import com.storenext.assortment.utils.SecurityUtils;


/**
 * Contains security functions to check different permissions.
 */
@Component("securityFunction")
public class SecurityFunctions {

    @Autowired
    private CompanyService companyService;

    /**
     * Check if user company has type ADMIN or MAIN and user is ADMIN.
     */
    public boolean hasAdminPermissions() {
        User user = SecurityUtils.currentUser();
        Company company = companyService.getCompany(user.getCompanyId());
        return ObjectUtils.allNotNull(user, company) && user.isEnabled() && user.getAuthority().equals(Authority.ADMIN)
                && company.isEnabled();
    }
    /**
     * Check if user company has type ADMIN and user is ADMIN.
     */
    public boolean isSuperAdmin() {
        User user = SecurityUtils.currentUser();
        Company company = companyService.getCompany(user.getCompanyId());
        return ObjectUtils.allNotNull(user, company) && user.isEnabled() && user.getAuthority().equals(Authority.ADMIN)
                && company.isEnabled() && company.getType().equals(CompanyType.ADMIN);
    }

    /**
     * Check if user company has type ADMIN and user is ADMIN.
     */
    public boolean isAdminInSameCompany(Long companyId) {
        User user = SecurityUtils.currentUser();
        Company company = companyService.getCompany(user.getCompanyId());
        return ObjectUtils.allNotNull(user, company) && user.isEnabled() && user.getAuthority().equals(Authority.ADMIN)
                && company.isEnabled() && company.getId().equals(companyId);
    }

    public boolean hasAccessToCompanyUser(Long companyId) {
		User user = SecurityUtils.currentUser();
		//USER has access to only his company OR any user/admin has access to his company
		if (user.getCompanyId().equals(companyId) || user.getAuthority().equals(Authority.USER)) {
			return Boolean.TRUE;
		}

		if (user.getAuthority().equals(Authority.ADMIN)) {
			Company userCompany = companyService.getCompany(user.getCompanyId());
			//Admin of admin company has access to all company
			if (userCompany.getType().equals(CompanyType.ADMIN)) {
				return Boolean.TRUE;
			}
			if (userCompany.getType().equals(CompanyType.MAIN)) {
				Company company = companyService.getCompany(companyId);
				//Any admin has access to secondary companies
				if (company.getType().equals(CompanyType.SECONDARY)) {
					return Boolean.TRUE;
				}
			}
		}

        return Boolean.FALSE;
    }

	public boolean hasAccessToCompany(CompanyType type) {
		User user = SecurityUtils.currentUser();
		Company userCompany = companyService.getCompany(user.getCompanyId());

		if (userCompany.getType().equals(CompanyType.ADMIN) && !type.equals(CompanyType.ADMIN)) {
			return Boolean.TRUE;
		}

		if (userCompany.getType().equals(CompanyType.MAIN) && type.equals(CompanyType.SECONDARY)) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

    public static boolean isAdminCompany(Company company) {
        return ADMIN_COMPANIES.contains(company.getType());
    }
}
