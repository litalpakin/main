package com.storenext.assortment.security;

import static com.storenext.assortment.utils.Constants.COMPANY_DISABLED_ERROR;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.storenext.assortment.model.Company;
import com.storenext.assortment.model.security.User;
import com.storenext.assortment.service.CompanyService;
import com.storenext.assortment.utils.LocaleUtils;

@Service
public class StorenextAuthenticationProvider  extends DaoAuthenticationProvider{

    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserAuthenticationFailureManager userAuthenticationFailureManager;

	@Inject
	public StorenextAuthenticationProvider(UserDetailsService userDetailsService,PasswordEncoder passwordEncoder) {
		setUserDetailsService(userDetailsService);
		setPasswordEncoder(passwordEncoder);
		this.hideUserNotFoundExceptions= false;
	}
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		Authentication response = super.authenticate(authentication);
		if(response != null && response.getPrincipal() != null){
			User user = (User)response.getPrincipal();
			if(userAuthenticationFailureManager.isMaxFailureCountReached(user.getId())){
				throw new DisabledException(LocaleUtils.getLocalized(LocaleUtils.USERLOGIN_LOCKED) + " " + user.getUsername());
    		}
			if(!validateCompany(user.getCompanyId())){
				throw new DisabledException(COMPANY_DISABLED_ERROR);
			}
		}
		return response;
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {
		try {
			super.additionalAuthenticationChecks(userDetails, authentication);
		} catch (AuthenticationException ae) {
			Long userId = ((User)userDetails).getId();
			userAuthenticationFailureManager.addFailureAndThrowAdjustedError(ae,userId);
		}
	}

	public boolean validateCompany(Long companyId){
		Company company = companyService.getCompany(companyId);
		return company.isEnabled();
	}
	
	
}
