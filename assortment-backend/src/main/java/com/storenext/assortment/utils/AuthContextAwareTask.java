package com.storenext.assortment.utils;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;

public class AuthContextAwareTask<V> implements Callable<V>{
	
	private static final Logger logger = LoggerFactory.getLogger(AuthContextAwareTask.class);
	
	private Callable<V> delegate;
	private Authentication authentication;
	
	private AuthContextAwareTask(Callable<V> callable) {
		this.delegate = callable;
		this.authentication = SecurityUtils.getAuthentication();
	}

	public static <V> FutureTask<V> create(Callable<V> callable) {
		return new FutureTask<V>(new AuthContextAwareTask<V>(callable));
	}

	@Override
	public V call() throws Exception {
		try {
			SecurityUtils.copyContext(authentication);
			return delegate.call();
		}catch(Exception ex) { 
			logger.error("Error while executing task " + delegate,ex);
			return null;
		}
	}
}
