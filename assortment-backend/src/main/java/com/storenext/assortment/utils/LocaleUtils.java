package com.storenext.assortment.utils;


import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.storenext.assortment.enums.Language;
import com.storenext.assortment.model.security.User;



public class LocaleUtils {

	
	private static final Logger logger = LoggerFactory.getLogger(LocaleUtils.class);
	public static final String USERLOGIN_BE_LOCKED1 = "errors.LOGIN_LOCK1";
	public static final String USERLOGIN_BE_LOCKED2 = "errors.LOGIN_LOCK2";
	public static final String USERLOGIN_LOCKED = "errors.LOGIN_FAILURE_EXCEPTION";
	
	private static MessageSource messageSource;

	static {
		ResourceBundleMessageSource messageSource1 = new ResourceBundleMessageSource();
		messageSource1.setBasename("i18n/messages");
		messageSource1.setDefaultEncoding("UTF-8");
		messageSource = messageSource1;
	}
	
	public static String getLocalized(String property, Object arg) {
		return getLocalized(property, new Object[] {arg});
	}
	
	public static String getLocalized(String property) {
		return getLocalized(property, null);
	}
 
	public static String getLocalized(String property, Object[] args) {
		try {
			return getLocalized(property, args, getCurrentLocale());
		} catch (Exception ex) {
			logger.debug("Message not found for property {}", property);
			return property;
		}
	}
	public static String getLocalized(String property,Object[] args,Locale locale) {
		try {
			return messageSource.getMessage(property, args,locale);
		} catch (Exception ex) {
			logger.debug("Message not found for property {}", property);
			return property;
		}
	}
	private static Locale getCurrentLocale() {
		User currentUser = SecurityUtils.currentUser();
		if(currentUser == null || currentUser.getLanguage() == null){
			return Language.HEBREW.getLocale();
		}
		Locale locale = currentUser.getLanguage().getLocale();
		return locale == null ? Locale.ENGLISH : locale;
	}
}