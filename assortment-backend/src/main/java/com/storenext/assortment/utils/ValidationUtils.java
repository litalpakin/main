package com.storenext.assortment.utils;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ValidationUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ValidationUtils.class);
	
	private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	
	public static <T> Set<ConstraintViolation<T>> getObjectValidationConstraints(T obj) {
		return validator.validate(obj);
	}
	
	public static void validateNotNull(Object obj) {
		if(obj == null) {
			throw new ValidationException("Object cannot be null");
		}
	}

	public static void validateObject(Object obj) {
		Set<ConstraintViolation<Object>> violations = getObjectValidationConstraints(obj);
		if(!violations.isEmpty()) {
			logger.error("Validation failed for " + obj + ": " + violations);
			throw new ValidationException("Invalid object"); 
		}
		
	}
	
}
