package com.storenext.assortment.utils;

import java.util.List;

import com.google.common.collect.Lists;
import com.storenext.assortment.model.CompanyType;

public class Constants {
	public static final String USER_NOT_FOUND_PLEASE_CHECK = "The user is not found. Please check the email address again.";
    public static final String COMPANY_DOESN_T_EXIST = "Company doesn't exist";
    public static final String CAN_T_CREATE_USER_FOR_DISABLED_COMPANY = "Can't create user for disabled company";
    public static final String COMPANY_ADMIN_CAN_T_CREATE_USER_FOR_ANOTHER_COMPANY = "Company admin can't create user for another company";
    public static final String ID_IS_NOT_SUPPORTED = "Id is not supported";
    public static final String TOKEN_IS_INVALID = "Email token is invalid";
    public static final String TOKEN_EXPIRED = "Token expired";
    public static final String CURRENT_PASSWORD_CAN_T_BE_EMPTY = "Current password can't be empty";
    public static final String NEW_PASSWORD_CAN_T_BE_EMPTY = "New password can't be empty";
    public static final String USER_ID_IS_MANDATORY = "User Id is mandatory";
    public static final String FIRST_NAME_IS_MANDATORY = "First name is mandatory";
    public static final String CAN_T_UPDATE_USER = "Can't update user: ";
    public static final String LAST_NAME_IS_MANDATORY = "Last name is mandatory";
    public static final String USER_CAN_T_BE_DISABLED = "User can't be disabled";
    public static final String THIS_USER_CAN_T_EDIT_USERNAME = "This user can't edit username";
    public static final String THIS_USER_CAN_T_EDIT_OTHERS = "This user can't edit others";
    public static final String THIS_USER_CAN_T_EDIT_AUTHORITY = "This user can't edit authority";
    public static final String THIS_USER_CAN_T_ASSIGN_SYSTEM_ADMINS = "This user can't assign system admins";
    public static final String THIS_USER_CAN_T_EDIT_COMPANY = "This user can't edit company";
    public static final String AUTHORITY_IS_MANDATORY = "Authority is mandatory";
    public static final String NOT_ENOUGH_PERMISSION = "Do not have permission to perfowm this action.";
	public static final String FUNCTIONS_IS_MANDATORY_FIELD = "Functions are mandatory field.";
    public static final String COMPANY_DISABLED_ERROR = "error.COMPANY_DISABLED_ERROR";
    public static final String USER_DISABLED_ERROR = "error.USER_DISABLED_ERROR";
    public static final String UNDER_CONSTRUCTION_MESSAGE = "under.construction";
	public static final String ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_INACTIVATED = "Admin or main Company can't be inactivated.";
    public static final String ADMIN_OR_MAIN_COMAPNY_CAN_T_BE_DELETED = "Admin or main Company can't be deleted.";
	public static final String COULD_NOT_CHANGE_COMPANY_TYPE = "Could not change company type.";
	public static final String DO_NOT_HAVE_PERMISSION_TO_USE_SUCH_USER_FUNCTION_ID = "Do not have permission to use such user function id.";
	public static final String COULD_NOT_MOVE_USER_TO_ANOTHER_COMPANY = "Could not move user to another company.";
	public static final String USER_HAS_NOT_ACCESS_TO_SUPPLIER_S_OR_SUB_CATEGORY_S = "User has not access to Supplier(%s) or sub category(%s).";

	public static final String COMPANY_ID_PRESENT = "Can't create company with id, id should be empty";
    public static final String MANDATORY_ID = "Id is mandatory";
    public static final String ID_IS_PRESENT = "Id should not be present";
    public static final String COMPANY_DOES_NOT_EXIST = "Company with id: %s doesn't exist";
    public static final String FUNCTION_DOES_NOT_EXIST = "UserFunction with id: %s doesn't exist";
    public static final String CANNOT_RETRIEVE_TOKEN = "Cannot retrieve credentials from token";
    public static final String UNKNOWN_USER_CRED = "Unknown user credentials";
    public static final String CANNOT_REFRESH_TOKEN = "Cannot refresh token";
    public static final String CANNOT_UPLOAD_IMAGE = "Cannot upload image ";
    public static final String CANNOT_UPLOAD_REPORT = "Cannot upload report ";
    public static final String CANNOT_SEND_MAIL = "Cannot send mail ";
    public static final String MAIL_BAD_RESPONSE = "Got bad response from sendgrid. Response message: ";
    public static final String RESEARCH_AND_STRATEGY = "errors.RESEARCH_AND_STRATEGY";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String COMPANY_IS_MANDATORY = "Company is mandatory";
    public static final String ADMIN_EMAIL = "admin@storenext.co.il";
    public static List<CompanyType> ADMIN_COMPANIES = Lists.newArrayList(CompanyType.ADMIN, CompanyType.MAIN);

    private Constants() {
    }
}
