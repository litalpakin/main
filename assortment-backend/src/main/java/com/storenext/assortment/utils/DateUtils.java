package com.storenext.assortment.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.joda.time.DateTime;

public class DateUtils {

	public static Date startOfTheDay(Date date) {
		if (date == null) {
			return null;
		}
		DateTime startOfTheDay = new DateTime(date.getTime()).withTimeAtStartOfDay();
		return new Date(startOfTheDay.getMillis());
	}
	
	public static Date getMinDate(Date... dates) {
		return Arrays.asList(dates).stream().min(Date::compareTo).get();
	}

	public static Date endOfTheDay(Date date) {
		if (date == null) {
			return null;
		}
		DateTime endOfDay = new DateTime(date.getTime()).withTime(23, 59, 59, 999);
		return new Date(endOfDay.getMillis());
	}

	public static Calendar toCalendar(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}
	
	public static Date addDays(Date date, int amount) {
		LocalDate ld = DateUtils.asLocalDate(date).plus(amount, ChronoUnit.DAYS);
		return asDate(ld);
	}
	public static Date addMinutes(Date date, int amount) {
		 Calendar now = toCalendar(date);
		 now.add(Calendar.MINUTE,amount);
		return now.getTime();
	}

	public static Date addMonths(Date date, int amount) {
		LocalDate ld = DateUtils.asLocalDate(date).plus(amount, ChronoUnit.MONTHS);
		return asDate(ld);
	}

	public static List<Date> getAllDatesInRange(Date startDate, Date endDate) {
		
		LocalDate localStart = asLocalDate(startDate);
		LocalDate localEnd = asLocalDate(endDate);
		
	    long numOfDaysBetween = ChronoUnit.DAYS.between(localStart, localEnd) + 1;
	    return IntStream.iterate(0, i -> i + 1)
	      .limit(numOfDaysBetween)
	      .mapToObj(i -> localStart.plusDays(i))
	      .map(ld -> Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant()))
	      .collect(Collectors.toList());
	}
	
	
	public static Date asDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	public static LocalDate asLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public static  int compareDate(Date updateDate1, Date updateDate2, boolean nullFirst) {
		int date = 0;
		if(updateDate1 == null) {
			if(updateDate2 != null) {
				date = nullFirst ? -1 : 1;
			}
		} else {
			if(updateDate2 == null) {
				date = nullFirst ? 1 : -1;
			} else {
				date = updateDate1.compareTo(updateDate2);
			}
		}

		return date;
	}
}
