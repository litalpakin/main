package com.storenext.assortment.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.storenext.assortment.model.security.User;
import com.storenext.assortment.security.UsernamePasswordCustomAuthenticationToken;

public class SecurityUtils {

	public static User currentUser() {
	       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        if (authentication != null && authentication.getPrincipal() != null && authentication.getPrincipal() instanceof User) {
	            return (User) authentication.getPrincipal();
	        }
	        return null;
	}
	public static String currentRequestId() {
	       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        if (authentication != null  && authentication instanceof UsernamePasswordCustomAuthenticationToken) {
	            return ((UsernamePasswordCustomAuthenticationToken) authentication).getSessionId();
	        }
	        return null;
	}
	public static Long currentUserId() {
		User cu = currentUser();
		return cu != null ? cu.getId() : null;
	}
	public static void copyContext(Authentication authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	public static Authentication getAuthentication() {
		return  SecurityContextHolder.getContext().getAuthentication();
	}

}
