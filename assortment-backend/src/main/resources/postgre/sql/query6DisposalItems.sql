with all_users_items as
(
		select count(*) total_sum_user_items_in_chain_2
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as all_users_items_inn1
		where sales_item_format_cur_1>0 
),
 m_q5_filterd_5_user_disposal_item as
(
		select count(*) sum_users_items_for_disposal_1
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where  item_classification_42 = 5
		and {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as m_q5_filterd_5_user_disposal_item_inn1
		where sales_item_format_cur_1>0 
)
select *,case when Prcnt_of_user_disposal_items_3 = 0 then null
	  when Prcnt_of_user_disposal_items_3 > 0 and Prcnt_of_user_disposal_items_3 <= 0.1 then 1
	  when Prcnt_of_user_disposal_items_3 < 0.2 and Prcnt_of_user_disposal_items_3 > 0.1 then 2
	  when Prcnt_of_user_disposal_items_3 >= 0.2 then 3 end clasification_Prcnt_of_user_disposal_items_4 -- Color classification 
from (
	select sum_users_items_for_disposal_1,total_sum_user_items_in_chain_2
	,case when total_sum_user_items_in_chain_2<>0 then cast(sum_users_items_for_disposal_1 as double precision)/total_sum_user_items_in_chain_2 else null end Prcnt_of_user_disposal_items_3 -- Value to display 
	from m_q5_filterd_5_user_disposal_item cross join all_users_items
)as inn1
