
with rp3_chain_qty_items_by_classification as
(
select  
    item_format_type_classification_code, item_format_type_classification_name,
    count(*) as num_item_in_classification , sum(count(*)) over() as total_num_item
    ,cast(count(*) as double precision)/(sum(count(*)) over()) as share_of_num_classitems_from_total
from(
    select dw_item_key, format_code  
    ,item_format_type_classification_code        
    ,item_format_type_classification_name  
    from migvan.assortment_item_store_indices 
    where item_format_top_classification_code in (1,2)
    and format_code = :formatParam
    group by dw_item_key, format_code, item_format_type_classification_code, item_format_type_classification_name
) as t
group by item_format_type_classification_code, item_format_type_classification_name
),
rp3_user_qty_items_by_classification as
(
select  item_format_type_classification_code, 
    item_format_type_classification_name  ,
    count(*) as user_num_item_in_classification ,
    sum(count(*)) over() user_total_num_item
    ,cast(count(*) as double precision)/(sum(count(*)) over()) as user_share_of_num_classitems_from_total
from(
    select dw_item_key, format_code  
    ,item_format_type_classification_code        
    ,item_format_type_classification_name   
    from migvan.assortment_item_store_indices 
    where item_format_top_classification_code in (1,2)
  and {FILTER_PLACEHOLDER}
    group by dw_item_key, format_code ,item_format_type_classification_code , item_format_type_classification_name
) as b
group by item_format_type_classification_code, item_format_type_classification_name
)

select cq.item_format_type_classification_code,cq.item_format_type_classification_name
,num_item_in_classification,total_num_item, share_of_num_classitems_from_total
,user_num_item_in_classification, user_total_num_item, user_share_of_num_classitems_from_total
,((1+user_share_of_num_classitems_from_total)/(1+share_of_num_classitems_from_total))-1  share_of_change_num_items_user_to_chain

,case when user_share_of_num_classitems_from_total is null then 0-share_of_num_classitems_from_total 
      when share_of_num_classitems_from_total is null then user_share_of_num_classitems_from_total-0 else user_share_of_num_classitems_from_total-share_of_num_classitems_from_total end as share_of_difference_num_items_user_to_chain
,case when user_share_of_num_classitems_from_total is null then user_num_item_in_classification*(0-share_of_num_classitems_from_total) 
      when share_of_num_classitems_from_total is null then user_num_item_in_classification*(user_share_of_num_classitems_from_total-0)
      else user_num_item_in_classification*(user_share_of_num_classitems_from_total-share_of_num_classitems_from_total) end as num_items_to_get_chain_prcnt
from rp3_chain_qty_items_by_classification cq left join rp3_user_qty_items_by_classification uq on cq.item_format_type_classification_code=uq.item_format_type_classification_code 
and cq.item_format_type_classification_name=uq.item_format_type_classification_name
