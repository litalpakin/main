select
format_code, dw_store_key, dw_item_key, item_id, item_format_classification_code, item_format_classification_name
,sub_category_key, category_key, class_key, sub_brand_hierarchy_2_key, supplier_key
,robi_prioritization_category,item_store_availability_cq
,item_store_sales_cq ,sales_ms_item_category_store_cq,ranking_item_category_store_cq
,sales_ms_item_category_format_cq,ranking_item_category_format_cq

,final_faces_allocation
,percent_of_volume

,case when num_allocated_must_items<>num_must_items then 1 else 0 end not_all_must_items_allocated
,case when (num_must_items+0.5*num_adequate_items)-(num_allocated_must_items+num_allocated_adequate_items)>0 then 1 else 0 end not_all_must_and_adequate_items_allocated
,case when num_allocated_must_items=num_must_items and num_allocated_adequate_items=num_adequate_items and total_volume_allocation_phase_2<total_volume then 1 else 0 end free_space_on_the_stand

,max_width_mm
,max_height_mm
,max_depth_mm

from(
select *
,FLOOR(final_volume_allocation_phase_2/item_actual_face_row_volume) as final_faces_allocation  --AO col
,final_volume_allocation_phase_2/total_volume as percent_of_volume --AP col
,sum(case when phase_2_valid_item=1 then final_volume_allocation_phase_2 end) over() total_volume_allocation_phase_2

from (
	select *
	,case when final_volume_allocation_phase_1>0 then total_item_actual_required_volume else total_item_actual_required_volume+final_volume_allocation_phase_1 end as final_volume_allocation_phase_2  --AN col



	from (
		select *
		,total_volume - cum_final_total_item_actual_required_volume_phase_1 as final_volume_allocation_phase_1 --AM col
	from(
			select *
			,sum(case when phase_2_valid_item=1 and item_format_classification_code in (1,2,6,9,26) then 1 else 0 end) over() num_allocated_must_items
			,sum(case when phase_2_valid_item=1 and item_format_classification_code in (3,7,10) then 1 else 0 end) over() num_allocated_adequate_items
			,sum(total_item_actual_required_volume) over (partition by category_key,dw_store_key,format_code order by item_num_of_needed_faces asc nulls last , item_face_row_units asc nulls last  rows UNBOUNDED PRECEDING ) as cum_final_total_item_actual_required_volume_phase_1

			from(
				select *
				--,case when phase_1_valid_item=1 or first_non_covered_item=1 then 1 else 0 end phase_2_valid_item --AC col
				,case when phase_1_valid_item=1 then 1 else 0 end phase_2_valid_item --AC col

				from(
					select *
					,case when phase_1_available_volume_left>=0 then 1 else 0 end phase_1_valid_item --Z col
					,min(case when phase_1_available_volume_left>0 then phase_1_available_volume_left end) over (partition by category_key,dw_store_key,format_code) extra_volume --AA col
					,case when phase_1_available_volume_left = max(case when phase_1_available_volume_left<0 then phase_1_available_volume_left end) over (partition by category_key,dw_store_key,format_code) then 1
					else 0 end first_non_covered_item --AB col
					from(
						select *
						,total_volume-cum_total_item_actual_required_volume_1 as phase_1_available_volume_left --Y col
						from(
							select *
							,case when total_item_actual_required_volume is not null then sum(total_item_actual_required_volume) over (partition by category_key,dw_store_key,format_code order by robi_prioritization_category asc,total_item_actual_required_volume desc nulls last rows UNBOUNDED PRECEDING ) end as cum_total_item_actual_required_volume_1
							from(
								select *
								,item_actual_face_row_volume * item_num_of_needed_faces as total_item_actual_required_volume --X col
								from(
									select *
									,ceil((item_unit_volume * num_of_units_adjusted_to_week)/cat_in_store_shelf_arrange_freq) as total_item_net_required_volume  --S col
									,item_unit_volume*item_face_row_units as item_net_face_row_volume --T col
									,case when cat_in_store_shelf_arrange_freq=0 or item_face_row_units=0 or item_face_row_units is null or cat_in_cluster_faces_multiplication=0 then null else GREATEST (ceil(((num_of_units_adjusted_to_week/cat_in_store_shelf_arrange_freq)/item_face_row_units)/cat_in_cluster_faces_multiplication)*cat_in_cluster_faces_multiplication , cat_in_cluster_min_faces_allocation) end as item_num_of_needed_faces --W col

									from (
										select *
										,cat_in_store_num_of_fields * stand_volume as total_volume --Q col
										,width_mm * height_mm * depth_mm as item_unit_volume --R col
										,FLOOR(cat_in_store_field_depth/depth_mm) as item_face_row_units --V col
										,cat_in_store_field_depth * width_mm * height_mm as item_actual_face_row_volume --U col
										from (
											select *
											----calc indices-----
											,cat_in_store_field_width * cat_in_store_field_height * cat_in_store_field_depth as stand_volume  --P col
											from
											(
											select format_code, dw_store_key, dw_item_key, item_id, item_format_classification_code, item_format_classification_name
											,sub_category_key, category_key, class_key, sub_brand_hierarchy_2_key, supplier_key
											,robi_prioritization_category, num_of_units_adjusted_to_week,item_store_availability_cq
											,item_store_sales_cq ,sales_ms_item_category_store_cq,ranking_item_category_store_cq
											,sales_ms_item_category_format_cq,ranking_item_category_format_cq
											,width_mm, height_mm, depth_mm

											,sum(case when item_format_classification_code in (1,2,6,9,26) then 1 else 0 end) over() num_must_items
											,sum(case when item_format_classification_code in (3,7,10) then 1 else 0 end) over() num_adequate_items
											
											,max(width_mm) over() max_width_mm
											,max(height_mm) over() max_height_mm
											,max(depth_mm) over() max_depth_mm

											----user's inputs-----
											, cast ({CAT_IN_STORE_NUM_OF_FIELDS_PLACEHOLDER} as double precision) as cat_in_store_num_of_fields
											, cast ({CAT_IN_STORE_FIELD_WIDTH_PLACEHOLDER} as double precision) as cat_in_store_field_width
											, cast ({CAT_IN_STORE_FIELD_HEIGHT_PLACEHOLDER} as double precision) as cat_in_store_field_height
											, cast ({CAT_IN_STORE_FIELD_DEPTH_PLACEHOLDER} as double precision) as cat_in_store_field_depth
											, cast ({CAT_IN_STORE_SHELF_ARRANGE_FREQ_PLACEHOLDER} as double precision) as cat_in_store_shelf_arrange_freq
											, cast ({CAT_IN_CLUSTER_FACES_MULTIPLICATION_PLACEHOLDER} as double precision) as cat_in_cluster_faces_multiplication
											, cast ({CAT_IN_CLUSTER_MIN_FACES_ALLOCATION_PLACEHOLDER} as double precision) as cat_in_cluster_min_faces_allocation


											from migvan.item_format_planogram_base
											where format_code = {FORMAT_CODE_PLACEHOLDER} and dw_store_key={DW_STORE_KEY_PLACEHOLDER} and category_key = {CATEGORY_KEY_PLACEHOLDER}
											and {USER_CLUSTER_AND_CATALOG_FILTER_PLACEHOLDER}
											) as sub1
										) as sub2
									) as sub3
								) as sub4
							) as sub5
						) as sub6
					) as sub7
				) as sub8
			) as sub9
		where phase_2_valid_item=1
		--order by item_num_of_needed_faces asc nulls last , item_face_row_units asc nulls last
		order by robi_prioritization_category asc nulls last 
		) as sub10
	) as sub11
) as sub12
) as sub13
--order by item_num_of_needed_faces asc nulls last , item_face_row_units asc nulls last
order by robi_prioritization_category asc nulls last 