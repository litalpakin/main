select dw_store_key,is_same_store,item_format_classification_code
from migvan.assortment_item_store_indices 
where {FILTER_PLACEHOLDER}
--and   user's selections : formats,stores, catalog
--and   user's permission : formats,stores, catalog
group by dw_store_key,is_same_store,item_format_classification_code
order by dw_store_key,item_format_classification_code