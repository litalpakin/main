     select *
    , sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cy end) over() bad_items_count_in_format_cy
    , sum(items_count_in_format_by_type_classification_cy) over() items_count_in_format_cy

    , case when coalesce(sum(items_count_in_format_by_type_classification_cy) over(),0) <= 0 or
      coalesce(sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cy end) over(),0) <= 0 then null else
      cast(sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cy end) over() as double precision)/(sum(items_count_in_format_by_type_classification_cy) over()) end  share_of_bad_items_in_format_cy

    , sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cy end) over()  great_items_count_in_format_cy
    , case when coalesce(sum(items_count_in_format_by_type_classification_cy) over(),0) <= 0 or coalesce(sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cy end) over(),0) <= 0 then null else
      cast(sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cy end) over() as double precision)/(sum(items_count_in_format_by_type_classification_cy) over()) end  share_of_great_items_in_format_cy

    , sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cq end) over() bad_items_count_in_format_cq
    , case when coalesce(sum(items_count_in_format_by_type_classification_cq) over(),0) <= 0 or coalesce(sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cq end) over() ,0) <= 0 then null else
      cast(sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_cq end) over()  as double precision)/(sum(items_count_in_format_by_type_classification_cq) over()) end  share_of_bad_items_in_format_cq

    , sum(items_count_in_format_by_type_classification_cq) over() items_count_in_format_cq
    , sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cq end) over()  great_items_count_in_format_cq
    , case when coalesce(sum(items_count_in_format_by_type_classification_cq) over(),0) <= 0 or
      coalesce(sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cq end) over(),0) <= 0 then null else
      cast((sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_cq end) over()) as double precision)/(sum(items_count_in_format_by_type_classification_cq) over()) end  share_of_great_items_in_format_cq

    , sum(items_count_in_format_by_type_classification_month0) over() items_count_in_format_month0
    , sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_month0 end) over() bad_items_count_in_format_month0
    , case when coalesce(sum(items_count_in_format_by_type_classification_month0) over(),0) <= 0 or coalesce(sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_month0 end) over(),0) <= 0 then null else
      cast((sum(case when item_format_type_classification_code in (4,5,8) then items_count_in_format_by_type_classification_month0 end) over()) as double precision)/(sum(items_count_in_format_by_type_classification_month0) over()) end  share_of_bad_items_in_format_month0

    , sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_month0 end) over()  great_items_count_in_format_month0
    , case when coalesce(sum(items_count_in_format_by_type_classification_month0) over(),0) <= 0 or
      coalesce(sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_month0 end) over(),0) <= 0 then null else
      cast((sum(case when item_format_type_classification_code in (1,2,6) then items_count_in_format_by_type_classification_month0 end) over()) as double precision)/(sum(items_count_in_format_by_type_classification_month0) over()) end  share_of_great_items_in_format_month0

    , sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cy end) over() bad_sales_in_format_cy
    , case when coalesce((sum(sales_in_format_by_type_classification_cy) over()),0) <= 0 or
      coalesce(sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cy end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cy end) over()) /(sum(sales_in_format_by_type_classification_cy) over()) end  share_of_sales_bad_items_in_format_cy

    , sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cy end) over()  great_sales_in_format_cy
    , case when coalesce(sum(sales_in_format_by_type_classification_cy) over(),0) <= 0 or
    coalesce(sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cy end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cy end) over()) /(sum(sales_in_format_by_type_classification_cy) over()) end  share_of_sales_great_items_in_format_cy

    , sum(sales_in_format_by_type_classification_cq) over() total_sales_in_format_cq
    , sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cq end) over() bad_sales_in_format_cq
    , case when coalesce(sum(sales_in_format_by_type_classification_cq) over(),0) <= 0 or
    coalesce(sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cq end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_cq end) over()) /(sum(sales_in_format_by_type_classification_cq) over()) end  share_of_sales_bad_items_in_format_cq

    , sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cq end) over()  great_sales_in_format_cq
    , case when coalesce(sum(sales_in_format_by_type_classification_cq) over(),0) <= 0 or
    coalesce(sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cq end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_cq end) over()) / (sum(sales_in_format_by_type_classification_cq) over()) end  share_of_sales_great_items_in_format_cq

    , sum(sales_in_format_by_type_classification_month0) over() total_sales_in_format_month0
    , sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_month0 end) over() bad_sales_in_format_month0
    , case when coalesce(sum(sales_in_format_by_type_classification_month0) over(),0) <= 0 or
    coalesce(sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_month0 end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (4,5,8) then sales_in_format_by_type_classification_month0 end) over()) /(sum(sales_in_format_by_type_classification_month0) over()) end  share_of_sales_bad_items_in_format_month0

    , sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_month0 end) over()  great_sales_in_format_month0
    , case when coalesce(sum(sales_in_format_by_type_classification_month0) over(),0) <= 0 or
    coalesce(sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_month0 end) over(),0) <= 0 then null else
      (sum(case when item_format_type_classification_code in (1,2,6) then sales_in_format_by_type_classification_month0 end) over()) / (sum(sales_in_format_by_type_classification_month0) over()) end  share_of_sales_great_items_in_format_month0

    , case when coalesce(sum(items_count_in_format_by_type_classification_cy) over(),0) <= 0 or coalesce(items_count_in_format_by_type_classification_cy,0) <= 0 then null else
      cast(items_count_in_format_by_type_classification_cy as double precision)/(sum(items_count_in_format_by_type_classification_cy) over()) end  share_of_items_count_in_format_by_type_classification_cy
    , case when coalesce(sum(items_count_in_format_by_type_classification_cq) over(),0) <= 0 or coalesce(sum(items_count_in_format_by_type_classification_cq) over(),0) <= 0 then null else
      cast(items_count_in_format_by_type_classification_cq as double precision) /(sum(items_count_in_format_by_type_classification_cq) over())
      end share_of_items_count_in_format_by_type_classification_cq


    , case when coalesce(sum(items_count_in_format_by_type_classification_month0) over(),0) <= 0 or coalesce(items_count_in_format_by_type_classification_month0,0) <= 0 then null else
     cast(items_count_in_format_by_type_classification_month0 as double precision)/(sum(items_count_in_format_by_type_classification_month0) over()) end share_of_items_count_in_format_by_type_classification_month0

    , sum(sales_in_format_by_type_classification_cy) over() total_sales_in_format_cy
    , case when coalesce(sum(sales_in_format_by_type_classification_cy) over(),0) <= 0 or coalesce(sales_in_format_by_type_classification_cy,0) <= 0 then null else
      sales_in_format_by_type_classification_cy /(sum(sales_in_format_by_type_classification_cy) over()) end  share_of_sales_in_format_by_type_classification_cy

    , case when coalesce(sum(sales_in_format_by_type_classification_cq) over(),0) <= 0 or coalesce(sales_in_format_by_type_classification_cq,0) <= 0 then null else
      sales_in_format_by_type_classification_cq /(sum(sales_in_format_by_type_classification_cq) over()) end share_of_sales_in_format_by_type_classification_cq


    , case when coalesce(sum(sales_in_format_by_type_classification_month0) over(),0) <= 0 or coalesce(sales_in_format_by_type_classification_month0,0) <= 0 then null else
      sales_in_format_by_type_classification_month0 /(sum(sales_in_format_by_type_classification_month0) over()) end share_of_sales_in_format_by_type_classification_month0

    from(
        SELECT  item_format_type_classification_code, item_format_type_classification_name
        , sum(case when sales_item_format_cy>0 then 1 else 0 end)  items_count_in_format_by_type_classification_cy


        , sum(case when sales_item_format_cq>0 then 1 else 0 end ) items_count_in_format_by_type_classification_cq

        , sum(case when sales_item_format_month0>0 then 1 else 0 end ) items_count_in_format_by_type_classification_month0

        , sum(case when sales_item_format_cy>0 then sales_item_format_cy end)  sales_in_format_by_type_classification_cy

        , sum(case when sales_item_format_cq>0 then sales_item_format_cq end ) sales_in_format_by_type_classification_cq

        , sum(case when sales_item_format_month0>0 then sales_item_format_month0 end ) sales_in_format_by_type_classification_month0

        FROM migvan.assortment_item_format_indices
         where item_format_type_classification_code<>24 and item_format_type_classification_code<>25 and item_format_type_classification_code<>26
          and format_code = :formatParam
           {FILTER_PLACEHOLDER}
         
         group by format_code, item_format_type_classification_code, item_format_type_classification_name
) t
