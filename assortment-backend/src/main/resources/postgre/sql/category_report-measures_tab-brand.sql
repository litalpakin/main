select *
,case when rank_ms_potential_sales_brand_in_selectedworld_format_cy=0 then null else rank_ms_potential_sales_brand_in_selectedworld_format_cy ||' / '|| num_brand_in_selectedworld_format_cy end as ranking_ms_potential_sales_brand_in_selectedworld_format_cy
,case when rank_sales_brand_in_selectedworld_format_cq=0 then null else rank_sales_brand_in_selectedworld_format_cq ||' / '|| num_brand_in_selectedworld_format_cq end as ranking_sales_brand_in_selectedworld_format_cq
,case when rank_qty_brand_in_selectedworld_format_cq=0 then null else rank_qty_brand_in_selectedworld_format_cq ||' / '|| num_brand_in_selectedworld_format_cq end as ranking_qty_brand_in_selectedworld_format_cq
,case when rank_ms_potential_sales_brand_in_selectedworld_format_cq=0 then null else rank_ms_potential_sales_brand_in_selectedworld_format_cq ||' / '|| num_brand_in_selectedworld_format_cq end as ranking_ms_potential_sales_brand_in_selectedworld_format_cq
,case when rank_sales_brand_in_selectedworld_format_month0=0 then null else rank_sales_brand_in_selectedworld_format_month0 ||' / '|| num_brand_in_selectedworld_format_month0 end as ranking_sales_brand_in_selectedworld_format_month0
,case when rank_qty_brand_in_selectedworld_format_month0=0 then null else rank_qty_brand_in_selectedworld_format_month0 ||' / '|| num_brand_in_selectedworld_format_month0 end as ranking_qty_brand_in_selectedworld_format_month0

from(
  select *,
	case when coalesce(sales_ms_brand_selectedworld_format_py,0) <= 0 or
    coalesce(sales_ms_brand_selectedworld_format_cy,0) <= 0 then null else
    ((sales_ms_brand_selectedworld_format_cy+1)/(sales_ms_brand_selectedworld_format_py+1))-1
    end sales_ms_change_brand_selectedworld_format_cypy
	,case when coalesce(sales_ms_brand_selectedworld_format_pq,0) <= 0 or
    coalesce(sales_ms_brand_selectedworld_format_cq,0) <= 0 then null else
    ((sales_ms_brand_selectedworld_format_cq+1)/(sales_ms_brand_selectedworld_format_pq+1))-1
    end sales_ms_change_brand_selectedworld_format_cqpq
	,case when coalesce(sales_ms_brand_selectedworld_format_month1,0) <= 0 or
    coalesce(sales_ms_brand_selectedworld_format_month0,0) <= 0 then null
    else
    ((sales_ms_brand_selectedworld_format_month0+1)/(sales_ms_brand_selectedworld_format_month1+1))-1
    end sales_ms_change_brand_selectedworld_format_month0to1
	--,sales_ms_brand_selectedworld_format_py
	--,sales_ms_brand_selectedworld_format_cq
	--,sales_ms_brand_selectedworld_format_pq
	--,sales_ms_brand_selectedworld_format_month0
	--,sales_ms_brand_selectedworld_format_month1
	--,relative_qty_item_selectedworld_format_cy
	--,relative_qty_item_selectedworld_format_py
	--,relative_qty_item_selectedworld_format_cq
	--,relative_qty_item_selectedworld_format_pq
	--,relative_qty_item_selectedworld_format_month0
	--,relative_qty_item_selectedworld_format_month1
	--, ms_potential_sales_item_selectedworld_format_cy
	--, ms_potential_sales_item_selectedworld_format_cq

	--, qty_price_brand_in_selectedworld_format_market_cy
	--,case when qty_price_brand_in_selectedworld_format_cy<=0 or
    --qty_price_brand_in_selectedworld_format_cy is null or
    --qty_price_brand_in_selectedworld_format_market_cy<=0 or
    --qty_price_brand_in_selectedworld_format_market_cy is null then null
   -- else(qty_price_brand_in_selectedworld_format_cy/qty_price_brand_in_selectedworld_format_market_cy)-1
    --end qty_price_change_brand_selectedworld_format_chaintomarket_cy

	--,qty_price_brand_in_selectedworld_format_cq
	--,qty_price_brand_in_selectedworld_format_market_cq
	--,case when qty_price_brand_in_selectedworld_format_cq<=0 or
    --qty_price_brand_in_selectedworld_format_cq is null or
    --qty_price_brand_in_selectedworld_format_market_cq<=0 or
    --qty_price_brand_in_selectedworld_format_market_cq is null then null
    --else(qty_price_brand_in_selectedworld_format_cq/qty_price_brand_in_selectedworld_format_market_cq)-1
    --end qty_price_change_brand_selectedworld_format_chaintomarket_cq

	from (
          SELECT *
    ,sum(case when sales_brand_in_selectedworld_format_month0>0 then 1 else 0 end) over() num_brand_in_selectedworld_format_month0
    ,rank() over (order by sales_brand_in_selectedworld_format_month0 desc)- max (case when sales_brand_in_selectedworld_format_month0 is null then 1 else 0 end) over () rank_sales_brand_in_selectedworld_format_month0

        --, sales_brand_in_selectedworld_format_month1
    ,sum(case when sales_brand_in_selectedworld_format_cq>0 then 1 else 0 end) over() num_brand_in_selectedworld_format_cq
    ,rank() over (order by sales_brand_in_selectedworld_format_cq desc)- max (case when sales_brand_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_sales_brand_in_selectedworld_format_cq

    ,rank() over (order by sales_brand_in_selectedworld_format_cy desc)- max (case when sales_brand_in_selectedworld_format_cy is null then 1 else 0 end) over () rank_sales_brand_in_selectedworld_format_cy
   ,case when (
    rank() over (order by sales_brand_in_selectedworld_format_cy desc)- max (case when sales_brand_in_selectedworld_format_cy is null then 1 else 0 end) over () )    =0 then null
    else (rank() over (order by sales_brand_in_selectedworld_format_cy desc)- max (case when sales_brand_in_selectedworld_format_cy is null then 1 else 0 end) over ()) ||' / '|| (sum(case when sales_brand_in_selectedworld_format_cy>=0 then 1 else 0 end) over()) end as ranking_sales_brand_in_selectedworld_format_cy

        ,sum(sales_brand_in_selectedworld_format_cy) over () sales_selectedworld_format_cy
        ,sum(sales_brand_in_selectedworld_format_py) over () sales_selectedworld_format_py
        ,sum(sales_brand_in_selectedworld_format_cq) over () sales_selectedworld_format_cq
        ,sum(sales_brand_in_selectedworld_format_pq) over () sales_selectedworld_format_pq
        ,sum(sales_brand_in_selectedworld_format_month0) over () sales_selectedworld_format_month0
        ,sum(sales_brand_in_selectedworld_format_month1) over () sales_selectedworld_format_month1

        ,case when coalesce(sum(sales_brand_in_selectedworld_format_cy) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_cy,0) <= 0 then null else
          (sales_brand_in_selectedworld_format_cy/(sum(sales_brand_in_selectedworld_format_cy) over ()))
			end sales_ms_brand_selectedworld_format_cy
        ,case when coalesce(sum(sales_brand_in_selectedworld_format_py) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_py,0) <= 0 then null else
          (sales_brand_in_selectedworld_format_py/(sum(sales_brand_in_selectedworld_format_py) over ()))
			end sales_ms_brand_selectedworld_format_py
        ,case when coalesce(sum(sales_brand_in_selectedworld_format_cq) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_cq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_cq/(sum(sales_brand_in_selectedworld_format_cq) over ()))
			end sales_ms_brand_selectedworld_format_cq
        ,case when coalesce(sum(sales_brand_in_selectedworld_format_pq) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_pq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_pq/(sum(sales_brand_in_selectedworld_format_pq) over ()))
			end sales_ms_brand_selectedworld_format_pq
        ,case when coalesce(sum(sales_brand_in_selectedworld_format_month0) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_month0,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_month0/(sum(sales_brand_in_selectedworld_format_month0) over ()))
			end sales_ms_brand_selectedworld_format_month0
        ,case when coalesce(sum(sales_brand_in_selectedworld_format_month1) over (),0) <= 0 or
          coalesce(sales_brand_in_selectedworld_format_month1,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_month1/(sum(sales_brand_in_selectedworld_format_month1) over ()))
			end sales_ms_brand_selectedworld_format_month1

        ,case when coalesce(sales_brand_in_selectedworld_format_py,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_cy,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_cy-sales_brand_in_selectedworld_format_py) end sales_difference_brand_in_selectedworld_format_cypy
        ,case when coalesce(sales_brand_in_selectedworld_format_pq,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_cq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_cq-sales_brand_in_selectedworld_format_pq) end sales_difference_brand_in_selectedworld_format_cqpq
        ,case when coalesce(sales_brand_in_selectedworld_format_month1,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_month0,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_month0-sales_brand_in_selectedworld_format_month1) end sales_difference_brand_in_selectedworld_format_month0to1

        ,case when coalesce(sales_brand_in_selectedworld_format_py,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_cy,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_cy/sales_brand_in_selectedworld_format_py)-1
		end sales_change_brand_in_selectedworld_format_cypy
        ,case when coalesce(sales_brand_in_selectedworld_format_pq,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_cq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_cq/sales_brand_in_selectedworld_format_pq)-1
		end sales_change_brand_in_selectedworld_format_cqpq
        ,case when coalesce(sales_brand_in_selectedworld_format_month1,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_month0,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_month0/sales_brand_in_selectedworld_format_month1)-1
		end sales_change_brand_in_selectedworld_format_month0to1

        --, sales_brand_in_selectedworld_format_ss_cy
        --, sales_brand_in_selectedworld_format_ss_py
        --, sales_brand_in_selectedworld_format_ss_cq
        --, sales_brand_in_selectedworld_format_ss_pq
        --, sales_brand_in_selectedworld_format_ss_month0
        --, sales_brand_in_selectedworld_format_ss_month1
--		  ,case when coalesce(sales_brand_in_selectedworld_format_month0,0)<=0 or coalesce(qty_brand_in_selectedworld_format_month0,0)<=0 then null
--    		else sales_brand_in_selectedworld_format_month0/qty_brand_in_selectedworld_format_month0
--    		end qty_price_brand_in_selectedworld_format_month0
      ,dense_rank() over (order by qty_brand_in_selectedworld_format_month0 desc)- max (case when qty_brand_in_selectedworld_format_month0 is null then 1 else 0 end) over () rank_qty_brand_in_selectedworld_format_month0

        ,case when coalesce(sales_brand_in_selectedworld_format_ss_py,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_cy,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_cy-sales_brand_in_selectedworld_format_ss_py) end sales_difference_brand_in_selectedworld_format_ss_cypy
        ,case when coalesce(sales_brand_in_selectedworld_format_ss_pq,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_cq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_cq-sales_brand_in_selectedworld_format_ss_pq) end sales_difference_brand_in_selectedworld_format_ss_cqpq
        ,case when coalesce(sales_brand_in_selectedworld_format_ss_month1,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_month0,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_month0-sales_brand_in_selectedworld_format_ss_month1) end sales_difference_brand_in_selectedworld_format_ss_month0to1

        ,case when coalesce(sales_brand_in_selectedworld_format_ss_py,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_cy,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_cy/sales_brand_in_selectedworld_format_ss_py)-1 end sales_change_brand_in_selectedworld_format_ss_cypy
        ,case when coalesce(sales_brand_in_selectedworld_format_ss_pq,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_cq,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_cq/sales_brand_in_selectedworld_format_ss_pq)-1 end sales_change_brand_in_selectedworld_format_ss_cqpq
        ,case when coalesce(sales_brand_in_selectedworld_format_ss_month1,0) <= 0 or coalesce(sales_brand_in_selectedworld_format_ss_month0,0) <= 0 then null else
        (sales_brand_in_selectedworld_format_ss_month0/sales_brand_in_selectedworld_format_ss_month1)-1 end sales_change_brand_in_selectedworld_format_ss_month0to1
        ,sum(qty_brand_in_selectedworld_format_cy) over () qty_selectedworld_format_cy
        ,sum(qty_brand_in_selectedworld_format_py) over () qty_selectedworld_format_py
        ,sum(qty_brand_in_selectedworld_format_cq) over () qty_selectedworld_format_cq
        ,sum(qty_brand_in_selectedworld_format_pq) over () qty_selectedworld_format_pq
        ,sum(qty_brand_in_selectedworld_format_month0) over () qty_selectedworld_format_month0
        ,sum(qty_brand_in_selectedworld_format_month1) over () qty_selectedworld_format_month1
		--,qty_price_brand_in_selectedworld_format_market_month0
		,dense_rank() over (order by qty_brand_in_selectedworld_format_cq desc)- max (case when qty_brand_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_qty_brand_in_selectedworld_format_cq
    ,dense_rank() over (order by qty_brand_in_selectedworld_format_cy desc)- max (case when qty_brand_in_selectedworld_format_cy is null then 1 else 0 end) over () rank_qty_brand_in_selectedworld_format_cy
    ,case when (dense_rank() over (order by qty_brand_in_selectedworld_format_cy desc)- max (case when qty_brand_in_selectedworld_format_cy is null then 1 else 0 end) over ()) =0 then null
     else (dense_rank() over (order by qty_brand_in_selectedworld_format_cy desc)- max (case when qty_brand_in_selectedworld_format_cy is null then 1 else 0 end) over ())
         ||' / '|| (sum(case when sales_brand_in_selectedworld_format_cy>=0 then 1 else 0 end) over())
     end as ranking_qty_brand_in_selectedworld_format_cy
      ,sum(case when sales_brand_in_selectedworld_format_cy>=0 then 1 else 0 end) over() num_brand_in_selectedworld_format_cy

		,case when coalesce(sum(qty_brand_in_selectedworld_format_month0) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_month0,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_month0 as double precision)/(sum(qty_brand_in_selectedworld_format_month0) over ()))
			end relative_qty_item_selectedworld_format_month0
		,case when coalesce(sum(qty_brand_in_selectedworld_format_month1) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_month1,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_month1 as double precision)/sum(qty_brand_in_selectedworld_format_month1) over ())
			end relative_qty_item_selectedworld_format_month1
      	--,case when coalesce(sales_brand_in_selectedworld_format_cy,0)<=0 or
        --    coalesce(qty_brand_in_selectedworld_format_cy,0)<=0 then null
         --   else sales_brand_in_selectedworld_format_cy/qty_brand_in_selectedworld_format_cy
         --   end qty_price_brand_in_selectedworld_format_cy
		,case when coalesce(sum(qty_brand_in_selectedworld_format_cy) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_cy,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_cy as double precision)/(sum(qty_brand_in_selectedworld_format_cy) over ()))
			end relative_qty_item_selectedworld_format_cy
		,case when coalesce(sum(qty_brand_in_selectedworld_format_py) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_cy,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_py as double precision)/(sum(qty_brand_in_selectedworld_format_py) over ()))
			end relative_qty_item_selectedworld_format_py
		,case when coalesce(sum(qty_brand_in_selectedworld_format_cq) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_cq,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_cq as double precision)/(sum(qty_brand_in_selectedworld_format_cq) over ()))
			end relative_qty_item_selectedworld_format_cq
		,case when coalesce(sum(qty_brand_in_selectedworld_format_pq) over (),0) <= 0 or
			coalesce(qty_brand_in_selectedworld_format_pq,0) <= 0 then null else
			(cast(qty_brand_in_selectedworld_format_pq as double precision)/(sum(qty_brand_in_selectedworld_format_pq) over ()))
			end relative_qty_item_selectedworld_format_pq

        --, potential_sales_brand_in_selectedworld_format_cy
        --, potential_sales_brand_in_selectedworld_format_cq
        --, qty_price_brand_in_selectedworld_format_month0
      ,dense_rank() over (order by potential_sales_brand_in_selectedworld_format_cq desc)- max (case when potential_sales_brand_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_ms_potential_sales_brand_in_selectedworld_format_cq

        ,sum(potential_sales_brand_in_selectedworld_format_cy) over () potential_sales_selectedworld_format_cy
        ,sum(potential_sales_brand_in_selectedworld_format_cq) over () potential_sales_selectedworld_format_cq
		,case when coalesce(sum(potential_sales_brand_in_selectedworld_format_cy) over (),0) <= 0 or
			coalesce(potential_sales_brand_in_selectedworld_format_cy,0) <= 0 then null else
			(potential_sales_brand_in_selectedworld_format_cy/(sum(potential_sales_brand_in_selectedworld_format_cy) over ()))
			end ms_potential_sales_item_selectedworld_format_cy
		,case when coalesce(sum(potential_sales_brand_in_selectedworld_format_cq) over (),0) <= 0 or
			coalesce(potential_sales_brand_in_selectedworld_format_cq,0) <= 0 then null else
			(potential_sales_brand_in_selectedworld_format_cq/(sum(potential_sales_brand_in_selectedworld_format_cq) over ()))
			end ms_potential_sales_item_selectedworld_format_cq
  	--,case when coalesce(qty_price_brand_in_selectedworld_format_month0,0)<=0 or
	   -- coalesce(qty_price_brand_in_selectedworld_format_market_month0,0)<=0 then null
		--	else(qty_price_brand_in_selectedworld_format_month0/qty_price_brand_in_selectedworld_format_market_month0)-1
		--  end qty_price_change_brand_selectedworld_format_chaintomarket_m0
		,case when potential_sales_brand_in_selectedworld_format_cy<sales_brand_in_selectedworld_format_cy then null 
		else potential_sales_brand_in_selectedworld_format_cy-sales_brand_in_selectedworld_format_cy end inc_for_sales_from_potential_implementaion_in_selectedworld_cy
 
		,case when potential_sales_brand_in_selectedworld_format_cq<sales_brand_in_selectedworld_format_cq then null 
		else potential_sales_brand_in_selectedworld_format_cq-sales_brand_in_selectedworld_format_cq end inc_for_sales_from_potential_implementaion_in_selectedworld_cq

	    FROM (
        SELECT {CATALOG_FIELDS}
        ,	FORMAT_CODE
       
        ,	SUM(SALES_ITEM_FORMAT_CY)			SALES_BRAND_IN_SELECTEDWORLD_FORMAT_CY
        ,	SUM(SALES_ITEM_FORMAT_PY)			SALES_BRAND_IN_SELECTEDWORLD_FORMAT_PY
        ,	SUM(SALES_ITEM_FORMAT_CQ)			SALES_BRAND_IN_SELECTEDWORLD_FORMAT_CQ
        ,	SUM(SALES_ITEM_FORMAT_PQ)			SALES_BRAND_IN_SELECTEDWORLD_FORMAT_PQ
        ,	SUM(SALES_ITEM_FORMAT_MONTH0)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_MONTH0
        ,	SUM(SALES_ITEM_FORMAT_MONTH1)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_MONTH1
        ,	SUM(SALES_ITEM_FORMAT_SS_CY)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_CY
        ,	SUM(SALES_ITEM_FORMAT_SS_PY)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_PY
        ,	SUM(SALES_ITEM_FORMAT_SS_CQ)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_CQ
        ,	SUM(SALES_ITEM_FORMAT_SS_PQ)		SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_PQ
        ,	SUM(SALES_ITEM_FORMAT_SS_MONTH0)	SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_MONTH0
        ,	SUM(SALES_ITEM_FORMAT_SS_MONTH1)	SALES_BRAND_IN_SELECTEDWORLD_FORMAT_SS_MONTH1
        ,	SUM(QTY_ITEM_FORMAT_CY)				QTY_BRAND_IN_SELECTEDWORLD_FORMAT_CY
        ,	SUM(QTY_ITEM_FORMAT_PY)				QTY_BRAND_IN_SELECTEDWORLD_FORMAT_PY
        ,	SUM(QTY_ITEM_FORMAT_CQ)				QTY_BRAND_IN_SELECTEDWORLD_FORMAT_CQ
        ,	SUM(QTY_ITEM_FORMAT_PQ)				QTY_BRAND_IN_SELECTEDWORLD_FORMAT_PQ
        ,	SUM(QTY_ITEM_FORMAT_MONTH0)			QTY_BRAND_IN_SELECTEDWORLD_FORMAT_MONTH0
        ,	SUM(QTY_ITEM_FORMAT_MONTH1)			QTY_BRAND_IN_SELECTEDWORLD_FORMAT_MONTH1
        ,	SUM(POTENTIAL_SALES_ITEM_FORMAT_CY)	POTENTIAL_SALES_BRAND_IN_SELECTEDWORLD_FORMAT_CY
        ,	SUM(POTENTIAL_SALES_ITEM_FORMAT_CQ)	POTENTIAL_SALES_BRAND_IN_SELECTEDWORLD_FORMAT_CQ
        ,dense_rank() over (order by SUM(POTENTIAL_SALES_ITEM_FORMAT_CY) desc)- max (case when SUM(POTENTIAL_SALES_ITEM_FORMAT_CY) is null then 1 else 0 end) over () rank_ms_potential_sales_brand_in_selectedworld_format_cy


		 --   ,case when coalesce(SUM(SALES_ITEM_FORMAT_MONTH0),0)<=0 or
		 --     coalesce(SUM(QTY_ITEM_FORMAT_MONTH0),0)<=0 then null
    	--	  else SUM(SALES_ITEM_FORMAT_MONTH0)/SUM(QTY_ITEM_FORMAT_MONTH0)
    	--	end qty_price_brand_in_selectedworld_format_month0

		--  ,case when sum(salesmarket_item_format_cy)<=0 or sum(salesmarket_item_format_cy) is null or
		--	  sum(qtymarket_item_format_cy)<=0 or sum(qtymarket_item_format_cy) is null then null
		--	  else sum(salesmarket_item_format_cy)/sum(qtymarket_item_format_cy)
		--	  end qty_price_brand_in_selectedworld_format_market_cy

		 -- ,case when coalesce(sum(salesmarket_item_format_month0),0)<=0 or coalesce(sum(qtymarket_item_format_month0),0)<=0 then null
    	--	else sum(salesmarket_item_format_month0)/sum(qtymarket_item_format_month0)
    	--	end qty_price_brand_in_selectedworld_format_market_month0

		--,case when sum(salesmarket_item_format_cq)<=0 or sum(salesmarket_item_format_cq) is null or sum(qtymarket_item_format_cq)<=0 or
		--	sum(qtymarket_item_format_cq) is null then null
		--	else sum(salesmarket_item_format_cq)/sum(qtymarket_item_format_cq)
		--	end qty_price_brand_in_selectedworld_format_market_cq

		--,case when coalesce(SUM(SALES_ITEM_FORMAT_CY),0)<=0  or
		--	coalesce(SUM(QTY_ITEM_FORMAT_CQ),0)<=0 then null
		--	else SUM(SALES_ITEM_FORMAT_CQ)/SUM(SALES_ITEM_FORMAT_CY)
		--	end qty_price_brand_in_selectedworld_format_cq

		FROM
          MIGVAN.ASSORTMENT_ITEM_FORMAT_INDICES SS
        WHERE item_format_type_classification_code not in (24,25,26)  
        and format_code = :formatParam
         {FILTER_PLACEHOLDER}
        GROUP BY {GROUP_CATALOG_FIELDS}
        ,	FORMAT_CODE
        
--		HAVING SUM(SALES_ITEM_FORMAT_CY)>0
		) T
	  WHERE {TIME_FILTER_FIELD} >0
	  ) b
	) a
-- variable depends on time filter  sales_brand_in_selectedworld_format_cy>0 or sales_brand_in_selectedworld_format_cq>0 or sales_brand_in_selectedworld_format_month0>0
