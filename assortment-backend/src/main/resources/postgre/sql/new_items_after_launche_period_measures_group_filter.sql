----new_items_after_launche_period_measures_group_filter----

with selected_store_agg_2 as 
( ---------------------------------------------------------distribution fix-------------------------------------
	SELECT count(distinct case when sales_store_month0>0 then dw_store_key end) count_stores_month0
	,count(distinct case when sales_store_month1>0 then dw_store_key end) count_stores_month1
	,count(distinct case when sales_store_month2>0 then dw_store_key end) count_stores_month2
	,count(distinct case when (coalesce(sales_store_month0,0)+coalesce(sales_store_month1,0)+coalesce(sales_store_month2,0))>0 then dw_store_key end) count_stores_3months

	FROM migvan.assortment_store_indices 
	where  {STORES_FILTER_PLACEHOLDER}

)

, {SELECTED_STORE_AGG}


select *
--		item_id, item_name, dw_item_key
			
-- 			,sub_category_key
-- 			,category_key
-- 			,sub_brand_hierarchy_2_key
-- 			,class_key
-- 			,supplier_key 
			{RETAILER_CATALOG_PLACEHOLDER}
-- 			,format_code, format_name
-- 			,general_status_key, general_status_name
--
-- 			,launches_week,total_weeks_since_launche,item_format_examination_code ,item_format_classification_code ,is_item_classification_equal_for_all_format
-- 			,is_special_item
-- 			,sales_iteminselectedstores_month0,sales_iteminselectedstores_month1,sales_iteminselectedstores_month2
-- 			,sales_iteminselectedstores_ss_month0,sales_iteminselectedstores_ss_month1,sales_iteminselectedstores_ss_month2
-- 			,sales_subcatinselectedstores_month0,sales_subcatinselectedstores_month1,sales_subcatinselectedstores_month2
-- 			,sales_subcatinselectedstores_ss_month0,sales_subcatinselectedstores_ss_month1,sales_subcatinselectedstores_ss_month2
--
-- 			,sales_brandinsubcatinselectedstores_month0,sales_brandinsubcatinselectedstores_month1,sales_brandinsubcatinselectedstores_month2
-- 			,sales_brandinsubcatinselectedstores_ss_month0,sales_brandinsubcatinselectedstores_ss_month1,sales_brandinsubcatinselectedstores_ss_month2
-- 			,salechange_iteminselectedstores_month0to1,salechange_iteminselectedstores_month1to2,salechange_iteminselectedstores_month0to2
-- 			,salechange_iteminselectedstores_ss_month0to1,salechange_iteminselectedstores_ss_month1to2,salechange_iteminselectedstores_ss_month0to2
-- 			,saleschange_subcatinselectedstores_month0to1,saleschange_subcatinselectedstores_month1to2
-- 			,saleschange_subcatinselectedstores_ss_month0to1,saleschange_subcatinselectedstores_ss_month1to2
-- 			,saleschange_brandinsubcat_month0to1,saleschange_brandinsubcat_month1to2
-- 			,saleschange_brandinsubcat_ss_month0to1,saleschange_brandinsubcat_ss_month1to2
--
-- 			,qty_iteminselectedstores_month0,qty_iteminselectedstores_month1,qty_iteminselectedstores_month2
-- 			,qty_iteminselectedstores_ss_month0,qty_iteminselectedstores_ss_month1,qty_iteminselectedstores_ss_month2
-- 			,qty_subcatinselectedstores_month0,qty_subcatinselectedstores_month1,qty_subcatinselectedstores_month2
-- 			,qtychange_iteminselectedstores_month0to1,qtychange_iteminselectedstores_month1to2,qtychange_iteminselectedstores_month0to2
-- 			,qtychange_iteminselectedstores_ss_month0to1,qtychange_iteminselectedstores_ss_month1to2,qtychange_iteminselectedstores_ss_month0to2
-- 			,qtyms_iteminsubcat_month0, qtymschange_iteminsubcat_month0to1 ,qtymschange_iteminsubcat_month0to2
-- 			,salesms_iteminselectedstores_month0 , salesms_iteminselectedstores_month1,salesms_iteminselectedstores_month2
-- 			,salesms_iteminsubcat_month0 ,salesms_iteminsubcat_month1,salesms_iteminsubcat_month2,salesms_itemincategory_month0, salesms_itemincategory_month1, salesms_itemincategory_month2
--
-- 			,salesms_iteminbrand_month0,salesms_iteminbrand_month1,salesms_iteminbrand_month2
-- 			,salesms_brandinsubcat_month0 ,salesms_brandinsubcat_month1,salesms_brandinsubcat_month2
-- 			,salesmschange_iteminsubcat_month0to1, salesmschange_iteminsubcat_month1to2, salesmschange_iteminsubcat_month0to2
-- 			,salesmstrend_iteminsubcat_month0and2--,salesstorems_iteminsubcat_month0,a_stores_level_salesms_item_subcat_month0
--
-- 			,salesrank_iteminformat_month0,salesmaxrank_iteminformat_month0,salesranking_iteminformat_month0
-- 			,salesrank_iteminsubcat_month0,salesmaxrank_iteminsubcat_month0,salesranking_iteminsubcat_month0
-- 			,salesrank_iteminsubcat_month1,salesmaxrank_iteminsubcat_month1,salesranking_iteminsubcat_month1
-- 			,salesrank_iteminbrand_month0,salesmaxrank_iteminbrand_month0,salesranking_iteminbrand_month0
-- 			,salesrank_itemincategory_month0,salesmaxrank_itemincategory_month0,salesranking_itemincategory_month0
-- 			,salechange_market_iteminmarket_month0to1
-- 			,salesms_market_iteminsubcat_month0,salesms_market_iteminsubcat_month1,salesms_market_iteminsubcat_month2
--
-- 			,salesms_market_itemincategory_month0
-- 			,salesms_market_iteminbrand_month0,salesms_market_iteminbrand_month1,salesms_market_iteminbrand_month2
-- 			,salesms_market_iteminmarket_month0
-- 			,salesmschange_market_iteminsubcat_month0to1, salesmschange_market_iteminsubcat_month0to2
--
-- 			,sales_market_ranking_iteminsubcat_month0,sales_market_ranking_itemincategory_month0,sales_market_ranking_iteminmarket_month0
-- 			,salesmsdiff_iteminsubcat_cahintomarket_month0,salesmsdiff_iteminbrand_cahintomarket_month0,salesmsdiff_itemincategory_cahintomarket_month0
-- 			,salesmschange_iteminsubcat_cahintomarket_month0,salesmschange_iteminbrand_cahintomarket_month0,salesmschange_itemincategory_cahintomarket_month0
-- 			,distribution_iteminselectedstores_month0,distribution_iteminselectedstores_month1,distribution_iteminselectedstores_month2,distribution_iteminselectedstores_3months
-- 			,distribution_change_iteminselectedstores_month0to1,distribution_change_iteminselectedstores_month1to2,avg_availability_item_format_in_selectedstores_3months
-- 			,avgavailability_stores_group_a_item_format_3months,avgavailability_stores_group_b_item_format_3months,avgavailability_stores_group_c_item_format_3months
-- 			,avgavailability_ratio_stores_group_a_to_c_item_format_3months,avgsalesms_ratio_stores_group_a_to_c_item_format_3months
-- 			,item_format_sales_potential_inc_3months
--
-- 			--,salesmsdiff_iteminselectedstores_leadingtochainavg_month0
-- 			,salesmsdiff_iteminsubcat_leadingtochainavg_month0
-- 			--,salesmsdiversity_iteminselectedstores_month0
-- 			,salesmsdiversity_iteminsubcat_month0
--
-- 			,salesms_brandinsubcatformat_allformat_month0,salesms_subcatinformat_allformat_month0
--
-- 			,penetration_iteminsubcat_month0,penetration_iteminsubcat_month1,penetration_iteminsubcat_month2
-- 			,penetrationchange_iteminsubcat_month0to1,penetrationchange_iteminsubcat_month1to2
-- 			,avgsalesbasketsize_month0,avgsalesbasketsize_month1,avgsalesbasketsize_month2
-- 			,avgsalesbasketsize_month0to1,avgsalesbasketsize_month1to2
-- 			,avgqtybasketsize_month0,avgqtybasketsize_month1,avgqtybasketsize_month2
-- 			,avgqtybasketsize_month0to1,avgqtybasketsize_month1to2
-- 			,repeatbuyingrank_month0,repeatbuyingrank_month1,repeatbuyingrank_month2
-- 			,salesavgms_iteminsubcat_month0,salesavgms_iteminsubcat_month1,salesavgms_iteminsubcat_month2
--
--
-- 			,avg_sales_stores_group_a_item_format_month0,avg_sales_stores_group_b_item_format_month0,avg_sales_stores_group_c_item_format_month0
-- 			,avg_sales_stores_group_a_item_format_month1,avg_sales_stores_group_b_item_format_month1,avg_sales_stores_group_c_item_format_month1
-- 			,avg_sales_stores_group_a_item_format_month2,avg_sales_stores_group_b_item_format_month2,avg_sales_stores_group_c_item_format_month2
-- 			,avg_salesms_group_a_iteminsubcatformat_month0,avg_salesms_group_b_iteminsubcatformat_month0,avg_salesms_group_c_iteminsubcatformat_month0
-- 			,avg_salesms_group_a_iteminsubcatformat_month1,avg_salesms_group_b_iteminsubcatformat_month1,avg_salesms_group_c_iteminsubcatformat_month1
-- 			,avg_salesms_group_a_iteminsubcatformat_month2,avg_salesms_group_b_iteminsubcatformat_month2,avg_salesms_group_c_iteminsubcatformat_month2
-- 			,avg_salesms_group_a_iteminsubcatformat_3months,avg_salesms_group_b_iteminsubcatformat_3months,avg_salesms_group_c_iteminsubcatformat_3months
-- 			,avg_qty_stores_group_a_item_format_month0,avg_qty_stores_group_b_item_format_month0,avg_qty_stores_group_c_item_format_month0
-- 			,avg_qty_stores_group_a_item_format_month1,avg_qty_stores_group_b_item_format_month1,avg_qty_stores_group_c_item_format_month1
-- 			,avg_qty_stores_group_a_item_format_month2,avg_qty_stores_group_b_item_format_month2,avg_qty_stores_group_c_item_format_month2
-- 			,avgsaleschange_stores_group_a_iteminformat_month0to1,avgsaleschange_stores_group_c_iteminformat_month0to1
-- 			,avgsaleschange_stores_group_a_iteminformat_ss_month0to1,avgsaleschange_stores_group_c_iteminformat_ss_month0to1
-- 			,total_sales_inc_allstores_group_a_3months
-- 			,sales_potential_market_to_chain_month0,item_format_inc_in_chain_month0,inc_sales_by_iteminsubcat_salesms_3months
--
-- 			,qty_price_item_allformat_month0,qty_price_item_iteminselectedstores_month0,qty_price_item_format_market_month0,qty_price_change_iteminselectedstores_cahintomarket_month0
-- 			,avgqty_price_item_stores_group_a_item_format_month0,avgqty_price_item_stores_group_b_item_format_month0,avgqty_price_item_stores_group_c_item_format_month0
--
-- 			,qty_price_item_allformat_3months,qty_price_item_iteminselectedstores_3months,qty_price_item_format_market_3months,qty_price_change_iteminselectedstores_cahintomarket_3months
-- 			,avgqty_price_item_stores_group_a_item_format_3months,avgqty_price_item_stores_group_b_item_format_3months,avgqty_price_item_stores_group_c_item_format_3months
--
			,case when coalesce(distribution_iteminselectedstores_month1,0) <= 0 or coalesce(distribution_iteminselectedstores_month0,0) <= 0
			then null else (distribution_iteminselectedstores_month0/distribution_iteminselectedstores_month1)-1 end distribution_change_iteminselectedstores_month0to1
			,case when coalesce(distribution_iteminselectedstores_month2,0) <= 0 or coalesce(distribution_iteminselectedstores_month1,0) <= 0
			then null else (distribution_iteminselectedstores_month1/distribution_iteminselectedstores_month2)-1 end distribution_change_iteminselectedstores_month1to2

			,case when salesmsdiff_iteminsubcat_leadingtochainavg_month0 <=1.3  then 1
			      when salesmsdiff_iteminsubcat_leadingtochainavg_month0 > 1.3 and salesmsdiff_iteminsubcat_leadingtochainavg_month0 <= 2 then 2
			      when salesmsdiff_iteminsubcat_leadingtochainavg_month0 > 2  and salesmsdiff_iteminsubcat_leadingtochainavg_month0 <= 3 then 3
			      when salesmsdiff_iteminsubcat_leadingtochainavg_month0 > 3 and salesmsdiff_iteminsubcat_leadingtochainavg_month0 <= 4 then 4 else 5 end salesmsdiversity_iteminsubcat_month0

			,case when salesms_iteminsubcat_3months>0 and max_salesmsmarket_iteminsubcat_3months>0 and (sales_subcatinselectedstores_month0+sales_subcatinselectedstores_month1+sales_subcatinselectedstores_month2)>0 and max_salesmsmarket_iteminsubcat_3months-salesms_iteminsubcat_3months>0
			then (max_salesmsmarket_iteminsubcat_3months-salesms_iteminsubcat_3months)*(sales_subcatinselectedstores_month0+sales_subcatinselectedstores_month1+sales_subcatinselectedstores_month2) else null end inc_sales_by_iteminsubcat_salesms_3months
from (

			--,case when salesmsdiff_iteminselectedstores_leadingtochainavg_month0 <=1.3  then 1
			--      when salesmsdiff_iteminselectedstores_leadingtochainavg_month0 > 1.3 and salesmsdiff_iteminselectedstores_leadingtochainavg_month0 <= 2 then 2
			--     when salesmsdiff_iteminselectedstores_leadingtochainavg_month0 > 2  and salesmsdiff_iteminselectedstores_leadingtochainavg_month0 <= 3 then 3
			--      when salesmsdiff_iteminselectedstores_leadingtochainavg_month0 > 3 and salesmsdiff_iteminselectedstores_leadingtochainavg_month0 <= 4 then 4 else 5 end salesmsdiversity_iteminselectedstores_month0
			--,case when sales_iteminselectedstores_month0<=0 or sales_iteminselectedstores_month0 is null or qty_iteminselectedstores_month0<=0 or qty_iteminselectedstores_month0 is null then null else sales_iteminselectedstores_month0/qty_iteminselectedstores_month0 end qty_price_item_iteminselectedstores_month0
			--,case when (sales_iteminselectedstores_month0+sales_iteminselectedstores_month1+sales_iteminselectedstores_month2)<=0 or (sales_iteminselectedstores_month0+sales_iteminselectedstores_month1+sales_iteminselectedstores_month2) is null or (qty_iteminselectedstores_month0+qty_iteminselectedstores_month1+qty_iteminselectedstores_month2)<=0 or (qty_iteminselectedstores_month0+qty_iteminselectedstores_month1+qty_iteminselectedstores_month2) is null then null else (sales_iteminselectedstores_month0+sales_iteminselectedstores_month1+sales_iteminselectedstores_month2)/(qty_iteminselectedstores_month0+qty_iteminselectedstores_month1+qty_iteminselectedstores_month2) end qty_price_item_iteminselectedstores_3months

      select
              dw_item_key,ni.sub_category_key,  ni.category_key
             ,ni.sub_brand_hierarchy_2_key
            ,class_key, supplier_key ,ni.format_code, general_status_key, general_status_name,is_united_dw_item_key,is_private_label
            {REGIONAL_ITEMS_PLACEHOLDER}
            {MAX_RETAILER_CATALOG_PLACEHOLDER}
            ,max(is_special_new_item) is_special_item

            ,max(salesmstrend_iteminsubcat_month0and2) salesmstrend_iteminsubcat_month0and2
            ,max(a_stores_level_salesms_item_subcat_3months) a_stores_level_salesms_item_subcat_3months

            ,max(sales_rank_iteminformat_month0) salesrank_iteminformat_month0
            ,max(items_count_format_month0) salesmaxrank_iteminformat_month0
            ,case when max(sales_rank_iteminformat_month0)<>0 then max(sales_rank_iteminformat_month0)||' / '||max(items_count_format_month0) else null end as salesranking_iteminformat_month0
            ,max(sales_rank_iteminsubcat_month0) salesrank_iteminsubcat_month0
            ,max(items_count_subcatinformat_month0) salesmaxrank_iteminsubcat_month0
            ,case when max(sales_rank_iteminsubcat_month0)<>0 then max(sales_rank_iteminsubcat_month0)||' / '||max(items_count_subcatinformat_month0) else null end as salesranking_iteminsubcat_month0

            ,max(sales_rank_iteminsubcat_month1) salesrank_iteminsubcat_month1
            ,max(items_count_subcatinformat_month1) salesmaxrank_iteminsubcat_month1
            ,case when max(sales_rank_iteminsubcat_month1)<>0 then max(sales_rank_iteminsubcat_month1)||' / '||max(items_count_subcatinformat_month1) else null end as salesranking_iteminsubcat_month1

            ,max(sales_rank_iteminbrand_month0) salesrank_iteminbrand_month0
            ,max(items_count_brandinsubcat_month0) salesmaxrank_iteminbrand_month0
            ,case when max(sales_rank_iteminbrand_month0)<>0 then max(sales_rank_iteminbrand_month0)||' / '||max(items_count_brandinsubcat_month0) else null end as salesranking_iteminbrand_month0
            ,max(sales_rank_itemincategory_month0) salesrank_itemincategory_month0
            ,max(items_count_categoryinformat_month0) salesmaxrank_itemincategory_month0
            ,case when max(sales_rank_itemincategory_month0)<>0 then max(sales_rank_itemincategory_month0)||' / '||max(items_count_categoryinformat_month0) else null end as salesranking_itemincategory_month0

            ,max(salesmarket_item_month0) sales_market_item_month0
            ,max(salesmarket_item_month1) sales_market_item_month1

            ,max(salesmsmarket_iteminmarket_month0) salesms_market_iteminmarket_month0

            ,max(salesmschange_market_iteminsubcat_month0to1) salesmschange_market_iteminsubcat_month0to1
            ,max(salesmschange_market_iteminsubcat_month0to2) salesmschange_market_iteminsubcat_month0to2

            ,case when coalesce(max(salesmarket_item_month1),0) <= 0 or coalesce(max(salesmarket_item_month0),0) <= 0 then null else (max(salesmarket_item_month0)/max(salesmarket_item_month1))-1 end salechange_market_iteminmarket_month0to1

            ,max(salesmarketranking_iteminsubcat_month0) sales_market_ranking_iteminsubcat_month0
            ,max(salesmarketranking_itemincategory_month0) sales_market_ranking_itemincategory_month0
            ,max(salesmarketranking_iteminmarket_month0) sales_market_ranking_iteminmarket_month0

            ,avg(item_format_availability_in_store_3months) avg_availability_item_format_in_selectedstores_3months
            ,max(avgavailability_stores_group_a_item_format_3months) avgavailability_stores_group_a_item_format_3months
            ,max(avgavailability_stores_group_b_item_format_3months) avgavailability_stores_group_b_item_format_3months
            ,max(avgavailability_stores_group_c_item_format_3months) avgavailability_stores_group_c_item_format_3months
            ,max(avgavailability_ratio_stores_group_a_to_c_item_format_3months) avgavailability_ratio_stores_group_a_to_c_item_format_3months
            ,max(avgsalesms_ratio_stores_group_a_to_c_item_format_3months) avgsalesms_ratio_stores_group_a_to_c_item_format_3months
            ,max(item_format_sales_potential_inc_3months) item_format_sales_potential_inc_3months

      			,max(total_sales_inc_allstores_group_a_3months) total_sales_inc_allstores_group_a_3months

            ,max(avgsales_stores_group_a_item_format_month0) avg_sales_stores_group_a_item_format_month0
            ,max(avgsales_stores_group_b_item_format_month0) avg_sales_stores_group_b_item_format_month0
            ,max(avgsales_stores_group_c_item_format_month0) avg_sales_stores_group_c_item_format_month0

            ,max(avgsales_stores_group_a_item_format_month1) avg_sales_stores_group_a_item_format_month1
            ,max(avgsales_stores_group_b_item_format_month1) avg_sales_stores_group_b_item_format_month1
            ,max(avgsales_stores_group_c_item_format_month1) avg_sales_stores_group_c_item_format_month1

            ,case when coalesce(max(avgsales_stores_group_a_item_format_month1),0) <= 0 or coalesce(max(avgsales_stores_group_a_item_format_month0),0) <= 0 then null else (max(avgsales_stores_group_a_item_format_month0)/nullif(max(avgsales_stores_group_a_item_format_month1),0))-1 end avgsaleschange_stores_group_a_iteminformat_month0to1
            ,case when coalesce(max(avgsales_stores_group_c_item_format_month1),0) <= 0 or coalesce(max(avgsales_stores_group_c_item_format_month0),0) <= 0 then null else (max(avgsales_stores_group_c_item_format_month0)/nullif(max(avgsales_stores_group_c_item_format_month1),0))-1 end avgsaleschange_stores_group_c_iteminformat_month0to1


            ,max(avgsales_stores_group_a_item_format_month2) avg_sales_stores_group_a_item_format_month2
            ,max(avgsales_stores_group_b_item_format_month2) avg_sales_stores_group_b_item_format_month2
            ,max(avgsales_stores_group_c_item_format_month2) avg_sales_stores_group_c_item_format_month2


            ,max(avgsalesms_group_a_iteminsubcatformat_month1) avg_salesms_group_a_iteminsubcatformat_month1
            ,max(avgsalesms_group_b_iteminsubcatformat_month1) avg_salesms_group_b_iteminsubcatformat_month1
            ,max(avgsalesms_group_c_iteminsubcatformat_month1) avg_salesms_group_c_iteminsubcatformat_month1

            ,max(avgsalesms_group_a_iteminsubcatformat_month2) avg_salesms_group_a_iteminsubcatformat_month2
            ,max(avgsalesms_group_b_iteminsubcatformat_month2) avg_salesms_group_b_iteminsubcatformat_month2
            ,max(avgsalesms_group_c_iteminsubcatformat_month2) avg_salesms_group_c_iteminsubcatformat_month2

            ,max(avgsalesms_group_a_iteminsubcatformat_3months) avg_salesms_group_a_iteminsubcatformat_3months
            ,max(avgsalesms_group_b_iteminsubcatformat_3months) avg_salesms_group_b_iteminsubcatformat_3months
            ,max(avgsalesms_group_c_iteminsubcatformat_3months) avg_salesms_group_c_iteminsubcatformat_3months

            ,max(avgqty_stores_group_a_item_format_month0) avg_qty_stores_group_a_item_format_month0
            ,max(avgqty_stores_group_b_item_format_month0) avg_qty_stores_group_b_item_format_month0
            ,max(avgqty_stores_group_c_item_format_month0) avg_qty_stores_group_c_item_format_month0

            ,max(avgqty_stores_group_a_item_format_month1) avg_qty_stores_group_a_item_format_month1
            ,max(avgqty_stores_group_b_item_format_month1) avg_qty_stores_group_b_item_format_month1
            ,max(avgqty_stores_group_c_item_format_month1) avg_qty_stores_group_c_item_format_month1

            ,max(avgqty_stores_group_a_item_format_month2) avg_qty_stores_group_a_item_format_month2
            ,max(avgqty_stores_group_b_item_format_month2) avg_qty_stores_group_b_item_format_month2
            ,max(avgqty_stores_group_c_item_format_month2) avg_qty_stores_group_c_item_format_month2


            ,max(avgsales_stores_group_a_item_format_ss_month0) avgsales_stores_group_a_item_format_ss_month0
            ,max(avgsales_stores_group_c_item_format_ss_month0) avgsales_stores_group_c_item_format_ss_month0

            ,max(avgsales_stores_group_a_item_format_ss_month1) avgsales_stores_group_a_item_format_ss_month1
            ,max(avgsales_stores_group_c_item_format_ss_month1) avgsales_stores_group_c_item_format_ss_month1

            ,case when coalesce(max(avgsales_stores_group_a_item_format_ss_month1),0) <= 0 or coalesce(max(avgsales_stores_group_a_item_format_ss_month0),0) <= 0 then null else (max(avgsales_stores_group_a_item_format_ss_month0)/nullif(max(avgsales_stores_group_a_item_format_ss_month1),0))-1 end avgsaleschange_stores_group_a_iteminformat_ss_month0to1
            ,case when coalesce(max(avgsales_stores_group_c_item_format_ss_month1),0) <= 0 or coalesce(max(avgsales_stores_group_c_item_format_ss_month0),0) <= 0 then null else (max(avgsales_stores_group_c_item_format_ss_month0)/nullif(max(avgsales_stores_group_c_item_format_ss_month1),0))-1 end avgsaleschange_stores_group_c_iteminformat_ss_month0to1


            ,launches_week
            ,total_weeks_since_launche
            ,item_format_examination_code
            ,item_format_classification_code
            ,is_item_classification_equal_for_all_format

            ,case when coalesce(sum(qtystore_iteminformat_month1),0) <= 0 or coalesce(sum(qtystore_iteminformat_month0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_month0) as double precision)/nullif(sum(qtystore_iteminformat_month1),0))-1 end qtychange_iteminselectedstores_month0to1
            ,case when coalesce(sum(qtystore_iteminformat_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_month1),0) <= 0 then null else (cast(sum(qtystore_iteminformat_month1) as double precision)/nullif(sum(qtystore_iteminformat_month2),0))-1 end qtychange_iteminselectedstores_month1to2
            ,case when coalesce(sum(qtystore_iteminformat_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_month0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_month0) as double precision)/nullif(sum(qtystore_iteminformat_month2),0))-1 end qtychange_iteminselectedstores_month0to2

    		--,case when max(num_store_pos_sales_3months)>0 then cast(count(distinct case when salesstore_iteminformat_3months>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_3months),0) end   distribution_iteminselectedstores_3months

            ,max(qty_sales_subcat_format_month0) qty_subcatinselectedstores_month0
            ,max(qty_sales_subcat_format_month1) qty_subcatinselectedstores_month1
            ,max(qty_sales_subcat_format_month2) qty_subcatinselectedstores_month2

            ,case when coalesce(max(qty_sales_subcat_format_month0),0) <= 0 or coalesce(sum(qtystore_iteminformat_month0),0) <= 0
            then null else (cast(sum(qtystore_iteminformat_month0) as double precision)/nullif(max(qty_sales_subcat_format_month0),0)) end qtyms_iteminsubcat_month0
            ,case when coalesce(max(qty_sales_subcat_format_month1),0) <= 0 or coalesce(sum(qtystore_iteminformat_month1),0) <= 0
            then null else (cast(sum(qtystore_iteminformat_month1) as double precision)/nullif(max(qty_sales_subcat_format_month1),0)) end qtyms_iteminsubcat_month1
            ,case when coalesce(max(qty_sales_subcat_format_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_month2),0) <= 0
            then null else (cast(sum(qtystore_iteminformat_month2) as double precision)/nullif(max(qty_sales_subcat_format_month2),0)) end qtyms_iteminsubcat_month2

            ,case when coalesce(max(qty_sales_subcat_format_month1),0) <= 0 or coalesce(sum(qtystore_iteminformat_month1),0) <= 0
            or coalesce(max(qty_sales_subcat_format_month0),0) <= 0 or coalesce(sum(qtystore_iteminformat_month0),0) <= 0
            then null else ((cast(sum(qtystore_iteminformat_month0) as double precision)/nullif(max(qty_sales_subcat_format_month0),0))/nullif((cast(sum(qtystore_iteminformat_month1) as double precision)/max(qty_sales_subcat_format_month1)),0))-1 end qtymschange_iteminsubcat_month0to1
            ,case when coalesce(max(qty_sales_subcat_format_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_month2),0) <= 0
            or coalesce(max(qty_sales_subcat_format_month0),0) <= 0 or coalesce(sum(qtystore_iteminformat_month0),0) <= 0
            then null else ((cast(sum(qtystore_iteminformat_month0) as double precision)/nullif(max(qty_sales_subcat_format_month0),0))/nullif((cast(sum(qtystore_iteminformat_month2) as double precision)/max(qty_sales_subcat_format_month2)),0))-1 end qtymschange_iteminsubcat_month0to2


            ,sum(qtystore_iteminformat_ss_month0) qty_iteminselectedstores_ss_month0
            ,sum(qtystore_iteminformat_ss_month1) qty_iteminselectedstores_ss_month1
            ,sum(qtystore_iteminformat_ss_month2) qty_iteminselectedstores_ss_month2

          ,case when coalesce(sum(qtystore_iteminformat_ss_month1),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_month0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_month0) as double precision)/nullif(sum(qtystore_iteminformat_ss_month1),0))-1 end qtychange_iteminselectedstores_ss_month0to1
          ,case when coalesce(sum(qtystore_iteminformat_ss_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_month1),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_month1) as double precision)/nullif(sum(qtystore_iteminformat_ss_month2),0))-1 end qtychange_iteminselectedstores_ss_month1to2
          ,case when coalesce(sum(qtystore_iteminformat_ss_month2),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_month0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_month0) as double precision)/nullif(sum(qtystore_iteminformat_ss_month2),0))-1 end qtychange_iteminselectedstores_ss_month0to2

            ,max(qty_sales_category_format_month0) qty_categoryinselectedstores_month0
            ,max(qty_sales_category_format_month1) qty_categoryinselectedstores_month1
            ,max(qty_sales_category_format_month2) qty_categoryinselectedstores_month2

            ,sum(salesstore_iteminformat_month0) sales_iteminselectedstores_month0
            ,sum(salesstore_iteminformat_month1) sales_iteminselectedstores_month1
            ,sum(salesstore_iteminformat_month2) sales_iteminselectedstores_month2
            ,sum(salesstore_iteminformat_ss_month0) sales_iteminselectedstores_ss_month0
            ,sum(salesstore_iteminformat_ss_month1) sales_iteminselectedstores_ss_month1
            ,sum(salesstore_iteminformat_ss_month2) sales_iteminselectedstores_ss_month2

          --,case when max(num_store_pos_sales_month0)>0 then cast(count(distinct case when salesstore_iteminformat_month0>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_month0),0) end  distribution_iteminselectedstores_month0
          --,case when max(num_store_pos_sales_month1)>0 then cast(count(distinct case when salesstore_iteminformat_month1>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_month1),0) end  distribution_iteminselectedstores_month1
          --,case when max(num_store_pos_sales_month2)>0 then cast(count(distinct case when salesstore_iteminformat_month2>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_month2),0) end  distribution_iteminselectedstores_month2

		  			---------------------------------------------------------distribution fix 250819-------------------------------------
			,case when max(count_selectedstores_month0)>0 then cast(count(distinct case when salesstore_iteminformat_month0>0 then dw_store_key end) as double precision)/max(count_selectedstores_month0) end  distribution_iteminselectedstores_month0
			,case when max(count_selectedstores_month1)>0 then cast(count(distinct case when salesstore_iteminformat_month1>0 then dw_store_key end) as double precision)/max(count_selectedstores_month1) end  distribution_iteminselectedstores_month1
			,case when max(count_selectedstores_month2)>0 then cast(count(distinct case when salesstore_iteminformat_month2>0 then dw_store_key end) as double precision)/max(count_selectedstores_month2) end  distribution_iteminselectedstores_month2
			,case when max(count_selectedstores_3months)>0 then cast(count(distinct case when salesstore_iteminformat_3months>0 then dw_store_key end) as double precision)/max(count_selectedstores_3months) end   distribution_iteminselectedstores_3months
			
			---------------------------------------------------------------------------------------------------------------
		  
		  
            ,case when coalesce(sum(salesstore_iteminformat_ss_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_month0),0) <= 0 then null else (sum(salesstore_iteminformat_ss_month0)/sum(salesstore_iteminformat_ss_month1))-1 end salechange_iteminselectedstores_ss_month0to1
            ,case when coalesce(sum(salesstore_iteminformat_ss_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_month1),0) <= 0 then null else (sum(salesstore_iteminformat_ss_month1)/sum(salesstore_iteminformat_ss_month2))-1 end salechange_iteminselectedstores_ss_month1to2
            ,case when coalesce(sum(salesstore_iteminformat_ss_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_month0),0) <= 0 then null else (sum(salesstore_iteminformat_ss_month0)/sum(salesstore_iteminformat_ss_month2))-1 end salechange_iteminselectedstores_ss_month0to2


            ,case when coalesce(sum(salesstore_iteminformat_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(sum(salesstore_iteminformat_month1),0))-1 end salechange_iteminselectedstores_month0to1
            ,case when coalesce(sum(salesstore_iteminformat_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
            then null else (sum(salesstore_iteminformat_month1)/nullif(sum(salesstore_iteminformat_month2),0))-1 end salechange_iteminselectedstores_month1to2
            ,case when coalesce(sum(salesstore_iteminformat_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(sum(salesstore_iteminformat_month2),0))-1 end salechange_iteminselectedstores_month0to2

            ,max(sales_in_format_month0) sales_selectedstores_month0
            ,max(sales_in_format_month1) sales_selectedstores_month1
            ,max(sales_in_format_month2) sales_selectedstores_month2

            ,max(sales_category_format_month0) sales_categoryinselectedstores_month0
            ,max(sales_category_format_month1) sales_categoryinselectedstores_month1
            ,max(sales_category_format_month2) sales_categoryinselectedstores_month2

    			,case when coalesce(max(sales_in_format_month0),0) <= 0 or coalesce(max(sales_category_format_month0),0) <= 0
    			then null else (max(sales_category_format_month0)/nullif(max(sales_in_format_month0),0)) end salesms_categoryinselectedstores_month0
		    	,case when coalesce(max(sales_in_format_month0),0) <= 0 or coalesce(max(sales_subcat_format_month0),0) <= 0
		    	then null else (max(sales_subcat_format_month0)/nullif(max(sales_in_format_month0),0)) end salesms_subcatinselectedstores_month0

          ,case when ((max(salesmsmarket_categoryinmarket_month0)/nullif((max(sales_category_format_month0)/nullif(max(sales_in_format_month0),0)),0))-1>=0.05)
          and ((max(salesmsmarket_subcatinmarket_month0)/nullif((max(sales_subcat_format_month0)/nullif(max(sales_in_format_month0),0)),0))-1>=0.05)
          then 1 else 0 end is_pass_potential_ms_test

          ,case when ((max(salesmsmarket_categoryinmarket_month0)/nullif((max(sales_category_format_month0)/nullif(max(sales_in_format_month0),0)),0))-1>=0.05)
          and ((max(salesmsmarket_subcatinmarket_month0)/nullif((max(sales_subcat_format_month0)/nullif(max(sales_in_format_month0),0)),0))-1>=0.05)
          and (sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))>0
          and max(salesmsmarket_iteminsubcat_month0)>0 and max(sales_subcat_format_month0)>0
          then ((max(salesmsmarket_iteminsubcat_month0)/nullif((sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0)),0))-1)*max(sales_subcat_format_month0)
          else null end sales_potential_market_to_chain_month0

            ,case when coalesce(max(sales_in_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(max(sales_in_format_month0),0)) end salesms_iteminselectedstores_month0
            ,case when coalesce(max(sales_in_format_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
            then null else (sum(salesstore_iteminformat_month1)/nullif(max(sales_in_format_month1),0)) end salesms_iteminselectedstores_month1
            ,case when coalesce(max(sales_in_format_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
            then null else (sum(salesstore_iteminformat_month2)/nullif(max(sales_in_format_month2),0)) end salesms_iteminselectedstores_month2

            ,max(sales_subcat_format_month0) sales_subcatinselectedstores_month0
            ,max(sales_subcat_format_month1) sales_subcatinselectedstores_month1
            ,max(sales_subcat_format_month2) sales_subcatinselectedstores_month2

            ,max(sales_brand_subcat_month0) sales_brandinsubcatinselectedstores_month0
            ,max(sales_brand_subcat_month1) sales_brandinsubcatinselectedstores_month1
            ,max(sales_brand_subcat_month2) sales_brandinsubcatinselectedstores_month2

            ,case when coalesce(max(sales_brand_subcat_month1),0) <= 0 or coalesce(max(sales_brand_subcat_month0),0) <= 0 then null else (max(sales_brand_subcat_month0)/nullif(max(sales_brand_subcat_month1),0))-1 end saleschange_brandinsubcat_month0to1
            ,case when coalesce(max(sales_brand_subcat_month2),0) <= 0 or coalesce(max(sales_brand_subcat_month1),0) <= 0 then null else (max(sales_brand_subcat_month1)/nullif(max(sales_brand_subcat_month2),0))-1 end saleschange_brandinsubcat_month1to2

            ,case when coalesce(max(sales_subcat_format_month1),0) <= 0 or coalesce(max(sales_subcat_format_month0),0) <= 0 then null else (max(sales_subcat_format_month0)/nullif(max(sales_subcat_format_month1),0))-1 end saleschange_subcatinselectedstores_month0to1
            ,case when coalesce(max(sales_subcat_format_month2),0) <= 0 or coalesce(max(sales_subcat_format_month1),0) <= 0 then null else (max(sales_subcat_format_month1)/nullif(max(sales_subcat_format_month2),0))-1 end saleschange_subcatinselectedstores_month1to2

            ,case when coalesce((sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2)),0) <= 0
            or coalesce(max(sales_subcat_format_month0)+max(sales_subcat_format_month1)+max(sales_subcat_format_month2),0) <= 0 then null
            else ((sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2))/nullif(max(sales_subcat_format_month0)+max(sales_subcat_format_month1)+max(sales_subcat_format_month2),0)) end salesms_iteminsubcat_3months

            ,case when coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(max(sales_brand_subcat_month0),0) <= 0 then null else (max(sales_brand_subcat_month0)/nullif(max(sales_subcat_format_month0),0)) end salesms_brandinsubcat_month0
            ,case when coalesce(max(sales_subcat_format_month1),0) <= 0 or coalesce(max(sales_brand_subcat_month1),0) <= 0 then null else (max(sales_brand_subcat_month1)/nullif(max(sales_subcat_format_month1),0)) end salesms_brandinsubcat_month1
            ,case when coalesce(max(sales_subcat_format_month2),0) <= 0 or coalesce(max(sales_brand_subcat_month2),0) <= 0 then null else (max(sales_brand_subcat_month2)/nullif(max(sales_subcat_format_month2),0)) end salesms_brandinsubcat_month2
            ,max(salesmsmarket_iteminsubcat_3months) max_salesmsmarket_iteminsubcat_3months
            ,max(salesmsmarket_iteminsubcat_3months) salesmsmarket_iteminsubcat_3months

            ,case when coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0)) end salesms_iteminsubcat_month0
            ,case when coalesce(max(sales_subcat_format_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
            then null else (sum(salesstore_iteminformat_month1)/nullif(max(sales_subcat_format_month1),0)) end salesms_iteminsubcat_month1
            ,case when coalesce(max(sales_subcat_format_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
            then null else (sum(salesstore_iteminformat_month2)/nullif(max(sales_subcat_format_month2),0)) end salesms_iteminsubcat_month2

            ,max(salesmsmarket_iteminsubcat_month0) salesms_market_iteminsubcat_month0
            ,max(salesmsmarket_iteminsubcat_month1) salesms_market_iteminsubcat_month1
            ,max(salesmsmarket_iteminsubcat_month2) salesms_market_iteminsubcat_month2

            ,case when coalesce(max(sales_brand_subcat_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(max(sales_brand_subcat_month0),0)) end salesms_iteminbrand_month0
            ,case when coalesce(max(sales_brand_subcat_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
            then null else (sum(salesstore_iteminformat_month1)/nullif(max(sales_brand_subcat_month1),0)) end salesms_iteminbrand_month1
            ,case when coalesce(max(sales_brand_subcat_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
            then null else (sum(salesstore_iteminformat_month2)/nullif(max(sales_brand_subcat_month2),0)) end salesms_iteminbrand_month2

            ,max(salesmsmarket_iteminbrand_month0) salesms_market_iteminbrand_month0
            ,max(salesmsmarket_iteminbrand_month1) salesms_market_iteminbrand_month1
            ,max(salesmsmarket_iteminbrand_month2) salesms_market_iteminbrand_month2

            ,case when coalesce(max(sales_category_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(max(sales_category_format_month0),0)) end salesms_itemincategory_month0
            ,case when coalesce(max(sales_category_format_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
            then null else (sum(salesstore_iteminformat_month1)/nullif(max(sales_category_format_month1),0)) end salesms_itemincategory_month1
            ,case when coalesce(max(sales_category_format_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
            then null else (sum(salesstore_iteminformat_month2)/nullif(max(sales_category_format_month2),0)) end salesms_itemincategory_month2

                ,max(salesmsmarket_itemincategory_month0) salesms_market_itemincategory_month0

          ,case when coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then 0-max(salesmsmarket_iteminsubcat_month0)
          when max(salesmsmarket_iteminsubcat_month0) is null
              then (sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))-0 else (sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))-max(salesmsmarket_iteminsubcat_month0)
              end salesmsdiff_iteminsubcat_cahintomarket_month0
          ,case when coalesce(max(sales_brand_subcat_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
           then 0-max(salesmsmarket_iteminbrand_month0)
              when max(salesmsmarket_iteminbrand_month0) is null
              then (sum(salesstore_iteminformat_month0)/nullif(max(sales_brand_subcat_month0),0))-0 else (sum(salesstore_iteminformat_month0)/nullif(max(sales_brand_subcat_month0),0))-max(salesmsmarket_iteminbrand_month0) end salesmsdiff_iteminbrand_cahintomarket_month0
          ,case when coalesce(max(sales_category_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then 0-max(salesmsmarket_itemincategory_month0)
              when max(salesmsmarket_itemincategory_month0) is null
              then (sum(salesstore_iteminformat_month0)/nullif(max(sales_category_format_month0),0))-0 else (sum(salesstore_iteminformat_month0)/nullif(max(sales_category_format_month0),0))-max(salesmsmarket_itemincategory_month0) end salesmsdiff_itemincategory_cahintomarket_month0

          ,case when coalesce(max(salesmsmarket_iteminsubcat_month0),0) <= 0
          or coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then null else ((sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))/nullif(max(salesmsmarket_iteminsubcat_month0),0))-1 end salesmschange_iteminsubcat_cahintomarket_month0
          ,case when coalesce(max(salesmsmarket_iteminbrand_month0),0) <= 0
          or coalesce(max(sales_brand_subcat_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then null else ((sum(salesstore_iteminformat_month0)/nullif(max(sales_brand_subcat_month0),0))/nullif(max(salesmsmarket_iteminbrand_month0),0))-1 end salesmschange_iteminbrand_cahintomarket_month0
          ,case when coalesce(max(salesmsmarket_itemincategory_month0),0) <= 0
          or coalesce(max(sales_category_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then null else ((sum(salesstore_iteminformat_month0)/nullif(max(sales_category_format_month0),0))/nullif(max(salesmsmarket_itemincategory_month0),0))-1 end salesmschange_itemincategory_cahintomarket_month0


          ,case when coalesce(max(sales_subcat_format_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
           or coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then null else ((sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))/nullif((sum(salesstore_iteminformat_month1)/max(sales_subcat_format_month1)),0))-1 end salesmschange_iteminsubcat_month0to1
          ,case when coalesce(max(sales_subcat_format_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
          or coalesce(max(sales_subcat_format_month1),0) <= 0 or coalesce(sum(salesstore_iteminformat_month1),0) <= 0
          then null else ((sum(salesstore_iteminformat_month1)/nullif(max(sales_subcat_format_month1),0))/nullif((sum(salesstore_iteminformat_month2)/max(sales_subcat_format_month2)),0))-1 end salesmschange_iteminsubcat_month1to2
          ,case when coalesce(max(sales_subcat_format_month2),0) <= 0 or coalesce(sum(salesstore_iteminformat_month2),0) <= 0
          or coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
          then null else ((sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0))/nullif(sum(salesstore_iteminformat_month2)/max(sales_subcat_format_month2),0))-1 end salesmschange_iteminsubcat_month0to2

            ,max(sales_subcat_format_ss_month0) sales_subcatinselectedstores_ss_month0
            ,max(sales_subcat_format_ss_month1) sales_subcatinselectedstores_ss_month1
            ,max(sales_subcat_format_ss_month2) sales_subcatinselectedstores_ss_month2

            ,case when coalesce(max(sales_subcat_format_ss_month1),0) <= 0 or coalesce(max(sales_subcat_format_ss_month0),0) <= 0 then null else (max(sales_subcat_format_ss_month0)/nullif(max(sales_subcat_format_ss_month1),0))-1 end saleschange_subcatinselectedstores_ss_month0to1
            ,case when coalesce(max(sales_subcat_format_ss_month2),0) <= 0 or coalesce(max(sales_subcat_format_ss_month1),0) <= 0 then null else (max(sales_subcat_format_ss_month1)/nullif(max(sales_subcat_format_ss_month2),0))-1 end saleschange_subcatinselectedstores_ss_month1to2

            ,max(sales_brand_subcat_ss_month0) sales_brandinsubcatinselectedstores_ss_month0
            ,max(sales_brand_subcat_ss_month1) sales_brandinsubcatinselectedstores_ss_month1
            ,max(sales_brand_subcat_ss_month2) sales_brandinsubcatinselectedstores_ss_month2

            ,case when coalesce(max(sales_brand_subcat_ss_month1),0) <= 0 or coalesce(max(sales_brand_subcat_ss_month0),0) <= 0 then null else (max(sales_brand_subcat_ss_month0)/nullif(max(sales_brand_subcat_ss_month1),0))-1 end saleschange_brandinsubcat_ss_month0to1
            ,case when coalesce(max(sales_brand_subcat_ss_month2),0) <= 0 or coalesce(max(sales_brand_subcat_ss_month1),0) <= 0 then null else (max(sales_brand_subcat_ss_month1)/nullif(max(sales_brand_subcat_ss_month2),0))-1 end saleschange_brandinsubcat_ss_month1to2

            ,max(sales_iteminformat_month0) sales_iteminallformat_month0
            ,max(sales_subcatinformat_month0) sales_subcatallformat_month0

            ,case when coalesce(max(sales_iteminformat_month0),0) <= 0 or coalesce(max(sales_subcatinformat_month0),0) <= 0
            then null else (max(sales_iteminformat_month0)/nullif(max(sales_subcatinformat_month0),0)) end salesms_iteminsubcat_allformat_month0
            ,case when (max(salesmsmarket_iteminsubcat_month0)-(coalesce(max(sales_iteminformat_month0),0)/nullif(max(sales_subcatinformat_month0),0)))<0  then null
            else (max(salesmsmarket_iteminsubcat_month0)-(coalesce(max(sales_iteminformat_month0),0)/nullif(max(sales_subcatinformat_month0),0)))*max(sales_iteminformat_month0) end item_format_inc_in_chain_month0

            ,max(avgsalesms_group_a_iteminsubcatformat_month0) avg_salesms_group_a_iteminsubcatformat_month0
            ,max(avgsalesms_group_b_iteminsubcatformat_month0) avg_salesms_group_b_iteminsubcatformat_month0
            ,max(avgsalesms_group_c_iteminsubcatformat_month0) avg_salesms_group_c_iteminsubcatformat_month0

            ,case when coalesce(max(sales_subcat_format_month0),0) <= 0 or coalesce(sum(salesstore_iteminformat_month0),0) <= 0
            then null else (sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0)) end salesms_iteminsubcat_month0
            --,((1+salesms_iteminselectedstores_month0)/(1+avg_salesms_group_a_iteminsubcatformat_month0))-1  salesmsdiff_iteminselectedstores_leadingtochainavg_month0
            ,(((1+(sum(salesstore_iteminformat_month0)/nullif(max(sales_subcat_format_month0),0)))/nullif((1+max(avgsalesms_group_a_iteminsubcatformat_month0)),0))-1)  salesmsdiff_iteminsubcat_leadingtochainavg_month0

            ,max(salesms_brandinsubcatformat_allformat_month0) salesms_brandinsubcatformat_allformat_month0
            ,max(salesms_subcatinformat_allformat_month0) salesms_subcatinformat_allformat_month0

            ,max(penetration_iteminsubcat_month0) penetration_iteminsubcat_month0
            ,max(penetration_iteminsubcat_month1) penetration_iteminsubcat_month1
            ,max(penetration_iteminsubcat_month2) penetration_iteminsubcat_month2
            ,max(penetrationchange_iteminsubcat_month0to1) penetrationchange_iteminsubcat_month0to1
            ,max(penetrationchange_iteminsubcat_month1to2) penetrationchange_iteminsubcat_month1to2
            ,max(avgsalesbasketsize_month0) avgsalesbasketsize_month0
            ,max(avgsalesbasketsize_month1) avgsalesbasketsize_month1
            ,max(avgsalesbasketsize_month2) avgsalesbasketsize_month2
            ,max(avgsalesbasketsize_month0to1) avgsalesbasketsize_month0to1
            ,max(avgsalesbasketsize_month1to2) avgsalesbasketsize_month1to2
            ,max(avgqtybasketsize_month0) avgqtybasketsize_month0
            ,max(avgqtybasketsize_month1) avgqtybasketsize_month1
            ,max(avgqtybasketsize_month2) avgqtybasketsize_month2
            ,max(avgqtybasketsize_month0to1) avgqtybasketsize_month0to1
            ,max(avgqtybasketsize_month1to2) avgqtybasketsize_month1to2
            ,max(repeatbuyingrank_month0) repeatbuyingrank_month0
            ,max(repeatbuyingrank_month1) repeatbuyingrank_month1
            ,max(repeatbuyingrank_month2) repeatbuyingrank_month2
            --,max(salesavgms_iteminsubcat_month0) salesavgms_iteminsubcat_month0
            --,max(salesavgms_iteminsubcat_month1) salesavgms_iteminsubcat_month1
            --,max(salesavgms_iteminsubcat_month2) salesavgms_iteminsubcat_month2

            ,max(qty_price_item_format_market_month0) qty_price_item_format_market_month0
            -------
            ,sum(weightstore_iteminformat_month0) weight_iteminselectedstores_month0
            ,sum(weightstore_iteminformat_month1) weight_iteminselectedstores_month1
            ,sum(weightstore_iteminformat_month2) weight_iteminselectedstores_month2

            ,sum(qtystore_iteminformat_month0) qty_iteminselectedstores_month0
            ,sum(qtystore_iteminformat_month1) qty_iteminselectedstores_month1
            ,sum(qtystore_iteminformat_month2) qty_iteminselectedstores_month2

          ,case when max(weighted)='True' and sum(weightstore_iteminformat_month0)=0   then null
            when max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0),0)<>0
            then (1000/nullif(sum(weightstore_iteminformat_month0),0))*sum(salesstore_iteminformat_month0)
            when coalesce(sum(salesstore_iteminformat_month0),0)<=0 or coalesce(sum(qtystore_iteminformat_month0),0)<=0 then null
              else sum(salesstore_iteminformat_month0)/nullif(sum(qtystore_iteminformat_month0),0) end qty_price_item_iteminselectedstores_month0

          ,case when (max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0),0)<=0)
            or coalesce(sum(salesstore_iteminformat_month0),0)<=0 or coalesce(sum(qtystore_iteminformat_month0),0)<=0
            or coalesce(max(qty_price_item_format_market_month0),0)<=0 then null
            when max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0),0)>0
            then (((1000/nullif(sum(weightstore_iteminformat_month0),0))*sum(salesstore_iteminformat_month0))/nullif(max(qty_price_item_format_market_month0),0))-1
            else ((sum(salesstore_iteminformat_month0)/nullif(sum(qtystore_iteminformat_month0),0))/nullif(max(qty_price_item_format_market_month0),0))-1 end qty_price_change_iteminselectedstores_cahintomarket_month0

          ,case when max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0)=0
            then null
            when max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0)<>0
            then (1000/nullif(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0))*(sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2))
            when coalesce(sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2),0)<=0
            or coalesce(sum(qtystore_iteminformat_month0)+sum(qtystore_iteminformat_month1)+sum(qtystore_iteminformat_month2),0)<=0 then null
            else (sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2))/nullif(sum(qtystore_iteminformat_month0)+sum(qtystore_iteminformat_month1)+sum(qtystore_iteminformat_month2),0) end qty_price_item_iteminselectedstores_3months

            ,max(qty_price_item_format_month0) qty_price_item_allformat_month0

            ,max(avgqty_price_item_stores_group_a_item_format_month0) avgqty_price_item_stores_group_a_item_format_month0
            ,max(avgqty_price_item_stores_group_b_item_format_month0) avgqty_price_item_stores_group_b_item_format_month0
            ,max(avgqty_price_item_stores_group_c_item_format_month0) avgqty_price_item_stores_group_c_item_format_month0

            ,max(qty_price_item_format_3months) qty_price_item_allformat_3months

            ,max(qty_price_item_format_market_3months) qty_price_item_format_market_3months

          ,case when (max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0)=0)
            or coalesce(max(qty_price_item_format_market_3months),0)<=0
            or coalesce(sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2),0)<=0
            or coalesce(sum(qtystore_iteminformat_month0)+sum(qtystore_iteminformat_month1)+sum(qtystore_iteminformat_month2),0)<=0
            then null
            when max(weighted)='True' and coalesce(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0)<>0
            then ((1000/nullif(sum(weightstore_iteminformat_month0)+sum(weightstore_iteminformat_month1)+sum(weightstore_iteminformat_month2),0))*(sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2)))/max(qty_price_item_format_market_3months)-1
            else ((sum(salesstore_iteminformat_month0)+sum(salesstore_iteminformat_month1)+sum(salesstore_iteminformat_month2))/nullif(sum(qtystore_iteminformat_month0)+sum(qtystore_iteminformat_month1)+sum(qtystore_iteminformat_month2),0))/nullif(max(qty_price_item_format_market_3months),0)-1 end qty_price_change_iteminselectedstores_cahintomarket_3months

            ,max(avgqty_price_item_stores_group_a_item_format_3months) avgqty_price_item_stores_group_a_item_format_3months
            ,max(avgqty_price_item_stores_group_b_item_format_3months) avgqty_price_item_stores_group_b_item_format_3months
            ,max(avgqty_price_item_stores_group_c_item_format_3months) avgqty_price_item_stores_group_c_item_format_3months
			
			,max(normalized_max_theoretic_item_format_sales_at_risk) as max_theoretic_item_format_sales
			,max(item_format_nontransferable_spend) as item_format_nontransferable_spend
			,max(item_format_estimated_real_sales_at_risk) as item_format_estimated_real_sales_at_risk
			,max(avg_must_item_format_estimated_real_sales_at_risk) as avg_must_item_format_estimated_real_sales_at_risk
			,max(item_format_alternative_cost) as item_format_alternative_cost
			,max(avg_must_item_format_alternative_cost) as alternative_real_sales_at_risk
			,max(item_format_net_gain_loss_sales) as item_format_net_gain_loss_sales
			
			{REGIONAL_ITEMS_MONTH_PLACEHOLDER}
			

          from migvan.new_items_after_launche_period_measures ni left join selected_store_agg agg_inc on ni.sub_brand_hierarchy_2_key=agg_inc.sub_brand_hierarchy_2_key
          and  ni.sub_category_key=agg_inc.sub_category_key and  ni.category_key=agg_inc.category_key and  ni.format_code=agg_inc.format_code

          where item_format_classification_code = :item_classification
           and {PREFIX_STORES_FILTER_PLACEHOLDER}
          -- where item_format_classification_code = 9
          -- and  ni.dw_store_key is not null and  ni.format_code in (1395515)
          group by  dw_item_key,ni.sub_category_key,  ni.category_key,  ni.sub_brand_hierarchy_2_key
          ,class_key,  supplier_key,  ni.format_code, general_status_key, general_status_name,is_united_dw_item_key,is_private_label {REGIONAL_ITEMS_PLACEHOLDER}
		  ,launches_week ,total_weeks_since_launche
          ,item_format_examination_code,item_format_classification_code,is_item_classification_equal_for_all_format

 ) b
where  {CATALOG_FILTER_PLACEHOLDER}
--  supplier_key is not null  and sub_category_key is not null


