----m_query6b -detailed stores - core items----

select format_code,dw_store_key
	,dashboard_mark_for_2,dashboard_mark_color_for_2,total_dashboard_mark_for_2
	,case when total_dashboard_mark_for_2 >= 0.95 then 1
	  when total_dashboard_mark_for_2 > 0.85 and total_dashboard_mark_for_2 <0.95 then 2
	  when total_dashboard_mark_for_2 <= 0.85 then 3 end total_dashboard_mark_color_for_2 -- Color classification for total value
from
(
select *
	,case when dashboard_mark_for_2 >= 0.95 then 1
	  when dashboard_mark_for_2 > 0.85 and dashboard_mark_for_2 <0.95 then 2
	  when dashboard_mark_for_2 <= 0.85 then 3 end dashboard_mark_color_for_2 -- Color classification
	  
	,avg(dashboard_mark_for_2) over() total_dashboard_mark_for_2 --Total value to display 
from
(
select dw_store_key,format_code
	,avg(avg_availability_item_format_1) dashboard_mark_for_2 -- Value to display 
	from
	(
	select f.dw_item_key,f.format_code,f.dw_store_key,item_final_availability_in_store_quartermatcur_156 as avg_availability_item_format_1
	FROM migvan.old_items_measures f
	where item_classification_42 = 2
    and {FILTER_PLACEHOLDER}
    --and   user's selections : formats,stores, catalog
	--and   user's permission : formats,stores, catalog
	)as sub1
	group by dw_store_key,format_code
	) as sub2
) as sub3