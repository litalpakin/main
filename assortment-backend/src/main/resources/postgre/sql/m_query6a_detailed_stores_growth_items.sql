----m_query6a - detailed stores - growth items----

		select format_code,dw_store_key
		,dashboard_mark_for_1,dashboard_mark_color_for_1,total_dashboard_mark_for_1
		,case when total_dashboard_mark_for_1 is null then 2
			  when total_dashboard_mark_for_1 > 0.05 then 1
	 		  when total_dashboard_mark_for_1 >= 0 and total_dashboard_mark_for_1 <= 0.05 then 2
	 		  when total_dashboard_mark_for_1 < 0 then 3 end total_dashboard_mark_color_for_1 -- Color classification for total value
		
		from(
        select *
				,case 		when dashboard_mark_for_1 is null then 2
							when dashboard_mark_for_1 > 0.05 then 1
	 						when dashboard_mark_for_1 >= 0 and dashboard_mark_for_1 <= 0.05 then 2
	 						when dashboard_mark_for_1 < 0 then 3 end dashboard_mark_color_for_1 -- Color classification
				
				,avg(dashboard_mark_for_1) over() total_dashboard_mark_for_1  --Total value to display 
	 						  
        from(
            select * 
						,((1+sales_change_chain_item_qtr)/(1+sales_change_market_item_qtr))-1 dashboard_mark_for_1 -- Value to display 	 						  
						from(
						select *
						,case when is_same_store is null or coalesce(total_sales_chain_prv_qtr,0) = 0 then null else (total_sales_chain_cur_qtr/total_sales_chain_prv_qtr)-1 end sales_change_chain_item_qtr
						,case when is_same_store is null or coalesce(total_sales_market_prv_qtr,0) = 0 then null else (total_sales_market_cur_qtr/total_sales_market_prv_qtr)-1 end sales_change_market_item_qtr
						
						from(
							select format_code,dw_store_key,is_same_store
							,sum(sales_item_format_cur_qtr) total_sales_chain_cur_qtr
							,sum(sales_item_format_prv_qtr) total_sales_chain_prv_qtr
							
							,sum(sales_item_prv_market_qtr)  total_sales_market_prv_qtr
							,sum(sales_item_cur_market_qtr)  total_sales_market_cur_qtr
							
							from(
							SELECT  dw_item_key, format_code,dw_store_key,is_same_store
							, sales_item_store_quartermatcur_45 as sales_item_format_cur_qtr
							, sales_item_store_quartermatprv_46 as sales_item_format_prv_qtr
							
							, sales_item_market_quartermatcur_90 as sales_item_cur_market_qtr
							, sales_item_market_quartermatprv_91 as sales_item_prv_market_qtr
			
							FROM migvan.old_items_measures 
							where  item_classification_42 = 1  and  item_format_examination_code_44 = 1  --and is_same_store=1
							and {FILTER_PLACEHOLDER}
							--and   user's selections : formats,stores, catalog
							--and   user's permission : formats,stores, catalog
							) as sub1
							group by   format_code,dw_store_key,is_same_store
						)as sub2
						)as sub3
					)as sub4
					) as sub5
