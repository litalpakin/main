----m_query6e - detailed_format - star and intermediate items---- 

with sub_cat_sales as
(
        select sub_category_key,format_code
        ,sum(salesstore_iteminformat_week0) sales_subcat_format_week0
        ,sum(sales_item_store_monthmatcur) sales_subcat_format_month0_tmp
        from migvan.assortment_item_store_indices
        where  {FILTER_PLACEHOLDER}
        group by sub_category_key,format_code

),
ass_item_sales as
(
	select avg(sales_ms_item_subcat_3)  total_avg_sales_ms_item_4 ,format_code
	from(
        select ni.dw_item_key,ni.sub_category_key,ni.format_code,item_format_classification_code
		
        ,case when item_format_classification_code={CLASSIFICATION_PLACEHOLDER1} 
		then case when max(sales_subcat_format_week0) = 0 then null else sum (salesstore_iteminformat_week0)/max(sales_subcat_format_week0) end 
		when item_format_classification_code={CLASSIFICATION_PLACEHOLDER2} 
		then case when max(sales_subcat_format_month0_tmp) = 0 then null else sum (sales_item_store_monthmatcur)/max(sales_subcat_format_month0_tmp) end end sales_ms_item_subcat_3
		
        FROM migvan.assortment_item_store_indices ni left join sub_cat_sales on ni.sub_category_key=sub_cat_sales.sub_category_key and ni.format_code=sub_cat_sales.format_code
		where ni.item_format_classification_code in ({CLASSIFICATION_PLACEHOLDER})
		and item_format_examination_code in (2,4,5,6,1,3,23)
		and {PREFIX_FILTER_PLACEHOLDER}
        group by ni.dw_item_key,ni.sub_category_key,ni.format_code,item_format_classification_code
        ) as ass_item_sales_inn1
	group by format_code

),
market_item_sales as
(

	 select avg(sales_ms_item_subcat_market_7) total_avg_sales_ms_item_market_8 , format_code
	 from (
        select dw_item_key,sub_category_key,format_code,item_format_classification_code
        ,case when item_format_classification_code={CLASSIFICATION_PLACEHOLDER1} 
		then case when max(salesmarket_subcat_week0) = 0 then null else max(salesmarket_item_week0)/max(salesmarket_subcat_week0) end
                  when item_format_classification_code={CLASSIFICATION_PLACEHOLDER2} 
				  then case when max(salesmarket_subcat_monthmatcur) = 0 then null else max(salesmarket_item_monthmatcur)/max(salesmarket_subcat_monthmatcur) end end sales_ms_item_subcat_market_7
        
        FROM migvan.assortment_item_store_indices
        where item_format_classification_code in ({CLASSIFICATION_PLACEHOLDER})
		and item_format_examination_code in (2,4,5,6,1,3,23)
		and  {FILTER_PLACEHOLDER}
        group by dw_item_key,sub_category_key,format_code,item_format_classification_code
        ) as market_item_sales_inn1
		group by format_code

)
select format_code,total_sales_ms_chain_market_13 as value_column -- Value to display
		,case when total_sales_ms_chain_market_13 >= 0.1 then 1
			  when total_sales_ms_chain_market_13 > -0.1 and total_sales_ms_chain_market_13 < 0.1 then 2
			  when total_sales_ms_chain_market_13 <= -0.1 then 3 end color_column -- Classification Color  
	from (
		select ass_item_sales.format_code,total_avg_sales_ms_item_4
		,total_avg_sales_ms_item_market_8
		--,total_avg_sales_ms_item_4 - total_avg_sales_ms_item_market_8 total_diff_sales_ms_chain_market_12
		,case when total_avg_sales_ms_item_market_8<>0 then(total_avg_sales_ms_item_4/ total_avg_sales_ms_item_market_8)-1 else null end total_sales_ms_chain_market_13 
		from ass_item_sales join market_item_sales on ass_item_sales.format_code=market_item_sales.format_code
		)as inn1;
