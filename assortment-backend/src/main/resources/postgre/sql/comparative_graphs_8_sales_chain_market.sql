with rp8_chain_market_sales as
(

SELECT  item_classification
, sales_chain_cy_mat
, sales_chain_py_mat
-- ,case when sales_chain_py_mat<=0 then 0 else sales_chain_cy_mat/sales_chain_py_mat-1 end 
,sales_change_chain_cypy_mat

, sales_market_cy_mat
, sales_market_py_mat
--,case when sales_market_py_mat<=0 then 0 else sales_market_cy_mat/sales_market_py_mat-1 end 
,sales_change_market_cypy_mat

,case when sales_change_chain_cypy_mat = 0 then null
      else (sales_change_market_cypy_mat-sales_change_chain_cypy_mat)/sales_change_chain_cypy_mat end sales_change_chain_to_market_cypy_mat
      
,case when sales_change_chain_cypy_mat = 0 then null
      else (sales_change_market_cypy_mat-sales_change_chain_cypy_mat)*sales_chain_cy_mat end potential_gap_sales_from_market_cypy_mat

, sales_chain_ss_cy_mat
, sales_chain_ss_py_mat
-- ,case when sales_chain_ss_py_mat<=0 then 0 else sales_chain_ss_cy_mat/sales_chain_ss_py_mat-1 end 
,sales_change_chain_ss_cypy_mat

,case when sales_change_chain_ss_cypy_mat = 0 then null
      else (sales_change_market_cypy_mat-sales_change_chain_ss_cypy_mat)/sales_change_chain_ss_cypy_mat end sales_change_chain_to_market_ss_cypy_mat
      
,case when sales_change_chain_ss_cypy_mat = 0 then null
      else (sales_change_market_cypy_mat-sales_change_chain_ss_cypy_mat)*sales_chain_cy_mat end potential_gap_sales_from_market_ss_cypy_mat
      
      
, sales_chain_cq_mat
, sales_chain_pq_mat
--,case when sales_chain_pq_mat<=0 then 0 else sales_chain_cq_mat/sales_chain_pq_mat-1 end 
,sales_change_chain_cqpq_mat

, sales_market_cq_mat
, sales_market_pq_mat
-- ,case when sales_market_pq_mat<=0 then 0 else sales_market_cq_mat/sales_market_pq_mat-1 end 
,sales_change_market_cqpq_mat

,case when sales_change_chain_cqpq_mat = 0 then null
      else (sales_change_market_cqpq_mat-sales_change_chain_cqpq_mat)/sales_change_chain_cqpq_mat end sales_change_chain_to_market_cqpq_mat
      
,case when sales_change_chain_cqpq_mat = 0 then null
      else (sales_change_market_cqpq_mat-sales_change_chain_cqpq_mat)*sales_chain_cq_mat end potential_gap_sales_from_market_cqpq_mat
      
, sales_chain_ss_cq_mat
, sales_chain_ss_pq_mat
-- ,case when sales_chain_ss_pq_mat<=0 then 0 else sales_chain_ss_cq_mat/sales_chain_ss_pq_mat-1 end 
,sales_change_chain_ss_cqpq_mat

,case when sales_change_chain_ss_cqpq_mat = 0 then null
      else (sales_change_market_cqpq_mat-sales_change_chain_ss_cqpq_mat)/sales_change_chain_ss_cqpq_mat end sales_change_chain_to_market_ss_cqpq_mat
      
,case when sales_change_chain_ss_cqpq_mat = 0 then null
      else (sales_change_market_cqpq_mat-sales_change_chain_ss_cqpq_mat)*sales_chain_cy_mat end potential_gap_sales_from_market_ss_cqpq_mat      

, sales_chain_ch_mat
, sales_chain_ph_mat
--,case when sales_chain_ph_mat<=0 then 0 else sales_chain_ch_mat/sales_chain_ph_mat-1 end 
,sales_change_chain_chph_mat

, sales_market_ch_mat
, sales_market_ph_mat
-- ,case when sales_market_ph_mat<=0 then 0 else sales_market_ch_mat/sales_market_ph_mat-1 end 
, sales_change_market_chph_mat

,case when sales_change_chain_chph_mat = 0 then null
      else (sales_change_market_chph_mat-sales_change_chain_chph_mat)/sales_change_chain_chph_mat end sales_change_chain_to_market_chph_mat
      
,case when sales_change_chain_chph_mat = 0 then null
      else (sales_change_market_chph_mat-sales_change_chain_chph_mat)*sales_chain_ch_mat end potential_gap_sales_from_market_chph_mat 

,sales_chain_ss_ch_mat
,sales_chain_ss_ph_mat
-- ,case when sales_chain_ss_ph_mat<=0 then 0 else sales_chain_ss_ch_mat/sales_chain_ss_ph_mat-1 end 
, sales_change_chain_ss_chph_mat

,case when sales_change_chain_ss_chph_mat = 0 then null
      else (sales_change_market_chph_mat-sales_change_chain_ss_chph_mat)/sales_change_chain_ss_chph_mat end sales_change_chain_to_market_ss_chph_mat
      
,case when sales_change_chain_ss_chph_mat = 0 then null
      else (sales_change_market_chph_mat-sales_change_chain_ss_chph_mat)*sales_chain_cy_mat end potential_gap_sales_from_market_ss_chph_mat
from (
     SELECT item_format_classification_61 item_classification
,case when sum(sales_item_market_yearmatprv_79)<=0 then 0 
    else sum(sales_item_market_yearmatcur_78)/sum(sales_item_market_yearmatprv_79)-1 end sales_change_market_cypy_mat
,case when sum(sales_item_format_prv_2)<=0 then 0 
    else sum(sales_item_format_cur_1)/sum(sales_item_format_prv_2)-1 end sales_change_chain_cypy_mat
,case when sum(sales_item_format_ss_prv)<=0 then 0 
    else sum(sales_item_format_ss_cur)/sum(sales_item_format_ss_prv)-1 end sales_change_chain_ss_cypy_mat
,case when sum(sales_item_format_quartermatprv_65)<=0 then 0 
    else sum(sales_item_format_quartermatcur_64)/sum(sales_item_format_quartermatprv_65)-1 end sales_change_chain_cqpq_mat
,case when sum(sales_item_market_quartermatprv_86)<=0 then 0 
    else sum(sales_item_market_quartermatcur_85)/sum(sales_item_market_quartermatprv_86)-1 end sales_change_market_cqpq_mat

,case when sum(sales_item_format_ss_quartermatprv)<=0 then 0
    else sum(sales_item_format_ss_quartermatcur)/sum(sales_item_format_ss_quartermatprv)-1 end sales_change_chain_ss_cqpq_mat

,sum(sales_item_format_cur_1) sales_chain_cy_mat
,sum(sales_item_format_prv_2) sales_chain_py_mat
,sum(sales_item_market_yearmatcur_78) sales_market_cy_mat
,sum(sales_item_market_yearmatprv_79) sales_market_py_mat
,sum(sales_item_format_ss_cur) sales_chain_ss_cy_mat
,sum(sales_item_format_ss_prv) sales_chain_ss_py_mat
,sum(sales_item_format_quartermatcur_64) sales_chain_cq_mat
,sum(sales_item_format_quartermatprv_65) sales_chain_pq_mat
,sum(sales_item_market_quartermatcur_85) sales_market_cq_mat
,sum(sales_item_market_quartermatprv_86) sales_market_pq_mat
,sum(sales_item_format_ss_quartermatcur) sales_chain_ss_cq_mat
,sum(sales_item_format_ss_quartermatprv) sales_chain_ss_pq_mat

,case when sum(sales_item_format_halfmatprv_163)<=0 then 0 
    else sum(sales_item_format_halfmatcur_162)/sum(sales_item_format_halfmatprv_163)-1 end sales_change_chain_chph_mat

,sum(sales_item_format_halfmatcur_162) sales_chain_ch_mat
,sum(sales_item_format_halfmatprv_163) sales_chain_ph_mat

,case when sum(sales_item_market_halfmatprv_165)<=0 then 0 
    else sum(sales_item_market_halfmatcur_164)/sum(sales_item_market_halfmatprv_165)-1 end sales_change_market_chph_mat

,sum(sales_item_market_halfmatcur_164) sales_market_ch_mat
,sum(sales_item_market_halfmatprv_165) sales_market_ph_mat

,case when sum(sales_item_format_ss_halfmatprv)<=0 then 0 
    else sum(sales_item_format_ss_halfmatcur)/sum(sales_item_format_ss_halfmatprv)-1 end sales_change_chain_ss_chph_mat
,sum(sales_item_format_ss_halfmatcur) sales_chain_ss_ch_mat
,sum(sales_item_format_ss_halfmatprv) sales_chain_ss_ph_mat
FROM migvan.old_items_id
where item_format_classification_61  = :classification_item
and format_code = :formatParam
group by item_classification
    

    ) t
group by item_classification
, sales_chain_cy_mat, sales_chain_py_mat, sales_change_chain_cypy_mat, sales_market_cy_mat, sales_market_py_mat
,sales_change_market_cypy_mat, sales_chain_ss_cy_mat, sales_chain_ss_py_mat,sales_change_chain_ss_cypy_mat
, sales_chain_cq_mat, sales_chain_pq_mat,sales_change_chain_cqpq_mat, sales_market_cq_mat, sales_market_pq_mat,sales_change_market_cqpq_mat
, sales_chain_ss_cq_mat, sales_chain_ss_pq_mat,sales_change_chain_ss_cqpq_mat,sales_chain_ch_mat, sales_chain_ph_mat
, sales_change_chain_chph_mat, sales_market_ch_mat, sales_market_ph_mat
, sales_change_market_chph_mat,sales_chain_ss_ch_mat,sales_chain_ss_ph_mat, sales_change_chain_ss_chph_mat

),
rp8_user_chain_market_sales as
(
SELECT  item_classification
, user_sales_chain_cy_mat
, user_sales_chain_py_mat
-- ,case when user_sales_chain_py_mat<=0 then 0 else user_sales_chain_cy_mat/user_sales_chain_py_mat-1 end 
, user_sales_change_chain_cypy_mat

, user_sales_market_cy_mat
, user_sales_market_py_mat
-- ,case when user_sales_market_py_mat<=0 then 0 else user_sales_market_cy_mat/user_sales_market_py_mat-1 end 
,user_sales_change_market_cypy_mat

,case when user_sales_change_chain_cypy_mat = 0 then null
      else (user_sales_change_market_cypy_mat-user_sales_change_chain_cypy_mat)/user_sales_change_chain_cypy_mat end user_sales_change_chain_to_market_cypy_mat
      
,case when user_sales_change_chain_cypy_mat = 0 then null
      else (user_sales_change_market_cypy_mat-user_sales_change_chain_cypy_mat)*user_sales_chain_cy_mat end user_potential_gap_sales_from_market_cypy_mat
      
, user_sales_chain_ss_cy_mat
, user_sales_chain_ss_py_mat
--,case when user_sales_chain_ss_py_mat<=0 then 0 else user_sales_chain_ss_cy_mat/user_sales_chain_ss_py_mat-1 end 
,user_sales_change_chain_ss_cypy_mat

,case when user_sales_change_chain_ss_cypy_mat = 0 then null
      else (user_sales_change_market_cypy_mat-user_sales_change_chain_ss_cypy_mat)/user_sales_change_chain_ss_cypy_mat end user_sales_change_chain_to_market_ss_cypy_mat
      
,case when user_sales_change_chain_ss_cypy_mat = 0 then null
      else (user_sales_change_market_cypy_mat-user_sales_change_chain_ss_cypy_mat)*user_sales_chain_cy_mat end user_potential_gap_sales_from_market_ss_cypy_mat


, user_sales_chain_cq_mat
, user_sales_chain_pq_mat
--,case when user_sales_chain_pq_mat<=0 then 0 else user_sales_chain_cq_mat/user_sales_chain_pq_mat-1 end 
, user_sales_change_chain_cqpq_mat

, user_sales_market_cq_mat
, user_sales_market_pq_mat
--,case when user_sales_market_pq_mat<=0 then 0 else user_sales_market_cq_mat/user_sales_market_pq_mat-1 end 
, user_sales_change_market_cqpq_mat

,case when user_sales_change_chain_cqpq_mat = 0 then null
      else (user_sales_change_market_cqpq_mat-user_sales_change_chain_cqpq_mat)/user_sales_change_chain_cqpq_mat end user_sales_change_chain_to_market_cqpq_mat
      
,case when user_sales_change_chain_cqpq_mat = 0 then null
      else (user_sales_change_market_cqpq_mat-user_sales_change_chain_cqpq_mat)*user_sales_chain_cq_mat end user_potential_gap_sales_from_market_cqpq_mat  
      
, user_sales_chain_ss_cq_mat
, user_sales_chain_ss_pq_mat
--,case when user_sales_chain_ss_pq_mat<=0 then 0 else user_sales_chain_ss_cq_mat/user_sales_chain_ss_pq_mat-1 end 
, user_sales_change_chain_ss_cqpq_mat

,case when user_sales_change_chain_ss_cqpq_mat = 0 then null
      else (user_sales_change_market_cqpq_mat-user_sales_change_chain_ss_cqpq_mat)/user_sales_change_chain_ss_cqpq_mat end user_sales_change_chain_to_market_ss_cqpq_mat
      
,case when user_sales_change_chain_ss_cqpq_mat = 0 then null
      else (user_sales_change_market_cqpq_mat-user_sales_change_chain_ss_cqpq_mat)*user_sales_chain_cq_mat end user_potential_gap_sales_from_market_ss_cqpq_mat


, user_sales_chain_ch_mat
, user_sales_chain_ph_mat
--,case when user_sales_chain_ph_mat<=0 then 0 else user_sales_chain_ch_mat/user_sales_chain_ph_mat-1 end 
, user_sales_change_chain_chph_mat

, user_sales_market_ch_mat
, user_sales_market_ph_mat
--,case when user_sales_market_ph_mat<=0 then 0 else user_sales_market_ch_mat/user_sales_market_ph_mat-1 end 
,user_sales_change_market_chph_mat

,case when user_sales_change_chain_chph_mat = 0 then null
      else (user_sales_change_market_chph_mat-user_sales_change_chain_chph_mat)/user_sales_change_chain_chph_mat end user_sales_change_chain_to_market_chph_mat

,case when user_sales_change_chain_chph_mat = 0 then null
      else (user_sales_change_market_chph_mat-user_sales_change_chain_chph_mat)*user_sales_chain_ch_mat end user_potential_gap_sales_from_market_chph_mat
      
      
, user_sales_chain_ss_ch_mat
, user_sales_chain_ss_ph_mat
--,case when user_sales_chain_ss_ph_mat<=0 then 0 else user_sales_chain_ss_ch_mat/user_sales_chain_ss_ph_mat-1 end 
,user_sales_change_chain_ss_chph_mat

,case when user_sales_change_chain_ss_chph_mat = 0 then null
      else (user_sales_change_market_chph_mat-user_sales_change_chain_ss_chph_mat)/user_sales_change_chain_ss_chph_mat end user_sales_change_chain_to_market_ss_chph_mat
      
,case when user_sales_change_chain_ss_chph_mat = 0 then null
      else (user_sales_change_market_chph_mat-user_sales_change_chain_ss_chph_mat)*user_sales_chain_ch_mat end user_potential_gap_sales_from_market_ss_chph_mat
      
      
from
(
    SELECT  item_classification
,sum(user_sales_chain_cy_mat_tmp) user_sales_chain_cy_mat
,sum(user_sales_chain_py_mat_tmp) user_sales_chain_py_mat
,case when sum(user_sales_chain_py_mat_tmp)<=0 then 0 
    else sum(user_sales_chain_cy_mat_tmp)/sum(user_sales_chain_py_mat_tmp)-1 end user_sales_change_chain_cypy_mat

,sum(user_sales_market_cy_mat_tmp) user_sales_market_cy_mat
,sum(user_sales_market_py_mat_tmp) user_sales_market_py_mat
,case when sum(user_sales_market_py_mat_tmp)<=0 then 0 
    else sum(user_sales_market_cy_mat_tmp)/sum(user_sales_market_py_mat_tmp)-1 end user_sales_change_market_cypy_mat
      
,sum(user_sales_chain_ss_cy_mat_tmp) user_sales_chain_ss_cy_mat
,sum(user_sales_chain_ss_py_mat_tmp) user_sales_chain_ss_py_mat
,case when sum(user_sales_chain_ss_py_mat_tmp)<=0 then 0 
    else sum(user_sales_chain_ss_cy_mat_tmp)/sum(user_sales_chain_ss_py_mat_tmp)-1 end user_sales_change_chain_ss_cypy_mat

,sum(user_sales_chain_cq_mat_tmp) user_sales_chain_cq_mat
,sum(user_sales_chain_pq_mat_tmp) user_sales_chain_pq_mat
,case when sum(user_sales_chain_pq_mat_tmp)<=0 then 0 
    else sum(user_sales_chain_cq_mat_tmp)/sum(user_sales_chain_pq_mat_tmp)-1 end user_sales_change_chain_cqpq_mat

,sum(user_sales_market_cq_mat_tmp) user_sales_market_cq_mat
,sum(user_sales_market_pq_mat_tmp) user_sales_market_pq_mat
,case when sum(user_sales_market_pq_mat_tmp)<=0 then 0 
    else sum(user_sales_market_cq_mat_tmp)/sum(user_sales_market_pq_mat_tmp)-1 end user_sales_change_market_cqpq_mat
      
,sum(user_sales_chain_ss_cq_mat_tmp) user_sales_chain_ss_cq_mat
,sum(user_sales_chain_ss_pq_mat_tmp) user_sales_chain_ss_pq_mat
,case when sum(user_sales_chain_ss_pq_mat_tmp)<=0 then 0 else sum(user_sales_chain_ss_cq_mat_tmp)/sum(user_sales_chain_ss_pq_mat_tmp)-1 end user_sales_change_chain_ss_cqpq_mat

,sum(user_sales_chain_ch_mat_tmp) user_sales_chain_ch_mat
,sum(user_sales_chain_ph_mat_tmp) user_sales_chain_ph_mat
,case when sum(user_sales_chain_ph_mat_tmp)<=0 then 0 
    else sum(user_sales_chain_ch_mat_tmp)/sum(user_sales_chain_ph_mat_tmp)-1 end  user_sales_change_chain_chph_mat

,sum(user_sales_market_ch_mat_tmp) user_sales_market_ch_mat
,sum(user_sales_market_ph_mat_tmp) user_sales_market_ph_mat
,case when sum(user_sales_market_ph_mat_tmp)<=0 then 0 
    else sum(user_sales_market_ch_mat_tmp)/sum(user_sales_market_ph_mat_tmp)-1 end  user_sales_change_market_chph_mat
      
,sum(user_sales_chain_ss_ch_mat_tmp) user_sales_chain_ss_ch_mat
,sum(user_sales_chain_ss_ph_mat_tmp) user_sales_chain_ss_ph_mat
,case when sum(user_sales_chain_ss_ph_mat_tmp)<=0 then 0 
             else sum(user_sales_chain_ss_ch_mat_tmp)/sum(user_sales_chain_ss_ph_mat_tmp)-1 end user_sales_change_chain_ss_chph_mat

    FROM (
select dw_item_key,format_code, item_classification_42 item_classification
,sum(sales_item_store_cur_1) user_sales_chain_cy_mat_tmp
,sum(sales_item_store_prv_2) user_sales_chain_py_mat_tmp

,sum(sales_item_store_ss_cur) user_sales_chain_ss_cy_mat_tmp
,sum(sales_item_store_ss_prv) user_sales_chain_ss_py_mat_tmp

,max(sales_item_market_yearmatcur_88) user_sales_market_cy_mat_tmp
,max(sales_item_market_yearmatprv_89) user_sales_market_py_mat_tmp

,sum(sales_item_store_quartermatcur_45) user_sales_chain_cq_mat_tmp
,sum(sales_item_store_quartermatprv_46) user_sales_chain_pq_mat_tmp

,sum(sales_item_store_ss_quartermatcur) user_sales_chain_ss_cq_mat_tmp
,sum(sales_item_store_ss_quartermatprv) user_sales_chain_ss_pq_mat_tmp

,max(sales_item_market_quartermatcur_90) user_sales_market_cq_mat_tmp
,max(sales_item_market_quartermatprv_91) user_sales_market_pq_mat_tmp

,sum(sales_item_store_halfmatcur_47) user_sales_chain_ch_mat_tmp
,sum(sales_item_store_halfmatprv_48) user_sales_chain_ph_mat_tmp

,sum(sales_item_store_ss_halfmatcur) user_sales_chain_ss_ch_mat_tmp
,sum(sales_item_store_ss_halfmatprv) user_sales_chain_ss_ph_mat_tmp

,max(sales_item_market_halfmatcur_92) user_sales_market_ch_mat_tmp
,max(sales_item_market_halfmatprv_93) user_sales_market_ph_mat_tmp

FROM migvan.old_items_measures 

where item_classification_42  = :classification_item
and format_code = :formatParam
and {FILTER_PLACEHOLDER}

group by dw_item_key,format_code, item_classification_42
) t
group by item_classification
) b
group by item_classification, user_sales_chain_cy_mat, user_sales_chain_py_mat, user_sales_change_chain_cypy_mat
,user_sales_market_cy_mat,user_sales_market_py_mat,user_sales_change_market_cypy_mat,user_sales_chain_ss_cy_mat
,user_sales_chain_ss_py_mat,user_sales_change_chain_ss_cypy_mat,user_sales_chain_cq_mat,user_sales_chain_pq_mat
,user_sales_change_chain_cqpq_mat,user_sales_market_cq_mat, user_sales_market_pq_mat,user_sales_change_market_cqpq_mat
,user_sales_chain_ss_cq_mat,user_sales_chain_ss_pq_mat,user_sales_change_chain_ss_cqpq_mat,user_sales_chain_ch_mat
,user_sales_chain_ph_mat,user_sales_change_chain_chph_mat,user_sales_market_ch_mat,user_sales_market_ph_mat
,user_sales_change_market_chph_mat,user_sales_chain_ss_ch_mat,user_sales_chain_ss_ph_mat,user_sales_change_chain_ss_chph_mat
)

select cs.item_classification
,sales_change_chain_cypy_mat,sales_change_market_cypy_mat,sales_change_chain_to_market_cypy_mat,potential_gap_sales_from_market_cypy_mat
,sales_change_chain_cqpq_mat,sales_change_market_cqpq_mat,sales_change_chain_to_market_cqpq_mat,potential_gap_sales_from_market_cqpq_mat
,sales_change_chain_chph_mat,sales_change_market_chph_mat,sales_change_chain_to_market_chph_mat,potential_gap_sales_from_market_chph_mat

,sales_change_chain_ss_cypy_mat,sales_change_chain_to_market_ss_cypy_mat,potential_gap_sales_from_market_ss_cypy_mat
,sales_change_chain_ss_cqpq_mat,sales_change_chain_to_market_ss_cqpq_mat,potential_gap_sales_from_market_ss_cqpq_mat
,sales_change_chain_ss_chph_mat,sales_change_chain_to_market_ss_chph_mat,potential_gap_sales_from_market_ss_chph_mat

,user_sales_change_chain_cypy_mat,user_sales_change_market_cypy_mat,user_sales_change_chain_to_market_cypy_mat,user_potential_gap_sales_from_market_cypy_mat    
,user_sales_change_chain_cqpq_mat,user_sales_change_market_cqpq_mat,user_sales_change_chain_to_market_cqpq_mat,user_potential_gap_sales_from_market_cqpq_mat
,user_sales_change_chain_chph_mat,user_sales_change_market_chph_mat,user_sales_change_chain_to_market_chph_mat,user_potential_gap_sales_from_market_chph_mat

,user_sales_change_chain_ss_cypy_mat,user_sales_change_chain_to_market_ss_cypy_mat,user_potential_gap_sales_from_market_ss_cypy_mat
,user_sales_change_chain_ss_cqpq_mat,user_sales_change_chain_to_market_ss_cqpq_mat,user_potential_gap_sales_from_market_ss_cqpq_mat
,user_sales_change_chain_ss_chph_mat,user_sales_change_chain_to_market_ss_chph_mat,user_potential_gap_sales_from_market_ss_chph_mat

from rp8_chain_market_sales cs join rp8_user_chain_market_sales us on cs.item_classification=us.item_classification