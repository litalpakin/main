selected_store_agg as
(
  select *
    from(
      SELECT *
     	,sum(sales_brand_subcat_cur) over (partition by category_key,format_code) sales_category_format_cur
      ,sum(sales_brand_subcat_prv) over (partition by category_key,format_code) sales_category_format_prv
      ,sum(sales_brand_subcat_quartermatcur) over (partition by category_key,format_code) sales_category_format_quartermatcur
      ,sum(sales_brand_subcat_quartermatprv) over (partition by category_key,format_code) sales_category_format_quartermatprv
      ,sum(sales_brand_subcat_halfmatcur) over (partition by category_key,format_code) sales_category_format_halfmatcur
      ,sum(sales_brand_subcat_halfmatprv) over (partition by category_key,format_code) sales_category_format_halfmatprv

      ,sum(sales_brand_subcat_cur) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_cur
      ,sum(sales_brand_subcat_prv) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_prv
      ,sum(sales_brand_subcat_quartermatcur) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_quartermatcur
      ,sum(sales_brand_subcat_quartermatprv) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_quartermatprv
      ,sum(sales_brand_subcat_halfmatcur) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_halfmatcur
      ,sum(sales_brand_subcat_halfmatprv) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_halfmatprv

      ,sum(sales_brand_subcat_cur) over (partition by sub_brand_hierarchy_2_key, format_code) sales_brand_in_format_yearmatcur

      ,sum(sales_brand_subcat_cur) over (partition by format_code) sales_format_yearmatcur
      ,sum(sales_brand_subcat_prv) over (partition by format_code) sales_format_yearmatprv
      ,sum(sales_brand_subcat_quartermatcur) over (partition by format_code) sales_format_quartermatcur
      ,sum(sales_brand_subcat_quartermatprv) over (partition by format_code) sales_format_quartermatprv
      ,sum(sales_brand_subcat_halfmatcur) over (partition by format_code) sales_format_halfmatcur
      ,sum(sales_brand_subcat_halfmatprv) over (partition by format_code) sales_format_halfmatprv
      ,sum(qty_brand_subcat_cur) over (partition by category_key,format_code) qty_sales_category_format_cur
      ,sum(qty_brand_subcat_prv) over (partition by category_key,format_code) qty_sales_category_format_prv
      ,sum(qty_brand_subcat_quartermatcur) over (partition by category_key,format_code) qty_sales_category_format_quartermatcur
      ,sum(qty_brand_subcat_quartermatprv) over (partition by category_key,format_code) qty_sales_category_format_quartermatprv
      ,sum(qty_brand_subcat_halfmatcur) over (partition by category_key,format_code) qty_sales_category_format_halfmatcur
      ,sum(qty_brand_subcat_halfmatprv) over (partition by category_key,format_code) qty_sales_category_format_halfmatprv

      ,sum(qty_brand_subcat_cur) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_cur
      ,sum(qty_brand_subcat_prv) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_prv
      ,sum(qty_brand_subcat_quartermatcur) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_quartermatcur
      ,sum(qty_brand_subcat_quartermatprv) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_quartermatprv
      ,sum(qty_brand_subcat_halfmatcur) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_halfmatcur
      ,sum(qty_brand_subcat_halfmatprv) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_halfmatprv

      ,sum(qty_brand_subcat_cur) over (partition by sub_brand_hierarchy_2_key, format_code) qty_brand_in_format_cur
      ,sum(qty_brand_subcat_cur) over (partition by format_code) qty_sales_in_format_cur
---
      ,sum(sales_brand_subcat_cur_ss) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_cur_ss
	  ,sum(sales_brand_subcat_prv_ss) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_prv_ss
	  ,sum(sales_brand_subcat_cur_ss) over (partition by format_code) sales_format_yearmatcur_ss
      ,sum(sales_brand_subcat_prv_ss) over (partition by format_code) sales_format_yearmatprv_ss
  ,max(sales_brand_subcat_cur) over (partition by sub_brand_hierarchy_2_key,sub_category_key, category_key, format_code) sales_brand_subcat_format_cur
---
  FROM (
          select
           sub_brand_hierarchy_2_key,sub_category_key, category_key, format_code

          ,sum(sales_brand_subcat_store_cur) sales_brand_subcat_cur
          ,sum(sales_brand_subcat_store_prv) sales_brand_subcat_prv
---
         	,sum(case when is_same_store=1 then sales_brand_subcat_store_cur end) sales_brand_subcat_cur_ss
        	,sum(case when is_same_store=1 then sales_brand_subcat_store_prv end) sales_brand_subcat_prv_ss
---
          ,sum(sales_brand_subcat_store_quartermatcur) sales_brand_subcat_quartermatcur
          ,sum(sales_brand_subcat_store_quartermatprv) sales_brand_subcat_quartermatprv
          ,sum(sales_brand_subcat_store_halfmatcur) sales_brand_subcat_halfmatcur
          ,sum(sales_brand_subcat_store_halfmatprv) sales_brand_subcat_halfmatprv

          ,sum(qty_brand_subcat_store_cur) qty_brand_subcat_cur
          ,sum(qty_brand_subcat_store_prv) qty_brand_subcat_prv
          ,sum(qty_brand_subcat_store_quartermatcur) qty_brand_subcat_quartermatcur
          ,sum(qty_brand_subcat_store_quartermatprv) qty_brand_subcat_quartermatprv
          ,sum(qty_brand_subcat_store_halfmatcur) qty_brand_subcat_halfmatcur
          ,sum(qty_brand_subcat_store_halfmatprv) qty_brand_subcat_halfmatprv
		  
		 ,max(selected_store_agg_2.count_stores_cur) count_selectedstores_yearmatcur
		 ,max(selected_store_agg_2.count_stores_quartermatcur) count_selectedstores_quartermatcur
	     ,max(selected_store_agg_2.count_stores_halfmatcur) count_selectedstores_halfmatcur

	
		from migvan.assortment_store_indices cross join selected_store_agg_2
        where  {STORES_FILTER_PLACEHOLDER}
        group by  sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
        ) k
    ) t
	--where  sub_category_key is not null  and  sub_brand_hierarchy_2_key in (34983)
	where  {CATEGORY_FILTER_PLACEHOLDER}
)
