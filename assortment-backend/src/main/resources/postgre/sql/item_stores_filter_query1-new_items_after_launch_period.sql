----item_stores_filter_query1-new_items_after_launch_period----

select *
from (
    select *
       ,case when coalesce(sales_ms_item_subcat_store_month1,0) <= 0 or coalesce(sales_ms_item_subcat_store_month0,0) <= 0 then null else
       (sales_ms_item_subcat_store_month0/sales_ms_item_subcat_store_month1)-1 end sales_change_ms_item_subcat_store_month0to1
       ,case when coalesce(sales_ms_item_subcat_store_month2,0) <= 0 or coalesce(sales_ms_item_subcat_store_month1,0) <= 0 then null else
       (sales_ms_item_subcat_store_month1/sales_ms_item_subcat_store_month2)-1 end sales_change_ms_item_subcat_store_month1to2
       ,case when coalesce(sales_ms_item_subcat_store_month2,0) <= 0 or coalesce(sales_ms_item_subcat_store_month0,0) <= 0 then null else
       (sales_ms_item_subcat_store_month0/sales_ms_item_subcat_store_month2)-1 end sales_change_ms_item_subcat_store_month0to2
       ,case when coalesce(qty_item_format_store_month1,0) <= 0 or coalesce(qty_item_format_store_month0,0) <= 0 then null else (qty_item_format_store_month0/qty_item_format_store_month1)-1 end qty_change_item_store_month0to1
       ,case when coalesce(qty_item_format_store_month2,0) <= 0 or coalesce(qty_item_format_store_month1,0) <= 0 then null else (qty_item_format_store_month1/qty_item_format_store_month2)-1 end qty_change_item_store_month1to2
       ,case when coalesce(qty_item_format_store_month2,0) <= 0 or coalesce(qty_item_format_store_month0,0) <= 0 then null else (qty_item_format_store_month0/qty_item_format_store_month2)-1 end qty_change_item_store_month0to2

         ,case when coalesce(qty_ms_item_subcat_store_month1,0) <= 0 or coalesce(qty_ms_item_subcat_store_month0,0) <= 0 then null else
         (qty_ms_item_subcat_store_month0/qty_ms_item_subcat_store_month1)-1 end qty_change_ms_item_subcat_store_month0to1
         ,case when coalesce(qty_ms_item_subcat_store_month2,0) <= 0 or coalesce(qty_ms_item_subcat_store_month1,0) <= 0 then null else
         (qty_ms_item_subcat_store_month2/qty_ms_item_subcat_store_month1)-1 end qty_change_ms_item_subcat_store_month1to2
         ,case when coalesce(qty_ms_item_subcat_store_month2,0) <= 0 or coalesce(qty_ms_item_subcat_store_month0,0) <= 0 then null else
         (qty_ms_item_subcat_store_month2/qty_ms_item_subcat_store_month0)-1 end qty_change_ms_item_subcat_store_month0to2

         ,case when is_pass_potential_ms_test=1 and (salesmsmarket_iteminsubcat_3months/sales_ms_item_subcat_store_3months)>0 and sales_ms_item_subcat_store_3months>0 and salesmsmarket_iteminsubcat_3months>0 and (sales_item_format_store_month0+sales_item_format_store_month1+sales_item_format_store_month2)>0
          then (salesmsmarket_iteminsubcat_3months/sales_ms_item_subcat_store_3months)*(sales_item_format_store_month0+sales_item_format_store_month1+sales_item_format_store_month2) else null end sales_potential_market_to_store_3months



    from (
      SELECT *

       ,case when coalesce(qty_item_format_store_month0,0) <= 0 or coalesce(qty_subcat_format_store_month0,0) <= 0 then null else
        (qty_item_format_store_month0/qty_subcat_format_store_month0) end qty_ms_item_subcat_store_month0
       ,case when coalesce(qty_item_format_store_month1,0) <= 0 or coalesce(qty_subcat_format_store_month1,0) <= 0 then null else
        (qty_item_format_store_month1/qty_subcat_format_store_month1) end qty_ms_item_subcat_store_month1
       ,case when coalesce(qty_item_format_store_month2,0) <= 0 or coalesce(qty_subcat_format_store_month2,0) <= 0 then null else
        (qty_item_format_store_month2/qty_subcat_format_store_month2) end qty_ms_item_subcat_store_month2

         ,case when coalesce(sales_item_format_store_month1,0) <= 0 or coalesce(sales_item_format_store_month0,0) <= 0 then null else (sales_item_format_store_month0/sales_item_format_store_month1)-1 end sales_change_item_store_month0to1
         ,case when coalesce(sales_item_format_store_month2,0) <= 0 or coalesce(sales_item_format_store_month1,0) <= 0 then null else (sales_item_format_store_month1/sales_item_format_store_month2)-1 end sales_change_item_store_month1to2
         ,case when coalesce(sales_item_format_store_month2,0) <= 0 or coalesce(sales_item_format_store_month0,0) <= 0 then null else (sales_item_format_store_month0/sales_item_format_store_month2)-1 end sales_change_item_store_month0to2
         ,case when coalesce(sales_item_format_store_month0,0) <= 0 or coalesce(sales_subcat_format_store_month0,0) <= 0 then null else
          (sales_item_format_store_month0/sales_subcat_format_store_month0) end sales_ms_item_subcat_store_month0
         ,case when coalesce(sales_item_format_store_month1,0) <= 0 or coalesce(sales_subcat_format_store_month1,0) <= 0 then null else
          (sales_item_format_store_month1/sales_subcat_format_store_month1) end sales_ms_item_subcat_store_month1
         ,case when coalesce(sales_item_format_store_month2,0) <= 0 or coalesce(sales_subcat_format_store_month2,0) <= 0 then null else
          (sales_item_format_store_month2/sales_subcat_format_store_month2) end sales_ms_item_subcat_store_month2
         ,case when coalesce(sales_item_format_store_month0,0) <= 0 or coalesce(sales_category_format_store_month0,0) <= 0 then null else
         (sales_item_format_store_month0/sales_category_format_store_month0) end sales_ms_item_category_store_month0
         ,case when coalesce(sales_item_format_store_month1,0) <= 0 or coalesce(sales_category_format_store_month1,0) <= 0 then null else
         (sales_item_format_store_month1/sales_category_format_store_month1) end sales_ms_item_category_store_month1
         ,case when coalesce(sales_item_format_store_month2,0) <= 0 or coalesce(sales_category_format_store_month2,0) <= 0 then null else
         (sales_item_format_store_month2/sales_category_format_store_month2) end sales_ms_item_category_store_month2

         ,case when coalesce(sales_subcat_format_store_month1,0) <= 0 or coalesce(sales_subcat_format_store_month0,0) <= 0 then null else (sales_subcat_format_store_month0/sales_subcat_format_store_month1)-1 end sales_change_subcat_store_month0to1
         ,case when coalesce(sales_subcat_format_store_month2,0) <= 0 or coalesce(sales_subcat_format_store_month1,0) <= 0 then null else (sales_subcat_format_store_month1/sales_subcat_format_store_month2)-1 end sales_change_subcat_store_month1to2
         ,case when coalesce(sales_subcat_format_store_month2,0) <= 0 or coalesce(sales_subcat_format_store_month0,0) <= 0 then null else (sales_subcat_format_store_month0/sales_subcat_format_store_month2)-1 end sales_change_subcat_store_month0to2

         ,case when coalesce(sales_category_format_store_month1,0) <= 0 or coalesce(sales_category_format_store_month0,0) <= 0 then null else (sales_category_format_store_month0/sales_category_format_store_month1)-1 end sales_change_category_store_month0to1
         ,case when coalesce(sales_category_format_store_month2,0) <= 0 or coalesce(sales_category_format_store_month1,0) <= 0 then null else (sales_category_format_store_month1/sales_category_format_store_month2)-1 end sales_change_category_store_month1to2
         ,case when coalesce(sales_category_format_store_month2,0) <= 0 or coalesce(sales_category_format_store_month0,0) <= 0 then null else (sales_category_format_store_month0/sales_category_format_store_month2)-1 end sales_change_category_store_month0to2
         ,case when coalesce(avg_monthly_item_store_sales_3months,0) <= 0 or coalesce(avg_monthly_item_market_qty_price_3months,0) <= 0 then null else
         (avg_monthly_item_market_qty_price_3months/avg_monthly_item_store_sales_3months)-1 end qty_price_item_market_to_store_gap_3months

		 ,case when ((salesmsmarket_categoryinmarket_3months/sales_ms_category_store_3months)-1>=0.05) and ((salesmsmarket_subcatinmarket_3months/sales_ms_subcat_store_3months)-1>=0.05) then 1 else 0 end is_pass_potential_ms_test


        FROM (
         select  dw_item_key
         ,dw_store_key,is_same_store
         ,sub_category_key, retailer_sub_category_key
         ,category_key, retailer_category_key
         ,sub_brand_hierarchy_2_key, retailer_sub_brand_hierarchy_2_key
         ,class_key, retailer_class_key
         ,supplier_key , retailer_supplier_key
         ,format_code
		 ,general_status_key, general_status_name,is_united_dw_item_key,is_private_label{REGIONAL_ITEMS_PLACEHOLDER}
         ,is_special_new_item as is_special_item
          , salesmsmarket_subcatinmarket_3months
          , salesmsmarket_iteminsubcat_3months
          , salesstore_subcatinformat_3months
         , item_format_examination_code
         , item_format_classification_code item_classification
         , is_item_classification_equal_for_all_format
         , total_weeks_since_launche
         , count(dw_item_key) over(partition by format_code,dw_item_key) num_stores
         , store_group_classification_3months store_group_classification
		 , item_with_enough_stores_for_abc
         , salesmsmarket_categoryinmarket_3months
         ,case when coalesce((salesstore_format_month0+salesstore_format_month1+salesstore_format_month2),0) <= 0 or coalesce((salesstore_categoryinformat_month0+salesstore_categoryinformat_month1+salesstore_categoryinformat_month2),0) <= 0 then null else ((salesstore_categoryinformat_month0+salesstore_categoryinformat_month1+salesstore_categoryinformat_month2)/(salesstore_format_month0+salesstore_format_month1+salesstore_format_month2)) end sales_ms_category_store_3months
         ,case when coalesce((salesstore_format_month0+salesstore_format_month1+salesstore_format_month2),0) <= 0 or coalesce((salesstore_subcatinformat_month0+salesstore_subcatinformat_month1+salesstore_subcatinformat_month2),0) <= 0 then null else ((salesstore_subcatinformat_month0+salesstore_subcatinformat_month1+salesstore_subcatinformat_month2)/(salesstore_format_month0+salesstore_format_month1+salesstore_format_month2)) end sales_ms_subcat_store_3months

         , salesstore_iteminformat_month0 sales_item_format_store_month0
         , salesstore_iteminformat_month1 sales_item_format_store_month1
         , salesstore_iteminformat_month2 sales_item_format_store_month2

         , salesstore_subcatinformat_month0 sales_subcat_format_store_month0
         , salesstore_subcatinformat_month1 sales_subcat_format_store_month1
         , salesstore_subcatinformat_month2 sales_subcat_format_store_month2

         , salesstore_categoryinformat_month0 sales_category_format_store_month0
         , salesstore_categoryinformat_month1 sales_category_format_store_month1
         , salesstore_categoryinformat_month2 sales_category_format_store_month2

         , qtystore_iteminformat_month0 qty_item_format_store_month0
         , qtystore_iteminformat_month1 qty_item_format_store_month1
         , qtystore_iteminformat_month2 qty_item_format_store_month2

         ,qtystore_subcatinformat_month0 qty_subcat_format_store_month0
         ,qtystore_subcatinformat_month1 qty_subcat_format_store_month1
         ,qtystore_subcatinformat_month2 qty_subcat_format_store_month2

         ,is_iteminstore_pos_sales_3months item_store_distribution_3months
         ,item_format_availability_in_store_3months
         ,avgavailability_stores_group_a_item_format_3months
         ,avgavailability_stores_group_b_item_format_3months
         ,avgavailability_stores_group_c_item_format_3months
          ,salesstorems_iteminsubcat_3months sales_ms_item_subcat_store_3months

         ,avgsales_stores_group_a_item_format_month0 ,avgsales_stores_group_b_item_format_month0 ,avgsales_stores_group_c_item_format_month0
         ,avgsales_stores_group_a_item_format_month1 ,avgsales_stores_group_b_item_format_month1 ,avgsales_stores_group_c_item_format_month1
         ,avgsales_stores_group_a_item_format_month2 ,avgsales_stores_group_b_item_format_month2 ,avgsales_stores_group_c_item_format_month2
         ,avgsalesms_group_a_iteminsubcatformat_month0, avgsalesms_group_b_iteminsubcatformat_month0, avgsalesms_group_c_iteminsubcatformat_month0
         ,avgsalesms_group_a_iteminsubcatformat_month1, avgsalesms_group_b_iteminsubcatformat_month1, avgsalesms_group_c_iteminsubcatformat_month1
         ,avgsalesms_group_a_iteminsubcatformat_month2, avgsalesms_group_b_iteminsubcatformat_month2, avgsalesms_group_c_iteminsubcatformat_month2
         ,avgqty_stores_group_a_item_format_month0, avgqty_stores_group_b_item_format_month0, avgqty_stores_group_c_item_format_month0
         ,avgqty_stores_group_a_item_format_month1, avgqty_stores_group_b_item_format_month1, avgqty_stores_group_c_item_format_month1
         ,avgqty_stores_group_a_item_format_month2, avgqty_stores_group_b_item_format_month2, avgqty_stores_group_c_item_format_month2

         --,dense_rank() over (partition by store_group_classification order by salesstorems_iteminsubcat_3months desc) - max (case when salesstorems_iteminsubcat_3months is null then 1 else 0 end) over (partition by store_group_classification)||' / '||sum(1) over(partition by store_group_classification) ranking_salesms_stores_group_item_subcat_3months
         --,dense_rank() over (partition by format_code order by item_format_availability_in_store_3months desc) - max (case when item_format_availability_in_store_3months is null then 1 else 0 end) over (partition by format_code)||' / '|| num_stores as ranking_store_item_format_availability_3months
         --,dense_rank() over (partition by format_code order by sales_ms_item_subcat_store_3months desc) - max (case when sales_ms_item_subcat_store_3months is null then 1 else 0 end) over (partition by format_code)
         --||' / '|| num_stores as ranking_store_sales_ms_item_subcat_3months
         ,ranking_salesms_stores_group_item_subcat_3months
         ,ranking_store_item_format_availability_3months
         ,ranking_store_sales_ms_item_subcat_3months

         ,avgsalesms_group_a_iteminsubcatformat_3months, avgsalesms_group_b_iteminsubcatformat_3months, avgsalesms_group_c_iteminsubcatformat_3months
         ,avgavailability_stores_group_a_item_format_3months, avgavailability_stores_group_b_item_format_3months, avgavailability_stores_group_c_item_format_3months

         ,avgsalesms_store_group_iteminsubcatformat_month0
         ,avgsalesms_change_store_group_iteminsubcatformat_month0to1
         ,avgavailability_ratio_stores_group_a_to_c_item_format_3months
         ,salesms_ratio_group_a_to_store_item_subcat_3months
         ,salesms_ratio_group_b_to_store_item_subcat_3months
         ,salesms_ratio_group_c_to_store_item_subcat_3months
         ,avgsalesms_ratio_stores_group_a_to_c_item_format_3months
         ,sales_inc_store_to_salesms_group_a_3months
         ,sales_inc_store_to_salesms_group_b_3months
         ,sales_inc_store_to_salesms_group_c_3months

         ,case when (1-item_format_availability_in_store_3months)*salesstore_iteminformat_3months<0 then null else (1-item_format_availability_in_store_3months)*salesstore_iteminformat_3months end full_availability_to_cur_sales_difference_item_store_3months

         ,qty_price_item_store_month0
         ,qty_price_item_format_month0 as qty_price_item_allformat_month0
         ,qty_price_change_item_store_to_format_month0
         ,qty_price_item_format_market_month0 as qty_price_item_format_market_month0_item_rep
         ,avg_monthly_item_market_qty_price_3months
         ,avgqty_price_item_stores_group_a_item_format_month0,avgqty_price_item_stores_group_b_item_format_month0,avgqty_price_item_stores_group_c_item_format_month0

         ,qty_price_item_store_3months
         ,qty_price_item_format_3months as qty_price_item_allformat_3months
         ,qty_price_change_item_store_to_format_3months
         ,avgqty_price_item_stores_group_a_item_format_3months,avgqty_price_item_stores_group_b_item_format_3months,avgqty_price_item_stores_group_c_item_format_3months

         ,(salesstore_iteminformat_month0+salesstore_iteminformat_month1+salesstore_iteminformat_month2)/3 avg_monthly_item_store_sales_3months


        FROM migvan.new_items_after_launche_period_measures
        where dw_item_key = :dw_item_key
        and format_code= :format_code
		and (salesstore_iteminformat_month0>0 or salesstore_iteminformat_month1>0 or salesstore_iteminformat_month2>0)
         
     ) e
  ) b
) t
where {FILTER_PLACEHOLDER}
 {DW_STORE_KEY_PLACEHOLDER}





