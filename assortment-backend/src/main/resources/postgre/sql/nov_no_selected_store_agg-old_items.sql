selected_store_agg as
(
  select *
  FROM (
          select
           sub_brand_hierarchy_2_key,sub_category_key, category_key, format_code

          ,sales_brand_subcat_cur
          ,sales_brand_subcat_prv
---
         	, sales_brand_subcat_cur_ss
        	, sales_brand_subcat_prv_ss
---
          , sales_brand_subcat_quartermatcur
          , sales_brand_subcat_quartermatprv
          ,sales_brand_subcat_halfmatcur
          , sales_brand_subcat_halfmatprv

          , qty_brand_subcat_cur
          , qty_brand_subcat_prv
          , qty_brand_subcat_quartermatcur
          , qty_brand_subcat_quartermatprv
          , qty_brand_subcat_halfmatcur
          , qty_brand_subcat_halfmatprv
          
     	,sales_category_format_cur
      , sales_category_format_prv
      , sales_category_format_quartermatcur
      , sales_category_format_quartermatprv
      ,sales_category_format_halfmatcur
      , sales_category_format_halfmatprv

      ,sales_subcat_format_cur
      ,sales_subcat_format_prv
      ,sales_subcat_format_quartermatcur
      , sales_subcat_format_quartermatprv
      , sales_subcat_format_halfmatcur
      , sales_subcat_format_halfmatprv

      ,sales_brand_in_format_yearmatcur

      , sales_format_yearmatcur
      , sales_format_yearmatprv
      , sales_format_quartermatcur
      ,sales_format_quartermatprv
      , sales_format_halfmatcur
      , sales_format_halfmatprv
      , qty_sales_category_format_cur
      ,qty_sales_category_format_prv
      ,qty_sales_category_format_quartermatcur
      , qty_sales_category_format_quartermatprv
      ,qty_sales_category_format_halfmatcur
      ,qty_sales_category_format_halfmatprv

      , qty_sales_subcat_format_cur
      , qty_sales_subcat_format_prv
      , qty_sales_subcat_format_quartermatcur
      , qty_sales_subcat_format_quartermatprv
      , qty_sales_subcat_format_halfmatcur
      , qty_sales_subcat_format_halfmatprv

      , qty_brand_in_format_cur
      , qty_sales_in_format_cur
---
      , sales_subcat_format_cur_ss
	  , sales_subcat_format_prv_ss
	  , sales_format_yearmatcur_ss
      ,sales_format_yearmatprv_ss
  ,max(sales_brand_subcat_cur) over (partition by sub_brand_hierarchy_2_key,sub_category_key, category_key, format_code) sales_brand_subcat_format_cur
---
		  
		 ,max(selected_store_agg_2.count_stores_cur) over() count_selectedstores_yearmatcur
		 ,max(selected_store_agg_2.count_stores_quartermatcur) over() count_selectedstores_quartermatcur
	     ,max(selected_store_agg_2.count_stores_halfmatcur) over() count_selectedstores_halfmatcur

	
		from migvan.assortment_catalog_sales_indices cross join selected_store_agg_2
		where  {STORES_FILTER_PLACEHOLDER}
		  
    ) t
	--where  sub_category_key is not null  and  sub_brand_hierarchy_2_key in (34983)
	where  {CATEGORY_FILTER_PLACEHOLDER}
)
