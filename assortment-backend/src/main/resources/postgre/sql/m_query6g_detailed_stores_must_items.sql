----m_query6g -detailed stores - must items----

select format_code, dw_store_key 
,availability_of_must_items_in_store_cq,availability_of_must_items_in_store_cq_color
,total_availability_of_must_items_in_store_cq
,case when total_availability_of_must_items_in_store_cq >= 0.95 then 1
	  when total_availability_of_must_items_in_store_cq > 0.85 and total_availability_of_must_items_in_store_cq <0.95 then 2
	  when total_availability_of_must_items_in_store_cq <= 0.85 then 3 end total_availability_of_must_items_in_store_cq_color -- Color classification for total value

from(
select *
,case when availability_of_must_items_in_store_cq >= 0.95 then 1
	  when availability_of_must_items_in_store_cq > 0.85 and availability_of_must_items_in_store_cq <0.95 then 2
	  when availability_of_must_items_in_store_cq <= 0.85 then 3 end availability_of_must_items_in_store_cq_color -- Color classification
	  
,avg(availability_of_must_items_in_store_cq) over() total_availability_of_must_items_in_store_cq --Total value to display

from(
select format_code, dw_store_key, avg(item_store_availability_cq) availability_of_must_items_in_store_cq -- Value to display 
from(
SELECT dw_item_key, format_code, dw_store_key, item_store_availability_cq
FROM migvan.assortment_core_growth_star_items_store_indices
where {FILTER_PLACEHOLDER}
--and   user's selections : formats,stores, catalog
--and   user's permission : formats,stores, catalog
) as sub1
group by format_code, dw_store_key
) as sub2
) as sub3