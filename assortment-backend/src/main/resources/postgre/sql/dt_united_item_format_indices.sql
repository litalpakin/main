 SELECT united_dw_item_key,united_item_id, united_item_name, dw_item_key, item_name, item_id
,sub_category_key ,sub_category_name 
,category_key ,category_name
,class_key ,class_name 
,supplier_key ,supplier_name
,sub_brand_hierarchy_2_key ,sub_brand_hierarchy_2_name   


from migvan.dt_united_item_format_indices
where {FILTER_PLACEHOLDER}
/* and
user's permission from the back end 
only  format_code  and catalog hierarchy 
*/

 group by united_dw_item_key,united_item_id, united_item_name, dw_item_key, item_name, item_id
,sub_category_key ,sub_category_name 
,category_key ,category_name
,class_key ,class_name 
,supplier_key ,supplier_name
,sub_brand_hierarchy_2_key ,sub_brand_hierarchy_2_name  