select *

  ,case when rank_sales_supplier_in_selectedworld_format_cy=0 then null else rank_sales_supplier_in_selectedworld_format_cy ||' / '|| num_supplier_in_selectedworld_format_cy end as ranking_sales_supplier_in_selectedworld_format_cy
  ,case when rank_qty_supplier_in_selectedworld_format_cy=0 then null else rank_qty_supplier_in_selectedworld_format_cy ||' / '|| num_supplier_in_selectedworld_format_cy end as ranking_qty_supplier_in_selectedworld_format_cy
  ,case when rank_ms_potential_sales_supplier_in_selectedworld_format_cy=0 then null else rank_ms_potential_sales_supplier_in_selectedworld_format_cy ||' / '|| num_supplier_in_selectedworld_format_cy end as ranking_ms_potential_sales_supplier_in_selectedworld_format_cy

  ,case when rank_sales_supplier_in_selectedworld_format_cq=0 then null else rank_sales_supplier_in_selectedworld_format_cq ||' / '|| num_supplier_in_selectedworld_format_cq end as ranking_sales_supplier_in_selectedworld_format_cq

  ,case when rank_qty_supplier_in_selectedworld_format_cq=0 then null else rank_qty_supplier_in_selectedworld_format_cq ||' / '|| num_supplier_in_selectedworld_format_cq end as ranking_qty_supplier_in_selectedworld_format_cq
  ,case when rank_ms_potential_sales_supplier_in_selectedworld_format_cq=0 then null else rank_ms_potential_sales_supplier_in_selectedworld_format_cq ||' / '|| num_supplier_in_selectedworld_format_cq end as ranking_ms_potential_sales_supplier_in_selectedworld_format_cq
  ,case when rank_sales_supplier_in_selectedworld_format_month0=0 then null else rank_sales_supplier_in_selectedworld_format_month0 ||' / '|| num_supplier_in_selectedworld_format_month0 end as ranking_sales_supplier_in_selectedworld_format_month0
  ,case when rank_qty_supplier_in_selectedworld_format_month0=0 then null else rank_qty_supplier_in_selectedworld_format_month0 ||' / '|| num_supplier_in_selectedworld_format_month0 end as ranking_qty_supplier_in_selectedworld_format_month0

  ,case when coalesce(sales_ms_supplier_selectedworld_format_py,0) <= 0 or coalesce(sales_ms_supplier_selectedworld_format_cy,0) <= 0 then null else
  ((sales_ms_supplier_selectedworld_format_cy+1)/(sales_ms_supplier_selectedworld_format_py+1))-1 end sales_ms_change_supplier_selectedworld_format_cypy
  ,case when coalesce(sales_ms_supplier_selectedworld_format_pq,0) <= 0 or coalesce(sales_ms_supplier_selectedworld_format_cq,0) <= 0 then null else
  ((sales_ms_supplier_selectedworld_format_cq+1)/(sales_ms_supplier_selectedworld_format_pq+1))-1 end sales_ms_change_supplier_selectedworld_format_cqpq
  ,case when coalesce(sales_ms_supplier_selectedworld_format_month1,0) <= 0 or coalesce(sales_ms_supplier_selectedworld_format_month0,0) <= 0 then null else
  ((sales_ms_supplier_selectedworld_format_month0+1)/(sales_ms_supplier_selectedworld_format_month1+1))-1 end sales_ms_change_supplier_selectedworld_format_month0to1

  from(
    select *
        ,sum(case when sales_supplier_in_selectedworld_format_cy>0 then 1 else 0 end) over() num_supplier_in_selectedworld_format_cy
        ,rank() over (order by sales_supplier_in_selectedworld_format_cy desc)- max (case when sales_supplier_in_selectedworld_format_cy is null then 1 else 0 end) over () rank_sales_supplier_in_selectedworld_format_cy

        ,sum(sales_supplier_in_selectedworld_format_cy) over () sales_selectedworld_format_cy
        ,sum(sales_supplier_in_selectedworld_format_py) over () sales_selectedworld_format_py
        ,sum(sales_supplier_in_selectedworld_format_cq) over () sales_selectedworld_format_cq
        ,sum(sales_supplier_in_selectedworld_format_pq) over () sales_selectedworld_format_pq
        ,sum(sales_supplier_in_selectedworld_format_month0) over () sales_selectedworld_format_month0
        ,sum(sales_supplier_in_selectedworld_format_month1) over () sales_selectedworld_format_month1
        ,sum(case when sales_supplier_in_selectedworld_format_month0>0 then 1 else 0 end) over() num_supplier_in_selectedworld_format_month0
       ,rank() over (order by sales_supplier_in_selectedworld_format_month0 desc)- max (case when sales_supplier_in_selectedworld_format_month0 is null then 1 else 0 end) over () rank_sales_supplier_in_selectedworld_format_month0

      ,sum(case when sales_supplier_in_selectedworld_format_cq>0 then 1 else 0 end) over() num_supplier_in_selectedworld_format_cq
      ,rank() over (order by sales_supplier_in_selectedworld_format_cq desc)- max (case when sales_supplier_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_sales_supplier_in_selectedworld_format_cq
      ,dense_rank() over (order by qty_supplier_in_selectedworld_format_cq desc)- max (case when qty_supplier_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_qty_supplier_in_selectedworld_format_cq

        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_cy) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_cy,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_cy/(sum(sales_supplier_in_selectedworld_format_cy) over ())) end sales_ms_supplier_selectedworld_format_cy
        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_py) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_py,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_py/(sum(sales_supplier_in_selectedworld_format_py) over ())) end sales_ms_supplier_selectedworld_format_py
        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_cq) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_cq,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_cq/(sum(sales_supplier_in_selectedworld_format_cq) over ())) end sales_ms_supplier_selectedworld_format_cq
        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_pq) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_pq,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_pq/(sum(sales_supplier_in_selectedworld_format_pq) over ())) end sales_ms_supplier_selectedworld_format_pq
        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_month0) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_month0,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_month0/(sum(sales_supplier_in_selectedworld_format_month0) over ())) end sales_ms_supplier_selectedworld_format_month0
        ,case when coalesce(sum(sales_supplier_in_selectedworld_format_month1) over (),0) <= 0 or coalesce(sales_supplier_in_selectedworld_format_month1,0) <= 0 then null else
        (sales_supplier_in_selectedworld_format_month1/(sum(sales_supplier_in_selectedworld_format_month1) over ())) end sales_ms_supplier_selectedworld_format_month1

	,case when potential_sales_supplier_in_selectedworld_format_cy<sales_supplier_in_selectedworld_format_cy then null 
	else potential_sales_supplier_in_selectedworld_format_cy-sales_supplier_in_selectedworld_format_cy end inc_for_sales_from_potential_implementaion_in_selectedworld_cy
 
	,case when potential_sales_supplier_in_selectedworld_format_cq<sales_supplier_in_selectedworld_format_cq then null 
	else potential_sales_supplier_in_selectedworld_format_cq-sales_supplier_in_selectedworld_format_cq end inc_for_sales_from_potential_implementaion_in_selectedworld_cq

        ,sum(qty_supplier_in_selectedworld_format_cy) over () qty_selectedworld_format_cy
        ,sum(qty_supplier_in_selectedworld_format_py) over () qty_selectedworld_format_py
        ,sum(qty_supplier_in_selectedworld_format_cq) over () qty_selectedworld_format_cq
        ,sum(qty_supplier_in_selectedworld_format_pq) over () qty_selectedworld_format_pq
        ,sum(qty_supplier_in_selectedworld_format_month0) over () qty_selectedworld_format_month0
        ,sum(qty_supplier_in_selectedworld_format_month1) over () qty_selectedworld_format_month1

      ,dense_rank() over (order by qty_supplier_in_selectedworld_format_cy desc)- max (case when qty_supplier_in_selectedworld_format_cy is null then 1 else 0 end) over () rank_qty_supplier_in_selectedworld_format_cy
      ,dense_rank() over (order by qty_supplier_in_selectedworld_format_month0 desc)- max (case when qty_supplier_in_selectedworld_format_month0 is null then 1 else 0 end) over () rank_qty_supplier_in_selectedworld_format_month0

        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_cy) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_cy,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_cy as double precision)/(sum(qty_supplier_in_selectedworld_format_cy) over ())) end relative_qty_item_selectedworld_format_cy
        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_py) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_cy,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_py as double precision)/(sum(qty_supplier_in_selectedworld_format_py) over ())) end relative_qty_item_selectedworld_format_py
        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_cq) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_cq,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_cq as double precision)/(sum(qty_supplier_in_selectedworld_format_cq) over ())) end relative_qty_item_selectedworld_format_cq
        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_pq) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_pq,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_pq as double precision)/sum(qty_supplier_in_selectedworld_format_pq) over ()) end relative_qty_item_selectedworld_format_pq
        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_month0) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_month0,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_month0 as double precision)/(sum(qty_supplier_in_selectedworld_format_month0) over ())) end relative_qty_item_selectedworld_format_month0
        ,case when coalesce(sum(qty_supplier_in_selectedworld_format_month1) over (),0) <= 0 or coalesce(qty_supplier_in_selectedworld_format_month1,0) <= 0 then null else
        (cast(qty_supplier_in_selectedworld_format_month1 as double precision)/(sum(qty_supplier_in_selectedworld_format_month1) over ())) end relative_qty_item_selectedworld_format_month1

        ,sum(potential_sales_supplier_in_selectedworld_format_cy) over () potential_sales_selectedworld_format_cy
        ,sum(potential_sales_supplier_in_selectedworld_format_cq) over () potential_sales_selectedworld_format_cq

      ,dense_rank() over (order by potential_sales_supplier_in_selectedworld_format_cy desc)- max (case when potential_sales_supplier_in_selectedworld_format_cy is null then 1 else 0 end) over () rank_ms_potential_sales_supplier_in_selectedworld_format_cy
      ,dense_rank() over (order by potential_sales_supplier_in_selectedworld_format_cq desc)- max (case when potential_sales_supplier_in_selectedworld_format_cq is null then 1 else 0 end) over () rank_ms_potential_sales_supplier_in_selectedworld_format_cq
        ,case when coalesce(sum(potential_sales_supplier_in_selectedworld_format_cy) over (),0) <= 0 or coalesce(potential_sales_supplier_in_selectedworld_format_cy,0) <= 0 then null else
        (potential_sales_supplier_in_selectedworld_format_cy/(sum(potential_sales_supplier_in_selectedworld_format_cy) over ())) end ms_potential_sales_item_selectedworld_format_cy
        ,case when coalesce(sum(potential_sales_supplier_in_selectedworld_format_cq) over (),0) <= 0 or coalesce(potential_sales_supplier_in_selectedworld_format_cq,0) <= 0 then null else
        (potential_sales_supplier_in_selectedworld_format_cq/(sum(potential_sales_supplier_in_selectedworld_format_cq) over ())) end ms_potential_sales_item_selectedworld_format_cq

        --,case when qty_price_supplier_in_selectedworld_format_cy<=0 or qty_price_supplier_in_selectedworld_format_cy is null or qty_price_supplier_in_selectedworld_format_market_cy<=0 or qty_price_supplier_in_selectedworld_format_market_cy is null then null else(qty_price_supplier_in_selectedworld_format_cy/qty_price_supplier_in_selectedworld_format_market_cy)-1
        -- end qty_price_change_supplier_selectedworld_format_chaintomarket_cy
 
        --,case when coalesce (qty_price_supplier_in_selectedworld_format_month0,0)<=0 or
        -- coalesce (qty_price_supplier_in_selectedworld_format_market_month0,0)<=0 then null else(qty_price_supplier_in_selectedworld_format_month0/qty_price_supplier_in_selectedworld_format_market_month0)-1
       -- end qty_price_change_supplier_selectedworld_format_chaintomarket_m0

       -- ,case when qty_price_supplier_in_selectedworld_format_cq<=0 or qty_price_supplier_in_selectedworld_format_cq is null or qty_price_supplier_in_selectedworld_format_market_cq<=0 or qty_price_supplier_in_selectedworld_format_market_cq is null then null else(qty_price_supplier_in_selectedworld_format_cq/qty_price_supplier_in_selectedworld_format_market_cq)-1
       --  end qty_price_change_supplier_selectedworld_format_chaintomarket_cq

    from (
        SELECT {CATALOG_FIELDS}
        ,format_code

        ,sum(sales_item_format_cy) sales_supplier_in_selectedworld_format_cy
        ,sum(sales_item_format_py) sales_supplier_in_selectedworld_format_py
        ,sum(sales_item_format_cq) sales_supplier_in_selectedworld_format_cq
        ,sum(sales_item_format_pq) sales_supplier_in_selectedworld_format_pq
        ,sum(sales_item_format_month0) sales_supplier_in_selectedworld_format_month0
        ,sum(sales_item_format_month1) sales_supplier_in_selectedworld_format_month1

		--,case when coalesce (sum(sales_item_format_cq),0)<=0 or sum(qty_item_format_cq) is null then null
		-- else coalesce (sum(sales_item_format_cq),0)/sum(qty_item_format_cq) end qty_price_supplier_in_selectedworld_format_cq

        ,case when coalesce(sum(sales_item_format_py),0) <= 0 or coalesce(sum(sales_item_format_cy),0) <= 0 then null else
        (sum(sales_item_format_cy)-sum(sales_item_format_py)) end sales_difference_supplier_in_selectedworld_format_cypy
        ,case when coalesce(sum(sales_item_format_pq),0) <= 0 or coalesce(sum(sales_item_format_cq),0) <= 0 then null else
        (sum(sales_item_format_cq)-sum(sales_item_format_pq)) end sales_difference_supplier_in_selectedworld_format_cqpq
        ,case when coalesce(sum(sales_item_format_month1),0) <= 0 or coalesce(sum(sales_item_format_month0),0) <= 0 then null else
        (sum(sales_item_format_month0)-sum(sales_item_format_month1)) end sales_difference_supplier_in_selectedworld_format_month0to1

        ,case when coalesce(sum(sales_item_format_py),0) <= 0 or coalesce(sum(sales_item_format_cy),0) <= 0 then null else
        (sum(sales_item_format_cy)/sum(sales_item_format_py))-1 end sales_change_supplier_in_selectedworld_format_cypy
        ,case when coalesce(sum(sales_item_format_pq),0) <= 0 or coalesce(sum(sales_item_format_cq),0) <= 0 then null else
        (sum(sales_item_format_cq)/sum(sales_item_format_pq))-1 end sales_change_supplier_in_selectedworld_format_cqpq
        ,case when coalesce(sum(sales_item_format_month1),0) <= 0 or coalesce(sum(sales_item_format_month0),0) <= 0 then null else
        (sum(sales_item_format_month0)/sum(sales_item_format_month1))-1 end sales_change_supplier_in_selectedworld_format_month0to1

        ,sum(sales_item_format_ss_cy) sales_supplier_in_selectedworld_format_ss_cy
        ,sum(sales_item_format_ss_py) sales_supplier_in_selectedworld_format_ss_py
        ,sum(sales_item_format_ss_cq) sales_supplier_in_selectedworld_format_ss_cq
        ,sum(sales_item_format_ss_pq) sales_supplier_in_selectedworld_format_ss_pq
        ,sum(sales_item_format_ss_month0) sales_supplier_in_selectedworld_format_ss_month0
        ,sum(sales_item_format_ss_month1) sales_supplier_in_selectedworld_format_ss_month1


        ,case when coalesce(sum(sales_item_format_ss_py),0) <= 0 or coalesce(sum(sales_item_format_ss_cy),0) <= 0 then null else
        (sum(sales_item_format_ss_cy)-sum(sales_item_format_ss_py)) end sales_difference_supplier_in_selectedworld_format_ss_cypy
        ,case when coalesce(sum(sales_item_format_ss_pq),0) <= 0 or coalesce(sum(sales_item_format_ss_cq),0) <= 0 then null else
        (sum(sales_item_format_ss_cq)-sum(sales_item_format_ss_pq)) end sales_difference_supplier_in_selectedworld_format_ss_cqpq
        ,case when coalesce(sum(sales_item_format_ss_month1),0) <= 0 or coalesce(sum(sales_item_format_ss_month0),0) <= 0 then null else
        (sum(sales_item_format_ss_month0)-sum(sales_item_format_ss_month1)) end sales_difference_supplier_in_selectedworld_format_ss_month0to1

        ,case when coalesce(sum(sales_item_format_ss_py),0) <= 0 or coalesce(sum(sales_item_format_ss_cy),0) <= 0 then null else
        (sum(sales_item_format_ss_cy)/sum(sales_item_format_ss_py))-1 end sales_change_supplier_in_selectedworld_format_ss_cypy
        ,case when coalesce(sum(sales_item_format_ss_pq),0) <= 0 or coalesce(sum(sales_item_format_ss_cq),0) <= 0 then null else
        (sum(sales_item_format_ss_cq)/sum(sales_item_format_ss_pq))-1 end sales_change_supplier_in_selectedworld_format_ss_cqpq
        ,case when coalesce(sum(sales_item_format_ss_month1),0) <= 0 or coalesce(sum(sales_item_format_ss_month0),0) <= 0 then null else
        (sum(sales_item_format_ss_month0)/sum(sales_item_format_ss_month1))-1 end sales_change_supplier_in_selectedworld_format_ss_month0to1

        ,sum(qty_item_format_cy) qty_supplier_in_selectedworld_format_cy
        ,sum(qty_item_format_py) qty_supplier_in_selectedworld_format_py
        ,sum(qty_item_format_cq) qty_supplier_in_selectedworld_format_cq
        ,sum(qty_item_format_pq) qty_supplier_in_selectedworld_format_pq
        ,sum(qty_item_format_month0) qty_supplier_in_selectedworld_format_month0
        ,sum(qty_item_format_month1) qty_supplier_in_selectedworld_format_month1

        --,case when coalesce(sum(sales_item_format_cy),0)<=0 or coalesce(sum(qty_item_format_cy),0)<=0 then null
        -- else sum(sales_item_format_cy)/sum(qty_item_format_cy) end qty_price_supplier_in_selectedworld_format_cy

        ,sum(potential_sales_item_format_cy) potential_sales_supplier_in_selectedworld_format_cy
        ,sum(potential_sales_item_format_cq) potential_sales_supplier_in_selectedworld_format_cq

        --,case when sum(salesmarket_item_format_cy)<=0 or sum(salesmarket_item_format_cy) is null or sum(qtymarket_item_format_cy)<=0 or sum(qtymarket_item_format_cy) is null then null else sum(salesmarket_item_format_cy)/sum(qtymarket_item_format_cy) end qty_price_supplier_in_selectedworld_format_market_cy

        --,case when sum(salesmarket_item_format_cq)<=0 or sum(salesmarket_item_format_cq) is null or sum(qtymarket_item_format_cq)<=0 or sum(qtymarket_item_format_cq) is null then null else sum(salesmarket_item_format_cq)/sum(qtymarket_item_format_cq) end qty_price_supplier_in_selectedworld_format_market_cq

        --,case when coalesce ( sum(sales_item_format_month0),0)<=0 or  coalesce(sum(qty_item_format_month0),0)<=0 then null else sum(sales_item_format_month0)/sum(qty_item_format_month0) end qty_price_supplier_in_selectedworld_format_month0
        --,case when sum(salesmarket_item_format_month0)<=0 or sum(salesmarket_item_format_month0) is null or sum(qtymarket_item_format_month0)<=0 or sum(qtymarket_item_format_month0) is null then null else sum(salesmarket_item_format_month0)/sum(qtymarket_item_format_month0) end qty_price_supplier_in_selectedworld_format_market_month0

        FROM migvan.assortment_item_format_indices
        where item_format_type_classification_code<>24 and item_format_type_classification_code<>25 and item_format_type_classification_code<>26
        and format_code = :formatParam
         {FILTER_PLACEHOLDER}
        group by  {GROUP_CATALOG_FIELDS}, format_code
        -- having sum(sales_item_format_cy)>0
	) t
  where {TIME_FILTER_FIELD} >0
) b

-- variable depends on time filter  sales_supplier_in_selectedworld_format_cy>0 or sales_supplier_in_selectedworld_format_cq>0 or sales_supplier_in_selectedworld_format_month0>0
