with rp2_chain_sales_group_items_by_groups as
(
select item_format_top_classification_code, item_format_top_classification_name 
,sum(sales_item_cy ) as sales_group_items_cy
,sum(sum(sales_item_cy )) over() as total_sales_group_items_cy
,sum(sales_item_cy )/(sum(sum(sales_item_cy )) over()) as share_of_sales_group_items_from_total_cy
,sum(sales_item_ch ) as sales_group_items_ch
,sum(sum(sales_item_ch )) over() as total_sales_group_items_ch
,sum(sales_item_ch )/(sum(sum(sales_item_ch )) over()) as share_of_sales_group_items_from_total_ch
,sum(sales_item_cq ) as sales_group_items_cq
,sum(sum(sales_item_cq )) over() as total_sales_group_items_cq
,sum(sales_item_cq )/(sum(sum(sales_item_cq )) over()) as share_of_sales_group_items_from_total_cq
from(
select f.dw_item_key, 
    f.format_code,
    item_format_top_classification_code, 
    item_format_top_classification_name ,
    sum(sales_item_store_cur) as sales_item_cy,
    sum(sales_item_store_quartermatcur) as sales_item_cq,
    sum(sales_item_store_halfmatcur) as sales_item_ch
from migvan.assortment_item_store_indices f
where item_format_top_classification_code in (1,2) and f.format_code = :formatParam
group by f.dw_item_key, f.format_code,item_format_top_classification_code, item_format_top_classification_name 
) as b
group by item_format_top_classification_code, item_format_top_classification_name

),
rp2_user_sales_group_items_by_groups as
(
select item_format_top_classification_code, item_format_top_classification_name 
    ,sum(sales_item_cy ) as user_sales_group_items_cy
    ,sum(sum(sales_item_cy )) over() as user_total_sales_group_items_cy
    ,case when (sum(sum(sales_item_cy )) over())=0 then null else sum(sales_item_cy )/(sum(sum(sales_item_cy )) over()) end as user_share_of_sales_group_items_from_total_cy
    ,sum(sales_item_ch ) as user_sales_group_items_ch
    ,sum(sum(sales_item_ch )) over() as user_total_sales_group_items_ch
    ,case when (sum(sum(sales_item_ch )) over())=0 then null else sum(sales_item_ch )/(sum(sum(sales_item_ch )) over()) end as user_share_of_sales_group_items_from_total_ch
    ,sum(sales_item_cq ) as user_sales_group_items_cq
    ,sum(sum(sales_item_cq )) over() as user_total_sales_group_items_cq
    ,case when (sum(sum(sales_item_cq )) over())=0 then null else sum(sales_item_cq )/(sum(sum(sales_item_cq )) over()) end as user_share_of_sales_group_items_from_total_cq
from(
    select f.dw_item_key, f.format_code,item_format_top_classification_code, item_format_top_classification_name 
        ,sum(sales_item_store_cur) as sales_item_cy
        ,sum(sales_item_store_quartermatcur) as sales_item_cq
        ,sum(sales_item_store_halfmatcur) as sales_item_ch
    from migvan.assortment_item_store_indices f
    where  item_format_top_classification_code in (1,2)
    and {FILTER_PLACEHOLDER}
group by f.dw_item_key, f.format_code,item_format_top_classification_code, item_format_top_classification_name 
) as t
group by item_format_top_classification_code, item_format_top_classification_name 
)


select cs.item_format_top_classification_code,cs.item_format_top_classification_name
,sales_group_items_cy,total_sales_group_items_cy, share_of_sales_group_items_from_total_cy
,user_sales_group_items_cy,user_total_sales_group_items_cy, user_share_of_sales_group_items_from_total_cy 
,((1+user_share_of_sales_group_items_from_total_cy)/(1+share_of_sales_group_items_from_total_cy))-1  share_of_change_sales_group_items_user_to_chain_cy
,case when user_share_of_sales_group_items_from_total_cy is null then 0-share_of_sales_group_items_from_total_cy 
      else user_share_of_sales_group_items_from_total_cy-share_of_sales_group_items_from_total_cy end share_of_difference_sales_group_items_user_to_chain_cy
      
,sales_group_items_ch,total_sales_group_items_ch, share_of_sales_group_items_from_total_ch
,user_sales_group_items_ch,user_total_sales_group_items_ch, user_share_of_sales_group_items_from_total_ch 
,((1+user_share_of_sales_group_items_from_total_ch)/(1+share_of_sales_group_items_from_total_ch))-1  share_of_change_sales_group_items_user_to_chain_ch
,case when user_share_of_sales_group_items_from_total_ch is null then 0-share_of_sales_group_items_from_total_ch 
      else user_share_of_sales_group_items_from_total_ch-share_of_sales_group_items_from_total_ch end share_of_difference_sales_group_items_user_to_chain_ch
      
,sales_group_items_cq,total_sales_group_items_cq, share_of_sales_group_items_from_total_cq
,user_sales_group_items_cq,user_total_sales_group_items_cq, user_share_of_sales_group_items_from_total_cq 
,((1+user_share_of_sales_group_items_from_total_cq)/(1+share_of_sales_group_items_from_total_cq))-1  share_of_change_sales_group_items_user_to_chain_cq
,case when user_share_of_sales_group_items_from_total_cq is null then 0-share_of_sales_group_items_from_total_cq 
      else user_share_of_sales_group_items_from_total_cq-share_of_sales_group_items_from_total_cq end share_of_difference_sales_group_items_user_to_chain_cq

from rp2_chain_sales_group_items_by_groups cs left join rp2_user_sales_group_items_by_groups us on cs.item_format_top_classification_code=us.item_format_top_classification_code and cs.item_format_top_classification_name = us.item_format_top_classification_name
