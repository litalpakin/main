-----m_query6h - detailed stores - unnecessary items----

select dw_store_key,format_code
,share_of_unnecessary_items_in_store_cq,share_of_unnecessary_items_in_store_cq_color
,total_share_of_unnecessary_items_in_store_cq
,case when total_share_of_unnecessary_items_in_store_cq = 0 then null
	  when total_share_of_unnecessary_items_in_store_cq > 0 and total_share_of_unnecessary_items_in_store_cq <= 0.1 then 1
	  when total_share_of_unnecessary_items_in_store_cq < 0.2 and total_share_of_unnecessary_items_in_store_cq > 0.1 then 2
	  when total_share_of_unnecessary_items_in_store_cq >= 0.2 then 3 end total_share_of_unnecessary_items_in_store_cq_color -- Color classification for total value 
from
(
select *
,case when share_of_unnecessary_items_in_store_cq = 0 then null
	  when share_of_unnecessary_items_in_store_cq > 0 and share_of_unnecessary_items_in_store_cq <= 0.1 then 1
	  when share_of_unnecessary_items_in_store_cq < 0.2 and share_of_unnecessary_items_in_store_cq > 0.1 then 2
	  when share_of_unnecessary_items_in_store_cq >= 0.2 then 3 end share_of_unnecessary_items_in_store_cq_color -- Color classification  
	  
,avg(share_of_unnecessary_items_in_store_cq) over() total_share_of_unnecessary_items_in_store_cq  --Total value to display 
	  
from(
select sub1.dw_store_key,sub1.format_code
,total_unnecessary_items_in_store,total_items_in_store
,case when total_items_in_store<>0 then cast(total_unnecessary_items_in_store as double precision)/total_items_in_store else null end share_of_unnecessary_items_in_store_cq -- Value to display
 from 
 (	
		select dw_store_key, format_code ,count(*) total_items_in_store
		from(
		SELECT dw_item_key,dw_store_key, format_code 
		,sales_item_store_monthmatcur
		FROM migvan.assortment_item_store_indices
		where item_format_top_classification_code in (1,2) and {FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		) as all_users_items_inn1
		where sales_item_store_monthmatcur>0 
		group by dw_store_key, format_code 
		
 ) as sub1
 left join
 (			
		select dw_store_key, format_code ,count(*) total_unnecessary_items_in_store
		from(
		SELECT dw_item_key,dw_store_key, format_code 
		,item_format_classification_code ,sales_item_store_monthmatcur
		--,salesstore_iteminformat_week0
		--,case when item_format_classification_code = 8 and coalesce(salesstore_iteminformat_week0,0)>0 then 1 
		--      when item_format_classification_code in (5,11) and coalesce(sales_item_store_monthmatcur,0)>0 then 1 
		--  
		FROM migvan.assortment_item_store_indices
		where item_format_classification_code in (4,5,8,11)
		and {FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		) as all_users_items_inn1
		where sales_item_store_monthmatcur>0
		group by dw_store_key, format_code 
	
 ) as sub2 on sub1.dw_store_key=sub2.dw_store_key and sub1.format_code=sub2.format_code 
) as sub3
) as sub4