----m_query6d - detailed_format - disposal items----

with all_users_items as
(
		select count(*) total_sum_user_items_in_chain_2 ,format_code
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as all_users_items_inn1
		where sales_item_format_cur_1>0 
		group by format_code
),
 m_q5_filterd_5_user_disposal_item as
(
		select count(*) sum_users_items_for_disposal_1 , format_code
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures
		where  item_classification_42 = 5
		and {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as m_q5_filterd_5_user_disposal_item_inn1
		where sales_item_format_cur_1>0 
		group by format_code
)
select *, Prcnt_of_user_disposal_items_3 as value_column, case when Prcnt_of_user_disposal_items_3 = 0 then null
	  when Prcnt_of_user_disposal_items_3 > 0 and Prcnt_of_user_disposal_items_3 <= 0.1 then 1
	  when Prcnt_of_user_disposal_items_3 < 0.2 and Prcnt_of_user_disposal_items_3 > 0.1 then 2
	  when Prcnt_of_user_disposal_items_3 >= 0.2 then 3 end color_column -- Classification Color  
from (
	select all_users_items.format_code,sum_users_items_for_disposal_1,total_sum_user_items_in_chain_2
	,case when total_sum_user_items_in_chain_2<>0 then cast(sum_users_items_for_disposal_1 as double precision)/total_sum_user_items_in_chain_2 else null end Prcnt_of_user_disposal_items_3 -- Value to display 
	from m_q5_filterd_5_user_disposal_item join all_users_items on m_q5_filterd_5_user_disposal_item.format_code=all_users_items.format_code
)as inn1
