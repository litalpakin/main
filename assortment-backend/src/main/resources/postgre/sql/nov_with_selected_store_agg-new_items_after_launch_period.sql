selected_store_agg as
(
	select *
	,sum(sales_brand_subcat_month0) over (partition by category_key,format_code) sales_category_format_month0
	,sum(sales_brand_subcat_month1) over (partition by category_key,format_code) sales_category_format_month1
	,sum(sales_brand_subcat_month2) over (partition by category_key,format_code) sales_category_format_month2

	,sum(sales_brand_subcat_month0) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_month0
	,sum(sales_brand_subcat_month1) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_month1
	,sum(sales_brand_subcat_month2) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_month2

	,sum(sales_brand_subcat_ss_month0) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_month0
	,sum(sales_brand_subcat_ss_month1) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_month1
	,sum(sales_brand_subcat_ss_month2) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_month2

	,sum(sales_brand_subcat_month0) over (partition by format_code) sales_in_format_month0
	,sum(sales_brand_subcat_month1) over (partition by format_code) sales_in_format_month1
	,sum(sales_brand_subcat_month2) over (partition by format_code) sales_in_format_month2

	,sum(qty_brand_subcat_month0) over (partition by category_key,format_code) qty_sales_category_format_month0
	,sum(qty_brand_subcat_month1) over (partition by category_key,format_code) qty_sales_category_format_month1
	,sum(qty_brand_subcat_month2) over (partition by category_key,format_code) qty_sales_category_format_month2

	,sum(qty_brand_subcat_month0) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_month0
	,sum(qty_brand_subcat_month1) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_month1
	,sum(qty_brand_subcat_month2) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_month2

	from(
	select
	 sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
	,sum(sales_brand_subcat_store_month0) sales_brand_subcat_month0
	,sum(sales_brand_subcat_store_month1) sales_brand_subcat_month1
	,sum(sales_brand_subcat_store_month2) sales_brand_subcat_month2

	,sum(sales_brand_subcat_store_ss_month0) sales_brand_subcat_ss_month0
	,sum(sales_brand_subcat_store_ss_month0) sales_brand_subcat_ss_month1
	,sum(sales_brand_subcat_store_ss_month0) sales_brand_subcat_ss_month2

	,sum(qty_brand_subcat_store_month0) qty_brand_subcat_month0
	,sum(qty_brand_subcat_store_month1) qty_brand_subcat_month1
	,sum(qty_brand_subcat_store_month2) qty_brand_subcat_month2
	
	,max(selected_store_agg_2.count_stores_month0) count_selectedstores_month0
	,max(selected_store_agg_2.count_stores_month1) count_selectedstores_month1
	,max(selected_store_agg_2.count_stores_month2) count_selectedstores_month2
	,max(selected_store_agg_2.count_stores_3months) count_selectedstores_3months


	from migvan.assortment_store_indices cross join selected_store_agg_2
	where {STORES_FILTER_PLACEHOLDER}
	group by  sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
	) t
	-- where sub_category_key is not null
	where  {CATEGORY_FILTER_PLACEHOLDER}
)