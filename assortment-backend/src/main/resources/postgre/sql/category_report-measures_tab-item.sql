select *

  ,case when rank_sales_item_in_selectedworld_format_cy =0 then null else rank_sales_item_in_selectedworld_format_cy ||' / '|| num_items_in_selectedworld_format_cy end as ranking_sales_item_in_selectedworld_format_cy

  ,case when rank_qty_item_in_selectedworld_format_cy =0 then null else rank_qty_item_in_selectedworld_format_cy ||' / '|| num_items_in_selectedworld_format_cy end as ranking_qty_item_in_selectedworld_format_cy
  ,dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cy desc)- max (case when ms_potential_sales_item_selectedworld_format_cy is null then 1 else 0 end) over () rank_ms_potential_sales_item_in_selectedworld_format_cy
  ,case when (dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cy desc)- max (case when ms_potential_sales_item_selectedworld_format_cy is null then 1 else 0 end) over ()) = 0 then null
  else (dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cy desc)- max (case when ms_potential_sales_item_selectedworld_format_cy is null then 1 else 0 end) over ()) ||' / '|| num_items_in_selectedworld_format_cy
  end as ranking_ms_potential_sales_item_in_selectedworld_format_cy

  ,case when rank_sales_item_in_selectedworld_format_cq = 0 then null else rank_sales_item_in_selectedworld_format_cq ||' / '|| num_items_in_selectedworld_format_cq end as ranking_sales_item_in_selectedworld_format_cq
  ,dense_rank() over (order by qty_item_format_cq desc)- max (case when qty_item_format_cq is null then 1 else 0 end) over () rank_qty_item_in_selectedworld_format_cq
  ,case when (dense_rank() over (order by qty_item_format_cq desc)- max (case when qty_item_format_cq is null then 1 else 0 end) over () )=0 then null
  else (dense_rank() over (order by qty_item_format_cq desc)- max (case when qty_item_format_cq is null then 1 else 0 end) over () ) ||' / '|| num_items_in_selectedworld_format_cq
  end as ranking_qty_item_in_selectedworld_format_cq
  ,dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cq desc)- max (case when ms_potential_sales_item_selectedworld_format_cq is null then 1 else 0 end) over () rank_ms_potential_sales_item_in_selectedworld_format_cq
  ,case when (dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cq desc)- max (case when ms_potential_sales_item_selectedworld_format_cq is null then 1 else 0 end) over () ) =0 then null
  else (dense_rank() over (order by ms_potential_sales_item_selectedworld_format_cq desc)- max (case when ms_potential_sales_item_selectedworld_format_cq is null then 1 else 0 end) over () ) ||' / '|| num_items_in_selectedworld_format_cq end as ranking_ms_potential_sales_item_in_selectedworld_format_cq

  ,rank() over (order by sales_item_format_month0 desc)- max (case when sales_item_format_month0 is null then 1 else 0 end) over () rank_sales_item_in_selectedworld_format_month0
  ,case when (dense_rank() over (order by sales_item_format_month0 desc)- max (case when sales_item_format_month0 is null then 1 else 0 end) over ()) = 0 then null
  else (dense_rank() over (order by sales_item_format_month0 desc)- max (case when sales_item_format_month0 is null then 1 else 0 end) over ()) ||' / '|| num_items_in_selectedworld_format_month0
  end as ranking_sales_item_in_selectedworld_format_month0
  ,case when rank_qty_item_in_selectedworld_format_month0 = 0 then null else rank_qty_item_in_selectedworld_format_month0 ||' / '|| num_items_in_selectedworld_format_month0 end as ranking_qty_item_in_selectedworld_format_month0

  ,case when coalesce(sales_ms_item_selectedworld_format_py,0) <= 0 or coalesce(sales_ms_item_selectedworld_format_cy,0) <= 0 then null else
   ((sales_ms_item_selectedworld_format_cy+1)/(sales_ms_item_selectedworld_format_py+1))-1 end sales_ms_change_item_selectedworld_format_cypy
  ,case when coalesce(sales_ms_item_selectedworld_format_pq,0) <= 0 or coalesce(sales_ms_item_selectedworld_format_cq,0) <= 0 then null else
        ((sales_ms_item_selectedworld_format_cq+1)/(sales_ms_item_selectedworld_format_pq+1))-1 end sales_ms_change_item_selectedworld_format_cqpq
  ,case when coalesce(sales_ms_item_selectedworld_format_month1,0) <= 0 or coalesce(sales_ms_item_selectedworld_format_month0,0) <= 0 then null else
        ((sales_ms_item_selectedworld_format_month0+1)/(sales_ms_item_selectedworld_format_month1+1))-1 end sales_ms_change_item_selectedworld_format_month0to1
  ,case when qty_price_item_format_cy<=0 or qty_price_item_format_cy is null or qty_price_item_format_market_cy<=0 or qty_price_item_format_market_cy is null then null else(qty_price_item_format_cy/qty_price_item_format_market_cy)-1 end qty_price_change_item_format_chain_to_market_cy
  ,case when qty_price_item_format_cq<=0 or qty_price_item_format_cq is null or qty_price_item_format_market_cq<=0 or qty_price_item_format_market_cq is null then null else(qty_price_item_format_cq/qty_price_item_format_market_cq)-1 end qty_price_change_item_format_chain_to_market_cq
  ,case when qty_price_item_format_month0_catrep<=0 or qty_price_item_format_month0_catrep is null or qty_price_item_format_market_month0_catrep<=0 or qty_price_item_format_market_month0_catrep is null then null else(qty_price_item_format_month0_catrep/qty_price_item_format_market_month0_catrep)-1 end qty_price_change_item_format_chain_to_market_month0_catrep

  from (
        SELECT {CATALOG_FIELDS}
        ,format_code
		,general_status_key, general_status_name,is_united_dw_item_key,is_private_label{REGIONAL_ITEMS_PLACEHOLDER}
		,case when is_special_new_item = 1 then is_special_new_item else is_special_old_item end is_special_item
        ,item_format_type_classification_code,item_format_type_classification_name,is_item_classification_equal_for_all_format
        ,sales_item_format_cy, sales_item_format_py, sales_item_format_cq, sales_item_format_pq, sales_item_format_month0, sales_item_format_month1
		,weight_item_format_cy,weight_item_format_cq,weight_item_format_month0
        ,sum(case when sales_item_format_month0>0 then 1 else 0 end) over() num_items_in_selectedworld_format_month0
      ,sum(case when sales_item_format_cy>0 then 1 else 0 end) over() num_items_in_selectedworld_format_cy
      ,rank() over (order by sales_item_format_cy desc)- max (case when sales_item_format_cy is null then 1 else 0 end) over () rank_sales_item_in_selectedworld_format_cy
      ,dense_rank() over (order by qty_item_format_cy desc)- max (case when qty_item_format_cy is null then 1 else 0 end) over () rank_qty_item_in_selectedworld_format_cy
      ,sum(case when sales_item_format_cq>0 then 1 else 0 end) over() num_items_in_selectedworld_format_cq
      ,rank() over (order by sales_item_format_cq desc)- max (case when sales_item_format_cq is null then 1 else 0 end) over () rank_sales_item_in_selectedworld_format_cq

        ,item_format_distribution_cy,item_format_distribution_cq
        ,avg_item_format_availability_cy,avg_item_format_availability_cq

        ,sum(sales_item_format_cy) over () sales_selectedworld_format_cy
        ,sum(sales_item_format_py) over () sales_selectedworld_format_py
        ,sum(sales_item_format_cq) over () sales_selectedworld_format_cq
        ,sum(sales_item_format_pq) over () sales_selectedworld_format_pq
        ,sum(sales_item_format_month0) over () sales_selectedworld_format_month0
        ,sum(sales_item_format_month1) over () sales_selectedworld_format_month1
        ,sales_difference_item_format_cypy, sales_difference_item_format_cqpq, sales_difference_item_format_month0to1
        ,sales_change_item_format_cypy, sales_change_item_format_cqpq, sales_change_item_format_month0to1
        ,sales_difference_item_format_ss_cypy, sales_difference_item_format_ss_cqpq, sales_difference_item_format_ss_month0to1
        ,sales_change_item_format_ss_cypy, sales_change_item_format_ss_cqpq, sales_change_item_format_ss_month0to1

        ,dense_rank() over (order by qty_item_format_month0 desc)- max (case when qty_item_format_month0 is null then 1 else 0 end) over () rank_qty_item_in_selectedworld_format_month0

        ,case when coalesce(sum(sales_item_format_cy) over (),0) <= 0 or coalesce(sales_item_format_cy,0) <= 0 then null else
        (sales_item_format_cy/(sum(sales_item_format_cy) over ())) end sales_ms_item_selectedworld_format_cy
        ,case when coalesce(sum(sales_item_format_py) over (),0) <= 0 or coalesce(sales_item_format_py,0) <= 0 then null else
        (sales_item_format_py/(sum(sales_item_format_py) over ())) end sales_ms_item_selectedworld_format_py
        ,case when coalesce(sum(sales_item_format_cq) over (),0) <= 0 or coalesce(sales_item_format_cq,0) <= 0 then null else
        (sales_item_format_cq/(sum(sales_item_format_cq) over ())) end sales_ms_item_selectedworld_format_cq
        ,case when coalesce(sum(sales_item_format_pq) over (),0) <= 0 or coalesce(sales_item_format_pq,0) <= 0 then null else
        (sales_item_format_pq/(sum(sales_item_format_pq) over ())) end sales_ms_item_selectedworld_format_pq
        ,case when coalesce(sum(sales_item_format_month0) over (),0) <= 0 or coalesce(sales_item_format_month0,0) <= 0 then null else
        (sales_item_format_month0/(sum(sales_item_format_month0) over ())) end sales_ms_item_selectedworld_format_month0
        ,case when coalesce(sum(sales_item_format_month1) over (),0) <= 0 or coalesce(sales_item_format_month1,0) <= 0 then null else
        (sales_item_format_month1/sum(sales_item_format_month1) over ()) end sales_ms_item_selectedworld_format_month1

        ,qty_item_format_cy, qty_item_format_py, qty_item_format_cq, qty_item_format_pq, qty_item_format_month0, qty_item_format_month1

        ,sum(qty_item_format_cy) over () qty_selectedworld_format_cy
        ,sum(qty_item_format_py) over () qty_selectedworld_format_py
        ,sum(qty_item_format_cq) over () qty_selectedworld_format_cq
        ,sum(qty_item_format_pq) over () qty_selectedworld_format_pq
        ,sum(qty_item_format_month0) over () qty_selectedworld_format_month0
        ,sum(qty_item_format_month1) over () qty_selectedworld_format_month1

        ,case when coalesce(sum(qty_item_format_cy) over (),0) <= 0 or coalesce(qty_item_format_cy,0) <= 0 then null else
        (cast(qty_item_format_cy as double precision)/(sum(qty_item_format_cy) over ())) end relative_qty_item_selectedworld_format_cy
        ,case when coalesce(sum(qty_item_format_py) over (),0) <= 0 or coalesce(qty_item_format_py,0) <= 0 then null else
        (cast(qty_item_format_py as double precision)/(sum(qty_item_format_py) over ())) end relative_qty_item_selectedworld_format_py
        ,case when coalesce(sum(qty_item_format_cq) over (),0) <= 0 or coalesce(qty_item_format_cq,0) <= 0 then null else
        (cast(qty_item_format_cq as double precision)/(sum(qty_item_format_cq) over ())) end relative_qty_item_selectedworld_format_cq
        ,case when coalesce(sum(qty_item_format_pq) over (),0) <= 0 or coalesce(qty_item_format_pq,0) <= 0 then null else
        (cast(qty_item_format_pq as double precision)/(sum(qty_item_format_pq) over ())) end relative_qty_item_selectedworld_format_pq
        ,case when coalesce(sum(qty_item_format_month0) over (),0) <= 0 or coalesce(qty_item_format_month0,0) <= 0 then null else
        (cast(qty_item_format_month0 as double precision)/(sum(qty_item_format_month0) over ())) end relative_qty_item_selectedworld_format_month0
        ,case when coalesce(sum(qty_item_format_month1) over (),0) <= 0 or coalesce(qty_item_format_month1,0) <= 0 then null else
        (cast(qty_item_format_month1 as double precision)/sum(qty_item_format_month1) over ()) end relative_qty_item_selectedworld_format_month1

        ,potential_sales_item_format_cy, potential_sales_item_format_cq

        ,sum(potential_sales_item_format_cy) over () potential_sales_selectedworld_format_cy
        ,sum(potential_sales_item_format_cq) over () potential_sales_selectedworld_format_cq

        ,case when coalesce(sum(potential_sales_item_format_cy) over (),0) <= 0 or coalesce(potential_sales_item_format_cy,0) <= 0 then null else
        (potential_sales_item_format_cy/(sum(potential_sales_item_format_cy) over ())) end ms_potential_sales_item_selectedworld_format_cy
        ,case when coalesce(sum(potential_sales_item_format_cq) over (),0) <= 0 or coalesce(potential_sales_item_format_cq,0) <= 0 then null else
        (potential_sales_item_format_cq/(sum(potential_sales_item_format_cq) over ())) end ms_potential_sales_item_selectedworld_format_cq

        ,case when coalesce(sales_item_format_cy,0) <= 0 or coalesce(avg_item_format_availability_cy,0) <= 0 then null else
        (sales_item_format_cy/avg_item_format_availability_cy) end sales_item_format_full_availability_cy
        ,case when coalesce(sales_item_format_cq,0) <= 0 or coalesce(avg_item_format_availability_cq,0) <= 0 then null else
        (sales_item_format_cq/avg_item_format_availability_cq) end sales_item_format_full_availability_cq

        ,case when potential_sales_item_format_cy<sales_item_format_cy then null
         else potential_sales_item_format_cy-sales_item_format_cy end inc_for_sales_from_potential_implementaion_cy

        ,case when potential_sales_item_format_cq<sales_item_format_cq then null
        else potential_sales_item_format_cq-sales_item_format_cq end inc_for_sales_from_potential_implementaion_cq
        ,growth_potential_market_to_chain_cy
        ,growth_potential_market_to_chain_ss_cy
        ,growth_potential_market_to_chain_cq
        ,growth_potential_market_to_chain_ss_cq

        ,case when sales_item_format_cy<=0 or sales_item_format_cy is null or qty_item_format_cy<=0 or qty_item_format_cy is null then null else sales_item_format_cy/qty_item_format_cy end qty_price_item_format_cy
        ,case when salesmarket_item_format_cy<=0 or salesmarket_item_format_cy is null or qtymarket_item_format_cy<=0 or qtymarket_item_format_cy is null then null else salesmarket_item_format_cy/qtymarket_item_format_cy end qty_price_item_format_market_cy

        ,case when sales_item_format_cq<=0 or sales_item_format_cq is null or qty_item_format_cq<=0 or qty_item_format_cq is null then null else sales_item_format_cq/qty_item_format_cq end qty_price_item_format_cq
        ,case when salesmarket_item_format_cq<=0 or salesmarket_item_format_cq is null or qtymarket_item_format_cq<=0 or qtymarket_item_format_cq is null then null else salesmarket_item_format_cq/qtymarket_item_format_cq end qty_price_item_format_market_cq

        ,case when sales_item_format_month0<=0 or sales_item_format_month0 is null or qty_item_format_month0<=0 or qty_item_format_month0 is null then null else sales_item_format_month0/qty_item_format_month0 end qty_price_item_format_month0_catrep
        ,case when salesmarket_item_format_month0<=0 or salesmarket_item_format_month0 is null or qtymarket_item_format_month0<=0 or qtymarket_item_format_month0 is null then null else salesmarket_item_format_month0/qtymarket_item_format_month0 end qty_price_item_format_market_month0_catrep

		,is_item_format_relevant_to_model as ac_is_item_format_relevant_to_model
		,classification_time_flag as ac_classification_time_flag
		,normalized_max_theoretic_item_format_sales_at_risk as max_theoretic_item_format_sales
		,item_format_nontransferable_spend
		,item_format_estimated_real_sales_at_risk
		,avg_must_item_format_estimated_real_sales_at_risk
		,item_format_alternative_cost 
		,avg_must_item_format_alternative_cost as alternative_real_sales_at_risk
		,item_format_net_gain_loss_sales 
		
		
		
        FROM migvan.assortment_item_format_indices
        where item_format_type_classification_code not in (24,25,26) and format_code = :formatParam
        {FILTER_PLACEHOLDER}
        AND {TIME_FILTER_FIELD} >0 -- sales_item_format_cy >0 -- variable depends on time filter  sales_item_format_cy>0 or sales_item_format_cq>0 or sales_item_format_month0>0
        

  ) t


