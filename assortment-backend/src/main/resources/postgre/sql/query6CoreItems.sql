----m_query6b - core items----

select
dw_item_key
,format_code

,avg_availability_item_format_1
,total_avg_availability_in_chain_2
,case when total_avg_availability_in_chain_2 >= 0.95 then 1
	  when total_avg_availability_in_chain_2 > 0.85 and total_avg_availability_in_chain_2 <0.95 then 2
	  when total_avg_availability_in_chain_2 <= 0.85 then 3 end clasification_total_avg_availability_in_chain_3
from(
	select *,avg(avg_availability_item_format_1) over() total_avg_availability_in_chain_2 from (
	select f.dw_item_key,f.format_code,avg(case when item_final_availability_in_store_quartermatcur_156 is not NULL then item_final_availability_in_store_quartermatcur_156 end) avg_availability_item_format_1
	FROM migvan.old_items_measures f
	where item_classification_42 = 2
	and {FILTER_PLACEHOLDER}
	group by f.dw_item_key,f.format_code
	)as inn1
)as inn2;