----m_query6f - detailed_format - insignificant items----

with user_new_items_count as
(
		select count(*) total_sum_new_items_in_chain_2, format_code
		from(
		SELECT dw_item_key, format_code ,item_format_time_classification_code
		,case when item_format_time_classification_code in (1,5) then  sum(salesstore_iteminformat_week0)
			  when item_format_time_classification_code in (2,6) then  sum(sales_item_store_monthmatcur) end item_format_sales_first_period
		FROM migvan.assortment_item_store_indices
		where item_format_time_classification_code in (1,5,2,6) -- only new items
		and {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code ,item_format_time_classification_code
		) as user_new_items_count_inn1
		where item_format_sales_first_period>0
		group by format_code
),
 user_insignificant_new_items_count as
(
		select count(*) sum_users_very_bad_items_1 ,format_code
		from(
		SELECT dw_item_key, format_code ,item_format_classification_code
		,case when item_format_classification_code=8 then  sum(salesstore_iteminformat_week0)
			  when item_format_classification_code=11 then  sum(sales_item_store_monthmatcur) end item_format_sales_first_period
		FROM migvan.assortment_item_store_indices
		where item_format_classification_code in (8,11)
		and {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code ,item_format_classification_code
		) as user_insignificant_new_items_count_inn1
		where item_format_sales_first_period>0
		group by format_code
)
select *,prcnt_of_users_very_bad_items_3 as value_column, case when prcnt_of_users_very_bad_items_3 = 0 then null
	  when prcnt_of_users_very_bad_items_3 > 0 and prcnt_of_users_very_bad_items_3 <= 0.2 then 1
	  when prcnt_of_users_very_bad_items_3 < 0.3 and prcnt_of_users_very_bad_items_3 > 0.2 then 2
	  when prcnt_of_users_very_bad_items_3 >= 0.3 then 3 end color_column -- Classification Color  
from(
	select user_new_items_count.format_code,sum_users_very_bad_items_1
	,total_sum_new_items_in_chain_2
	,case when total_sum_new_items_in_chain_2=0 then null else cast(sum_users_very_bad_items_1 as double precision)/total_sum_new_items_in_chain_2 end prcnt_of_users_very_bad_items_3 -- Value to display 
	from user_insignificant_new_items_count  join user_new_items_count on user_insignificant_new_items_count.format_code=user_new_items_count.format_code
) inn1;