----m_query6c - detailed_format - examination items----

with all_users_items as
(
		select count(*) total_sum_user_items_in_chain_2,format_code
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where  {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as t2
		where t2.sales_item_format_cur_1>=0 
		group by format_code
),
 m_q5_filterd_4_user_examination_item as
(
		select count(*) sum_users_items_for_examination_1,format_code
		from(
		SELECT dw_item_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where  item_classification_42 = 4
		and  {FILTER_PLACEHOLDER}
		group by dw_item_key, format_code 
		) as t1
		where t1.sales_item_format_cur_1>=0 
		group by format_code
)

select format_code,sum_users_items_for_examination_1,total_sum_user_items_in_chain_2
,prcnt_of_user_examination_items_3 as value_column -- Value to display
,case when prcnt_of_user_examination_items_3 = 0 then null
	  when prcnt_of_user_examination_items_3 > 0 and prcnt_of_user_examination_items_3 <= 0.1 then 1
	  when prcnt_of_user_examination_items_3 < 0.2 and prcnt_of_user_examination_items_3 > 0.1 then 2
	  when prcnt_of_user_examination_items_3 >= 0.2 then 3 end color_column -- Classification Color  
from (
select
all_users_items.format_code ,sum_users_items_for_examination_1,total_sum_user_items_in_chain_2
,case when total_sum_user_items_in_chain_2<>0 then cast(sum_users_items_for_examination_1 as double precision)/total_sum_user_items_in_chain_2 else null end prcnt_of_user_examination_items_3 
 from m_q5_filterd_4_user_examination_item join all_users_items on m_q5_filterd_4_user_examination_item.format_code=all_users_items.format_code ) as t