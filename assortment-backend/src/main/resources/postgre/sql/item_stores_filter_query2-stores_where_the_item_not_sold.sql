----item_stores_filter_query2-stores_where_the_item_not_sold----

with item_store_indexes as (
	select format_code,dw_store_key ,max(sales_store_format_cur) sales_store_yearmatcur
	from migvan.assortment_item_store_indices
	where  format_code=:format_code
	and {FILTER_PLACEHOLDER}
	group by format_code,dw_store_key
)
select t.format_code,
--(select format_name from migvan.assortment_item_store_indices kk
--	where  kk.format_code=t.format_code limit 1) as format_name,
t.dw_store_key
--,(select store_name from migvan.assortment_item_store_indices kk
--	where  kk.dw_store_key=t.dw_store_key limit 1) as store_name
from item_store_indexes t
where sales_store_yearmatcur is not null and  dw_store_key not in (select dw_store_key
							from migvan.old_items_measures
							where format_code=:format_code
							and dw_item_key=:dw_item_key
							and (sales_item_store_cur_1>0  and item_final_availability_in_store_29>0)
							and {FILTER_PLACEHOLDER}
				)
	{DW_STORE_KEY_PLACEHOLDER}



