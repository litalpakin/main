with rp5_chain_sales_items_by_classification as
(
select  item_classification_code
    ,sales_item_format_cy
    ,sales_item_format_py
    , sales_item_format_ch
    , sales_item_format_ph
    , sales_item_format_cq
    , sales_item_format_pq
    , sales_item_format_ss_cy
    , sales_item_format_ss_py
    , sales_item_format_ss_ch
    , sales_item_format_ss_ph
    , sales_item_format_ss_cq
    , sales_item_format_ss_pq
    ,case when sales_item_format_cy is null then 0-sales_item_format_py 
      when sales_item_format_py is null then sales_item_format_cy-0 else sales_item_format_cy-sales_item_format_py end sales_item_difference_cypy
    ,case when sales_item_format_cy<=0 then null 
      when sales_item_format_py<=0 then null else (sales_item_format_cy/sales_item_format_py)-1 end sales_item_change_cypy 
    ,case when sales_item_format_ch is null then 0-sales_item_format_ph 
      when sales_item_format_ph is null then sales_item_format_ch-0 else sales_item_format_ch-sales_item_format_ph end sales_item_difference_chph
    ,case when sales_item_format_ch<=0 then null 
      when sales_item_format_ph<=0 then null else (sales_item_format_ch/sales_item_format_ph)-1 end sales_item_change_chph
    ,case when sales_item_format_cq is null then 0-sales_item_format_pq 
      when sales_item_format_pq is null then sales_item_format_cq-0 else sales_item_format_cq-sales_item_format_pq end sales_item_difference_cqpq
    ,case when sales_item_format_cq<=0 then null 
      when sales_item_format_pq<=0 then null else (sales_item_format_cq/sales_item_format_pq)-1 end sales_item_change_cqpq  
    ,case when sales_item_format_ss_cy is null then 0-sales_item_format_ss_py 
      when sales_item_format_ss_py is null then sales_item_format_ss_cy-0 else sales_item_format_ss_cy-sales_item_format_ss_py end sales_item_difference_ss_cypy
    ,case when sales_item_format_ss_cy<=0 then null 
      when sales_item_format_ss_py<=0 then null else (sales_item_format_ss_cy/sales_item_format_ss_py)-1 end sales_item_change_ss_cypy
    ,case when sales_item_format_ss_ch is null then 0-sales_item_format_ss_ph 
      when sales_item_format_ss_ph is null then sales_item_format_ss_ch-0 else sales_item_format_ss_ch-sales_item_format_ss_ph end sales_item_difference_ss_chph
    ,case when sales_item_format_ss_ch<=0 then null 
      when sales_item_format_ss_ph<=0 then null else (sales_item_format_ss_ch/sales_item_format_ss_ph)-1 end sales_item_change_ss_chph
    ,case when sales_item_format_ss_cq is null then 0-sales_item_format_ss_pq 
      when sales_item_format_ss_pq is null then sales_item_format_ss_cq-0 else sales_item_format_ss_cq-sales_item_format_ss_pq end sales_item_difference_ss_cqpq
    ,case when sales_item_format_ss_cq<=0 then null 
      when sales_item_format_ss_pq<=0 then null else (sales_item_format_ss_cq/sales_item_format_ss_pq)-1 end sales_item_change_ss_cqpq 
      
    from (
        select item_format_classification_61 item_classification_code
    ,sum(sales_item_format_cur_1 ) sales_item_format_cy
    ,sum(sales_item_format_prv_2 ) sales_item_format_py

    ,sum(sales_item_format_halfmatcur_162 ) sales_item_format_ch
    ,sum(sales_item_format_halfmatprv_163 ) sales_item_format_ph

    ,sum(sales_item_format_quartermatcur_64 ) sales_item_format_cq
    ,sum(sales_item_format_quartermatprv_65 ) sales_item_format_pq

    ,sum(sales_item_format_ss_cur) sales_item_format_ss_cy
    ,sum(sales_item_format_ss_prv) sales_item_format_ss_py

    ,sum(sales_item_format_ss_halfmatcur ) sales_item_format_ss_ch
    ,sum(sales_item_format_ss_halfmatprv ) sales_item_format_ss_ph

    ,sum(sales_item_format_ss_quartermatcur ) sales_item_format_ss_cq
    ,sum(sales_item_format_ss_quartermatprv ) sales_item_format_ss_pq

        from migvan.old_items_id
        where  item_format_classification_61 = :classification_item
    and format_code = :formatParam
        group by item_format_classification_61
    ) t
    group by item_classification_code, 
    sales_item_format_cy, sales_item_format_py, sales_item_format_ch, sales_item_format_ph
    , sales_item_format_cq, sales_item_format_pq, sales_item_format_ss_cy, sales_item_format_ss_py
    , sales_item_format_ss_ch, sales_item_format_ss_ph, sales_item_format_ss_cq, sales_item_format_ss_pq
),

rp5_user_sales_items_by_classification as
(
select  item_classification_code
, user_sales_item_format_cy 
, user_sales_item_format_py
, user_sales_item_format_ch 
, user_sales_item_format_ph
, user_sales_item_format_cq 
, user_sales_item_format_pq
, user_sales_item_format_ss_cy 
, user_sales_item_format_ss_py
, user_sales_item_format_ss_ch 
, user_sales_item_format_ss_ph
, user_sales_item_format_ss_cq 
, user_sales_item_format_ss_pq

,case when user_sales_item_format_cy is null then 0-user_sales_item_format_py 
      when user_sales_item_format_py is null then user_sales_item_format_cy-0 else user_sales_item_format_cy-user_sales_item_format_py end user_sales_item_difference_cypy
,case when user_sales_item_format_cy<=0 then null 
      when user_sales_item_format_py<=0 then null else (user_sales_item_format_cy/user_sales_item_format_py)-1 end user_sales_item_change_cypy
      
,case when user_sales_item_format_ch is null then 0-user_sales_item_format_ph 
      when user_sales_item_format_ph is null then user_sales_item_format_ch-0 else user_sales_item_format_ch-user_sales_item_format_ph end user_sales_item_difference_chph
,case when user_sales_item_format_ch<=0 then null 
      when user_sales_item_format_ph<=0 then null else (user_sales_item_format_ch/user_sales_item_format_ph)-1 end user_sales_item_change_chph
      
,case when user_sales_item_format_cq is null then 0-user_sales_item_format_pq 
      when user_sales_item_format_pq is null then user_sales_item_format_cq-0 else user_sales_item_format_cq-user_sales_item_format_pq end user_sales_item_difference_cqpq
,case when user_sales_item_format_cq<=0 then null 
      when user_sales_item_format_pq<=0 then null else (user_sales_item_format_cq/user_sales_item_format_pq)-1 end user_sales_item_change_cqpq
      
,case when user_sales_item_format_ss_cy is null then 0-user_sales_item_format_ss_py 
      when user_sales_item_format_ss_py is null then user_sales_item_format_ss_cy-0 else user_sales_item_format_ss_cy-user_sales_item_format_ss_py end user_sales_item_difference_ss_cypy
,case when user_sales_item_format_ss_cy<=0 then null 
      when user_sales_item_format_ss_py<=0 then null else (user_sales_item_format_ss_cy/user_sales_item_format_ss_py)-1 end user_sales_item_change_ss_cypy
      
,case when user_sales_item_format_ss_ch is null then 0-user_sales_item_format_ss_ph 
      when user_sales_item_format_ss_ph is null then user_sales_item_format_ss_ch-0 else user_sales_item_format_ss_ch-user_sales_item_format_ss_ph end user_sales_item_difference_ss_chph
,case when user_sales_item_format_ss_ch<=0 then null 
      when user_sales_item_format_ss_ph<=0 then null else (user_sales_item_format_ss_ch/user_sales_item_format_ss_ph)-1 end user_sales_item_change_ss_chph
      
,case when user_sales_item_format_ss_cq is null then 0-user_sales_item_format_ss_pq 
      when user_sales_item_format_ss_pq is null then user_sales_item_format_ss_cq-0 else user_sales_item_format_ss_cq-user_sales_item_format_ss_pq end user_sales_item_difference_ss_cqpq
,case when user_sales_item_format_ss_cq<=0 then null 
      when user_sales_item_format_ss_pq<=0 then null else (user_sales_item_format_ss_cq/user_sales_item_format_ss_pq)-1 end user_sales_item_change_ss_cqpq
    from(
        select 
        item_classification_42 item_classification_code
    ,sum(sales_item_store_cur_1 ) user_sales_item_format_cy 
    ,sum(sales_item_store_prv_2 ) user_sales_item_format_py

    ,sum(sales_item_store_halfmatcur_47 ) user_sales_item_format_ch 
    ,sum(sales_item_store_halfmatprv_48 ) user_sales_item_format_ph

    ,sum(sales_item_store_quartermatcur_45 ) user_sales_item_format_cq 
    ,sum(sales_item_store_quartermatprv_46 ) user_sales_item_format_pq

    ,sum(sales_item_store_ss_cur ) user_sales_item_format_ss_cy 
    ,sum(sales_item_store_ss_prv ) user_sales_item_format_ss_py

    ,sum(sales_item_store_ss_halfmatcur ) user_sales_item_format_ss_ch 
    ,sum(sales_item_store_ss_halfmatprv ) user_sales_item_format_ss_ph

    ,sum(sales_item_store_ss_quartermatcur ) user_sales_item_format_ss_cq 
    ,sum(sales_item_store_ss_quartermatprv ) user_sales_item_format_ss_pq
    from migvan.old_items_measures
    where item_classification_42 =  :classification_item
  and {FILTER_PLACEHOLDER}
     group by item_classification_42
    ) as t
    group by item_classification_code, user_sales_item_format_cy , user_sales_item_format_py
        , user_sales_item_format_ch , user_sales_item_format_ph, user_sales_item_format_cq 
        , user_sales_item_format_pq , user_sales_item_format_ss_cy , user_sales_item_format_ss_py
        , user_sales_item_format_ss_ch , user_sales_item_format_ss_ph, user_sales_item_format_ss_cq 
        , user_sales_item_format_ss_pq

)

select cs.item_classification_code
,sales_item_format_cy, sales_item_format_py ,sales_item_difference_cypy ,sales_item_change_cypy
,user_sales_item_format_cy, user_sales_item_format_py,user_sales_item_difference_cypy, user_sales_item_change_cypy
,case when user_sales_item_format_cy is null then 0-sales_item_format_cy 
      when sales_item_format_cy is null then user_sales_item_format_cy-0 else user_sales_item_format_cy-sales_item_format_cy end user_to_chain_sales_item_difference_cy
,case when user_sales_item_format_cy<=0 then null 
      when sales_item_format_cy<=0 then null else (user_sales_item_format_cy/sales_item_format_cy)-1 end user_to_chain_sales_item_change_cy
      
,case when user_sales_item_format_py is null then 0-sales_item_format_py 
      when sales_item_format_py is null then user_sales_item_format_py-0 else user_sales_item_format_py-sales_item_format_py end user_to_chain_sales_item_difference_py
,case when user_sales_item_format_py<=0 then null 
      when sales_item_format_py<=0 then null else (user_sales_item_format_py/sales_item_format_py)-1 end user_to_chain_sales_item_change_py
      
,(user_sales_item_change_cypy/sales_item_change_cypy) user_to_chain_sales_item_growth_cypy

,sales_item_format_ss_cy, sales_item_format_ss_py ,sales_item_difference_ss_cypy ,sales_item_change_ss_cypy
,user_sales_item_format_ss_cy, user_sales_item_format_ss_py,user_sales_item_difference_ss_cypy, user_sales_item_change_ss_cypy

,case when user_sales_item_format_ss_cy is null then 0-sales_item_format_ss_cy 
      when sales_item_format_ss_cy is null then user_sales_item_format_ss_cy-0 else user_sales_item_format_ss_cy-sales_item_format_ss_cy end user_to_chain_sales_item_difference_ss_cy
,case when user_sales_item_format_ss_cy<=0 then null 
      when sales_item_format_ss_cy<=0 then null else (user_sales_item_format_ss_cy/sales_item_format_ss_cy)-1 end user_to_chain_sales_item_change_ss_cy
      
,case when user_sales_item_format_ss_py is null then 0-sales_item_format_ss_py 
      when sales_item_format_ss_py is null then user_sales_item_format_ss_py-0 else user_sales_item_format_ss_py-sales_item_format_ss_py end user_to_chain_sales_item_difference_ss_py
,case when user_sales_item_format_ss_py<=0 then null 
      when sales_item_format_ss_py<=0 then null else (user_sales_item_format_ss_py/sales_item_format_ss_py)-1 end user_to_chain_sales_item_change_ss_py

,(user_sales_item_change_ss_cypy/sales_item_change_ss_cypy) user_to_chain_sales_item_growth_ss_cypy

,sales_item_format_ch, sales_item_format_ph ,sales_item_difference_chph ,sales_item_change_chph
,user_sales_item_format_ch, user_sales_item_format_ph,user_sales_item_difference_chph, user_sales_item_change_chph
,case when user_sales_item_format_ch is null then 0-sales_item_format_ch 
      when sales_item_format_ch is null then user_sales_item_format_ch-0 else user_sales_item_format_ch-sales_item_format_ch end user_to_chain_sales_item_difference_ch
,case when user_sales_item_format_ch<=0 then null 
      when sales_item_format_ch<=0 then null else (user_sales_item_format_ch/sales_item_format_ch)-1 end user_to_chain_sales_item_change_ch
      
,case when user_sales_item_format_ph is null then 0-sales_item_format_ph 
      when sales_item_format_ph is null then user_sales_item_format_ph-0 else user_sales_item_format_ph-sales_item_format_ph end user_to_chain_sales_item_difference_ph
,case when user_sales_item_format_ph<=0 then null 
      when sales_item_format_ph<=0 then null else (user_sales_item_format_ph/sales_item_format_ph)-1 end user_to_chain_sales_item_change_ph
      
,(user_sales_item_change_chph/sales_item_change_chph) user_to_chain_sales_item_growth_chph

,sales_item_format_ss_ch, sales_item_format_ss_ph ,sales_item_difference_ss_chph ,sales_item_change_ss_chph
,user_sales_item_format_ss_ch, user_sales_item_format_ss_ph,user_sales_item_difference_ss_chph, user_sales_item_change_ss_chph
,case when user_sales_item_format_ss_ch is null then 0-sales_item_format_ss_ch 
      when sales_item_format_ss_ch is null then user_sales_item_format_ss_ch-0 else user_sales_item_format_ss_ch-sales_item_format_ss_ch end user_to_chain_sales_item_difference_ss_ch
,case when user_sales_item_format_ss_ch<=0 then null 
      when sales_item_format_ss_ch<=0 then null else (user_sales_item_format_ss_ch/sales_item_format_ss_ch)-1 end user_to_chain_sales_item_change_ss_ch
      
,case when user_sales_item_format_ss_ph is null then 0-sales_item_format_ss_ph 
      when sales_item_format_ss_ph is null then user_sales_item_format_ss_ph-0 else user_sales_item_format_ss_ph-sales_item_format_ss_ph end user_to_chain_sales_item_difference_ss_ph
,case when user_sales_item_format_ss_ph<=0 then null 
      when sales_item_format_ss_ph<=0 then null else (user_sales_item_format_ss_ph/sales_item_format_ss_ph)-1 end user_to_chain_sales_item_change_ss_ph
,(user_sales_item_change_ss_chph/sales_item_change_ss_chph) user_to_chain_sales_item_growth_ss_chph

,sales_item_format_cq, sales_item_format_pq ,sales_item_difference_cqpq ,sales_item_change_cqpq
,user_sales_item_format_cq, user_sales_item_format_pq,user_sales_item_difference_cqpq, user_sales_item_change_cqpq
,case when user_sales_item_format_cq is null then 0-sales_item_format_cq 
      when sales_item_format_cq is null then user_sales_item_format_cq-0 else user_sales_item_format_cq-sales_item_format_cq end user_to_chain_sales_item_difference_cq
,case when user_sales_item_format_cq<=0 then null 
      when sales_item_format_cq<=0 then null else (user_sales_item_format_cq/sales_item_format_cq)-1 end user_to_chain_sales_item_change_cq
      
,case when user_sales_item_format_pq is null then 0-sales_item_format_pq 
      when sales_item_format_pq is null then user_sales_item_format_pq-0 else user_sales_item_format_pq-sales_item_format_pq end user_to_chain_sales_item_difference_pq
,case when user_sales_item_format_pq<=0 then null 
      when sales_item_format_pq<=0 then null else (user_sales_item_format_pq/sales_item_format_pq)-1 end user_to_chain_sales_item_change_pq
      
,(user_sales_item_change_cqpq/sales_item_change_cqpq) user_to_chain_sales_item_growth_cqpq

,sales_item_format_ss_cq, sales_item_format_ss_pq ,sales_item_difference_ss_cqpq ,sales_item_change_ss_cqpq
,user_sales_item_format_ss_cq, user_sales_item_format_ss_pq,user_sales_item_difference_ss_cqpq, user_sales_item_change_ss_cqpq

,case when user_sales_item_format_ss_cq is null then 0-sales_item_format_ss_cq 
      when sales_item_format_ss_cq is null then user_sales_item_format_ss_cq-0 else user_sales_item_format_ss_cq-sales_item_format_ss_cq end user_to_chain_sales_item_difference_ss_cq
,case when user_sales_item_format_ss_cq<=0 then null 
      when sales_item_format_ss_cq<=0 then null else (user_sales_item_format_ss_cq/sales_item_format_ss_cq)-1 end user_to_chain_sales_item_change_ss_cq
      
,case when user_sales_item_format_ss_pq is null then 0-sales_item_format_ss_pq 
      when sales_item_format_ss_pq is null then user_sales_item_format_ss_pq-0 else user_sales_item_format_ss_pq-sales_item_format_ss_pq end user_to_chain_sales_item_difference_ss_pq
,case when user_sales_item_format_ss_pq<=0 then null 
      when sales_item_format_ss_pq<=0 then null else (user_sales_item_format_ss_pq/sales_item_format_ss_pq)-1 end user_to_chain_sales_item_change_ss_pq

,(user_sales_item_change_ss_cqpq/sales_item_change_ss_cqpq) user_to_chain_sales_item_growth_ss_cqpq
     
from rp5_chain_sales_items_by_classification cs join rp5_user_sales_items_by_classification us on cs.item_classification_code=us.item_classification_code

