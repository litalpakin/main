 selected_store_agg as
(
	select *
	,sum(sales_brand_subcat_week0) over (partition by category_key,format_code) sales_category_format_week0
	,sum(sales_brand_subcat_week1) over (partition by category_key,format_code) sales_category_format_week1
	,sum(sales_brand_subcat_week2) over (partition by category_key,format_code) sales_category_format_week2

	,sum(sales_brand_subcat_week0) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_week0
	,sum(sales_brand_subcat_week1) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_week1
	,sum(sales_brand_subcat_week2) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_week2

	,sum(sales_brand_subcat_week0) over (partition by format_code) sales_format_week0
	,sum(sales_brand_subcat_week1) over (partition by format_code) sales_format_week1
	,sum(sales_brand_subcat_week2) over (partition by format_code) sales_format_week2

	,sum(qty_brand_subcat_week0) over (partition by category_key,format_code) qty_sales_category_format_week0
	,sum(qty_brand_subcat_week1) over (partition by category_key,format_code) qty_sales_category_format_week1
	,sum(qty_brand_subcat_week2) over (partition by category_key,format_code) qty_sales_category_format_week2


	,sum(qty_brand_subcat_week0) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_week0
	,sum(qty_brand_subcat_week1) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_week1
	,sum(qty_brand_subcat_week2) over (partition by sub_category_key,category_key,format_code) qty_sales_subcat_format_week2

  ,sum(sales_brand_subcat_ss_week0) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_week0
  ,sum(sales_brand_subcat_ss_week1) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_week1
  ,sum(sales_brand_subcat_ss_week2) over (partition by sub_category_key,category_key,format_code) sales_subcat_format_ss_week2

	from(
      select
       sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
      ,sum(sales_brand_subcat_store_week0) sales_brand_subcat_week0
      ,sum(sales_brand_subcat_store_week1) sales_brand_subcat_week1
      ,sum(sales_brand_subcat_store_week2) sales_brand_subcat_week2

      ,sum(sales_brand_subcat_store_ss_week0) sales_brand_subcat_ss_week0
      ,sum(sales_brand_subcat_store_ss_week0) sales_brand_subcat_ss_week1
      ,sum(sales_brand_subcat_store_ss_week0) sales_brand_subcat_ss_week2

      ,sum(qty_brand_subcat_store_week0) qty_brand_subcat_week0
      ,sum(qty_brand_subcat_store_week1) qty_brand_subcat_week1
      ,sum(qty_brand_subcat_store_week2) qty_brand_subcat_week2
	  
	  ,max(selected_store_agg_2.count_stores_week0) count_selectedstores_week0
	  ,max(selected_store_agg_2.count_stores_week1) count_selectedstores_week1
	  ,max(selected_store_agg_2.count_stores_week2) count_selectedstores_week2
	  ,max(selected_store_agg_2.count_stores_3weeks) count_selectedstores_3weeks

      from migvan.assortment_store_indices cross join selected_store_agg_2
      where {STORES_FILTER_PLACEHOLDER}
      group by  sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
	) t
  where  {CATEGORY_FILTER_PLACEHOLDER}

)