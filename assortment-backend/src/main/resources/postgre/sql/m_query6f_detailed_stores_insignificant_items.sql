
----m_query6f - detailed stores - insignificant items----
select format_code,dw_store_key
	,dashboard_mark_for_8,dashboard_mark_color_for_8,total_dashboard_mark_for_8
	, case when total_dashboard_mark_for_8 = 0 then null
	 	   when total_dashboard_mark_for_8 > 0 and total_dashboard_mark_for_8 <= 0.2 then 1
	       when total_dashboard_mark_for_8 < 0.3 and total_dashboard_mark_for_8 > 0.2 then 2
       	   when total_dashboard_mark_for_8 >= 0.3 then 3 end total_dashboard_mark_color_for_8 -- Color classification for total value
	
	from(
	select *
	, case when dashboard_mark_for_8 = 0 then null
	  when dashboard_mark_for_8 > 0 and dashboard_mark_for_8 <= 0.2 then 1
	  when dashboard_mark_for_8 < 0.3 and dashboard_mark_for_8 > 0.2 then 2
	  when dashboard_mark_for_8 >= 0.3 then 3 end dashboard_mark_color_for_8 -- Color classification 
	  
	, avg(dashboard_mark_for_8) over()  total_dashboard_mark_for_8 --Total value to display
	
	from
	(
	select sub1.dw_store_key,sub1.format_code ,sum_users_very_bad_items_1
	,total_sum_new_items_in_chain_2
	,case when total_sum_new_items_in_chain_2=0 then null else cast(sum_users_very_bad_items_1 as double precision)/total_sum_new_items_in_chain_2 end dashboard_mark_for_8 -- Value to display 
	from 
	(
	
		select dw_store_key,format_code, sum(case when item_format_sales_first_period>0 then 1 else 0 end ) as total_sum_new_items_in_chain_2
		--, count(*) total_sum_new_items_in_chain_2
		from(
		SELECT dw_item_key,dw_store_key, format_code ,item_format_time_classification_code
		,case when item_format_time_classification_code in (1,5) then  sum(salesstore_iteminformat_week0)
			  when item_format_time_classification_code in (2,6) then  sum(sales_item_store_monthmatcur) end item_format_sales_first_period
		FROM migvan.assortment_item_store_indices
		where item_format_time_classification_code in (1,5,2,6) and {FILTER_PLACEHOLDER} -- only new items
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		group by dw_item_key,dw_store_key, format_code ,item_format_time_classification_code
		) as user_new_items_count_inn1
		--where item_format_sales_first_period>0
		group by dw_store_key,format_code
	
	) as sub1 
	left join 
	(
		select dw_store_key,format_code, sum(case when item_format_sales_first_period>0 then 1 else 0 end ) as sum_users_very_bad_items_1
		--,count(*) sum_users_very_bad_items_1
		from(
		SELECT dw_item_key,dw_store_key, format_code ,item_format_classification_code
		,case when item_format_classification_code=8 then  sum(salesstore_iteminformat_week0)
			  when item_format_classification_code=11 then  sum(sales_item_store_monthmatcur) end item_format_sales_first_period
		FROM migvan.assortment_item_store_indices
		where item_format_classification_code in (8,11) and {FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		group by dw_item_key,dw_store_key, format_code ,item_format_classification_code
		) as user_insignificant_new_items_count_inn1
		--where item_format_sales_first_period>0
		group by dw_store_key,format_code	
	) as sub2 on sub1.dw_store_key=sub2.dw_store_key and sub1.format_code=sub2.format_code 
	) as sub3
	) as sub4


