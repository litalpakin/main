----item_stores_filter_query1-new_items_launch_period----

select *
 ,case when ((salesmsmarket_categoryinmarket_3weeks/sales_ms_category_store_3weeks)-1>=0.05) and ((salesmsmarket_subcatinmarket_3weeks/sales_ms_subcat_store_3weeks)-1>=0.05)
 and (salesmsmarket_iteminsubcat_3weeks/sales_ms_item_subcat_store_3weeks)>0 and sales_ms_item_subcat_store_3weeks>0 and salesmsmarket_iteminsubcat_3weeks>0 and (sales_item_format_store_week0+sales_item_format_store_week1+sales_item_format_store_week2)>0
  then (salesmsmarket_iteminsubcat_3weeks/sales_ms_item_subcat_store_3weeks)*(sales_item_format_store_week0+sales_item_format_store_week1+sales_item_format_store_week2) else null end sales_potential_market_to_store_3weeks

from (
  select *

 ,case when coalesce(sales_ms_item_subcat_store_week1,0) <= 0 or coalesce(sales_ms_item_subcat_store_week0,0) <= 0 then null else
 (sales_ms_item_subcat_store_week0/sales_ms_item_subcat_store_week1)-1 end sales_change_ms_item_subcat_store_week0to1
 ,case when coalesce(sales_ms_item_subcat_store_week2,0) <= 0 or coalesce(sales_ms_item_subcat_store_week1,0) <= 0 then null else
 (sales_ms_item_subcat_store_week1/sales_ms_item_subcat_store_week2)-1 end sales_change_ms_item_subcat_store_week1to2
 ,case when coalesce(sales_ms_item_subcat_store_week2,0) <= 0 or coalesce(sales_ms_item_subcat_store_week0,0) <= 0 then null else
 (sales_ms_item_subcat_store_week0/sales_ms_item_subcat_store_week2)-1 end sales_change_ms_item_subcat_store_week0to2
 

 ,case when coalesce(qty_ms_item_subcat_store_week1,0) <= 0 or coalesce(qty_ms_item_subcat_store_week0,0) <= 0 then null else
 (qty_ms_item_subcat_store_week0/qty_ms_item_subcat_store_week1)-1 end qty_change_ms_item_subcat_store_week0to1
 ,case when coalesce(qty_ms_item_subcat_store_week2,0) <= 0 or coalesce(qty_ms_item_subcat_store_week1,0) <= 0 then null else
 (qty_ms_item_subcat_store_week2/qty_ms_item_subcat_store_week1)-1 end qty_change_ms_item_subcat_store_week1to2
 ,case when coalesce(qty_ms_item_subcat_store_week2,0) <= 0 or coalesce(qty_ms_item_subcat_store_week0,0) <= 0 then null else
 (qty_ms_item_subcat_store_week2/qty_ms_item_subcat_store_week0)-1 end qty_change_ms_item_subcat_store_week0to2

 ,case when ((salesmsmarket_categoryinmarket_3weeks/sales_ms_category_store_3weeks)-1>=0.05) and ((salesmsmarket_subcatinmarket_3weeks/sales_ms_subcat_store_3weeks)-1>=0.05)
 then 1 else 0 end is_pass_potential_ms_test


  from (

    SELECT
     dw_item_key

 ,salesmsmarket_categoryinmarket_3weeks
 ,salesmsmarket_subcatinmarket_3weeks
 ,salesmsmarket_iteminsubcat_3weeks
 ,salesstore_subcatinformat_3weeks

 ,case when (1-item_format_availability_in_store_3weeks)*salesstore_iteminformat_3weeks < 0 then null
 else (1-item_format_availability_in_store_3weeks)*salesstore_iteminformat_3weeks end full_availability_to_cur_sales_difference_item_store_3weeks
 ,case when coalesce((salesstore_format_week0_17+salesstore_format_week1_18+salesstore_format_week2_19),0) <= 0 or coalesce((salesstore_categoryinformat_week0_11+salesstore_categoryinformat_week1_12+salesstore_categoryinformat_week2_13),0) <= 0
 then null else ((salesstore_categoryinformat_week0_11+salesstore_categoryinformat_week1_12+salesstore_categoryinformat_week2_13)/(salesstore_format_week0_17+salesstore_format_week1_18+salesstore_format_week2_19)) end sales_ms_category_store_3weeks
 ,case when coalesce((salesstore_format_week0_17+salesstore_format_week1_18+salesstore_format_week2_19),0) <= 0 or coalesce((salesstore_subcatinformat_week0_8+salesstore_subcatinformat_week1_9+salesstore_subcatinformat_week2_10),0) <= 0
 then null else ((salesstore_subcatinformat_week0_8+salesstore_subcatinformat_week1_9+salesstore_subcatinformat_week2_10)/(salesstore_format_week0_17+salesstore_format_week1_18+salesstore_format_week2_19)) end sales_ms_subcat_store_3weeks

     ,dw_store_key,is_same_store
     ,sub_category_key, retailer_sub_category_key
     ,category_key, retailer_category_key
     ,sub_brand_hierarchy_2_key, retailer_sub_brand_hierarchy_2_key
     ,class_key, retailer_class_key
     ,supplier_key , retailer_supplier_key
     ,format_code
     ,is_special_new_item as  is_special_item
     ,general_status_key, general_status_name,is_united_dw_item_key,is_private_label

     ,salesstore_categoryinformat_week0_11 sales_category_format_store_week0
     ,salesstore_categoryinformat_week1_12 sales_category_format_store_week1
     ,salesstore_categoryinformat_week2_13 sales_category_format_store_week2

     ,case when coalesce(salesstore_categoryinformat_week1_12,0) <= 0 or coalesce(salesstore_categoryinformat_week0_11,0) <= 0 then null else (salesstore_categoryinformat_week0_11/salesstore_categoryinformat_week1_12)-1 end sales_change_category_store_week0to1
     ,case when coalesce(salesstore_categoryinformat_week2_13,0) <= 0 or coalesce(salesstore_categoryinformat_week1_12,0) <= 0 then null else (salesstore_categoryinformat_week1_12/salesstore_categoryinformat_week2_13)-1 end sales_change_category_store_week1to2
     ,case when coalesce(salesstore_categoryinformat_week2_13,0) <= 0 or coalesce(salesstore_categoryinformat_week0_11,0) <= 0 then null else (salesstore_categoryinformat_week0_11/salesstore_categoryinformat_week2_13)-1 end sales_change_category_store_week0to2


     ,item_format_examination_code_149 item_format_examination_code
     , item_format_classification_3 item_classification
     , is_item_classification_equal_for_all_format_4 is_item_classification_equal_for_all_format
     , total_weeks_since_launche_2 total_weeks_since_launche
     , count(dw_item_key) over(partition by format_code,dw_item_key) num_stores
     , store_group_classification_3weeks store_group_classification
	 , item_with_enough_stores_for_abc

     , salesstore_iteminformat_week0_5 sales_item_format_store_week0
     , salesstore_iteminformat_week1_6 sales_item_format_store_week1
     , salesstore_iteminformat_week2_7 sales_item_format_store_week2
     , (salesstore_iteminformat_week0_5+salesstore_iteminformat_week1_6+salesstore_iteminformat_week2_7)/3 avg_weekly_item_store_sales_3weeks

     ,case when coalesce(salesstore_iteminformat_week1_6,0) <= 0 or coalesce(salesstore_iteminformat_week0_5,0) <= 0 then null
     else (salesstore_iteminformat_week0_5/salesstore_iteminformat_week1_6)-1 end sales_change_item_store_week0to1
     ,case when coalesce(salesstore_iteminformat_week2_7,0) <= 0 or coalesce(salesstore_iteminformat_week1_6,0) <= 0 then null
     else (salesstore_iteminformat_week1_6/salesstore_iteminformat_week2_7)-1 end sales_change_item_store_week1to2
     ,case when coalesce(salesstore_iteminformat_week2_7,0) <= 0 or coalesce(salesstore_iteminformat_week0_5,0) <= 0 then null
     else (salesstore_iteminformat_week0_5/salesstore_iteminformat_week2_7)-1 end sales_change_item_store_week0to2

     ,case when coalesce(salesstore_iteminformat_week0_5,0) <= 0 or coalesce(salesstore_subcatinformat_week0_8,0) <= 0 then null else
      (salesstore_iteminformat_week0_5/salesstore_subcatinformat_week0_8) end sales_ms_item_subcat_store_week0
     ,case when coalesce(salesstore_iteminformat_week1_6,0) <= 0 or coalesce(salesstore_subcatinformat_week1_9,0) <= 0 then null else
      (salesstore_iteminformat_week1_6/salesstore_subcatinformat_week1_9) end sales_ms_item_subcat_store_week1
     ,case when coalesce(salesstore_iteminformat_week2_7,0) <= 0 or coalesce(salesstore_subcatinformat_week2_10,0) <= 0 then null else
      (salesstore_iteminformat_week2_7/salesstore_subcatinformat_week2_10) end sales_ms_item_subcat_store_week2
     ,salesstorems_iteminsubcat_3weeks sales_ms_item_subcat_store_3weeks

     ,case when coalesce(salesstore_iteminformat_week0_5,0) <= 0 or coalesce(salesstore_categoryinformat_week0_11,0) <= 0 then null else
     (salesstore_iteminformat_week0_5/salesstore_categoryinformat_week0_11) end sales_ms_item_category_store_week0
     ,case when coalesce(salesstore_iteminformat_week1_6,0) <= 0 or coalesce(salesstore_categoryinformat_week1_12,0) <= 0 then null else
     (salesstore_iteminformat_week1_6/salesstore_categoryinformat_week1_12) end sales_ms_item_category_store_week1
     ,case when coalesce(salesstore_iteminformat_week2_7,0) <= 0 or coalesce(salesstore_categoryinformat_week2_13,0) <= 0 then null else
     (salesstore_iteminformat_week2_7/salesstore_categoryinformat_week2_13) end sales_ms_item_category_store_week2


     , salesstore_subcatinformat_week0_8 sales_subcat_format_store_week0
     , salesstore_subcatinformat_week1_9 sales_subcat_format_store_week1
     , salesstore_subcatinformat_week2_10 sales_subcat_format_store_week2

     ,case when coalesce(salesstore_subcatinformat_week1_9,0) <= 0 or coalesce(salesstore_subcatinformat_week0_8,0) <= 0 then null else (salesstore_subcatinformat_week0_8/salesstore_subcatinformat_week1_9)-1 end sales_change_subcat_store_week0to1
     ,case when coalesce(salesstore_subcatinformat_week2_10,0) <= 0 or coalesce(salesstore_subcatinformat_week1_9,0) <= 0 then null else (salesstore_subcatinformat_week1_9/salesstore_subcatinformat_week2_10)-1 end sales_change_subcat_store_week1to2
     ,case when coalesce(salesstore_subcatinformat_week2_10,0) <= 0 or coalesce(salesstore_subcatinformat_week0_8,0) <= 0 then null else (salesstore_subcatinformat_week0_8/salesstore_subcatinformat_week2_10)-1 end sales_change_subcat_store_week0to2

     , qtystore_subcatinformat_week0_23 qty_subcat_format_store_week0
     , qtystore_subcatinformat_week1_24 qty_subcat_format_store_week1
     , qtystore_subcatinformat_week2_25 qty_subcat_format_store_week2

     , qtystore_iteminformat_week0_20 qty_item_format_store_week0
     , qtystore_iteminformat_week1_21 qty_item_format_store_week1
     , qtystore_iteminformat_week2_22 qty_item_format_store_week2

     ,case when coalesce(qtystore_iteminformat_week1_21,0) <= 0 or coalesce(qtystore_iteminformat_week0_20,0) <= 0 then null else (qtystore_iteminformat_week0_20/qtystore_iteminformat_week1_21)-1 end qty_change_item_store_week0to1
     ,case when coalesce(qtystore_iteminformat_week2_22,0) <= 0 or coalesce(qtystore_iteminformat_week1_21,0) <= 0 then null else (qtystore_iteminformat_week1_21/qtystore_iteminformat_week2_22)-1 end qty_change_item_store_week1to2
     ,case when coalesce(qtystore_iteminformat_week2_22,0) <= 0 or coalesce(qtystore_iteminformat_week0_20,0) <= 0 then null else (qtystore_iteminformat_week0_20/qtystore_iteminformat_week2_22)-1 end qty_change_item_store_week0to2

     ,case when coalesce(qtystore_iteminformat_week0_20,0) <= 0 or coalesce(qtystore_subcatinformat_week0_23,0) <= 0 then null else
      (qtystore_iteminformat_week0_20/qtystore_subcatinformat_week0_23) end qty_ms_item_subcat_store_week0
     ,case when coalesce(qtystore_iteminformat_week1_21,0) <= 0 or coalesce(qtystore_subcatinformat_week1_24,0) <= 0 then null else
      (qtystore_iteminformat_week1_21/qtystore_subcatinformat_week1_24) end qty_ms_item_subcat_store_week1
     ,case when coalesce(qtystore_iteminformat_week2_22,0) <= 0 or coalesce(qtystore_subcatinformat_week2_25,0) <= 0 then null else
      (qtystore_iteminformat_week2_22/qtystore_subcatinformat_week2_25) end qty_ms_item_subcat_store_week2

     ,is_iteminstore_pos_sales_3weeks item_store_distribution_3weeks
     ,item_format_availability_in_store_3weeks
     ,avgavailability_stores_group_a_item_format_3weeks
     ,avgavailability_stores_group_b_item_format_3weeks
     ,avgavailability_stores_group_c_item_format_3weeks


     ,avgsales_stores_group_a_item_format_week0_128 avgsales_stores_group_a_item_format_week0
     ,avgsales_stores_group_b_item_format_week0_129 avgsales_stores_group_b_item_format_week0
     ,avgsales_stores_group_c_item_format_week0_130 avgsales_stores_group_c_item_format_week0
     ,avgsales_stores_group_a_item_format_week1_131 avgsales_stores_group_a_item_format_week1
     ,avgsales_stores_group_b_item_format_week1_132 avgsales_stores_group_b_item_format_week1
     ,avgsales_stores_group_c_item_format_week1_133 avgsales_stores_group_c_item_format_week1
     ,avgsales_stores_group_a_item_format_week2_134 avgsales_stores_group_a_item_format_week2
     ,avgsales_stores_group_b_item_format_week2_135 avgsales_stores_group_b_item_format_week2
     ,avgsales_stores_group_c_item_format_week2_136 avgsales_stores_group_c_item_format_week2
     ,avgsalesms_group_a_iteminsubcatformat_week0_155 avgsalesms_group_a_iteminsubcatformat_week0
     ,avgsalesms_group_b_iteminsubcatformat_week0_156 avgsalesms_group_b_iteminsubcatformat_week0
     ,avgsalesms_group_c_iteminsubcatformat_week0_157 avgsalesms_group_c_iteminsubcatformat_week0
     ,avgsalesms_group_a_iteminsubcatformat_week1_158 avgsalesms_group_a_iteminsubcatformat_week1
     ,avgsalesms_group_b_iteminsubcatformat_week1_159 avgsalesms_group_b_iteminsubcatformat_week1
     ,avgsalesms_group_c_iteminsubcatformat_week1_160 avgsalesms_group_c_iteminsubcatformat_week1
     ,avgsalesms_group_a_iteminsubcatformat_week2_161 avgsalesms_group_a_iteminsubcatformat_week2
     ,avgsalesms_group_b_iteminsubcatformat_week2_162 avgsalesms_group_b_iteminsubcatformat_week2
     ,avgsalesms_group_c_iteminsubcatformat_week2_163 avgsalesms_group_c_iteminsubcatformat_week2

     ,avgqty_stores_group_a_item_format_week0_146 avgqty_stores_group_a_item_format_week0
     ,avgqty_stores_group_b_item_format_week0_147 avgqty_stores_group_b_item_format_week0
     ,avgqty_stores_group_c_item_format_week0_148 avgqty_stores_group_c_item_format_week0
     ,avgqty_stores_group_a_item_format_week1_149 avgqty_stores_group_a_item_format_week1
     ,avgqty_stores_group_b_item_format_week1_150 avgqty_stores_group_b_item_format_week1
     ,avgqty_stores_group_c_item_format_week1_151 avgqty_stores_group_c_item_format_week1
     ,avgqty_stores_group_a_item_format_week2_152 avgqty_stores_group_a_item_format_week2
     ,avgqty_stores_group_b_item_format_week2_153 avgqty_stores_group_b_item_format_week2
     ,avgqty_stores_group_c_item_format_week2_154 avgqty_stores_group_c_item_format_week2
     --,dense_rank() over (partition by store_group_classification order by salesstorems_iteminsubcat_3weeks desc) - max (case when salesstorems_iteminsubcat_3weeks is null then 1 else 0 end) over (partition by store_group_classification)||' / '||sum(1) over(partition by store_group_classification) ranking_salesms_stores_group_item_subcat_3weeks
     --,dense_rank() over (partition by format_code order by item_format_availability_in_store_3weeks desc) - max (case when item_format_availability_in_store_3weeks is null then 1 else 0 end) over (partition by format_code)||' / '|| num_stores as ranking_store_item_format_availability_3weeks
     --,dense_rank() over (partition by format_code order by sales_ms_item_subcat_store_3weeks desc) - max (case when sales_ms_item_subcat_store_3weeks is null then 1 else 0 end) over (partition by format_code)
    --||' / '|| num_stores as ranking_store_sales_ms_item_subcat_3weeks
     ,ranking_salesms_stores_group_item_subcat_3weeks
     ,ranking_store_item_format_availability_3weeks
     ,ranking_store_sales_ms_item_subcat_3weeks


     ,avgsalesms_group_a_iteminsubcatformat_3weeks
     ,avgsalesms_group_b_iteminsubcatformat_3weeks
     ,avgsalesms_group_c_iteminsubcatformat_3weeks
     ,avgavailability_stores_group_a_item_format_3weeks
     ,avgavailability_stores_group_b_item_format_3weeks
     ,avgavailability_stores_group_c_item_format_3weeks

     ,avgsalesms_store_group_iteminsubcatformat_week0
     ,avgsalesms_change_store_group_iteminsubcatformat_week0to1
     ,avgavailability_ratio_stores_group_a_to_c_item_format_3weeks
     ,salesms_ratio_group_a_to_store_item_subcat_3weeks
     ,salesms_ratio_group_b_to_store_item_subcat_3weeks
     ,salesms_ratio_group_c_to_store_item_subcat_3weeks
     ,avgsalesms_ratio_stores_group_a_to_c_item_format_3weeks
     ,sales_inc_store_to_salesms_group_a_3weeks
     ,sales_inc_store_to_salesms_group_b_3weeks
     ,sales_inc_store_to_salesms_group_c_3weeks
     ,qty_price_item_store_week0
     ,qty_price_item_format_week0 as qty_price_item_allformat_week0
     ,qty_price_change_item_store_to_format_week0
     ,qty_price_item_format_market_week0 as qty_price_item_format_market_week0_item_rep
     ,avg_weekly_item_market_qty_price_3weeks
     ,avgqty_price_item_stores_group_a_item_format_week0,avgqty_price_item_stores_group_b_item_format_week0,avgqty_price_item_stores_group_c_item_format_week0

      ,case when coalesce(((salesstore_iteminformat_week0_5+salesstore_iteminformat_week1_6+salesstore_iteminformat_week2_7)/3),0) <= 0 or coalesce(avg_weekly_item_market_qty_price_3weeks,0) <= 0 then null else
     (avg_weekly_item_market_qty_price_3weeks/((salesstore_iteminformat_week0_5+salesstore_iteminformat_week1_6+salesstore_iteminformat_week2_7)/3))-1 end qty_price_item_market_to_store_gap_3weeks


     ,qty_price_item_store_3weeks
     ,qty_price_item_format_3weeks as qty_price_item_allformat_3weeks
     ,qty_price_change_item_store_to_format_3weeks
     ,avgqty_price_item_stores_group_a_item_format_3weeks,avgqty_price_item_stores_group_b_item_format_3weeks,avgqty_price_item_stores_group_c_item_format_3weeks
    FROM migvan.new_items_launche_period_measures
    where 
     dw_item_key = :dw_item_key
     and format_code= :format_code
	 and (salesstore_iteminformat_week0_5 > 0 or salesstore_iteminformat_week1_6>0 or salesstore_iteminformat_week2_7>0)
    ) b
) t
where
 {FILTER_PLACEHOLDER}
 {DW_STORE_KEY_PLACEHOLDER}
