----m_query6a - growth items----

select distinct
total_sales_chain_cur_qtr
,total_sales_chain_prv_qtr
,sales_change_chain_item_qtr
,sum(sales_item_cur_market_qtr) over() total_sales_market_cur_qtr
,sum(sales_item_prv_market_qtr) over() total_sales_market_prv_qtr
,sales_change_market_item_qtr
,total_diff_from_market_growth_13
,case when total_diff_from_market_growth_13 > 0.05 then 1
	  when total_diff_from_market_growth_13 >= 0 and total_diff_from_market_growth_13 <= 0.05 then 2
	  when total_diff_from_market_growth_13 < 0 then 3 end clasification_diff_from_market_growth_14 -- Color classification
from(
select *
,sum(sales_item_format_cur_qtr) over() total_sales_chain_cur_qtr
,((1+sales_change_chain_item_qtr)/(1+sales_change_market_item_qtr))-1 total_diff_from_market_growth_13 -- Value to display 
	from(
	select *,case when coalesce(total_sales_market_prv_qtr,0) = 0 then null else (sum(sales_item_cur_market_qtr) over()/total_sales_market_prv_qtr)-1 end sales_change_market_item_qtr
		from(
			select *,case when coalesce(total_sales_chain_prv_qtr,0) = 0 then null else (sum(sales_item_format_cur_qtr) over()/total_sales_chain_prv_qtr)-1 end sales_change_chain_item_qtr
				from(
					select *, sum(sales_item_prv_market_qtr) over() total_sales_market_prv_qtr
					from(
						select *, sum(sales_item_format_prv_qtr) over() total_sales_chain_prv_qtr
						from (
							SELECT  dw_item_key, format_code
							, sum(sales_item_store_quartermatcur_45)  sales_item_format_cur_qtr
							, sum(sales_item_store_quartermatprv_46) sales_item_format_prv_qtr
							, max( sales_item_market_quartermatcur_90) sales_item_cur_market_qtr
							, max(sales_item_market_quartermatprv_91) sales_item_prv_market_qtr
							FROM migvan.old_items_measures 
							where  item_classification_42 = 1  and  item_format_examination_code_44 = 1 and is_same_store=1
							and   {FILTER_PLACEHOLDER}
							group by  dw_item_key, format_code
						)as inn1
					) as inn2
				) as inn3
		)as inn4
	)as inn5
)as inn6;