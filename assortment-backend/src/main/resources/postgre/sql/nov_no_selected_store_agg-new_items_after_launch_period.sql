selected_store_agg as
(

	select *
	from(
	select sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
	, sales_brand_subcat_month0
	, sales_brand_subcat_month1
	, sales_brand_subcat_month2

	, sales_brand_subcat_ss_month0
	, sales_brand_subcat_ss_month1
	, sales_brand_subcat_ss_month2

	, qty_brand_subcat_month0
	, qty_brand_subcat_month1
	, qty_brand_subcat_month2
	
	, sales_category_format_month0
	,sales_category_format_month1
	, sales_category_format_month2

	,sales_subcat_format_month0
	, sales_subcat_format_month1
	, sales_subcat_format_month2

	, sales_subcat_format_ss_month0
	, sales_subcat_format_ss_month1
	,sales_subcat_format_ss_month2

	, sales_in_format_month0
	, sales_in_format_month1
	, sales_in_format_month2

	, qty_sales_category_format_month0
	, qty_sales_category_format_month1
	, qty_sales_category_format_month2

	, qty_sales_subcat_format_month0
	, qty_sales_subcat_format_month1
	, qty_sales_subcat_format_month2

	,max(selected_store_agg_2.count_stores_month0) over() count_selectedstores_month0
	,max(selected_store_agg_2.count_stores_month1)over() count_selectedstores_month1
	,max(selected_store_agg_2.count_stores_month2) over() count_selectedstores_month2
	,max(selected_store_agg_2.count_stores_3months) over() count_selectedstores_3months

	
	
	from migvan.assortment_catalog_sales_indices cross join selected_store_agg_2

 	where   {STORES_FILTER_PLACEHOLDER}
	) t
	-- where sub_category_key is not null
	where  {CATEGORY_FILTER_PLACEHOLDER}
)