with rp1_chain_num_group_items_by_groups as
(
	select *, cast(num_group_items as double precision)/total_num_group_items share_of_num_group_items_from_total
	from(
		select item_format_top_classification_code, item_format_top_classification_name 
		,count(dw_item_key) num_group_items
		,sum(count(dw_item_key)) over() total_num_group_items
			from(
				select dw_item_key, format_code,item_format_top_classification_code, item_format_top_classification_name 
				from migvan.assortment_item_store_indices f
				where item_format_top_classification_code in (1,2) 
					  and f.format_code = :formatParam
				group by f.dw_item_key, f.format_code,item_format_top_classification_code , item_format_top_classification_name 
				) t1
		group by item_format_top_classification_code, item_format_top_classification_name 
		) t2
),
rp1_user_num_group_items_by_groups as
(
	select *,case when user_total_num_group_items=0 then null else cast(user_num_group_items as double precision)/user_total_num_group_items end user_share_of_num_group_items_from_total
	from(
		select item_format_top_classification_code, item_format_top_classification_name 
		,count(dw_item_key) user_num_group_items
		,sum(count(dw_item_key)) over() user_total_num_group_items
		from(
			select f.dw_item_key, f.format_code
				,item_format_top_classification_code, item_format_top_classification_name 
			from migvan.assortment_item_store_indices f
			where item_format_top_classification_code in (1,2) 
					and {FILTER_PLACEHOLDER}
			group by f.dw_item_key, f.format_code,item_format_top_classification_code, item_format_top_classification_name 
		) r1
		group by item_format_top_classification_code, item_format_top_classification_name 
	) r2
)


select cn.item_format_top_classification_code , cn.item_format_top_classification_name
,num_group_items, total_num_group_items, share_of_num_group_items_from_total
,user_num_group_items ,user_total_num_group_items, user_share_of_num_group_items_from_total
,((1+user_share_of_num_group_items_from_total)/(1+share_of_num_group_items_from_total))-1  share_of_change_num_group_items_user_to_chain
,case when user_share_of_num_group_items_from_total is null then 0-share_of_num_group_items_from_total 
	  else user_share_of_num_group_items_from_total-share_of_num_group_items_from_total end share_of_difference_num_group_items_user_to_chain
from rp1_chain_num_group_items_by_groups cn left join rp1_user_num_group_items_by_groups un on cn.item_format_top_classification_code=un.item_format_top_classification_code and cn.item_format_top_classification_name=un.item_format_top_classification_name