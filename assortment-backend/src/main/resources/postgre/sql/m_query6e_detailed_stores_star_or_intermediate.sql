----m_query6e -detailed stores - star or intermediate items----

select format_code,dw_store_key
		,dashboard_mark_for_6,dashboard_mark_color_for_6,total_dashboard_mark_for_6
		,case when total_dashboard_mark_for_6 >= 0.1 then 1
			  when total_dashboard_mark_for_6 > -0.1 and total_dashboard_mark_for_6 < 0.1 then 2
			  when total_dashboard_mark_for_6 <= -0.1 then 3 end total_dashboard_mark_color_for_6  -- Color classification for total value
		
		from(
		
		select *
		,case when dashboard_mark_for_6 >= 0.1 then 1
			  when dashboard_mark_for_6 > -0.1 and dashboard_mark_for_6 < 0.1 then 2
			  when dashboard_mark_for_6 <= -0.1 then 3 end dashboard_mark_color_for_6 -- Color classification 
			  
		,avg(dashboard_mark_for_6) over() total_dashboard_mark_for_6 --Total value to display 
		
		from
		(
		select sub1.dw_store_key,sub1.format_code, total_avg_sales_ms_item_4
		,total_avg_sales_ms_item_market_8
		,total_avg_sales_ms_item_4 - total_avg_sales_ms_item_market_8 total_diff_sales_ms_chain_market_12
		,case when total_avg_sales_ms_item_market_8<>0 then(total_avg_sales_ms_item_4/ total_avg_sales_ms_item_market_8)-1 else null end dashboard_mark_for_6  -- Value to display
		from
		(
		select dw_store_key,format_code, avg(sales_ms_item_subcat_3)  total_avg_sales_ms_item_4 
		from(
        select ni.dw_item_key,ni.sub_category_key,ni.dw_store_key,ni.format_code,item_format_classification_code
        ,case when item_format_classification_code={CLASSIFICATION_PLACEHOLDER1}
		then case when max(sales_subcat_format_week0) = 0 then null else sum (salesstore_iteminformat_week0)/max(sales_subcat_format_week0) end when item_format_classification_code={CLASSIFICATION_PLACEHOLDER2}
		then case when max(sales_subcat_format_month0_tmp) = 0 then null else sum (sales_item_store_monthmatcur)/max(sales_subcat_format_month0_tmp) end end sales_ms_item_subcat_3
        FROM migvan.assortment_item_store_indices ni 
		left join 
		(
		     select sub_category_key,format_code,dw_store_key
       			 ,sum(salesstore_iteminformat_week0) sales_subcat_format_week0
       			 ,sum(sales_item_store_monthmatcur) sales_subcat_format_month0_tmp
       		 from migvan.assortment_item_store_indices
             where {FILTER_PLACEHOLDER}
     	  	--where and   user's selections : formats,stores, catalog
			--and   user's permission : formats,stores, catalog
        	group by sub_category_key,format_code,dw_store_key

		) as sub_cat_sales on ni.sub_category_key=sub_cat_sales.sub_category_key and ni.format_code=sub_cat_sales.format_code and ni.dw_store_key=sub_cat_sales.dw_store_key
		where ni.item_format_classification_code in  ({CLASSIFICATION_PLACEHOLDER})
		and item_format_examination_code in (2,4,5,6,1,3,23)
		and {NI_FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
        group by ni.dw_item_key,ni.sub_category_key,ni.dw_store_key,ni.format_code,item_format_classification_code
        ) as ass_item_sales_inn1
		group by dw_store_key,format_code
		) as sub1
		left join 
		(
		 	select format_code,dw_store_key,avg(sales_ms_item_subcat_market_7)  total_avg_sales_ms_item_market_8  
		 	from (
	        select dw_item_key,dw_store_key,sub_category_key,format_code,item_format_classification_code
	        ,case when item_format_classification_code={CLASSIFICATION_PLACEHOLDER1}
			then case when max(salesmarket_subcat_week0) = 0 then null else max(salesmarket_item_week0)/max(salesmarket_subcat_week0) end
	                  when item_format_classification_code={CLASSIFICATION_PLACEHOLDER2}
					  then case when max(salesmarket_subcat_monthmatcur) = 0 then null else max(salesmarket_item_monthmatcur)/max(salesmarket_subcat_monthmatcur) end end sales_ms_item_subcat_market_7
	        
	        FROM migvan.assortment_item_store_indices
	        where item_format_classification_code in  ({CLASSIFICATION_PLACEHOLDER})
			and item_format_examination_code in (2,4,5,6,1,3,23)
            and {FILTER_PLACEHOLDER}
			--and   user's selections : formats,stores, catalog
			--and   user's permission : formats,stores, catalog
	        group by dw_item_key,dw_store_key,sub_category_key,format_code,item_format_classification_code
	        ) as market_item_sales_inn1
			group by format_code,dw_store_key
		) as sub2 on sub1.format_code=sub2.format_code and sub1.dw_store_key=sub2.dw_store_key 
		) as sub3
		) as sub4
		
		