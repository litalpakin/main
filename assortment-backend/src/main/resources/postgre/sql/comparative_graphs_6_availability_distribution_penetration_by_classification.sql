with rp6_chain_availability_distribution_penetration as
(
select item_classification
,avg(avg_item_availability_cy_mat) avg_availability_cy_mat  
,avg(avg_item_availability_cq_mat) avg_availability_cq_mat
,avg(avg_item_availability_ch_mat) avg_availability_ch_mat 
,cast(sum(is_item_store_sold_yearmatcur)as double precision)/sum(is_store_sold_yearmatcur) distribution_cy_mat 
,cast(sum(is_item_store_sold_quartermatcur)as double precision)/sum(is_store_sold_quartermatcur) distribution_cq_mat
,cast(sum(is_item_store_sold_halfmatcur)as double precision)/sum(is_store_sold_halfmatcur) distribution_ch_mat
,avg(penetration_item_subcat_cy_mat_tmp) penetration_item_subcat_cy_mat
,avg(penetration_item_subcat_ch_mat_tmp) penetration_item_subcat_ch_mat
from(
SELECT 
    item_classification_42 item_classification ,dw_item_key,format_code
,avg(item_final_availability_in_store_29) avg_item_availability_cy_mat  
,avg(item_final_availability_in_store_quartermatcur_156) avg_item_availability_cq_mat
,avg(item_final_availability_in_store_halfmatcur_157) avg_item_availability_ch_mat 
,sum(is_item_store_sold_yearmatcur_170) is_item_store_sold_yearmatcur
,sum(is_store_sold_yearmatcur_173)  is_store_sold_yearmatcur
,sum(is_item_store_sold_quartermatcur_171) is_item_store_sold_quartermatcur
,sum(is_store_sold_quartermatcur_174)  is_store_sold_quartermatcur
,sum(is_item_store_sold_halfmatcur_172) is_item_store_sold_halfmatcur
,sum(is_store_sold_halfmatcur_175)  is_store_sold_halfmatcur
,max(penetration_item_subcat_cur_33) penetration_item_subcat_cy_mat_tmp
,max(penetration_item_subcat_halfmatcur_161) penetration_item_subcat_ch_mat_tmp

FROM migvan.old_items_measures 
where item_classification_42 = :classification_item
and format_code = :formatParam

group by item_classification , dw_item_key ,format_code
) t
group by item_classification
),

rp6_user_availability_distribution_penetration as
(

select   item_classification
,avg(user_avg_item_availability_cy_mat) user_avg_availability_cy_mat  
,avg(user_avg_item_availability_cq_mat) user_avg_availability_cq_mat
,avg(user_avg_item_availability_ch_mat) user_avg_availability_ch_mat
,case when sum(is_store_sold_yearmatcur)<>0 then cast(sum(is_item_store_sold_yearmatcur)as double precision)/sum(is_store_sold_yearmatcur) end user_distribution_cy_mat 
,case when sum(is_store_sold_quartermatcur)<>0 then cast(sum(is_item_store_sold_quartermatcur)as double precision)/sum(is_store_sold_quartermatcur) end user_distribution_cq_mat 
,case when sum(is_store_sold_halfmatcur)<>0 then cast(sum(is_item_store_sold_halfmatcur)as double precision)/sum(is_store_sold_halfmatcur) end user_distribution_ch_mat
,avg(user_penetration_item_subcat_cy_mat_tmp) user_penetration_item_subcat_cy_mat 
,avg(user_penetration_item_subcat_ch_mat_tmp) user_penetration_item_subcat_ch_mat
from
(
SELECT item_classification_42 item_classification , dw_item_key, format_code
,avg(item_final_availability_in_store_29) user_avg_item_availability_cy_mat  
,avg(item_final_availability_in_store_quartermatcur_156) user_avg_item_availability_cq_mat
,avg(item_final_availability_in_store_halfmatcur_157) user_avg_item_availability_ch_mat
,sum(is_item_store_sold_yearmatcur_170) is_item_store_sold_yearmatcur
,sum(is_store_sold_yearmatcur_173) is_store_sold_yearmatcur
,sum(is_item_store_sold_quartermatcur_171) is_item_store_sold_quartermatcur
,sum(is_store_sold_quartermatcur_174) is_store_sold_quartermatcur
,sum(is_item_store_sold_halfmatcur_172) is_item_store_sold_halfmatcur
,sum(is_store_sold_halfmatcur_175) is_store_sold_halfmatcur
,max(penetration_item_subcat_cur_33) user_penetration_item_subcat_cy_mat_tmp
,max(penetration_item_subcat_halfmatcur_161) user_penetration_item_subcat_ch_mat_tmp

FROM migvan.old_items_measures 
where item_classification_42 = :classification_item
and {FILTER_PLACEHOLDER}

group by item_classification , dw_item_key ,format_code
) b
group by item_classification
)

select cad.item_classification
----------------availability------------
,avg_availability_cy_mat ,user_avg_availability_cy_mat
,case when user_avg_availability_cy_mat is null then 0-avg_availability_cy_mat 
      when avg_availability_cy_mat is null then user_avg_availability_cy_mat-0 else user_avg_availability_cy_mat-avg_availability_cy_mat end avg_availability_difference_user_chain_cy_mat
,case when user_avg_availability_cy_mat<=0 then null 
      when avg_availability_cy_mat<=0 then null else (user_avg_availability_cy_mat/avg_availability_cy_mat)-1 end avg_availability_change_user_chain_cy_mat

,avg_availability_cq_mat ,user_avg_availability_cq_mat
,case when user_avg_availability_cq_mat is null then 0-avg_availability_cq_mat 
      when avg_availability_cq_mat is null then user_avg_availability_cq_mat-0 else user_avg_availability_cq_mat-avg_availability_cq_mat end avg_availability_difference_user_chain_cq_mat
,case when user_avg_availability_cq_mat<=0 then null 
      when avg_availability_cq_mat<=0 then null else (user_avg_availability_cq_mat/avg_availability_cq_mat)-1 end avg_availability_change_user_chain_cq_mat

,avg_availability_ch_mat ,user_avg_availability_ch_mat
,case when user_avg_availability_ch_mat is null then 0-avg_availability_ch_mat 
      when avg_availability_ch_mat is null then user_avg_availability_ch_mat-0 else user_avg_availability_ch_mat-avg_availability_ch_mat end avg_availability_difference_user_chain_ch_mat
,case when user_avg_availability_ch_mat<=0 then null 
      when avg_availability_ch_mat<=0 then null else (user_avg_availability_ch_mat/avg_availability_ch_mat)-1 end avg_availability_change_user_chain_ch_mat

----------------distribution------------
      
,distribution_cy_mat ,user_distribution_cy_mat
,case when user_distribution_cy_mat is null then 0-distribution_cy_mat 
      when distribution_cy_mat is null then user_distribution_cy_mat-0 else user_distribution_cy_mat-distribution_cy_mat end distribution_difference_user_chain_cy_mat
,case when user_distribution_cy_mat<=0 then null 
      when distribution_cy_mat<=0 then null else (user_distribution_cy_mat/distribution_cy_mat)-1 end distribution_change_user_chain_cy_mat

,distribution_cq_mat ,user_distribution_cq_mat
,case when user_distribution_cq_mat is null then 0-distribution_cq_mat 
      when distribution_cq_mat is null then user_distribution_cq_mat-0 else user_distribution_cq_mat-distribution_cq_mat end distribution_difference_user_chain_cq_mat
,case when user_distribution_cq_mat<=0 then null 
      when distribution_cq_mat<=0 then null else (user_distribution_cq_mat/distribution_cq_mat)-1 end distribution_change_user_chain_cq_mat
      
,distribution_ch_mat ,user_distribution_ch_mat
,case when user_distribution_ch_mat is null then 0-distribution_ch_mat 
      when distribution_ch_mat is null then user_distribution_ch_mat-0 else user_distribution_ch_mat-distribution_ch_mat end distribution_difference_user_chain_ch_mat
,case when user_distribution_ch_mat<=0 then null 
      when distribution_ch_mat<=0 then null else (user_distribution_ch_mat/distribution_ch_mat)-1 end distribution_change_user_chain_ch_mat

----------------penetration------------
,penetration_item_subcat_cy_mat, user_penetration_item_subcat_cy_mat 
,case when user_penetration_item_subcat_cy_mat<=0 then null 
      when penetration_item_subcat_cy_mat<=0 then null else (user_penetration_item_subcat_cy_mat/penetration_item_subcat_cy_mat) end penetration_change_user_chain_cy_mat

,penetration_item_subcat_ch_mat, user_penetration_item_subcat_ch_mat
,case when user_penetration_item_subcat_ch_mat<=0 then null 
      when penetration_item_subcat_ch_mat<=0 then null else (user_penetration_item_subcat_ch_mat/penetration_item_subcat_ch_mat) end penetration_change_user_chain_ch_mat


from rp6_chain_availability_distribution_penetration cad join rp6_user_availability_distribution_penetration uad on cad.item_classification=uad.item_classification
