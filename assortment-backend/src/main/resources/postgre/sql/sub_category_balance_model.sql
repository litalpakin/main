------sub_category_balance_model-----

select *
,case when sum_sales_difference is not null and sum_sales_difference<>0 then (sales_difference_sum_pos/sum_sales_difference)*sum_sales_change end sales_difference_pos_balance
,case when sum_sales_difference is not null and sum_sales_difference<>0 then (sales_difference_sum_neg/sum_sales_difference)*sum_sales_change end sales_difference_neg_balance
--,case when sales_difference>=0 then case when sales_difference_sum_pos is not null and sales_difference_sum_pos<>0 then (sales_difference/sales_difference_sum_pos) end
--      when sales_difference<0 then case when sales_difference_sum_neg is not null and sales_difference_sum_neg<>0 then (-sales_difference/sales_difference_sum_neg) end
--      end sales_difference_balance_ms

,case when sales_difference>=0 then case when sales_difference_sum_pos is not null and sales_difference_sum_pos<>0 then (sales_difference/sales_difference_sum_pos) end
      else null --when sales_difference<0 then case when sales_difference_sum_neg is not null and sales_difference_sum_neg<>0 then (-sales_difference/sales_difference_sum_neg) end
      end sales_difference_balance_ms_pos

,case --when sales_difference>=0 then case when sales_difference_sum_pos is not null and sales_difference_sum_pos<>0 then (sales_difference/sales_difference_sum_pos) end
      when sales_difference<0 then case when sales_difference_sum_neg is not null and sales_difference_sum_neg<>0 then (sales_difference/sales_difference_sum_neg) end
	  else null
      end sales_difference_balance_ms_neg
	  
from(
select *
,sum(sales_difference) over()as sum_sales_difference
,sum(case when sales_difference>=0 then sales_difference end) over() sales_difference_sum_pos
,sum(case when sales_difference<0 then sales_difference end) over() sales_difference_sum_neg
,case when sum_sales_prev is not null and sum_sales_prev<>0 then (sum_sales_cur/sum_sales_prev)-1 end sum_sales_change
from(
select *
,case when sales_cur is null then 0-sales_prev 
      when sales_prev is null then sales_cur-0 
      else sales_cur-sales_prev end sales_difference
,case when (sales_prev is null or sales_cur is null) or sales_prev=0 then null else (sales_cur/sales_prev)-1 end sales_change
,sum(sales_cur) over()as sum_sales_cur
,sum(sales_prev) over()as sum_sales_prev

from(
select sub_category_key,format_code,subcat_format_classification_code,  subcat_format_classification_name,is_having_assortment_problem
,{TIME_FILTER_PLACEHOLDER}

from migvan.sub_category_store_classification_indices
where 
format_code = :format_code
 and {FILTER_PLACEHOLDER}   {SUBCAT_FORMAT_CLASSIFICATION}

group by sub_category_key,format_code,subcat_format_classification_code,  subcat_format_classification_name,is_having_assortment_problem
) sub1
) sub2
) sub3