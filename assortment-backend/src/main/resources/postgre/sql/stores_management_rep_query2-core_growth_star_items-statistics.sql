-----stores_management_rep_query2-core_growth_star_items-statistics_tab-----
select *
, sum(share_of_items_count_by_classification) over() total_share_of_items_count_by_classification
,sum(share_of_items_sales_by_classification_cy) over() total_share_of_items_sales_by_classification_cy
from(

select *
  , case when coalesce((sum(items_count_by_classification) over() ),0)<>0
  then cast(items_count_by_classification as DOUBLE PRECISION)/(sum(items_count_by_classification) over()) end share_of_items_count_by_classification
  , case when coalesce (sum(items_sales_by_classification_cy) over(),0)<>0
  then items_sales_by_classification_cy/(sum(items_sales_by_classification_cy) over()) end share_of_items_sales_by_classification_cy
 ,sum(items_count_by_classification) over() as items_total_count_by_classification
 ,sum(items_sales_by_classification_cy) over() as items_total_sales_by_classification_cy
from (
  select  
  	item_format_type_classification_code as item_format_classification_code
  	,count (distinct dw_item_key) as items_count_by_classification
  	,sum(sales_item_store_cy) items_sales_by_classification_cy
  from migvan.assortment_core_growth_star_items_store_indices
  where  format_code = :format_code
 		{STORES_FILTER_PLACEHOLDER} 
 		{CATALOG_FILTER_PLACEHOLDER}
 group by item_format_type_classification_code
) t
)t2
