selected_store_agg as
(
	select *
	from(
      select
       sub_brand_hierarchy_2_key, sub_category_key, category_key, format_code
      , sales_brand_subcat_week0
      , sales_brand_subcat_week1
      , sales_brand_subcat_week2

      , sales_brand_subcat_ss_week0
      , sales_brand_subcat_ss_week1
      , sales_brand_subcat_ss_week2

      , qty_brand_subcat_week0
      , qty_brand_subcat_week1
      , qty_brand_subcat_week2
	  
	 	, sales_category_format_week0
	, sales_category_format_week1
	, sales_category_format_week2

	, sales_subcat_format_week0
	, sales_subcat_format_week1
	, sales_subcat_format_week2

	, sales_format_week0
	, sales_format_week1
	, sales_format_week2

	, qty_sales_category_format_week0
	, qty_sales_category_format_week1
	, qty_sales_category_format_week2


	, qty_sales_subcat_format_week0
	, qty_sales_subcat_format_week1
	, qty_sales_subcat_format_week2
  , sales_subcat_format_ss_week0
  , sales_subcat_format_ss_week1
  , sales_subcat_format_ss_week2

	  ,max(selected_store_agg_2.count_stores_week0) over() count_selectedstores_week0
	  ,max(selected_store_agg_2.count_stores_week1) over() count_selectedstores_week1
	  ,max(selected_store_agg_2.count_stores_week2) over() count_selectedstores_week2
	  ,max(selected_store_agg_2.count_stores_3weeks) over() count_selectedstores_3weeks

	from  migvan.assortment_catalog_sales_indices cross join selected_store_agg_2
	  where  {STORES_FILTER_PLACEHOLDER}
	) t
   where  {CATEGORY_FILTER_PLACEHOLDER}
	

)