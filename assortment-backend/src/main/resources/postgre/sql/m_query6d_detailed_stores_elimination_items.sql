-----m_query6d - detailed stores - elimination items----

select format_code,dw_store_key
	,dashboard_mark_for_5,dashboard_mark_color_for_5,total_dashboard_mark_for_5
,case when total_dashboard_mark_for_5 = 0 then null
	  when total_dashboard_mark_for_5 > 0 and total_dashboard_mark_for_5 <= 0.1 then 1
	  when total_dashboard_mark_for_5 < 0.2 and total_dashboard_mark_for_5 > 0.1 then 2
	  when total_dashboard_mark_for_5 >= 0.2 then 3 end total_dashboard_mark_color_for_5 -- Color classification for total value

from(
select *
,case when dashboard_mark_for_5 = 0 then null
	  when dashboard_mark_for_5 > 0 and dashboard_mark_for_5 <= 0.1 then 1
	  when dashboard_mark_for_5 < 0.2 and dashboard_mark_for_5 > 0.1 then 2
	  when dashboard_mark_for_5 >= 0.2 then 3 end dashboard_mark_color_for_5 -- Color classification	
	  
,avg(dashboard_mark_for_5) over() total_dashboard_mark_for_5 --Total value to display 
	  
from(
select sub1.dw_store_key,sub1.format_code
,sum_users_items_for_elimination_1,total_sum_user_items_in_chain_2
,case when total_sum_user_items_in_chain_2<>0 then cast(sum_users_items_for_elimination_1 as double precision)/total_sum_user_items_in_chain_2 else null end dashboard_mark_for_5 -- Value to display
 from 
 (	
		select dw_store_key, format_code ,count(*) total_sum_user_items_in_chain_2
		from(
		SELECT dw_item_key,dw_store_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures 
		where {FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		group by dw_item_key,dw_store_key, format_code 
		) as all_users_items_inn1
		where sales_item_format_cur_1>0 
		group by dw_store_key, format_code 
		
 ) as sub1
 left join
 (			
		select dw_store_key, format_code ,count(*) sum_users_items_for_elimination_1
		from(
		SELECT dw_item_key,dw_store_key, format_code 
		,sum(coalesce(sales_item_store_monthmatcur_177,0)) sales_item_format_cur_1
		FROM migvan.old_items_measures
		where  item_classification_42 = 5
        and {FILTER_PLACEHOLDER}
		--and   user's selections : formats,stores, catalog
		--and   user's permission : formats,stores, catalog
		group by dw_item_key,dw_store_key, format_code 
		) as m_q5_filterd_5_user_disposal_item_inn1
		where sales_item_format_cur_1>0 
		group by dw_store_key, format_code 
	
 ) as sub2 on sub1.dw_store_key=sub2.dw_store_key and sub1.format_code=sub2.format_code 

) as sub3
) as sub4