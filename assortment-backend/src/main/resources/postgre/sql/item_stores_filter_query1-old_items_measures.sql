----item_stores_filter_query1-old_items_measures----

select *
 ,case when is_pass_potential_ms_test=1 and (sales_ms_item_subcat_market_yearmatcur_109/sales_ms_item_subcat_store_yearmatcur)-1>0 and sales_ms_item_subcat_store_yearmatcur>0 and sales_ms_item_subcat_market_yearmatcur_109>0 and sales_item_store_cur_1>0 and (sales_ms_item_subcat_market_yearmatcur_109/sales_ms_item_subcat_store_yearmatcur)*sales_item_store_cur_1>0
  then (sales_ms_item_subcat_market_yearmatcur_109/sales_ms_item_subcat_store_yearmatcur)*sales_item_store_cur_1 else null end sales_potential_market_to_store_yearmatcur

 ,case when (sales_changegap_subcat_from_format_market_yearmatvsprv-sales_changegap_subcat_from_format_ss_yearmatvsprv>=0.03) and (sales_ms_subcat_format_market_yearmatcur_121>=sales_ms_subcat_store_yearmatcur_131) and (sales_ms_item_subcat_market_yearmatcur_109>=sales_ms_item_subcat_store_yearmatcur)  then 1 else 0 end is_pass_potential_change_test_ss

 ,case when (sales_changegap_subcat_from_format_market_yearmatvsprv-sales_changegap_subcat_from_format_ss_yearmatvsprv<0.03) or (sales_ms_subcat_format_market_yearmatcur_121<sales_ms_subcat_store_yearmatcur_131) or (sales_ms_item_subcat_market_yearmatcur_109<sales_ms_item_subcat_store_yearmatcur)
 or gap_growth_market_store_ss_yearmatcur*sales_item_store_ss_cur<0 then null else gap_growth_market_store_ss_yearmatcur*sales_item_store_ss_cur end growth_potential_market_to_store_ss_yearmatcur

 ,avg (sales_change_ms_item_subcat_store_yearmatvslasty) over(partition by store_group_classification , format_code ) avg_sales_change_ms_item_subcat_by_store_group_yearmatcur
 ,avg (sales_change_ms_item_subcat_store_quartermatvslasty) over(partition by store_group_classification , format_code ) avg_sales_change_ms_item_subcat_by_store_group_quartermatcur


from(
    SELECT *

        ,avg (sales_ms_item_subcat_store_yearmatcur) over(partition by store_group_classification , format_code ) avg_sales_ms_item_subcat_by_store_group_yearmatcur
        ,avg (sales_ms_item_subcat_store_quartermatcur) over(partition by store_group_classification , format_code ) avg_sales_ms_item_subcat_by_store_group_quartermatcur

        ,case when coalesce(sales_ms_item_subcat_store_yearmatprv,0) <= 0 or coalesce(sales_ms_item_subcat_store_yearmatcur,0) <= 0 then null else
        (sales_ms_item_subcat_store_yearmatcur/sales_ms_item_subcat_store_yearmatprv)-1 end sales_change_ms_item_subcat_store_yearmatvslasty
        ,case when coalesce(sales_ms_item_subcat_store_quartermatprv,0) <= 0 or coalesce(sales_ms_item_subcat_store_quartermatcur,0) <= 0 then null else
        (sales_ms_item_subcat_store_quartermatcur/sales_ms_item_subcat_store_quartermatprv)-1 end sales_change_ms_item_subcat_store_quartermatvslasty
        --,case when coalesce(sales_ms_item_subcat_store_halfmatprv_130,0) <= 0 or coalesce(sales_ms_item_subcat_store_halfmatcur_129,0) <= 0 then null else
        --(sales_ms_item_subcat_store_halfmatcur_129/sales_ms_item_subcat_store_halfmatprv_130)-1 end sales_change_ms_item_subcat_store_halfmatvslasty_140

	    ,case when coalesce(sales_store_format_cur_3,0) <= 0 or coalesce(sales_subcat_store_cur_7,0) <= 0 then null else
        (sales_subcat_store_cur_7/sales_store_format_cur_3) end sales_ms_subcat_store_yearmatcur_131

        /*,case when coalesce(sales_store_format_cur_3,0) <= 0 or coalesce(sales_subcat_store_cur_7,0) <= 0 then null else
        (sales_subcat_store_cur_7/sales_store_format_cur_3) end sales_ms_subcat_store_yearmatcur_131
        ,case when coalesce(sales_store_format_prv_4,0) <= 0 or coalesce(sales_subcat_store_prv_8,0) <= 0 then null else
        (sales_subcat_store_prv_8/sales_store_format_prv_4) end sales_ms_subcat_store_yearmatprv_132
        ,case when coalesce(sales_store_format_quartermatcur_49,0) <= 0 or coalesce(sales_subcat_store_quartermatcur_57,0) <= 0 then null else
        (sales_subcat_store_quartermatcur_57/sales_store_format_quartermatcur_49) end sales_ms_subcat_store_quartermatcur_133
        ,case when coalesce(sales_store_format_quartermatprv_50,0) <= 0 or coalesce(sales_subcat_store_quartermatprv_58,0) <= 0 then null else
        (sales_subcat_store_quartermatprv_58/sales_store_format_quartermatprv_50) end sales_ms_subcat_store_quartermatprv_134
        ,case when coalesce(sales_store_format_halfmatcur_51,0) <= 0 or coalesce(sales_subcat_store_halfmatcur_59,0) <= 0 then null else
        (sales_subcat_store_halfmatcur_59/sales_store_format_halfmatcur_51) end sales_ms_subcat_store_halfmatcur_135
        ,case when coalesce(sales_store_format_halfmatprv_52,0) <= 0 or coalesce(sales_subcat_store_halfmatprv_60,0) <= 0 then null else
        (sales_subcat_store_halfmatprv_60/sales_store_format_halfmatprv_52) end sales_ms_subcat_store_halfmatprv_136*/

        ,case when coalesce(sales_ms_item_category_store_yearmatcur,0) <= 0 or coalesce(sales_ms_item_category_store_yearmatprv,0) <= 0 then null else
        (sales_ms_item_category_store_yearmatcur/sales_ms_item_category_store_yearmatprv)-1 end sales_change_ms_item_category_store_yearmatvslasty
        ,case when coalesce(sales_ms_item_category_store_quartermatcur,0) <= 0 or coalesce(sales_ms_item_category_store_quartermatprv,0) <= 0 then null else
        (sales_ms_item_category_store_quartermatcur/sales_ms_item_category_store_quartermatprv)-1 end sales_change_ms_item_category_store_quartermatvslasty
        --,case when coalesce(sales_ms_item_category_store_halfmatcur_125,0) <= 0 or coalesce(sales_ms_item_category_store_halfmatprv_126,0) <= 0 then null else
        --(sales_ms_item_category_store_halfmatcur_125/sales_ms_item_category_store_halfmatprv_126)-1 end sales_change_ms_item_category_store_halfmatcur_138

        ,case when coalesce(relative_qty_category_store_yearmatcur,0) <= 0 or coalesce(relative_qty_category_store_yearmatprv,0) <= 0 then null else
        (relative_qty_category_store_yearmatcur/relative_qty_category_store_yearmatprv)-1 end relative_qty_change_item_category_store_yearmatvslasty
        ,case when coalesce(relative_qty_category_store_quartermatcur,0) <= 0 or coalesce(relative_qty_category_store_quartermatprv,0) <= 0 then null else
        (relative_qty_category_store_quartermatcur/relative_qty_category_store_quartermatprv)-1 end relative_qty_change_item_category_store_quartermatvslasty
        --,case when coalesce(relative_qty_category_store_halfmatcur_164,0) <= 0 or coalesce(relative_qty_category_store_halfmatprv_165,0) <= 0 then null else
        --(relative_qty_category_store_halfmatcur_164/relative_qty_category_store_halfmatprv_165)-1 end relative_qty_change_item_category_store_halfmatvslasty_171

        ,case when coalesce(relative_qty_subcat_store_yearmatcur ,0) <= 0 or coalesce(relative_qty_subcat_store_yearmatprv,0) <= 0 then null else
        (relative_qty_subcat_store_yearmatcur /relative_qty_subcat_store_yearmatprv)-1 end relative_qty_change_item_subcat_store_yearmatvslasty
        ,case when coalesce(relative_qty_subcat_store_quartermatcur ,0) <= 0 or coalesce(relative_qty_subcat_store_quartermatprv,0) <= 0 then null else
        (relative_qty_subcat_store_quartermatcur /relative_qty_subcat_store_quartermatprv)-1 end relative_qty_change_item_subcat_store_quartermatvslasty
        --,case when coalesce(relative_qty_subcat_store_halfmatcur_168 ,0) <= 0 or coalesce(relative_qty_subcat_store_halfmatprv_169,0) <= 0 then null else
        --(relative_qty_subcat_store_halfmatcur_168 /relative_qty_subcat_store_halfmatprv_169)-1 end relative_qty_change_item_subcat_store_halfmatvslasty_173

        ,case when sales_item_store_full_availability_yearmatcur-sales_item_store_cur_1<0 then null else sales_item_store_full_availability_yearmatcur-sales_item_store_cur_1 end full_availability_to_cur_sales_difference_item_store_yearmatcur
        ,case when sales_item_store_full_availability_quartermatcur-sales_item_store_quartermatcur_45<0 then null else sales_item_store_full_availability_quartermatcur-sales_item_store_quartermatcur_45 end full_availability_to_cur_sales_difference_item_store_cq

        ,case when coalesce(sales_category_store_full_availability_yearmatcur,0) <= 0 or coalesce(sales_item_store_full_availability_quartermatcur,0) <= 0 then null else
        (sales_item_store_full_availability_yearmatcur/sales_category_store_full_availability_yearmatcur) end sales_ms_full_availability_item_category_store_yearmatcur

        ,case when coalesce(sales_category_store_full_availability_quartermatcur,0) <= 0 or coalesce(sales_item_store_full_availability_yearmatcur,0) <= 0 then null else
        (sales_item_store_full_availability_quartermatcur/sales_category_store_full_availability_quartermatcur) end sales_ms_full_availability_item_category_store_quartermatcur

        ,cast(num_stores as double precision)/num_stores_sales_yearmatcur_281 item_distribution_yearmatcur
        ,cast(sum(case when sales_item_store_quartermatcur_45>0 and item_final_availability_in_store_quartermatcur_156>0 then 1 else 0 end) over(partition by format_code,dw_item_key) as double precision)/num_stores_sales_quartermatcur_282 item_distribution_quartermatcur

        ,case when ((sales_ms_category_format_market_yearmatcur/sales_ms_category_store_cur)-1>=0.05) and ((sales_ms_subcat_format_market_yearmatcur_121/sales_ms_item_subcat_store_yearmatcur)-1>=0.05) then 1 else 0 end is_pass_potential_ms_test

        ,case when coalesce((1+sales_change_format_ss_yearmatvsprv),0) <= 0 or coalesce((1+sales_change_subcat_ss_yearmatvsprv),0) <= 0 then null else
        ((1+sales_change_subcat_ss_yearmatvsprv)/(1+sales_change_format_ss_yearmatvsprv))-1 end sales_changegap_subcat_from_format_ss_yearmatvsprv

        ,((1+sales_change_item_market_yearmatvsprv_106)/(1+sales_change_item_store_ss))-1 gap_growth_market_store_ss_yearmatcur

    FROM (
        select dw_item_key
        ,dw_store_key,is_same_store
        ,sub_category_key, retailer_sub_category_key
        ,category_key, retailer_category_key
        ,sub_brand_hierarchy_2_key, retailer_sub_brand_hierarchy_2_key
        ,class_key, retailer_class_key
        ,supplier_key , retailer_supplier_key
        ,format_code
        ,is_special_old_item as  is_special_item
		,general_status_key, general_status_name,is_united_dw_item_key,is_private_label{REGIONAL_ITEMS_PLACEHOLDER}
        ,num_stores_sales_yearmatcur_281
        , num_stores_sales_quartermatcur_282
        ,sales_ms_category_format_market_yearmatcur
        ,item_format_examination_code_44 item_format_examination_code
        ,item_classification_42  item_classification
        ,is_item_classification_equal_for_all_format_43 is_item_classification_equal_for_all_format
        ,update_data
        ,store_group_classification_yearmatcur_252 store_group_classification
		,item_with_enough_stores_for_abc
		
        ,count(dw_item_key) over(partition by format_code,dw_item_key) num_stores

        ,sales_item_store_cur_1,sales_item_store_prv_2
        ,sales_item_store_quartermatcur_45 ,sales_item_store_quartermatprv_46
        --,sales_item_store_halfmatcur_47 ,sales_item_store_halfmatprv_48
        ,sales_ms_subcat_format_market_yearmatcur_121

        ,sales_category_store_cur_5 ,sales_category_store_prv_6
        ,sales_category_store_quartermatcur_53 ,sales_category_store_quartermatprv_54
        --,sales_category_store_halfmatcur_55 ,sales_category_store_halfmatprv_56

        ,sales_subcat_store_cur_7 ,sales_subcat_store_prv_8
        ,sales_subcat_store_quartermatcur_57 ,sales_subcat_store_quartermatprv_58
        --,sales_subcat_store_halfmatprv_60 ,sales_subcat_store_halfmatcur_59
        ,sales_changegap_subcat_from_format_market_yearmatvsprv
        ,sales_item_store_ss_cur
		
		,sales_store_format_cur_3

		
        ,case when coalesce(sales_subcat_store_quartermatprv_58,0) <= 0 or coalesce(sales_item_store_quartermatprv_46,0) <= 0 then null else
        (sales_item_store_quartermatprv_46/sales_subcat_store_quartermatprv_58) end sales_ms_item_subcat_store_quartermatprv
        --,case when coalesce(sales_subcat_store_halfmatcur_59,0) <= 0 or coalesce(sales_item_store_halfmatcur_47,0) <= 0 then null else
        --(sales_item_store_halfmatcur_47/sales_subcat_store_halfmatcur_59) end sales_ms_item_subcat_store_halfmatcur_129
        --,case when coalesce(sales_subcat_store_halfmatprv_60,0) <= 0 or coalesce(sales_item_store_halfmatprv_48,0) <= 0 then null else
        --(sales_item_store_halfmatprv_48/sales_subcat_store_halfmatprv_60) end sales_ms_item_subcat_store_halfmatprv_130
        ,case when coalesce(sales_subcat_store_prv_8,0) <= 0 or coalesce(sales_item_store_prv_2,0) <= 0 then null else
        (sales_item_store_prv_2/sales_subcat_store_prv_8) end sales_ms_item_subcat_store_yearmatprv

        ,case when coalesce(sales_subcat_store_cur_7,0) <= 0 or coalesce(sales_item_store_cur_1,0) <= 0 then null else
        (sales_item_store_cur_1/sales_subcat_store_cur_7) end sales_ms_item_subcat_store_yearmatcur
        ,case when coalesce(sales_subcat_store_quartermatcur_57,0) <= 0 or coalesce(sales_item_store_quartermatcur_45,0) <= 0 then null else
        (sales_item_store_quartermatcur_45/sales_subcat_store_quartermatcur_57) end sales_ms_item_subcat_store_quartermatcur

        --,sales_store_format_cur_3 ,sales_store_format_prv_4
        --,sales_store_format_quartermatcur_49 ,sales_store_format_quartermatprv_50
        --,sales_store_format_halfmatcur_51 ,sales_store_format_halfmatprv_52

        ,case when coalesce(sales_category_store_cur_5,0) <= 0 or coalesce(sales_item_store_cur_1,0) <= 0 then null else
        (sales_item_store_cur_1/sales_category_store_cur_5) end sales_ms_item_category_store_yearmatcur
        ,case when coalesce(sales_category_store_prv_6,0) <= 0 or coalesce(sales_item_store_prv_2,0) <= 0 then null else
        (sales_item_store_prv_2/sales_category_store_prv_6) end sales_ms_item_category_store_yearmatprv
        ,case when coalesce(sales_category_store_quartermatcur_53,0) <= 0 or coalesce(sales_item_store_quartermatcur_45,0) <= 0 then null else
        (sales_item_store_quartermatcur_45/sales_category_store_quartermatcur_53) end sales_ms_item_category_store_quartermatcur
        ,case when coalesce(sales_category_store_quartermatprv_54,0) <= 0 or coalesce(sales_item_store_quartermatprv_46,0) <= 0 then null else
        (sales_item_store_quartermatprv_46/sales_category_store_quartermatprv_54) end sales_ms_item_category_store_quartermatprv
        --,case when coalesce(sales_category_store_halfmatcur_55,0) <= 0 or coalesce(sales_item_store_halfmatcur_47,0) <= 0 then null else
        --(sales_item_store_halfmatcur_47/sales_category_store_halfmatcur_55) end sales_ms_item_category_store_halfmatcur_125
        --,case when coalesce(sales_category_store_halfmatprv_56,0) <= 0 or coalesce(sales_item_store_halfmatprv_48,0) <= 0 then null else
        --(sales_item_store_halfmatprv_48/sales_category_store_halfmatprv_56) end sales_ms_item_category_store_halfmatprv_126

        ,case when coalesce(sales_item_store_prv_2,0) <= 0 or coalesce(sales_item_store_cur_1,0) <= 0 then null else (sales_item_store_cur_1/sales_item_store_prv_2)-1 end sales_change_item_store_yearmatvslasty
        ,case when coalesce(sales_item_store_quartermatprv_46,0) <= 0 or coalesce(sales_item_store_quartermatcur_45,0) <= 0 then null else (sales_item_store_quartermatcur_45/sales_item_store_quartermatprv_46)-1 end sales_change_item_store_quartermatvslasty
        --,case when coalesce(sales_item_store_halfmatprv_48,0) <= 0 or coalesce(sales_item_store_halfmatcur_47,0) <= 0 then null else (sales_item_store_halfmatcur_47/sales_item_store_halfmatprv_48)-1 end sales_change_item_store_halfmatvslasty_75

        --,sales_item_format_cur_176, sales_item_format_quartermatcur_178
        --,sales_subcat_format_cur_11, sales_subcat_format_quartermatcur_65

        /*,sales_item_market_yearmatcur_88,sales_item_market_yearmatprv_89
        ,sales_item_market_quartermatcur_90 ,sales_item_market_quartermatprv_91
        ,sales_item_market_halfmatcur_92 ,sales_item_market_halfmatprv_93

        ,sales_subcat_market_yearmatcur_94,sales_subcat_market_yearmatprv_95
        ,sales_subcat_market_quartermatcur_96 ,sales_subcat_market_quartermatprv_97
        ,sales_subcat_market_halfmatcur_98,sales_subcat_market_halfmatprv_99

        ,sales_format_market_yearmatcur_100 ,sales_format_market_yearmatprv_101
        ,sales_format_market_quartermatcur_102 ,sales_format_market_quartermatprv_103
        ,sales_format_market_halfmatcur_104,sales_format_market_halfmatprv_105
        */
        ,sales_change_item_market_yearmatvsprv_106
        ,sales_change_item_market_quartermatvslasty_107
        ,sales_change_item_market_halfmatvslasty_108

        ,qty_item_store_cur_13  ,qty_item_store_prv_14
        ,qty_item_store_quartermatcur_72 ,qty_item_store_quartermatprv_73
        --,qty_item_store_halfmatcur_74 ,qty_item_store_halfmatprv_75

        ,qty_sales_category_store_cur_17 ,qty_sales_category_store_prv_18
        ,qty_sales_category_store_quartermatcur_80,qty_sales_category_store_quartermatprv_81
        --,qty_sales_category_store_halfmatcur_82 ,qty_sales_category_store_halfmatprv_83

        ,qty_sales_subcat_store_cur_19, qty_sales_subcat_store_prv_20
        ,qty_sales_subcat_store_quartermatcur_84, qty_sales_subcat_store_quartermatprv_85
        --,qty_sales_subcat_store_halfmatcur_86, qty_sales_subcat_store_halfmatprv_87

        ,case when coalesce(qty_item_store_cur_13,0) <= 0 or coalesce(qty_item_store_prv_14,0) <= 0 then null else (qty_item_store_cur_13/qty_item_store_prv_14)-1 end qty_change_item_store_yearmatvslasty
        ,case when coalesce(qty_item_store_quartermatcur_72,0) <= 0 or coalesce(qty_item_store_quartermatprv_73,0) <= 0 then null else (qty_item_store_quartermatcur_72/qty_item_store_quartermatprv_73)-1 end qty_change_item_store_quartermatvslasty
        --,case when coalesce(qty_item_store_halfmatcur_74,0) <= 0 or coalesce(qty_item_store_halfmatprv_75,0) <= 0 then null else (qty_item_store_halfmatcur_74/qty_item_store_halfmatprv_75)-1 end qty_change_item_store_halfmatvslasty_110

        /*,dense_rank() over (partition by category_key,dw_store_key,format_code order by sales_item_store_cur_1 desc) rank_item_in_category_store
        ,dense_rank() over (partition by category_key,dw_store_key,format_code order by sales_item_store_quartermatcur_45 desc) rank_item_in_category_store_quartermatcur
        ,dense_rank() over (partition by category_key,dw_store_key,format_code order by sales_item_store_halfmatcur_47 desc) rank_item_in_category_store_halfmatcur

        ,dense_rank() over (partition by sub_category_key,category_key,dw_store_key,format_code order by sales_item_store_cur_1 desc) rank_item_in_subcat_store
        ,dense_rank() over (partition by sub_category_key,category_key,dw_store_key,format_code order by sales_item_store_quartermatcur_45 desc) rank_item_in_subcat_store_quartermatcur
        ,dense_rank() over (partition by sub_category_key,category_key,dw_store_key,format_code order by sales_item_store_halfmatcur_47 desc) rank_item_in_subcat_store_halfmatcur

        ,dense_rank() over (partition by sub_category_key,sub_brand_hierarchy_2_key,dw_store_key,format_code order by sales_item_store_cur_1 desc) rank_item_in_brand_store
        ,dense_rank() over (partition by sub_category_key,sub_brand_hierarchy_2_key,dw_store_key,format_code order by sales_item_store_quartermatcur_45 desc) rank_item_in_brand_store_quartermatcur
        ,dense_rank() over (partition by sub_category_key,sub_brand_hierarchy_2_key,dw_store_key,format_code order by sales_item_store_halfmatcur_47 desc) rank_item_in_brand_store_halfmatcur*/

        --,items_count_categoryinformat_yearmatcur_212  max_rank_item_in_category_18
        --,items_count_categoryinformat_quartermatcur_213  max_rank_item_in_category_quartermatcur_112
        --,items_count_categoryinformat_halfmatcur_214  max_rank_item_in_category_halfmatcur_114
        --,items_count_subcatinformat_yearmatcur_215  max_rank_item_in_subcat_20
        --,items_count_subcatinformat_quartermatcur_216  max_rank_item_in_subcat_quartermatcur_116
        --,items_count_subcatinformat_halfmatcur_217  max_rank_item_in_subcat_halfmatcur_118
        --,items_count_brandinsubcat_yearmatcur_218  max_rank_item_in_brand_22
        --,items_count_brandinsubcat_quartermatcur_219  max_rank_item_in_brand_quartermatcur_120
        --,items_count_brandinsubcat_halfmatcur_220  max_rank_item_in_brand_halfmatcur_122




        --,sales_ms_item_subcat_market_yearmatcur_109
        --,sales_ms_item_subcat_market_yearmatprv_110
        --,sales_ms_item_subcat_market_quartermatcur_111
        --,sales_ms_item_subcat_market_quartermatprv_112
        --,sales_ms_item_subcat_market_halfmatcur_113
        --,sales_ms_item_subcat_market_halfmatprv_114

        /*,sales_ms_item_format_market_yearmatcur_115
        ,sales_ms_item_format_market_yearmatprv_116
        ,sales_ms_item_format_market_quartermatcur_117
        ,sales_ms_item_format_market_quartermatprv_118
        ,sales_ms_item_format_market_halfmatcur_119
        ,sales_ms_item_format_market_halfmatprv_120

        ,sales_ms_subcat_format_market_yearmatcur_121
        ,sales_ms_subcat_format_market_yearmatprv_122
        ,sales_ms_subcat_format_market_quartermatcur_123
        ,sales_ms_subcat_format_market_quartermatprv_124
        ,sales_ms_subcat_format_market_halfmatcur_125
        ,sales_ms_subcat_format_market_halfmatcur_126

        ,sales_change_ms_item_format_market_yearmatvsprv_127
        ,sales_change_ms_item_format_market_quartermatvslasty_128
        ,sales_change_ms_item_format_market_halfmatvslasty_129*/

        ,case when coalesce(qty_item_store_cur_13,0) <= 0 or coalesce(qty_sales_category_store_cur_17,0) <= 0 then null else
        (cast(qty_item_store_cur_13 as double precision)/qty_sales_category_store_cur_17) end relative_qty_category_store_yearmatcur
        ,case when coalesce(qty_item_store_prv_14,0) <= 0 or coalesce(qty_sales_category_store_prv_18 ,0) <= 0 then null else
        (cast(qty_item_store_prv_14 as double precision)/qty_sales_category_store_prv_18 ) end relative_qty_category_store_yearmatprv
        ,case when coalesce(qty_item_store_quartermatcur_72,0) <= 0 or coalesce(qty_sales_category_store_quartermatcur_80,0) <= 0 then null else
        (cast(qty_item_store_quartermatcur_72 as double precision)/qty_sales_category_store_quartermatcur_80) end relative_qty_category_store_quartermatcur
        ,case when coalesce(qty_item_store_quartermatprv_73,0) <= 0 or coalesce(qty_sales_category_store_quartermatprv_81 ,0) <= 0 then null else
        (cast(qty_item_store_quartermatprv_73 as double precision)/qty_sales_category_store_quartermatprv_81 ) end relative_qty_category_store_quartermatprv
        --,case when coalesce(qty_item_store_halfmatcur_74,0) <= 0 or coalesce(qty_sales_category_store_halfmatcur_82,0) <= 0 then null else
        --(cast(qty_item_store_halfmatcur_74 as double precision)/qty_sales_category_store_halfmatcur_82) end relative_qty_category_store_halfmatcur_164
        --,case when coalesce(qty_item_store_halfmatprv_75,0) <= 0 or coalesce(qty_sales_category_store_halfmatprv_83 ,0) <= 0 then null else
        --(cast(qty_item_store_halfmatprv_75 as double precision)/qty_sales_category_store_halfmatprv_83 ) end relative_qty_category_store_halfmatprv_165

        ,case when coalesce(qty_item_store_cur_13,0) <= 0 or coalesce(qty_sales_subcat_store_cur_19,0) <= 0 then null else
        (cast(qty_item_store_cur_13 as double precision)/qty_sales_subcat_store_cur_19) end relative_qty_subcat_store_yearmatcur
        ,case when coalesce(qty_item_store_prv_14,0) <= 0 or coalesce(qty_sales_subcat_store_prv_20 ,0) <= 0 then null else
        (cast(qty_item_store_prv_14 as double precision)/qty_sales_subcat_store_prv_20 ) end relative_qty_subcat_store_yearmatprv
        ,case when coalesce(qty_item_store_quartermatcur_72,0) <= 0 or coalesce(qty_sales_subcat_store_quartermatcur_84,0) <= 0 then null else
        (cast(qty_item_store_quartermatcur_72 as double precision)/qty_sales_subcat_store_quartermatcur_84) end relative_qty_subcat_store_quartermatcur
        ,case when coalesce(qty_item_store_quartermatprv_73,0) <= 0 or coalesce(qty_sales_subcat_store_quartermatprv_85 ,0) <= 0 then null else
        (cast(qty_item_store_quartermatprv_73 as double precision)/qty_sales_subcat_store_quartermatprv_85 ) end relative_qty_subcat_store_quartermatprv
        --,case when coalesce(qty_item_store_halfmatcur_74,0) <= 0 or coalesce(qty_sales_subcat_store_halfmatcur_86,0) <= 0 then null else
        --(cast(qty_item_store_halfmatcur_74 as double precision)/qty_sales_subcat_store_halfmatcur_86) end relative_qty_subcat_store_halfmatcur_168
        --,case when coalesce(qty_item_store_halfmatprv_75,0) <= 0 or coalesce(qty_sales_subcat_store_halfmatprv_87 ,0) <= 0 then null else
        --(cast(qty_item_store_halfmatprv_75 as double precision)/qty_sales_subcat_store_halfmatprv_87 ) end relative_qty_subcat_store_halfmatprv_169

        ,ms_potential_sales_item_in_category_22
        ,ms_potential_sales_item_in_category_quartermatcur_135
        --,ms_potential_sales_item_in_category_halfmatcur_146

        ,ms_potential_sales_item_in_subcat_23
        ,ms_potential_sales_item_in_subcat_quartermatcur_136
        --,ms_potential_sales_item_in_subcat_halfmatcur_147

        --,ms_potential_sales_item_in_brand_24
        --,ms_potential_sales_item_in_brand_quartermatcur_137
        --,ms_potential_sales_item_in_brand_halfmatcur_148


        --,rank_potential_item_in_subcat_25||' / '|| max_rank_potential_item_in_subcat_26 as ranking_potential_item_in_subcat_yearmatcur
        --,rank_potential_item_in_subcat_quartermatcur_139 ||' / '|| max_rank_potential_item_in_subcat_quartermatcur_140 as ranking_potential_item_in_subcat_quartermatcur
        --,rank_potential_item_in_subcat_halfmatcur_150
        --,max_rank_potential_item_in_subcat_halfmatcur_151

        ,item_final_availability_in_store_29
        ,item_final_availability_in_store_quartermatcur_156
        --,item_final_availability_in_store_halfmatcur_157

        ,avg (item_final_availability_in_store_29) over(partition by store_group_classification_yearmatcur_252 , format_code ) avg_item_availability_by_store_group_yearmatcur
        ,avg (item_final_availability_in_store_quartermatcur_156) over(partition by store_group_classification_yearmatcur_252 , format_code ) avg_item_availability_by_store_group_quartermatcur

        --,dense_rank() over (partition by format_code order by item_final_availability_in_store_29 desc) - max (case when item_final_availability_in_store_29 is null then 1 else 0 end) over (partition by format_code)  rank_item_availability_in_store_yearmatcur
        --,rank_item_availability_in_store_yearmatcur ||' / '|| num_stores as ranking_store_item_availability_yearmatcur
        --,dense_rank() over (partition by format_code order by item_final_availability_in_store_quartermatcur_156 desc) - max (case when item_final_availability_in_store_quartermatcur_156 is null then 1 else 0 end) over (partition by format_code) rank_item_availability_in_store_quartermatcur
        --,rank_item_availability_in_store_quartermatcur ||' / '|| num_stores as ranking_store_item_availability_quartermatcur
        ,ranking_store_item_availability_yearmatcur
        ,ranking_store_item_availability_quartermatcur

        ,potential_sales_item_store_yearmatcur_252 sales_item_store_full_availability_yearmatcur
        ,potential_sales_item_store_quartermatcur_270 sales_item_store_full_availability_quartermatcur

        ,potential_sales_subcat_store_yearmatcur_255 sales_subcat_store_full_availability_yearmatcur
        ,potential_sales_subcat_store_quartermatcur_273 sales_subcat_store_full_availability_quartermatcur

        --,case when coalesce(sales_subcat_store_full_availability_yearmatcur,0) <= 0 or coalesce(sales_item_store_full_availability_quartermatcur,0) <= 0 then null else
        --(sales_item_store_full_availability_yearmatcur/sales_subcat_store_full_availability_yearmatcur) end sales_ms_full_availability_item_subcat_store_yearmatcur
        --,case when coalesce(sales_subcat_store_full_availability_quartermatcur,0) <= 0 or coalesce(sales_item_store_full_availability_yearmatcur,0) <= 0 then null else
        --(sales_item_store_full_availability_quartermatcur/sales_subcat_store_full_availability_quartermatcur) end sales_ms_full_availability_item_subcat_store_quartermatcur
        ,sales_ms_full_availability_item_subcat_store_yearmatcur
        ,sales_ms_full_availability_item_subcat_store_quartermatcur

        --,dense_rank() over (partition by format_code order by sales_ms_full_availability_item_subcat_store_yearmatcur desc) - max (case when sales_ms_full_availability_item_subcat_store_yearmatcur is null then 1 else 0 end) over (partition by format_code)  rank_sales_ms_full_availability_item_subcat_store_yearmatcur
        --,rank_sales_ms_full_availability_item_subcat_store_yearmatcur ||' / '|| num_stores as ranking_store_sales_ms_full_availability_item_subcat_yearmatcur
        --,dense_rank() over (partition by format_code order by sales_ms_full_availability_item_subcat_store_quartermatcur desc) - max (case when sales_ms_full_availability_item_subcat_store_quartermatcur is null then 1 else 0 end) over (partition by format_code) rank_sales_ms_full_availability_item_subcat_store_quartermatcur
        --,rank_sales_ms_full_availability_item_subcat_store_quartermatcur ||' / '|| num_stores as ranking_store_sales_ms_full_availability_item_subcat_quartermatcur
        ,ranking_store_sales_ms_full_availability_item_subcat_yearmatcur
        ,ranking_store_sales_ms_full_availability_item_subcat_quartermatcur as ranking_store_sales_ms_full_availability_item_subcat_cq

        ,potential_sales_category_store_yearmatcur_254 sales_category_store_full_availability_yearmatcur
        ,potential_sales_category_store_quartermatcur_272 sales_category_store_full_availability_quartermatcur
        --, store_group_classification_yearmatcur_252
        , sales_ms_item_subcat_market_yearmatcur_109
        /*,avg_ms_item_subcat_hh_cur_30
        ,avg_ms_item_subcat_hh_prv_31
        ,avg_ms_item_subcat_change_32
        ,avg_ms_item_subcat_hh_halfmatcur_158
        ,avg_ms_item_subcat_hh_halfmatprv_159
        ,avg_ms_item_subcat_change_halfmatvslasty_160

        ,penetration_item_subcat_cur_33
        ,penetration_item_subcat_prv_34
        ,penetration_item_subcat_change_35
        ,penetration_item_subcat_halfmatcur_161
        ,penetration_item_subcat_halfmatprv_162
        ,penetration_item_subcat_change_halfmatvslasty_163

        ,avg_sales_basket_size_cur_36
        ,avg_sales_basket_size_prv_37
        ,avg_sales_basket_size_change_38
        ,avg_sales_basket_size_halfmatcur_164
        ,avg_sales_basket_size_halfmatprv_165
        ,avg_sales_basket_size_change_halfmatvslasty_166

        ,purchase_frequency_cur_39
        ,purchase_frequency_prv_40
        ,purchase_frequency_change_41


        ,avg_ms_sales_item_subcat_market_high_sub_chain_130*/

        --,dense_rank() over (partition by format_code order by sales_ms_item_subcat_store_yearmatcur desc) - max (case when sales_ms_item_subcat_store_yearmatcur is null then 1 else 0 end) over (partition by format_code)  rank_store_sales_ms_item_subcat_yearmatcur
        --,rank_store_sales_ms_item_subcat_yearmatcur ||' / '|| num_stores as ranking_store_sales_ms_item_subcat_yearmatcur
        --,dense_rank() over (partition by format_code order by sales_ms_item_subcat_store_quartermatcur desc) - max (case when sales_ms_item_subcat_store_quartermatcur is null then 1 else 0 end) over (partition by format_code) rank_store_sales_ms_item_subcat_quartermatcur
        --,rank_store_sales_ms_item_subcat_quartermatcur ||' / '|| num_stores as ranking_store_sales_ms_item_subcat_quartermatcur
        ,ranking_store_sales_ms_item_subcat_yearmatcur
        ,ranking_store_sales_ms_item_subcat_quartermatcur

        --,dense_rank() over (partition by store_group_classification order by sales_ms_item_subcat_store_yearmatcur desc) - max (case when sales_ms_item_subcat_store_yearmatcur is null then 1 else 0 end) over (partition by store_group_classification)||' / '||sum(1) over(partition by store_group_classification) ranking_avgsalesms_stores_group_item_subcat_yearmatcur
        ,ranking_avgsalesms_stores_group_item_subcat_yearmatcur

        ,salesms_ratio_group_a_to_store_item_subcat_yearmatcur
        ,salesms_ratio_group_b_to_store_item_subcat_yearmatcur
        ,salesms_ratio_group_c_to_store_item_subcat_yearmatcur
        ,sales_inc_store_to_salesms_group_a_yearmatcur
        ,sales_inc_store_to_salesms_group_b_yearmatcur
        ,sales_inc_store_to_salesms_group_c_yearmatcur
        ,avgsalesms_stores_group_a_item_subcat_yearmatcur
        ,avgsalesms_stores_group_b_item_subcat_yearmatcur
        ,avgsalesms_stores_group_c_item_subcat_yearmatcur
        ,avgavailability_stores_group_a_item_format_yearmatcur
        ,avgavailability_stores_group_b_item_format_yearmatcur
        ,avgavailability_stores_group_c_item_format_yearmatcur
        ,avgsalesms_ratio_stores_group_a_to_c_item_subcat_yearmatcur
        ,avgavailability_ratio_stores_group_a_to_c_item_format_yearmatcur as avgavailability_ratio_stores_group_a_to_c_item_format_cy

        ,qty_price_item_store_yearmatcur
        ,qty_price_item_format_yearmatcur as qty_price_item_allformat_yearmatcur
        ,qty_price_change_item_store_to_format_yearmatcur
        ,qty_price_item_format_market_yearmatcur as qty_price_item_format_market_yearmatcur_item_rep
        ,avgqty_price_item_stores_group_a_item_format_yearmatcur
        ,avgqty_price_item_stores_group_b_item_format_yearmatcur
        ,avgqty_price_item_stores_group_c_item_format_yearmatcur

        ,qty_price_item_store_quartermatcur
        ,qty_price_item_format_quartermatcur as qty_price_item_allformat_quartermatcur
        ,qty_price_change_item_store_to_format_quartermatcur
        ,avgqty_price_item_stores_group_a_item_format_quartermatcur
        ,avgqty_price_item_stores_group_b_item_format_quartermatcur
        ,avgqty_price_item_stores_group_c_item_format_quartermatcur

         ,case when coalesce(qty_price_item_store_yearmatcur,0) <= 0 or coalesce(qty_price_item_format_market_yearmatcur,0) <= 0 then null else
         (qty_price_item_format_market_yearmatcur/qty_price_item_store_yearmatcur)-1 end qty_price_item_market_to_store_gap_yearmatcur

         ,case when coalesce(qty_price_item_store_quartermatcur,0) <= 0 or coalesce(qty_price_item_format_market_quartermatcur,0) <= 0 then null else
         (qty_price_item_format_market_quartermatcur/qty_price_item_store_quartermatcur)-1 end qty_price_item_market_to_store_gap_quartermatcur

         ----------------------

        ,case when coalesce(sales_category_store_cur_5,0) <= 0 or coalesce(sales_store_format_cur_3,0) <= 0 then null else
        (sales_category_store_cur_5/sales_store_format_cur_3) end sales_ms_category_store_cur

          ----------------------

        ,case when is_same_store<>1 or coalesce(sales_subcat_store_prv_8,0) <= 0 or coalesce(sales_subcat_store_cur_7,0) <= 0 then null else
        (sales_subcat_store_cur_7/sales_subcat_store_prv_8)-1 end sales_change_subcat_ss_yearmatvsprv

        ,case when is_same_store<>1 or coalesce(sales_store_format_prv_4,0) <= 0 or coalesce(sales_store_format_cur_3,0) <= 0 then null else
        (sales_store_format_cur_3/sales_store_format_prv_4)-1 end sales_change_format_ss_yearmatvsprv

        ,case when coalesce(sales_item_store_ss_prv,0) <= 0 or coalesce(sales_item_store_ss_cur,0) <= 0 then null else (sales_item_store_ss_cur/sales_item_store_ss_prv)-1 end sales_change_item_store_ss

        from migvan.old_items_measures
        where format_code= :format_code
          and dw_item_key = :dw_item_key
		  and (sales_item_store_cur_1>0  and item_final_availability_in_store_29>0)
    ) k
) t
where {FILTER_PLACEHOLDER}
 {DW_STORE_KEY_PLACEHOLDER}
