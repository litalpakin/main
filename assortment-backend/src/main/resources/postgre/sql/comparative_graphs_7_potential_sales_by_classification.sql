 with rp7_chain_potential_sales as
(
SELECT item_format_classification_61  item_classification
,sum(sales_item_format_cur_1) sales_cy_mat  
,sum(sales_item_format_quartermatcur_64) sales_cq_mat
,sum(sales_item_format_halfmatcur_162) sales_ch_mat 
,sum(total_potential_sales_item_in_format_39) potential_sales_cy_mat  
,sum(total_potential_sales_item_in_format_quartermatcur_167) potential_sales_cq_mat  
,sum(total_potential_sales_item_in_format_halfmatcur_166) potential_sales_ch_mat 
,avg(avg_ms_item_subcat_hh_cur_49) avg_sales_ms_item_subcat_cy_mat
,avg(avg_ms_item_subcat_hh_halfmatcur_168) avg_sales_ms_item_subcat_ch_mat

    FROM migvan.old_items_id
    where item_format_classification_61  = :classification_item
  and format_code = :formatParam
group by item_format_classification_61 
),

rp7_user_potential_sales as
(

select item_classification
,sum(user_sales_item_format_cy_mat) user_sales_cy_mat 
,sum(user_sales_item_format_cq_mat) user_sales_cq_mat
,sum(user_sales_item_format_ch_mat) user_sales_ch_mat 
,sum(user_potential_sales_item_cy_mat) user_potential_sales_cy_mat     
,sum(user_potential_sales_item_cq_mat) user_potential_sales_cq_mat
,sum(user_potential_sales_item_ch_mat) user_potential_sales_ch_mat 
,avg(user_sales_ms_item_subcat_cy_mat) user_avg_sales_ms_item_subcat_cy_mat
--,avg(user_sales_ms_item_subcat_cq_mat) user_avg_sales_ms_item_subcat_cq_mat
,avg(user_sales_ms_item_subcat_ch_mat) user_avg_sales_ms_item_subcat_ch_mat

from(
    SELECT dw_item_key, format_code ,item_classification_42 item_classification
        ,sum(sales_item_store_cur_1) user_sales_item_format_cy_mat 
        ,sum(sales_item_store_quartermatcur_45) user_sales_item_format_cq_mat
        ,sum(sales_item_store_halfmatcur_47) user_sales_item_format_ch_mat 
        ,max(total_potential_sales_item_in_format_21) user_potential_sales_item_cy_mat     
        ,max(total_potential_sales_item_in_format_quartermatcur_134) user_potential_sales_item_cq_mat
        ,max(total_potential_sales_item_in_format_halfmatcur_145) user_potential_sales_item_ch_mat 
        ,max(avg_ms_item_subcat_hh_cur_30) user_sales_ms_item_subcat_cy_mat 
        ,max(avg_ms_item_subcat_hh_halfmatcur_158) user_sales_ms_item_subcat_ch_mat 
    FROM migvan.old_items_measures 
  where item_classification_42  = :classification_item
  and {FILTER_PLACEHOLDER}
    group by dw_item_key, format_code ,item_classification_42
)  as t0

    group by item_classification 


)

select item_classification

----------------potential_sales------------

,sales_cy_mat ,potential_sales_cy_mat
,sales_difference_potential_and_cur_cy_mat
,sales_change_potential_to_cur_cy_mat
 ,user_sales_cy_mat ,user_potential_sales_cy_mat
,user_sales_difference_potential_and_cur_cy_mat
,user_sales_change_potential_to_cur_cy_mat
      
,case when user_sales_change_potential_to_cur_cy_mat is null then 0-sales_change_potential_to_cur_cy_mat 
      when sales_change_potential_to_cur_cy_mat is null then user_sales_change_potential_to_cur_cy_mat-0 else user_sales_change_potential_to_cur_cy_mat-sales_change_potential_to_cur_cy_mat end sales_change_potential_difference_user_to_chain_cy_mat
,case when user_sales_change_potential_to_cur_cy_mat<=0 then null 
      when sales_change_potential_to_cur_cy_mat<=0 then null else ((1+user_sales_change_potential_to_cur_cy_mat)/(1+sales_change_potential_to_cur_cy_mat))-1  end sales_change_potential_user_to_chain_cy_mat
,user_sales_difference_potential_and_cur_cy_mat/sales_cy_mat user_potential_growth_to_chain_income_cy_mat
,user_sales_difference_potential_and_cur_cy_mat/sales_difference_potential_and_cur_cy_mat user_potential_growth_to_chain_potential_income_cy_mat
      
,sales_cq_mat ,potential_sales_cq_mat
,sales_difference_potential_and_cur_cq_mat
,sales_change_potential_to_cur_cq_mat

,user_sales_cq_mat ,user_potential_sales_cq_mat
,user_sales_difference_potential_and_cur_cq_mat
,user_sales_change_potential_to_cur_cq_mat
      
,case when user_sales_change_potential_to_cur_cq_mat is null then 0-sales_change_potential_to_cur_cq_mat 
      when sales_change_potential_to_cur_cq_mat is null then user_sales_change_potential_to_cur_cq_mat-0 else user_sales_change_potential_to_cur_cq_mat-sales_change_potential_to_cur_cq_mat end sales_change_potential_difference_user_to_chain_cq_mat
,case when user_sales_change_potential_to_cur_cq_mat<=0 then null 
      when sales_change_potential_to_cur_cq_mat<=0 then null else ((1+user_sales_change_potential_to_cur_cq_mat)/(1+sales_change_potential_to_cur_cq_mat))-1  end sales_change_potential_user_to_chain_cq_mat
,user_sales_difference_potential_and_cur_cq_mat/sales_cq_mat user_potential_growth_to_chain_income_cq_mat
,user_sales_difference_potential_and_cur_cq_mat/sales_difference_potential_and_cur_cq_mat user_potential_growth_to_chain_potential_income_cq_mat

,sales_ch_mat ,potential_sales_ch_mat
,sales_difference_potential_and_cur_ch_mat
,sales_change_potential_to_cur_ch_mat

,user_sales_ch_mat ,user_potential_sales_ch_mat
,user_sales_difference_potential_and_cur_ch_mat
,user_sales_change_potential_to_cur_ch_mat

,case when user_sales_change_potential_to_cur_ch_mat is null then 0-sales_change_potential_to_cur_ch_mat 
      when sales_change_potential_to_cur_ch_mat is null then user_sales_change_potential_to_cur_ch_mat-0 else user_sales_change_potential_to_cur_ch_mat-sales_change_potential_to_cur_ch_mat end sales_change_potential_difference_user_to_chain_ch_mat
,case when user_sales_change_potential_to_cur_ch_mat<=0 then null 
      when sales_change_potential_to_cur_ch_mat<=0 then null else ((1+user_sales_change_potential_to_cur_ch_mat)/(1+sales_change_potential_to_cur_ch_mat))-1  end sales_change_potential_user_to_chain_ch_mat
,user_sales_difference_potential_and_cur_ch_mat/sales_ch_mat user_potential_growth_to_chain_income_ch_mat
,user_sales_difference_potential_and_cur_ch_mat/sales_difference_potential_and_cur_ch_mat user_potential_growth_to_chain_potential_income_ch_mat

----------------avg_ms_sales------------

,avg_sales_ms_item_subcat_cy_mat ,user_avg_sales_ms_item_subcat_cy_mat
,sales_change_ms_item_user_to_chain_cy_mat


,avg_sales_ms_item_subcat_ch_mat ,user_avg_sales_ms_item_subcat_ch_mat
,sales_change_ms_item_user_to_chain_ch_mat
FROM (

select cp.item_classification

----------------potential_sales------------

,sales_cy_mat ,potential_sales_cy_mat
,case when potential_sales_cy_mat is null then 0-sales_cy_mat 
      when sales_cy_mat is null then potential_sales_cy_mat-0 else potential_sales_cy_mat-sales_cy_mat end sales_difference_potential_and_cur_cy_mat
,case when potential_sales_cy_mat<=0 then null 
      when sales_cy_mat<=0 then null else (potential_sales_cy_mat/sales_cy_mat)-1 end sales_change_potential_to_cur_cy_mat
      
,user_sales_cy_mat ,user_potential_sales_cy_mat
,case when user_potential_sales_cy_mat is null then 0-user_sales_cy_mat 
      when user_sales_cy_mat is null then user_potential_sales_cy_mat-0 else user_potential_sales_cy_mat-user_sales_cy_mat end user_sales_difference_potential_and_cur_cy_mat
,case when user_potential_sales_cy_mat<=0 then null 
      when user_sales_cy_mat<=0 then null else (user_potential_sales_cy_mat/user_sales_cy_mat)-1 end user_sales_change_potential_to_cur_cy_mat

,sales_cq_mat ,potential_sales_cq_mat
,case when potential_sales_cq_mat is null then 0-sales_cq_mat 
      when sales_cq_mat is null then potential_sales_cq_mat-0 else potential_sales_cq_mat-sales_cq_mat end sales_difference_potential_and_cur_cq_mat
,case when potential_sales_cq_mat<=0 then null 
      when sales_cq_mat<=0 then null else (potential_sales_cq_mat/sales_cq_mat)-1 end sales_change_potential_to_cur_cq_mat

,user_sales_cq_mat ,user_potential_sales_cq_mat
,case when user_potential_sales_cq_mat is null then 0-user_sales_cq_mat 
      when user_sales_cq_mat is null then user_potential_sales_cq_mat-0 else user_potential_sales_cq_mat-user_sales_cq_mat end user_sales_difference_potential_and_cur_cq_mat
,case when user_potential_sales_cq_mat<=0 then null 
      when user_sales_cq_mat<=0 then null else (user_potential_sales_cq_mat/user_sales_cq_mat)-1 end user_sales_change_potential_to_cur_cq_mat

,sales_ch_mat ,potential_sales_ch_mat
,case when potential_sales_ch_mat is null then 0-sales_ch_mat 
      when sales_ch_mat is null then potential_sales_ch_mat-0 else potential_sales_ch_mat-sales_ch_mat end sales_difference_potential_and_cur_ch_mat
,case when potential_sales_ch_mat<=0 then null 
      when sales_ch_mat<=0 then null else (potential_sales_ch_mat/sales_ch_mat)-1 end sales_change_potential_to_cur_ch_mat
,user_sales_ch_mat ,user_potential_sales_ch_mat
,case when user_potential_sales_ch_mat is null then 0-user_sales_ch_mat 
      when user_sales_ch_mat is null then user_potential_sales_ch_mat-0 else user_potential_sales_ch_mat-user_sales_ch_mat end user_sales_difference_potential_and_cur_ch_mat
,case when user_potential_sales_ch_mat<=0 then null 
      when user_sales_ch_mat<=0 then null else (user_potential_sales_ch_mat/user_sales_ch_mat)-1 end user_sales_change_potential_to_cur_ch_mat

,avg_sales_ms_item_subcat_cy_mat ,user_avg_sales_ms_item_subcat_cy_mat
,case when user_avg_sales_ms_item_subcat_cy_mat<=0 then null 
      when avg_sales_ms_item_subcat_cy_mat<=0 then null else (user_avg_sales_ms_item_subcat_cy_mat/avg_sales_ms_item_subcat_cy_mat)  end sales_change_ms_item_user_to_chain_cy_mat
,avg_sales_ms_item_subcat_ch_mat ,user_avg_sales_ms_item_subcat_ch_mat
,case when user_avg_sales_ms_item_subcat_ch_mat<=0 then null 
      when avg_sales_ms_item_subcat_ch_mat<=0 then null else (user_avg_sales_ms_item_subcat_ch_mat/avg_sales_ms_item_subcat_ch_mat)  end sales_change_ms_item_user_to_chain_ch_mat

    
from rp7_chain_potential_sales cp join rp7_user_potential_sales up on cp.item_classification=up.item_classification
    ) t