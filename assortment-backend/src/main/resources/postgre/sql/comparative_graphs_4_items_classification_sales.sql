with rp4_chain_sales_items_by_classification as
(
select  item_format_type_classification_code ,item_format_type_classification_name 
    ,sum(sales_item_cy) as sales_items_in_classification_cy
    ,sum(sum(sales_item_cy)) over() as total_sales_items_cy
    ,sum(sales_item_cy) /(sum(sum(sales_item_cy)) over()) as share_of_sales_classitems_from_total_cy
    ,sum(sales_item_ch) as sales_items_in_classification_ch
    ,sum(sum(sales_item_ch)) over() as total_sales_items_ch
    ,sum(sales_item_ch) /(sum(sum(sales_item_ch)) over()) as share_of_sales_classitems_from_total_ch
    ,sum(sales_item_cq) as sales_items_in_classification_cq
    ,sum(sum(sales_item_cq)) over() as total_sales_items_cq
    ,sum(sales_item_cq) /(sum(sum(sales_item_cq)) over()) as share_of_sales_classitems_from_total_cq
from(
    select f.dw_item_key, f.format_code 
    ,item_format_type_classification_code         
    ,item_format_type_classification_name 
    ,sum(sales_item_store_cur) sales_item_cy
    ,sum(sales_item_store_quartermatcur) sales_item_cq
    ,sum(sales_item_store_halfmatcur) sales_item_ch
    from migvan.assortment_item_store_indices f
    where item_format_top_classification_code in (1,2)
    and f.format_code = :formatParam
    group by f.dw_item_key, f.format_code,item_format_type_classification_code,item_format_type_classification_name 
) as t
group by item_format_type_classification_code ,item_format_type_classification_name 
),
rp4_user_sales_items_by_classification as
(
select  item_format_type_classification_code ,item_format_type_classification_name 
    ,sum(sales_item_cy) user_sales_items_in_classification_cy
    ,sum(sum(sales_item_cy)) over() as user_total_sales_items_cy
    ,sum(sales_item_cy) /(sum(sum(sales_item_cy)) over()) as user_share_of_sales_classitems_from_total_cy
    ,sum(sales_item_ch) as user_sales_items_in_classification_ch
    ,sum(sum(sales_item_ch)) over() as user_total_sales_items_ch
    ,sum(sales_item_ch) /(sum(sum(sales_item_ch)) over()) as user_share_of_sales_classitems_from_total_ch
    ,sum(sales_item_cq) as user_sales_items_in_classification_cq
    ,sum(sum(sales_item_cq)) over() as user_total_sales_items_cq
    ,sum(sales_item_cq)/(sum(sum(sales_item_cq)) over()) as user_share_of_sales_classitems_from_total_cq
from(
    select f.dw_item_key, f.format_code ,item_format_type_classification_code ,item_format_type_classification_name 
    ,sum(sales_item_store_cur) sales_item_cy
    ,sum(sales_item_store_quartermatcur) sales_item_cq
    ,sum(sales_item_store_halfmatcur) sales_item_ch
    from migvan.assortment_item_store_indices f
    where item_format_top_classification_code in (1,2)
    and {FILTER_PLACEHOLDER}
    group by f.dw_item_key, f.format_code,item_format_type_classification_code ,item_format_type_classification_name 
) as b
group by item_format_type_classification_code ,item_format_type_classification_name 
)

select cq.item_format_type_classification_code,cq.item_format_type_classification_name
,sales_items_in_classification_cy, total_sales_items_cy, share_of_sales_classitems_from_total_cy
,user_sales_items_in_classification_cy, user_total_sales_items_cy, user_share_of_sales_classitems_from_total_cy
,((1+user_share_of_sales_classitems_from_total_cy)/(1+share_of_sales_classitems_from_total_cy))-1  share_of_change_sales_items_user_to_chain_cy
,case when user_share_of_sales_classitems_from_total_cy is null then 0-share_of_sales_classitems_from_total_cy
      when share_of_sales_classitems_from_total_cy is null then user_share_of_sales_classitems_from_total_cy-0 else user_share_of_sales_classitems_from_total_cy-share_of_sales_classitems_from_total_cy end share_of_difference_sales_items_user_to_chain_cy

,sales_items_in_classification_ch,
total_sales_items_ch,
share_of_sales_classitems_from_total_ch,
user_sales_items_in_classification_ch, user_total_sales_items_ch, user_share_of_sales_classitems_from_total_ch
,((1+user_share_of_sales_classitems_from_total_ch)/(1+share_of_sales_classitems_from_total_ch))-1  share_of_change_sales_items_user_to_chain_ch
,case when user_share_of_sales_classitems_from_total_ch is null then 0-share_of_sales_classitems_from_total_ch
      when share_of_sales_classitems_from_total_ch is null then user_share_of_sales_classitems_from_total_ch-0 else user_share_of_sales_classitems_from_total_ch-share_of_sales_classitems_from_total_ch end share_of_difference_sales_items_user_to_chain_ch

,sales_items_in_classification_cq, total_sales_items_cq, share_of_sales_classitems_from_total_cq
,user_sales_items_in_classification_cq, user_total_sales_items_cq, user_share_of_sales_classitems_from_total_cq
,((1+user_share_of_sales_classitems_from_total_cq)/(1+share_of_sales_classitems_from_total_cq))-1  share_of_change_sales_items_user_to_chain_cq
,case when user_share_of_sales_classitems_from_total_cq is null then 0-share_of_sales_classitems_from_total_cq
      when share_of_sales_classitems_from_total_cq is null then user_share_of_sales_classitems_from_total_cq-0 else user_share_of_sales_classitems_from_total_cq-share_of_sales_classitems_from_total_cq end share_of_difference_sales_items_user_to_chain_cq

from rp4_chain_sales_items_by_classification cq left join rp4_user_sales_items_by_classification uq on cq.item_format_type_classification_code=uq.item_format_type_classification_code 
and cq.item_format_type_classification_name=uq.item_format_type_classification_name

