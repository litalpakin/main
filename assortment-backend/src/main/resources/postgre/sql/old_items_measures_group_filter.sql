
with selected_store_agg_2 as 
( ---------------------------------------------------------distribution fix-------------------------------------
	SELECT count(distinct case when sales_store_cur>0 then dw_store_key end) count_stores_cur
	,count(distinct case when sales_store_quartermatcur>0 then dw_store_key end) count_stores_quartermatcur
	,count(distinct case when sales_store_halfmatcur>0 then dw_store_key end) count_stores_halfmatcur


	FROM migvan.assortment_store_indices 
	where  {STORES_FILTER_PLACEHOLDER}

)


, {SELECTED_STORE_AGG}

select *
,case when is_pass_potential_ms_test=1 and sales_ms_item_subcat_cur_25>0 and sales_ms_item_subcat_market_yearmatcur_141>0 and sales_subcat_format_cur_7>0 
and (sales_ms_item_subcat_market_yearmatcur_141-sales_ms_item_subcat_cur_25)>0  then (sales_ms_item_subcat_market_yearmatcur_141-sales_ms_item_subcat_cur_25)*sales_subcat_format_cur_7 else null end inc_sales_by_iteminsubcat_salesms_yearmatcur
from(
select *
,case when rank_potential_item_in_subcat_halfmatcur_185<>0 then rank_potential_item_in_subcat_halfmatcur_185 ||' / '|| max_rank_potential_item_in_subcat_halfmatcur_186  else null end  as ranking_potential_item_in_subcat_halfmatcur
,case when rank_potential_item_in_subcat_38<>0 then rank_potential_item_in_subcat_38 ||' / '|| max_rank_potential_item_in_subcat_39  else null end  as ranking_potential_item_in_subcat_yearmatcur
,case when rank_potential_item_in_subcat_quartermatcur_183<>0 then rank_potential_item_in_subcat_quartermatcur_183 ||' / '|| max_rank_potential_item_in_subcat_quartermatcur_184  else null end  as ranking_potential_item_in_subcat_quartermatcur
,case when rank_potential_item_in_brand_subcat_format_40<>0 then rank_potential_item_in_brand_subcat_format_40 ||' / '|| max_potential_item_in_brand_subcat_format_41  else null end  as ranking_potential_item_in_brand_subcat_format_yearmatcur
,case when rank_potential_item_in_brand_subcat_format_quartermatcur_187<>0 then rank_potential_item_in_brand_subcat_format_quartermatcur_187 ||' / '|| max_rank_potential_item_in_brand_subcat_format_quartermatcur_188  else null end  as ranking_potential_item_in_brand_subcat_format_quartermatcur
,case when rank_potential_item_in_brand_subcat_format_halfmatcur_189<>0 then rank_potential_item_in_brand_subcat_format_halfmatcur_189 ||' / '|| max_rank_potential_item_in_brand_subcat_format_halfmatcur_190  else null end  as ranking_potential_item_in_brand_subcat_format_halfmatcur
,case when rank_potential_item_in_format_yearmatcur_191<>0 then rank_potential_item_in_format_yearmatcur_191 ||' / '|| max_rank_potential_item_in_format_yearmatcur_192  else null end  as ranking_potential_item_in_format_yearmatcur
,case when rank_potential_item_in_format_quartermatcur_193<>0 then rank_potential_item_in_format_quartermatcur_193 ||' / '|| max_rank_potential_item_in_format_quartermatcur_194  else null end  as ranking_potential_item_in_format_quartermatcur
,case when rank_potential_item_in_format_halfmatcur_195<>0 then rank_potential_item_in_format_halfmatcur_195 ||' / '|| max_rank_potential_item_in_format_halfmatcur_196 else null end  as ranking_potential_item_in_format_halfmatcur

,case when rank_item_in_category_17<>0 then rank_item_in_category_17 ||' / '|| max_rank_item_in_category_18 else null end as ranking_item_in_category_format_yearmatcur
,case when rank_item_in_format<>0 then rank_item_in_format ||' / '|| max_rank_item_in_format else null end as ranking_item_in_format_yearmatcur
,case when rank_item_in_category_quartermatcur_111<>0 then rank_item_in_category_quartermatcur_111 ||' / '|| max_rank_item_in_category_quartermatcur_112 else null end as ranking_item_in_category_format_quartermatcur
,case when rank_item_in_category_halfmatcur_113<>0 then rank_item_in_category_halfmatcur_113 ||' / '|| max_rank_item_in_category_halfmatcur_114 else null end as ranking_item_in_category_format_halfmatcur
,case when rank_item_in_subcat_19<>0 then rank_item_in_subcat_19 ||' / '|| max_rank_item_in_subcat_20 else null end as ranking_item_in_subcat_format_yearmatcur
,case when rank_qty_item_in_subcat_format_cy<>0 then rank_qty_item_in_subcat_format_cy ||' / '|| max_rank_item_in_subcat_20 else null end as ranking_qty_item_in_subcat_format_cy

,case when rank_item_in_subcat_quartermatcur_115<>0 then rank_item_in_subcat_quartermatcur_115 ||' / '|| max_rank_item_in_subcat_quartermatcur_116 else null end as ranking_item_in_subcat_format_quartermatcur
,case when rank_item_in_subcat_halfmatcur_117<>0 then rank_item_in_subcat_halfmatcur_117 ||' / '|| max_rank_item_in_subcat_halfmatcur_118 else null end as ranking_item_in_subcat_format_halfmatcur
,case when rank_item_in_brand_format<>0 then rank_item_in_brand_format ||' / '|| max_rank_item_in_brand_format else null end as ranking_item_in_brand_format_yearmatcur
,case when rank_item_in_brand_subcat_format_21<>0 then rank_item_in_brand_subcat_format_21 ||' / '|| max_rank_item_in_brand_subcat_format_22 else null end as ranking_item_in_brand_subcat_format_yearmatcur
,case when rank_item_in_brand_subcat_format_quartermatcur_119<>0 then rank_item_in_brand_subcat_format_quartermatcur_119 ||' / '||  max_rank_item_in_brand_subcat_format_quartermatcur_120 else null end  as ranking_item_in_brand_subcat_format_quartermatcur
,case when rank_item_in_brand_subcat_format_halfmatcur_121<>0 then rank_item_in_brand_subcat_format_halfmatcur_121 ||' / '||   max_rank_item_in_brand_subcat_format_halfmatcur_122 else null end as ranking_item_in_brand_subcat_format_halfmatcur

    ,case when coalesce(relative_qty_subcat_cur_31 ,0) <= 0 or coalesce(relative_qty_subcat_prv_32,0) <= 0 then null else
    (relative_qty_subcat_cur_31 /relative_qty_subcat_prv_32)-1 end relative_qty_change_item_subcat_34
    ,case when coalesce(relative_qty_subcat_quartermatcur_166 ,0) <= 0 or coalesce(relative_qty_subcat_quartermatprv_167,0) <= 0 then null else
    (relative_qty_subcat_quartermatcur_166 /relative_qty_subcat_quartermatprv_167)-1 end relative_qty_change_item_subcat_quartermatvslasty_172
    ,case when coalesce(relative_qty_subcat_halfmatcur_168 ,0) <= 0 or coalesce(relative_qty_subcat_halfmatprv_169,0) <= 0 then null else
    (relative_qty_subcat_halfmatcur_168 /relative_qty_subcat_halfmatprv_169)-1 end relative_qty_change_item_subcat_halfmatvslasty_173
	
	,case when qty_price_item_iteminselectedstores_quartermatcur<=0 or qty_price_item_iteminselectedstores_quartermatcur is null or qty_price_item_format_market_qtrmatcur<=0 or qty_price_item_format_market_qtrmatcur is null then null else(qty_price_item_iteminselectedstores_quartermatcur/qty_price_item_format_market_qtrmatcur)-1 end qty_price_change_iteminselectedstores_cahintomarket_cq

	,case when ((sales_ms_category_format_market_yearmatcur_t2/sales_ms_category_format_cur)-1>=0.05) and ((sales_ms_subcat_format_market_yearmatcur_153/sales_ms_subcat_format_yearmatcur_131)-1>=0.05) then 1 else 0 end is_pass_potential_ms_test

	

from(
  SELECT *
    ,case when coalesce(sales_ms_item_category_cur_23,0) <= 0 or coalesce(sales_ms_item_category_prv_24,0) <= 0 then null else
    (sales_ms_item_category_cur_23/sales_ms_item_category_prv_24)-1 end sales_change_ms_item_category_27
    ,case when coalesce(sales_ms_item_category_quartermatcur_123,0) <= 0 or coalesce(sales_ms_item_category_quartermatprv_124,0) <= 0 then null else
    (sales_ms_item_category_quartermatcur_123/sales_ms_item_category_quartermatprv_124)-1 end sales_change_ms_item_category_quartermatcur_137
    ,case when coalesce(sales_ms_item_category_halfmatcur_125,0) <= 0 or coalesce(sales_ms_item_category_halfmatprv_126,0) <= 0 then null else
    (sales_ms_item_category_halfmatcur_125/sales_ms_item_category_halfmatprv_126)-1 end sales_change_ms_item_category_halfmatcur_138

    ,case when coalesce(sales_ms_item_subcat_prv_26,0) <= 0 or coalesce(sales_ms_item_subcat_cur_25,0) <= 0 then null else
    (sales_ms_item_subcat_cur_25/sales_ms_item_subcat_prv_26)-1 end sales_change_ms_item_subcat_28
    ,case when coalesce(sales_ms_item_subcat_quartermatprv_128,0) <= 0 or coalesce(sales_ms_item_subcat_quartermatcur_127,0) <= 0 then null else
    (sales_ms_item_subcat_quartermatcur_127/sales_ms_item_subcat_quartermatprv_128)-1 end sales_change_ms_item_subcat_quartermatvslasty_139
    ,case when coalesce(sales_ms_item_subcat_halfmatprv_130,0) <= 0 or coalesce(sales_ms_item_subcat_halfmatcur_129,0) <= 0 then null else
    (sales_ms_item_subcat_halfmatcur_129/sales_ms_item_subcat_halfmatprv_130)-1 end sales_change_ms_item_subcat_halfmatvslasty_140

    ,case when coalesce(sales_ms_item_format_prv,0) <= 0 or coalesce(sales_ms_item_format_cur,0) <= 0 then null else
    (sales_ms_item_format_cur/sales_ms_item_format_prv)-1 end sales_change_ms_item_format_yearmatvslasty
    ,case when coalesce(sales_ms_item_format_quartermatprv,0) <= 0 or coalesce(sales_ms_item_format_quartermatcur,0) <= 0 then null else
    (sales_ms_item_format_quartermatcur/sales_ms_item_format_quartermatprv)-1 end sales_change_ms_item_format_quartermatvslasty
    ,case when coalesce(sales_ms_item_format_halfmatprv,0) <= 0 or coalesce(sales_ms_item_format_halfmatcur,0) <= 0 then null else
    (sales_ms_item_format_halfmatcur/sales_ms_item_format_halfmatprv)-1 end sales_change_ms_item_format_halfmatvslasty
    ----
    ,case when coalesce(sales_ms_subcat_format_quartermatprv_134,0) <= 0 or coalesce(sales_ms_subcat_format_quartermatcur_133,0) <= 0 then null else
    (sales_ms_subcat_format_quartermatcur_133/sales_ms_subcat_format_quartermatprv_134)-1 end sales_change_ms_subcat_format_quartermatvslasty
    ,case when coalesce(sales_ms_subcat_format_halfmatprv_136,0) <= 0 or coalesce(sales_ms_subcat_format_halfmatcur_135,0) <= 0 then null else
    (sales_ms_subcat_format_halfmatcur_135/sales_ms_subcat_format_halfmatprv_136)-1 end sales_change_ms_subcat_format_halfmatvslasty
	
	,case when coalesce(sales_item_format_ss_quartermatprv,0) <= 0 or coalesce(sales_item_format_ss_quartermatcur,0) <= 0 then null else (sales_item_format_ss_quartermatcur/sales_item_format_ss_quartermatprv)-1 end sales_change_item_format_ss_quartermatvslasty
	,case when coalesce(sales_category_format_ss_halfmatprv,0) <= 0 or coalesce(sales_category_format_ss_halfmatcur,0) <= 0 then null else (sales_category_format_ss_halfmatcur/sales_category_format_ss_halfmatprv)-1 end sales_change_item_format_ss_halfmatvslasty

    ,case when coalesce(sales_ms_item_subcat_cur_25,0) <= 0 or coalesce(sales_ms_item_subcat_market_yearmatcur_141,0) <= 0 then null else
    (sales_ms_item_subcat_cur_25 /sales_ms_item_subcat_market_yearmatcur_141)-1 end sales_ms_gap_item_subcat_market_chain_yearmatcur

    ,case when coalesce(qty_item_format_cur_10,0) <= 0 or coalesce(qty_sales_subcat_format_cur_14,0) <= 0 then null else
    (cast(qty_item_format_cur_10 as double precision)/qty_sales_subcat_format_cur_14) end relative_qty_subcat_cur_31
    ,case when coalesce(qty_item_format_prv_11,0) <= 0 or coalesce(qty_sales_subcat_format_prv_15 ,0) <= 0 then null else
    (cast(qty_item_format_prv_11 as double precision)/qty_sales_subcat_format_prv_15 ) end relative_qty_subcat_prv_32
    ,case when coalesce(qty_item_quartermatcur_97,0) <= 0 or coalesce(qty_sales_subcat_quartermatcur_105,0) <= 0 then null else
    (cast(qty_item_quartermatcur_97 as double precision)/qty_sales_subcat_quartermatcur_105) end relative_qty_subcat_quartermatcur_166
    ,case when coalesce(qty_item_quartermatprv_98,0) <= 0 or coalesce(qty_sales_subcat_quartermatprv_106 ,0) <= 0 then null else
    (cast(qty_item_quartermatprv_98 as double precision)/qty_sales_subcat_quartermatprv_106 ) end relative_qty_subcat_quartermatprv_167
    ,case when coalesce(qty_item_halfmatcur_99,0) <= 0 or coalesce(qty_sales_subcat_halfmatcur_107,0) <= 0 then null else
    (cast(qty_item_halfmatcur_99 as double precision)/qty_sales_subcat_halfmatcur_107) end relative_qty_subcat_halfmatcur_168
    ,case when coalesce(qty_item_halfmatprv_100,0) <= 0 or coalesce(qty_sales_subcat_halfmatprv_108 ,0) <= 0 then null else
    (cast(qty_item_halfmatprv_100 as double precision)/qty_sales_subcat_halfmatprv_108 ) end relative_qty_subcat_halfmatprv_169

    ,case when coalesce(relative_qty_category_cur_29,0) <= 0 or coalesce(relative_qty_category_prv_30,0) <= 0 then null else
    (relative_qty_category_cur_29/relative_qty_category_prv_30)-1 end relative_qty_change_item_category_33
    ,case when coalesce(relative_qty_category_quartermatcur_162,0) <= 0 or coalesce(relative_qty_category_quartermatprv_163,0) <= 0 then null else
    (relative_qty_category_quartermatcur_162/relative_qty_category_quartermatprv_163)-1 end relative_qty_change_item_category_quartermatvslasty_170
    ,case when coalesce(relative_qty_category_halfmatcur_164,0) <= 0 or coalesce(relative_qty_category_halfmatprv_165,0) <= 0 then null else
    (relative_qty_category_halfmatcur_164/relative_qty_category_halfmatprv_165)-1 end relative_qty_change_item_category_halfmatvslasty_171

    ,case when coalesce(qty_sales_brand_foramt_cur,0) <= 0 or coalesce(qty_sales_format_cur,0) <= 0 then null else
    (cast(qty_sales_brand_foramt_cur as double precision)/qty_sales_format_cur) end relative_qty_brand_format_cur

    ,case when coalesce(qty_item_quartermatcur_97,0) <= 0 or coalesce(qty_item_quartermatprv_98,0) <= 0 then null else
    (cast(qty_item_quartermatcur_97 as double precision)/qty_item_quartermatprv_98)-1 end qty_change_item_format_quartermatvslasty
	
	,case when coalesce(qty_item_format_ss_cur,0) <= 0 or coalesce(qty_item_format_ss_prv,0) <= 0 then null else (cast(qty_item_format_ss_cur as double precision)/qty_item_format_ss_prv)-1 end qty_change_item_ss
	,case when coalesce(qty_item_ss_halfmatcur,0) <= 0 or coalesce(qty_item_ss_halfmatprv,0) <= 0 then null else (cast(qty_item_ss_halfmatcur as double precision)/qty_item_ss_halfmatprv)-1 end qty_change_item_ss_halfmatvslasty
	,case when coalesce(qty_item_ss_quartermatcur,0) <= 0 or coalesce(qty_item_ss_quartermatprv,0) <= 0 then null else (cast(qty_item_ss_quartermatcur as double precision)/qty_item_ss_quartermatprv)-1 end qty_change_item_format_ss_quartermatvslasty


    ,potential_sales_item_in_format-sales_item_allformat_yearmatcur annual_inc_for_sales_from_potential_implementaion
    ,potential_sales_item_in_format_quartermatcur-sales_item_allformat_quartermatcur quarterly_inc_for_sales_from_potential_implementaion


    ,case when coalesce(avg_ms_sales_item_subcat_high_sub_chain_213,0) <= 0 or coalesce(sales_ms_item_subcat_cur_25,0) <= 0 then null else
    (sales_ms_item_subcat_cur_25/avg_ms_sales_item_subcat_high_sub_chain_213)-1 end  sales_change_ms_item_subcat_chain_to_market_high_sub_chain

    ,case when coalesce(avg_ms_sales_item_market_high_sub_chain,0) <= 0 or coalesce(sales_ms_item_format_yearmatcur,0) <= 0 then null else
    (sales_ms_item_format_yearmatcur/avg_ms_sales_item_market_high_sub_chain)-1 end  sales_change_ms_item_format_chain_to_market_high_sub_chain
----
   ,case when (max_sales_changegap_subcat_from_format_market_yearmatvsprv-sales_changegap_subcat_from_format_ss_yearmatvsprv>=0.03)
    and (sales_ms_subcat_format_market_yearmatcur_153>=sales_ms_subcat_format_yearmatcur_131)
    and (sales_ms_item_subcat_market_yearmatcur_141>=sales_ms_item_subcat_cur_25)  then 1 else 0 end is_pass_potential_change_test_ss

   ,case when (max_sales_changegap_subcat_from_format_market_yearmatvsprv-sales_changegap_subcat_from_format_ss_yearmatvsprv<0.03)
    or (sales_ms_subcat_format_market_yearmatcur_153<sales_ms_subcat_format_yearmatcur_131)
    or (sales_ms_item_subcat_market_yearmatcur_141<sales_ms_item_subcat_cur_25)
    or gap_growth_market_chain_ss_yearmatcur*sales_item_format_ss_cur<0 then null
    else gap_growth_market_chain_ss_yearmatcur*sales_item_format_ss_cur end growth_potential_market_to_chain_ss_yearmatcur
	
	
	,case when coalesce(sales_category_format_cur_5,0) <= 0 or coalesce(sales_format_yearmatcur_68,0) <= 0 then null else
	(sales_category_format_cur_5/sales_format_yearmatcur_68) end sales_ms_category_format_cur
	
	,case when weighted='True' and weight_itemformat_yearmatcur=0   then null
	  when weighted='True' and weight_itemformat_yearmatcur<>0  then (1000/weight_itemformat_yearmatcur)*sales_item_format_cur_3
	  when sales_item_format_cur_3<=0 or sales_item_format_cur_3 is null or qty_item_format_cur_10<=0 or qty_item_format_cur_10 is null then null 
	  else sales_item_format_cur_3/qty_item_format_cur_10 end qty_price_item_iteminselectedstores_cur


,case when qty_price_item_allformat_cur<=0 or qty_price_item_allformat_cur is null or qty_price_item_format_market_cur<=0 or qty_price_item_format_market_cur is null then null else(qty_price_item_allformat_cur/qty_price_item_format_market_cur)-1 end qty_price_change_iteminselectedstores_cahintomarket_cur

--,case when sales_item_format_quartermatcur_56<=0 or sales_item_format_quartermatcur_56 is null or qty_item_quartermatcur_97<=0 or qty_item_quartermatcur_97 is null then null else sales_item_format_quartermatcur_56/qty_item_quartermatcur_97 end qty_price_item_iteminselectedstores_quartermatcur
,case when weighted='True' and weight_item_format_quartermatcur=0   then null
	  when weighted='True' and weight_item_format_quartermatcur<>0  then (1000/weight_item_format_quartermatcur)*sales_item_format_quartermatcur_56
	  when sales_item_format_quartermatcur_56<=0 or sales_item_format_quartermatcur_56 is null or qty_item_quartermatcur_97<=0 or qty_item_quartermatcur_97 is null then null 
	  else sales_item_format_quartermatcur_56/qty_item_quartermatcur_97 end qty_price_item_iteminselectedstores_quartermatcur


----
  FROM (

    select  dw_item_key,oi.sub_category_key, oi.category_key,  oi.sub_brand_hierarchy_2_key
    ,class_key, supplier_key, oi.format_code
	
	,general_status_key, general_status_name,is_united_dw_item_key,is_private_label,is_item_format_special_needs
	{REGIONAL_ITEMS_PLACEHOLDER}
	{MAX_RETAILER_CATALOG_PLACEHOLDER}

	,max(is_special_old_item) is_special_item
	
    ,max(item_format_examination_code_44) item_format_examination_code
    ,item_classification_42 item_classification_1, max(is_item_classification_equal_for_all_format_43) is_item_classification_equal_for_all_format_2
    ,sum(sales_item_store_cur_1) sales_item_format_cur_3
    ,sum(sales_item_store_prv_2) sales_item_format_prv_4
    ,sum(sales_difference_item_store_236) sales_difference_item_format_yearmatvslasty_214
	,sum(sales_difference_item_store_ss) sales_difference_item_format_ss_yearmatvslasty
    ,sum(sales_item_store_quartermatcur_45) sales_item_format_quartermatcur_56
    ,sum(sales_item_store_quartermatprv_46) sales_item_format_quartermatprv_57
    ,sum(sales_item_store_halfmatcur_47) sales_item_format_halfmatcur_58
    ,sum(sales_item_store_halfmatprv_48) sales_item_format_halfmatprv_59

    ,max(sales_item_format_quartermatcur_178) sales_item_allformat_quartermatcur
    ,max(sales_item_format_cur_176) sales_item_allformat_yearmatcur

    ,avg(item_final_availability_in_store_29) avg_item_final_availability_42
    ,avg(item_final_availability_in_store_quartermatcur_156) avg_item_final_availability_quartermatcur_197
    ,avg(item_final_availability_in_store_halfmatcur_157) avg_item_final_availability_halfmatcur_198

    ,sum(potential_sales_item_store_yearmatcur_252) sales_item_format_full_availability_yearmatcur_215
    ,sum(potential_sales_item_store_yearmatcur_252)-sum(sales_item_store_cur_1) full_availability_to_cur_sales_difference_item_format_cy
	

	
	,case when max(count_selectedstores_yearmatcur)<>0 then cast(sum(is_item_store_sold_yearmatcur_170)as double precision)/max(count_selectedstores_yearmatcur) end item_distribution_43
	,case when max(count_selectedstores_quartermatcur)<>0 then cast(sum(is_item_store_sold_quartermatcur_171)as double precision)/max(count_selectedstores_quartermatcur) end  item_distribution_quartermatcur_199
	,case when max(count_selectedstores_halfmatcur)<>0 then  cast(sum(is_item_store_sold_halfmatcur_172)as double precision)/max(count_selectedstores_halfmatcur) end item_distribution_halfmatcur_200

   -- ,case when sum(is_store_sold_yearmatcur_173)<>0 then cast(sum(is_item_store_sold_yearmatcur_170)as double precision)/sum(is_store_sold_yearmatcur_173) end item_distribution_43
   -- ,case when sum(is_store_sold_quartermatcur_174)<>0 then cast(sum(is_item_store_sold_quartermatcur_171)as double precision)/sum(is_store_sold_quartermatcur_174) end  item_distribution_quartermatcur_199
   -- ,case when sum(is_store_sold_halfmatcur_175)<>0 then  cast(sum(is_item_store_sold_halfmatcur_172)as double precision)/sum(is_store_sold_halfmatcur_175) end item_distribution_halfmatcur_200

    ,max(avg_ms_item_subcat_hh_cur_30) avg_ms_item_subcat_hh_cur_44
    ,max(avg_ms_item_subcat_hh_prv_31) avg_ms_item_subcat_hh_prv_45
    ,max(avg_ms_item_subcat_change_32) avg_ms_item_subcat_change_46
    ,max(avg_ms_item_subcat_hh_halfmatcur_158) avg_ms_item_subcat_hh_halfmatcur_201
    ,max(avg_ms_item_subcat_hh_halfmatprv_159) avg_ms_item_subcat_hh_halfmatprv_202
    ,max(avg_ms_item_subcat_change_halfmatvslasty_160) avg_ms_item_subcat_change_halfmatvslasty_203

    ,max(penetration_item_subcat_cur_33) penetration_item_subcat_cur_47
    ,max(penetration_item_subcat_prv_34) penetration_item_subcat_prv_48
    ,max(penetration_item_subcat_change_35) penetration_item_subcat_change_49
    ,max(penetration_item_subcat_halfmatcur_161) penetration_item_subcat_halfmatcur_204
    ,max(penetration_item_subcat_halfmatprv_162) penetration_item_subcat_halfmatprv_205
    ,max(penetration_item_subcat_change_halfmatvslasty_163) penetration_item_subcat_change_halfmatvslasty_206

    ,max(avg_sales_basket_size_cur_36) avg_sales_basket_size_cur_50
    ,max(avg_sales_basket_size_prv_37) avg_sales_basket_size_prv_51
    ,max(avg_sales_basket_size_change_38) avg_sales_basket_size_change_52
    ,max(avg_sales_basket_size_halfmatcur_164) avg_sales_basket_size_halfmatcur_207
    ,max(avg_sales_basket_size_halfmatprv_165) avg_sales_basket_size_halfmatprv_208
    ,max(avg_sales_basket_size_change_halfmatvslasty_166) avg_sales_basket_size_halfmatvslasty_209

    ,max(purchase_frequency_cur_39) purchase_frequency_cur_53
    ,max(purchase_frequency_prv_40) purchase_frequency_prv_54
    ,max(purchase_frequency_change_41) purchase_frequency_change_55
    ,max(purchase_frequency_cur_39) purchase_frequency_halfmatcur_210
    ,max(purchase_frequency_prv_40) purchase_frequency_halfmatprv_211
    ,max(purchase_frequency_change_41) purchase_frequency_change_halfmatvslasty_212

    ,max(avg_ms_brand_sub_category_format_286) avg_ms_brand_sub_category_format
    ,max(penetration_brand_subcat_cur_287) penetration_brand_subcat_cur

    ,max(avg_ms_sales_item_subcat_market_high_sub_chain_130) avg_ms_sales_item_subcat_high_sub_chain_213

    ,max(avg_ms_sales_item_market_high_sub_chain_283) avg_ms_sales_item_market_high_sub_chain


    ,max(total_potential_sales_item_in_format_21) potential_sales_item_in_format
    ,max(total_potential_sales_item_in_format_quartermatcur_134) potential_sales_item_in_format_quartermatcur

    ,max(ms_potential_sales_item_in_category_22) ms_potential_sales_item_in_category_35
    ,max(ms_potential_sales_item_in_category_quartermatcur_135) ms_potential_sales_item_in_category_quartermatcur_174
    ,max(ms_potential_sales_item_in_category_halfmatcur_146) ms_potential_sales_item_in_halfmatcur_category_175

    ,max(ms_potential_sales_item_in_subcat_23) ms_potential_sales_item_in_subcat_36
    ,max(ms_potential_sales_item_in_subcat_quartermatcur_136) ms_potential_sales_item_in_subcat_quartermatcur_176
    ,max(ms_potential_sales_item_in_subcat_halfmatcur_147) ms_potential_sales_item_in_subcat_halfmatcur_177

    ,max(ms_potential_sales_item_in_format_131) ms_potential_sales_item_in_format_yearmatcur_178
    ,max(ms_potential_sales_item_in_format_quartermatcur_138) ms_potential_sales_item_in_format_quartermatcur_179
    ,max(ms_potential_sales_item_in_format_halfmatcur_149) ms_potential_sales_item_in_format_halfmatcur_180

    ,max(ms_potential_sales_item_in_brand_24) ms_potential_sales_item_in_brand_37
    ,max(ms_potential_sales_item_in_brand_quartermatcur_137) ms_potential_sales_item_in_brand_quartermatcur_181
    ,max(ms_potential_sales_item_in_brand_halfmatcur_148) ms_potential_sales_item_in_brand_halfmatcur_182

    ,max(rank_potential_item_in_subcat_25) rank_potential_item_in_subcat_38
    ,max(max_rank_potential_item_in_subcat_26) max_rank_potential_item_in_subcat_39
    ,max(rank_potential_item_in_subcat_quartermatcur_139) rank_potential_item_in_subcat_quartermatcur_183
    ,max(max_rank_potential_item_in_subcat_quartermatcur_140) max_rank_potential_item_in_subcat_quartermatcur_184
    ,max(rank_potential_item_in_subcat_halfmatcur_150) rank_potential_item_in_subcat_halfmatcur_185
    ,max(max_rank_potential_item_in_subcat_halfmatcur_151) max_rank_potential_item_in_subcat_halfmatcur_186

    ,max(rank_potential_item_in_brand_subcat_format_27) rank_potential_item_in_brand_subcat_format_40
    ,max(max_rank_potential_item_in_brand_subcat_format_28)  max_potential_item_in_brand_subcat_format_41
    ,max(rank_potential_item_in_brand_subcat_format_quartermatcur_141) rank_potential_item_in_brand_subcat_format_quartermatcur_187
    ,max(max_rank_potential_item_in_brand_subcat_format_quartermatcur_142) max_rank_potential_item_in_brand_subcat_format_quartermatcur_188
    ,max(rank_potential_item_in_brand_subcat_format_halfmatcur_152) rank_potential_item_in_brand_subcat_format_halfmatcur_189
    ,max(max_rank_potential_item_in_brand_subcat_format_halfmatcur_153) max_rank_potential_item_in_brand_subcat_format_halfmatcur_190

    ,max(rank_potential_item_in_format_132) rank_potential_item_in_format_yearmatcur_191
    ,max(max_rank_potential_item_in_format_133)  max_rank_potential_item_in_format_yearmatcur_192
    ,max(rank_potential_item_in_format_quartermatcur_143) rank_potential_item_in_format_quartermatcur_193
    ,max(max_rank_potential_item_in_format_quartermatcur_144) max_rank_potential_item_in_format_quartermatcur_194
    ,max(rank_potential_item_in_format_halfmatcur_154) rank_potential_item_in_format_halfmatcur_195
    ,max(max_rank_potential_item_in_format_halfmatcur_155) max_rank_potential_item_in_format_halfmatcur_196

	,max(sales_ms_category_format_market_yearmatcur) sales_ms_category_format_market_cur
    ,max(sales_category_format_cur) sales_category_format_cur_5
    ,max(sales_category_format_prv) sales_category_format_prv_6
    ,max(sales_category_format_quartermatcur) sales_category_format_quartermatcur_60
    ,max(sales_category_format_quartermatprv) sales_category_format_quartermatprv_61
    ,max(sales_category_format_halfmatcur) sales_category_format_halfmatcur_62
    ,max(sales_category_format_halfmatprv) sales_category_format_halfmatprv_63

    ,max(sales_subcat_format_cur) sales_subcat_format_cur_7
    ,max(sales_subcat_format_prv) sales_subcat_format_prv_8
    ,max(sales_subcat_format_quartermatcur) sales_subcat_format_quartermatcur_64
    ,max(sales_subcat_format_quartermatprv) sales_subcat_format_quartermatprv_65
    ,max(sales_subcat_format_halfmatprv) sales_subcat_format_halfmatprv_66
    ,max(sales_subcat_format_halfmatcur) sales_subcat_format_halfmatcur_67

    ,max(sales_format_yearmatcur) sales_format_yearmatcur_68
    ,max(sales_format_yearmatprv) sales_format_yearmatprv_69
    ,max(sales_format_quartermatcur) sales_format_quartermatcur_70
    ,max(sales_format_quartermatprv) sales_format_quartermatprv_71
    ,max(sales_format_halfmatcur) sales_format_halfmatcur_72
    ,max(sales_format_halfmatprv) sales_format_halfmatprv_73

    ,max(sales_brand_in_format_yearmatcur)  sales_brand_format_yearmatcur


    ,case when coalesce(sum(sales_item_store_prv_2),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else (sum(sales_item_store_cur_1)/sum(sales_item_store_prv_2))-1 end sales_change_item_format_9
    ,case when coalesce(sum(sales_item_store_quartermatprv_46),0) <= 0 or coalesce(sum(sales_item_store_quartermatcur_45),0) <= 0 then null else (sum(sales_item_store_quartermatcur_45)/sum(sales_item_store_quartermatprv_46))-1 end sales_change_item_format_quartermatvslasty_74
    ,case when coalesce(sum(sales_item_store_halfmatprv_48),0) <= 0 or coalesce(sum(sales_item_store_halfmatcur_47),0) <= 0 then null else (sum(sales_item_store_halfmatcur_47)/sum(sales_item_store_halfmatprv_48))-1 end sales_change_item_format_halfmatvslasty_75

    ,max(sales_item_market_yearmatcur_88) sales_item_market_yearmatcur_76 ,max(sales_item_market_yearmatprv_89) sales_item_market_yearmatprv_77
    ,max(sales_item_market_quartermatcur_90) sales_item_market_quartermatcur_78 ,max(sales_item_market_quartermatprv_91) sales_item_market_quartermatprv_79
    ,max(sales_item_market_halfmatcur_92) sales_item_market_halfmatcur_80 ,max(sales_item_market_halfmatprv_93) sales_item_market_halfmatprv_81

    ,max(sales_subcat_market_yearmatcur_94) sales_subcat_market_yearmatcur_82 ,max(sales_subcat_market_yearmatprv_95) sales_subcat_market_yearmatprv_83
    ,max(sales_subcat_market_quartermatcur_96) sales_subcat_market_quartermatcur_84 ,max(sales_subcat_market_quartermatprv_97) sales_subcat_market_quartermatprv_85
    ,max(sales_subcat_market_halfmatcur_98) sales_subcat_market_halfmatcur_86,max(sales_subcat_market_halfmatprv_99) sales_subcat_market_halfmatprv_87

    ,max(sales_format_market_yearmatcur_100) sales_format_market_yearmatcur_88 ,max(sales_format_market_yearmatprv_101) sales_format_market_yearmatprv_89
    ,max(sales_format_market_quartermatcur_102) sales_format_market_quartermatcur_90 ,max(sales_format_market_quartermatprv_103) sales_format_market_quartermatprv_91
    ,max(sales_format_market_halfmatcur_104) sales_format_market_halfmatcur_92,max(sales_format_market_halfmatprv_105) sales_format_market_halfmatprv_93

    ,max(sales_change_item_market_yearmatvsprv_106) sales_change_item_market_yearmatvsprv_94
    ,max(sales_change_item_market_quartermatvslasty_107) sales_change_item_market_quartermatvslasty_95
    ,max(sales_change_item_market_halfmatvslasty_108) sales_change_item_market_halfmatvslasty_96

    ,max(item_format_contribution_to_growth) item_format_contribution_to_growth
    ,case when coalesce(sum(sales_item_store_prv_2),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else ((1+max(sales_change_item_market_yearmatvsprv_106))/(1+(sum(sales_item_store_cur_1)/sum(sales_item_store_prv_2)-1 )))-1 end gap_growth_market_chain_yearmatcur_217
    ,case when coalesce(sum(sales_item_store_prv_2),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 or 
    (((1+max(sales_change_item_market_yearmatvsprv_106))/(1+(sum(sales_item_store_cur_1)/sum(sales_item_store_prv_2)-1 )))-1) < 0
     or sum(sales_item_store_cur_1)<=0 then null else (((1+max(sales_change_item_market_yearmatvsprv_106))/(1+(sum(sales_item_store_cur_1)/sum(sales_item_store_prv_2)-1)))-1)*sum(sales_item_store_cur_1) end growth_potential_market_to_chain_yearmatcur_218

    ,max(sales_ms_item_subcat_market_yearmatcur_109) sales_ms_item_subcat_market_yearmatcur_141
    ,max(sales_ms_item_subcat_market_yearmatprv_110) sales_ms_item_subcat_market_yearmatprv_142
    ,max(sales_ms_item_subcat_market_quartermatcur_111) sales_ms_item_subcat_market_quartermatcur_143
    ,max(sales_ms_item_subcat_market_quartermatprv_112) sales_ms_item_subcat_market_quartermatprv_144
    ,max(sales_ms_item_subcat_market_halfmatcur_113) sales_ms_item_subcat_market_halfmatcur_145
    ,max(sales_ms_item_subcat_market_halfmatprv_114) sales_ms_item_subcat_market_halfmatprv_146
    ,max(sales_change_ms_item_subcat_market_yearmatvsprv) sales_change_ms_item_subcat_market_yearmatvsprv

    ,max(sales_ms_item_format_market_yearmatcur_115) sales_ms_item_format_market_yearmatcur_147
    ,max(sales_ms_item_format_market_yearmatprv_116) sales_ms_item_format_market_yearmatprv_148
    ,max(sales_ms_item_format_market_quartermatcur_117) sales_ms_item_format_market_quartermatcur_149
    ,max(sales_ms_item_format_market_quartermatprv_118) sales_ms_item_format_market_quartermatprv_150
    ,max(sales_ms_item_format_market_halfmatcur_119) sales_ms_item_format_market_halfmatcur_151
    ,max(sales_ms_item_format_market_halfmatprv_120) sales_ms_item_format_market_halfmatprv_152

    ,max(sales_ms_subcat_format_market_yearmatcur_121) sales_ms_subcat_format_market_yearmatcur_153
    ,max(sales_ms_subcat_format_market_yearmatprv_122) sales_ms_subcat_format_market_yearmatprv_154
    ,max(sales_ms_subcat_format_market_quartermatcur_123) sales_ms_subcat_format_market_quartermatcur_155
    ,max(sales_ms_subcat_format_market_quartermatprv_124) sales_ms_subcat_format_market_quartermatprv_156
    ,max(sales_ms_subcat_format_market_halfmatcur_125) sales_ms_subcat_format_market_halfmatcur_157
    ,max(sales_ms_subcat_format_market_halfmatcur_126) sales_ms_subcat_format_market_halfmatprv_158

    ,max(sales_change_ms_item_format_market_yearmatvsprv_127) sales_change_ms_item_format_market_yearmatvsprv_159
    ,max(sales_change_ms_item_format_market_quartermatvslasty_128) sales_change_ms_item_format_market_quartermatvslasty_160
    ,max(sales_change_ms_item_format_market_halfmatvslasty_129) sales_change_ms_item_format_market_halfmatvslasty_161
    ,max(sales_change_ms_item_subcat_market_yearmatvsprv) sales_change_ms_item_subcat_format_market_yearmatvsprv

    ,max(salesmarket_rank_item_subcat_format_yearmatcur) salesmarket_rank_item_subcat_format_yearmatcur
    ,max(salesmarket_max_rank_item_subcat_format_yearmatcur) salesmarket_max_rank_item_subcat_format_yearmatcur
    ,max(salesmarket_ranking_item_subcat_format_yearmatcur) salesmarket_ranking_item_subcat_format_yearmatcur
    ,max(salesmarket_rank_item_format_yearmatcur) salesmarket_rank_item_format_yearmatcur
    ,max(salesmarket_max_rank_item_format_yearmatcur) salesmarket_max_rank_item_format_yearmatcur
    ,max(salesmarket_ranking_item_format_yearmatcur) salesmarket_ranking_item_format_yearmatcur


    ,sum(qty_item_store_cur_13) qty_item_format_cur_10
    ,sum(qty_item_store_prv_14) qty_item_format_prv_11
    ,sum(qty_item_store_quartermatcur_72) qty_item_quartermatcur_97
    ,sum(qty_item_store_quartermatprv_73) qty_item_quartermatprv_98
    ,sum(qty_item_store_halfmatcur_74) qty_item_halfmatcur_99
    ,sum(qty_item_store_halfmatprv_75) qty_item_halfmatprv_100

    ,max(qty_sales_category_format_cur) qty_sales_category_format_cur_12
    ,max(qty_sales_category_format_prv) qty_sales_category_format_prv_13
    ,max(qty_sales_category_format_quartermatcur) qty_sales_category_quartermatcur_101
    ,max(qty_sales_category_format_quartermatprv) qty_sales_category_quartermatprv_102
    ,max(qty_sales_category_format_halfmatcur) qty_sales_category_halfmatcur_103
    ,max(qty_sales_category_format_halfmatprv) qty_sales_category_halfmatprv_104

    ,max(qty_sales_subcat_format_cur) qty_sales_subcat_format_cur_14, max(qty_sales_subcat_format_prv) qty_sales_subcat_format_prv_15
    ,max(qty_sales_subcat_format_quartermatcur) qty_sales_subcat_quartermatcur_105, max(qty_sales_subcat_format_quartermatprv) qty_sales_subcat_quartermatprv_106
    ,max(qty_sales_subcat_format_halfmatcur) qty_sales_subcat_halfmatcur_107, max(qty_sales_subcat_format_halfmatprv) qty_sales_subcat_halfmatprv_108

    ,max(qty_brand_in_format_cur) qty_sales_brand_foramt_cur, max(qty_sales_in_format_cur) qty_sales_format_cur
	,sum(qty_item_store_ss_cur) qty_item_format_ss_cur
	,sum(qty_item_store_ss_prv) qty_item_format_ss_prv
	,sum(qty_item_store_ss_halfmatcur) qty_item_ss_halfmatcur
	,sum(qty_item_store_ss_halfmatprv) qty_item_ss_halfmatprv
	,sum(qty_item_store_ss_quartermatcur) qty_item_ss_quartermatcur
	,sum(qty_item_store_ss_quartermatprv) qty_item_ss_quartermatprv

    ,case when coalesce(sum(qty_item_store_cur_13),0) <= 0 or coalesce(sum(qty_item_store_prv_14),0) <= 0 then null else (sum(qty_item_store_cur_13)/sum(qty_item_store_prv_14))-1 end qty_change_item_16
    ,case when coalesce(sum(qty_item_store_quartermatcur_72),0) <= 0 or coalesce(sum(qty_item_store_quartermatprv_73),0) <= 0 then null else (sum(qty_item_store_quartermatcur_72)/sum(qty_item_store_quartermatprv_73))-1 end qty_change_item_quartermatvslasty_109
    ,case when coalesce(sum(qty_item_store_halfmatcur_74),0) <= 0 or coalesce(sum(qty_item_store_halfmatprv_75),0) <= 0 then null else (sum(qty_item_store_halfmatcur_74)/sum(qty_item_store_halfmatprv_75))-1 end qty_change_item_halfmatvslasty_110

    ,max(rank_item_in_category_237) rank_item_in_category_17
    ,max(rank_item_in_category_quartermatcur_241) rank_item_in_category_quartermatcur_111
    ,max(rank_item_in_category_halfmatcur_245) rank_item_in_category_halfmatcur_113
	,max(rank_qty_item_in_subcat_format_cy) rank_qty_item_in_subcat_format_cy

    ,max(rank_item_in_subcat_238) rank_item_in_subcat_19
    ,max(rank_item_in_subcat_quartermatcur_242) rank_item_in_subcat_quartermatcur_115
    ,max(rank_item_in_subcat_halfmatcur_246) rank_item_in_subcat_halfmatcur_117

    ,max(rank_item_in_brand_subcat_format_239) rank_item_in_brand_subcat_format_21
    ,max(rank_item_in_brand_subcat_format_quartermatcur_243) rank_item_in_brand_subcat_format_quartermatcur_119
    ,max(rank_item_in_brand_subcat_format_halfmatcur_247) rank_item_in_brand_subcat_format_halfmatcur_121

    ,max(rank_item_in_brand_format_285) rank_item_in_brand_format
    ,max(rank_item_in_format_240) rank_item_in_format

    ,max(items_count_categoryinformat_yearmatcur_212)  max_rank_item_in_category_18
    ,max(items_count_categoryinformat_quartermatcur_213)  max_rank_item_in_category_quartermatcur_112
    ,max(items_count_categoryinformat_halfmatcur_214)  max_rank_item_in_category_halfmatcur_114
    ,max(items_count_subcatinformat_yearmatcur_215)  max_rank_item_in_subcat_20
    ,max(items_count_subcatinformat_quartermatcur_216)  max_rank_item_in_subcat_quartermatcur_116
    ,max(items_count_subcatinformat_halfmatcur_217)  max_rank_item_in_subcat_halfmatcur_118
    ,max(items_count_brandinsubcat_yearmatcur_218)  max_rank_item_in_brand_subcat_format_22
    ,max(items_count_brandinsubcat_quartermatcur_219)  max_rank_item_in_brand_subcat_format_quartermatcur_120
    ,max(items_count_brandinsubcat_halfmatcur_220)  max_rank_item_in_brand_subcat_format_halfmatcur_122
    ,max(items_count_brandinformat_yearmatcur_284)  max_rank_item_in_brand_format
    ,max(items_count_format_yearmatcur_221)  max_rank_item_in_format

    ,case when coalesce(max(sales_brand_subcat_cur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_brand_subcat_cur)) end sales_ms_item_brand_subcat_format_cur

    ,case when coalesce(max(sales_brand_in_format_yearmatcur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_brand_in_format_yearmatcur)) end sales_ms_item_brand_format_cur

    ,case when coalesce(max(sales_subcat_format_cur),0) <= 0 or coalesce(max(sales_brand_in_format_yearmatcur),0) <= 0 then null else
    (max(sales_brand_in_format_yearmatcur)/max(sales_subcat_format_cur)) end sales_ms_brand_subcat_format_cur

    ,case when coalesce(max(sales_format_yearmatcur),0) <= 0 or coalesce(max(sales_brand_in_format_yearmatcur),0) <= 0 then null else
    (max(sales_brand_in_format_yearmatcur)/max(sales_format_yearmatcur)) end sales_ms_brand_format_cur

    ,case when coalesce(max(sales_format_yearmatcur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_format_yearmatcur)) end sales_ms_item_format_cur
    ,case when coalesce(max(sales_format_yearmatprv),0) <= 0 or coalesce(sum(sales_item_store_prv_2),0) <= 0 then null else
    (sum(sales_item_store_prv_2)/max(sales_format_yearmatprv)) end sales_ms_item_format_prv

    ,case when coalesce(max(sales_format_quartermatcur),0) <= 0 or coalesce(sum(sales_item_store_quartermatcur_45),0) <= 0 then null else
    (sum(sales_item_store_quartermatcur_45)/max(sales_format_quartermatcur)) end sales_ms_item_format_quartermatcur
    ,case when coalesce(max(sales_format_quartermatprv),0) <= 0 or coalesce(sum(sales_item_store_quartermatprv_46),0) <= 0 then null else
    (sum(sales_item_store_quartermatprv_46)/max(sales_format_quartermatprv)) end sales_ms_item_format_quartermatprv

    ,case when coalesce(max(sales_format_halfmatcur),0) <= 0 or coalesce(sum(sales_item_store_halfmatcur_47),0) <= 0 then null else
    (sum(sales_item_store_halfmatcur_47)/max(sales_format_halfmatcur)) end sales_ms_item_format_halfmatcur
    ,case when coalesce(max(sales_format_halfmatprv),0) <= 0 or coalesce(sum(sales_item_store_halfmatprv_48),0) <= 0 then null else
    (sum(sales_item_store_halfmatprv_48)/max(sales_format_halfmatprv)) end sales_ms_item_format_halfmatprv

    ,case when coalesce(max(sales_category_format_cur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_category_format_cur)) end sales_ms_item_category_cur_23
    ,case when coalesce(max(sales_category_format_prv),0) <= 0 or coalesce(sum(sales_item_store_prv_2),0) <= 0 then null else
    (sum(sales_item_store_prv_2)/max(sales_category_format_prv)) end sales_ms_item_category_prv_24
    ,case when coalesce(max(sales_category_format_quartermatcur),0) <= 0 or coalesce(sum(sales_item_store_quartermatcur_45),0) <= 0 then null else
    (sum(sales_item_store_quartermatcur_45)/max(sales_category_format_quartermatcur)) end sales_ms_item_category_quartermatcur_123
    ,case when coalesce(max(sales_category_format_quartermatprv),0) <= 0 or coalesce(sum(sales_item_store_quartermatprv_46),0) <= 0 then null else
    (sum(sales_item_store_quartermatprv_46)/max(sales_category_format_quartermatprv)) end sales_ms_item_category_quartermatprv_124
    ,case when coalesce(max(sales_category_format_halfmatcur),0) <= 0 or coalesce(sum(sales_item_store_halfmatcur_47),0) <= 0 then null else
    (sum(sales_item_store_halfmatcur_47)/max(sales_category_format_halfmatcur)) end sales_ms_item_category_halfmatcur_125
    ,case when coalesce(max(sales_category_format_halfmatprv),0) <= 0 or coalesce(sum(sales_item_store_halfmatprv_48),0) <= 0 then null else
    (sum(sales_item_store_halfmatprv_48)/max(sales_category_format_halfmatprv)) end sales_ms_item_category_halfmatprv_126

    ,case when coalesce(max(sales_subcat_format_cur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_subcat_format_cur)) end sales_ms_item_subcat_cur_25
    ,case when coalesce(max(sales_subcat_format_prv),0) <= 0 or coalesce(sum(sales_item_store_prv_2),0) <= 0 then null else
    (sum(sales_item_store_prv_2)/max(sales_subcat_format_prv)) end sales_ms_item_subcat_prv_26
    ,case when coalesce(max(sales_subcat_format_quartermatcur),0) <= 0 or coalesce(sum(sales_item_store_quartermatcur_45),0) <= 0 then null else
    (sum(sales_item_store_quartermatcur_45)/max(sales_subcat_format_quartermatcur)) end sales_ms_item_subcat_quartermatcur_127
    ,case when coalesce(max(sales_subcat_format_quartermatprv),0) <= 0 or coalesce(sum(sales_item_store_quartermatprv_46),0) <= 0 then null else
    (sum(sales_item_store_quartermatprv_46)/max(sales_subcat_format_quartermatprv)) end sales_ms_item_subcat_quartermatprv_128
    ,case when coalesce(max(sales_subcat_format_halfmatcur),0) <= 0 or coalesce(sum(sales_item_store_halfmatcur_47),0) <= 0 then null else
    (sum(sales_item_store_halfmatcur_47)/max(sales_subcat_format_halfmatcur)) end sales_ms_item_subcat_halfmatcur_129
    ,case when coalesce(max(sales_subcat_format_halfmatprv),0) <= 0 or coalesce(sum(sales_item_store_halfmatprv_48),0) <= 0 then null else
    (sum(sales_item_store_halfmatprv_48)/max(sales_subcat_format_halfmatprv)) end sales_ms_item_subcat_halfmatprv_130


    ,case when coalesce(max(sales_format_yearmatcur),0) <= 0 or coalesce(max(sales_subcat_format_cur),0) <= 0 then null else
    (max(sales_subcat_format_cur)/max(sales_format_yearmatcur)) end sales_ms_subcat_format_yearmatcur_131
    ,case when coalesce(max(sales_format_yearmatprv),0) <= 0 or coalesce(max(sales_subcat_format_prv),0) <= 0 then null else
    (max(sales_subcat_format_prv)/max(sales_format_yearmatprv)) end sales_ms_subcat_format_yearmatprv_132
    ,case when coalesce(max(sales_format_quartermatcur),0) <= 0 or coalesce(max(sales_subcat_format_quartermatcur),0) <= 0 then null else
    (max(sales_subcat_format_quartermatcur)/max(sales_format_quartermatcur)) end sales_ms_subcat_format_quartermatcur_133
    ,case when coalesce(max(sales_format_quartermatprv),0) <= 0 or coalesce(max(sales_subcat_format_quartermatprv),0) <= 0 then null else
    (max(sales_subcat_format_quartermatprv)/max(sales_format_quartermatprv)) end sales_ms_subcat_format_quartermatprv_134
    ,case when coalesce(max(sales_format_halfmatcur),0) <= 0 or coalesce(max(sales_subcat_format_halfmatcur),0) <= 0 then null else
    (max(sales_subcat_format_halfmatcur)/max(sales_format_halfmatcur)) end sales_ms_subcat_format_halfmatcur_135
    ,case when coalesce(max(sales_format_halfmatprv),0) <= 0 or coalesce(max(sales_subcat_format_halfmatprv),0) <= 0 then null else
    (max(sales_subcat_format_halfmatprv)/max(sales_format_halfmatprv)) end sales_ms_subcat_format_halfmatprv_136

    ,case when coalesce(max(sales_format_yearmatcur),0) <= 0 or coalesce(sum(sales_item_store_cur_1),0) <= 0 then null else
    (sum(sales_item_store_cur_1)/max(sales_format_yearmatcur)) end sales_ms_item_format_yearmatcur


    ,case when coalesce(sum(qty_item_store_cur_13),0) <= 0 or coalesce(max(qty_sales_category_format_cur),0) <= 0 then null else
    (cast(sum(qty_item_store_cur_13) as double precision)/max(qty_sales_category_format_cur)) end relative_qty_category_cur_29
    ,case when coalesce(sum(qty_item_store_prv_14),0) <= 0 or coalesce(max(qty_sales_category_format_prv) ,0) <= 0 then null else
    (cast(sum(qty_item_store_prv_14) as double precision)/max(qty_sales_category_format_prv) ) end relative_qty_category_prv_30
    ,case when coalesce(sum(qty_item_store_quartermatcur_72),0) <= 0 or coalesce(max(qty_sales_category_format_quartermatcur),0) <= 0 then null else
    (cast(sum(qty_item_store_quartermatcur_72) as double precision)/max(qty_sales_category_format_quartermatcur)) end relative_qty_category_quartermatcur_162
    ,case when coalesce(sum(qty_item_store_quartermatprv_73),0) <= 0 or coalesce(max(qty_sales_category_format_quartermatprv) ,0) <= 0 then null else
    (cast(sum(qty_item_store_quartermatprv_73) as double precision)/max(qty_sales_category_format_quartermatprv) ) end relative_qty_category_quartermatprv_163
    ,case when coalesce(sum(qty_item_store_halfmatcur_74),0) <= 0 or coalesce(max(qty_sales_category_format_halfmatcur),0) <= 0 then null else
    (cast(sum(qty_item_store_halfmatcur_74) as double precision)/max(qty_sales_category_format_halfmatcur)) end relative_qty_category_halfmatcur_164
    ,case when coalesce(sum(qty_item_store_halfmatprv_75),0) <= 0 or coalesce(max(qty_sales_category_format_halfmatprv) ,0) <= 0 then null else
    (cast(sum(qty_item_store_halfmatprv_75) as double precision)/max(qty_sales_category_format_halfmatprv) ) end relative_qty_category_halfmatprv_165
----
    ,max(sales_subcat_format_cur_ss) sales_subcat_format_ss_cur
    ,max(sales_subcat_format_prv_ss) sales_subcat_format_ss_prv
    ,max(sales_format_yearmatcur_ss) sales_format_ss_yearmatcur
    ,max(sales_format_yearmatprv_ss) sales_format_ss_yearmatprv

    ,case when coalesce(max(sales_subcat_format_prv_ss),0) <= 0 or coalesce(max(sales_subcat_format_cur_ss),0) <= 0
    then null else (max(sales_subcat_format_cur_ss)/max(sales_subcat_format_prv_ss))-1 end sales_change_subcat_ss_yearmatvsprv

    ,case when coalesce(max(sales_format_yearmatprv_ss),0) <= 0 or coalesce(max(sales_format_yearmatcur_ss),0) <= 0 then null
     else (max(sales_format_yearmatcur_ss)/max(sales_format_yearmatprv_ss))-1 end sales_change_format_ss_yearmatvsprv

--  converted  ,case when coalesce((1+sales_change_format_ss_yearmatvsprv),0) <= 0 or coalesce((1+sales_change_subcat_ss_yearmatvsprv),0) <= 0 then null else
--  converted  ((1+sales_change_subcat_ss_yearmatvsprv)/(1+sales_change_format_ss_yearmatvsprv))-1 end sales_changegap_subcat_from_format_ss_yearmatvsprv
    ,case when coalesce(max(sales_subcat_format_prv_ss),0) <= 0 or coalesce(max(sales_subcat_format_cur_ss),0) <= 0
    or coalesce(max(sales_format_yearmatprv_ss),0) <= 0 or coalesce(max(sales_format_yearmatcur_ss),0) <= 0
    or coalesce((max(sales_format_yearmatcur_ss)/max(sales_format_yearmatprv_ss)),0) <= 0
    or coalesce((max(sales_subcat_format_cur_ss)/max(sales_subcat_format_prv_ss)),0) <= 0 then null
    else  ((max(sales_subcat_format_cur_ss)/max(sales_subcat_format_prv_ss))/(max(sales_format_yearmatcur_ss)/max(sales_format_yearmatprv_ss))-1) end sales_changegap_subcat_from_format_ss_yearmatvsprv

--	  ,sales_changegap_subcat_from_format_market_yearmatvsprv
    ,max(sales_changegap_subcat_from_format_market_yearmatvsprv) as max_sales_changegap_subcat_from_format_market_yearmatvsprv

--    here - sales_item_store_ss_cur
    ,sum(sales_item_store_ss_cur) sales_item_format_ss_cur
    ,sum(sales_item_store_ss_prv) sales_item_format_ss_prv
	,sum(sales_item_store_ss_quartermatcur) sales_item_format_ss_quartermatcur
	,sum(sales_item_store_ss_quartermatprv) sales_item_format_ss_quartermatprv
	,sum(sales_item_store_ss_halfmatcur) sales_category_format_ss_halfmatcur
	,sum(sales_item_store_ss_halfmatprv) sales_category_format_ss_halfmatprv
    ,max(item_format_contribution_to_growth_ss) item_format_contribution_to_growth_ss

    ,case when coalesce(sum(sales_item_store_ss_prv),0) <= 0 or coalesce(sum(sales_item_store_ss_cur),0) <= 0 then null
     else (sum(sales_item_store_ss_cur)/sum(sales_item_store_ss_prv))-1 end sales_change_item_format_ss

    ,max(sales_change_item_market_yearmatvsprv_106) as max_sales_change_item_market_yearmatvsprv_106

    ,case when coalesce(sum(sales_item_store_ss_prv),0) <= 0 or coalesce(sum(sales_item_store_ss_cur),0) <= 0 then null
     else ((1+max(sales_change_item_market_yearmatvsprv_106))/(sum(sales_item_store_ss_cur)/sum(sales_item_store_ss_prv)))-1 end as gap_growth_market_chain_ss_yearmatcur
	 
	 ---------------------
	,max(weighted) as weighted
	,sum(weight_item_store_yearmatcur) weight_itemformat_yearmatcur
	,sum(weight_itemstore_quartermatcur) weight_item_format_quartermatcur
	,max(qty_price_item_format_yearmatcur) qty_price_item_allformat_cur
	,max(qty_price_item_format_market_yearmatcur) qty_price_item_format_market_cur
	,max(qty_price_item_format_market_quartermatcur) qty_price_item_format_market_qtrmatcur
	,max(avgqty_price_item_stores_group_a_item_format_yearmatcur) avgqty_price_item_stores_group_a_item_format_cur
	,max(avgqty_price_item_stores_group_b_item_format_yearmatcur) avgqty_price_item_stores_group_b_item_format_cur
	,max(avgqty_price_item_stores_group_c_item_format_yearmatcur) avgqty_price_item_stores_group_c_item_format_cur

	,max(qty_price_item_format_quartermatcur) qty_price_item_allformat_quartermatcur
	,max(qty_price_item_format_market_quartermatcur) qty_price_item_format_market_quartermatcur
	,max(avgqty_price_item_stores_group_a_item_format_quartermatcur) avgqty_price_item_stores_group_a_item_format_quartermatcur
	,max(avgqty_price_item_stores_group_b_item_format_quartermatcur) avgqty_price_item_stores_group_b_item_format_quartermatcur
	,max(avgqty_price_item_stores_group_c_item_format_quartermatcur) avgqty_price_item_stores_group_c_item_format_quartermatcur
	
	,max(sales_ms_category_format_market_yearmatcur) sales_ms_category_format_market_yearmatcur_t2
	
	,max(normalized_max_theoretic_item_format_sales_at_risk) as max_theoretic_item_format_sales
	,max(item_format_nontransferable_spend) as item_format_nontransferable_spend
	,max(item_format_estimated_real_sales_at_risk) as item_format_estimated_real_sales_at_risk
	,max(avg_must_item_format_estimated_real_sales_at_risk) as avg_must_item_format_estimated_real_sales_at_risk
	,max(item_format_alternative_cost) as item_format_alternative_cost
	,max(avg_must_item_format_alternative_cost) as alternative_real_sales_at_risk
	,max(item_format_net_gain_loss_sales) as item_format_net_gain_loss_sales
	
	{REGIONAL_ITEMS_CY_PLACEHOLDER}
------
----
    from migvan.old_items_measures oi left join selected_store_agg agg_inc on oi.sub_brand_hierarchy_2_key=agg_inc.sub_brand_hierarchy_2_key
    and  oi.sub_category_key=agg_inc.sub_category_key and  oi.category_key=agg_inc.category_key and  oi.format_code=agg_inc.format_code
     where item_classification_42 = :item_classification --- variable group filter
     and {PREFIX_STORES_FILTER_PLACEHOLDER}
	 
    group by  dw_item_key,oi.sub_category_key, oi.category_key,  oi.sub_brand_hierarchy_2_key
    ,class_key,  supplier_key, oi.format_code, general_status_key, general_status_name,is_united_dw_item_key,is_private_label,is_item_format_special_needs{REGIONAL_ITEMS_PLACEHOLDER} ,item_classification_42

    ) t1
) t0
) t
where
{CATALOG_FILTER_PLACEHOLDER}
