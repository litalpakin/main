----new_items_launche_period_measures_group_filter----

with selected_store_agg_2 as 
( ---------------------------------------------------------distribution fix-------------------------------------
	SELECT count(distinct case when sales_store_week0>0 then dw_store_key end) count_stores_week0
	,count(distinct case when sales_store_week1>0 then dw_store_key end) count_stores_week1
	,count(distinct case when sales_store_week2>0 then dw_store_key end) count_stores_week2
	,count(distinct case when (coalesce(sales_store_week0,0)+coalesce(sales_store_week1,0)+coalesce(sales_store_week2,0))>0 then dw_store_key end) count_stores_3weeks

	FROM migvan.assortment_store_indices 
	where  {STORES_FILTER_PLACEHOLDER}
)

, {SELECTED_STORE_AGG}

select *
			--,case when coalesce(sales_selectedstores_week0_17,0) <= 0 or coalesce(sales_subcatinselectedstores_week0_8,0) <= 0 then null else (sales_subcatinselectedstores_week0_8/sales_selectedstores_week0_17) end salesms_subcatinselectedstores_week0_54
			--,case when coalesce(sales_selectedstores_week1_18,0) <= 0 or coalesce(sales_subcatinselectedstores_week1_9,0) <= 0 then null else (sales_subcatinselectedstores_week1_9/sales_selectedstores_week1_18) end salesms_subcatinselectedstores_week1_55
			--,case when coalesce(sales_selectedstores_week2_19,0) <= 0 or coalesce(max(sales_subcat_format_week2),0) <= 0 then null else (max(sales_subcat_format_week2)/sales_selectedstores_week2_19) end salesms_subcatinselectedstores_week2_56
			--,case when salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 <=1.3  then 1
			--     when salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 > 1.3 and salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 <= 2 then 2
			--      when salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 > 2  and salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 <= 3 then 3
			--      when salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 > 3 and salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115 <= 4 then 4 else 5 end salesmsdiversity_iteminselectedstores_week0_117
			,case when coalesce(distribution_iteminselectedstores_week1_111,0) <= 0 or coalesce(distribution_iteminselectedstores_week0_110,0) <= 0 then null else (distribution_iteminselectedstores_week0_110/distribution_iteminselectedstores_week1_111)-1 end distribution_change_iteminselectedstores_week0to1_113
			,case when coalesce(distribution_iteminselectedstores_week2_112,0) <= 0 or coalesce(distribution_iteminselectedstores_week1_111,0) <= 0 then null else (distribution_iteminselectedstores_week1_111/distribution_iteminselectedstores_week2_112)-1 end distribution_change_iteminselectedstores_week1to2_114
			,case when salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 <=1.3  then 1
			      when salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 > 1.3 and salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 <= 2 then 2
			      when salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 > 2  and salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 <= 3 then 3
			      when salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 > 3 and salesmsdiff_iteminsubcat_leadingtochainavg_week0_116 <= 4 then 4 else 5 end salesmsdiversity_iteminsubcat_week0_118

from (
          SELECT dw_item_key,ni.sub_category_key
           ,ni.category_key, ni.sub_brand_hierarchy_2_key
            ,class_key, supplier_key, ni.format_code
            ,general_status_key, general_status_name,is_united_dw_item_key,is_private_label

            ,sum(qtystore_iteminformat_week0_20) qty_iteminselectedstores_week0_28
            ,sum(qtystore_iteminformat_week1_21) qty_iteminselectedstores_week1_29
            ,sum(qtystore_iteminformat_week2_22) qty_iteminselectedstores_week2_30
            ,sum(qtystore_iteminformat_ss_week0) qty_iteminselectedstores_ss_week0
            ,sum(qtystore_iteminformat_ss_week1) qty_iteminselectedstores_ss_week1
            ,sum(qtystore_iteminformat_ss_week2) qty_iteminselectedstores_ss_week2

            ,max(qty_sales_subcat_format_week0) qty_subcatinselectedstores_week0_31
            ,max(qty_sales_subcat_format_week1) qty_subcatinselectedstores_week1_32
            ,max(qty_sales_subcat_format_week2) qty_subcatinselectedstores_week2_33
            ,max(qty_sales_category_format_week0) qty_categoryinselectedstores_week0_34
            ,max(qty_sales_category_format_week1) qty_categoryinselectedstores_week1_35
            ,max(qty_sales_category_format_week2) qty_categoryinselectedstores_week2_36

			,case when coalesce(sum(qtystore_iteminformat_week1_21),0) <= 0 or coalesce(sum(qtystore_iteminformat_week0_20),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week0_20) as double precision)/nullif(sum(qtystore_iteminformat_week1_21),0))-1 end qtychange_iteminselectedstores_week0to1_37
			,case when coalesce(sum(qtystore_iteminformat_week2_22),0) <= 0 or coalesce(sum(qtystore_iteminformat_week1_21),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week1_21) as double precision)/nullif(sum(qtystore_iteminformat_week2_22),0))-1 end qtychange_iteminselectedstores_week1to2_38
			,case when coalesce(sum(qtystore_iteminformat_week2_22),0) <= 0 or coalesce(sum(qtystore_iteminformat_week0_20),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week0_20) as double precision)/nullif(sum(qtystore_iteminformat_week2_22),0))-1 end qtychange_iteminselectedstores_week0to2

			,case when coalesce(max(qty_sales_subcat_format_week0),0) <= 0 or coalesce(sum(qtystore_iteminformat_week0_20),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week0_20) as double precision)/nullif(max(qty_sales_subcat_format_week0),0)) end qtyms_iteminsubcat_week0
			,case when coalesce(max(qty_sales_subcat_format_week1),0) <= 0 or coalesce(sum(qtystore_iteminformat_week1_21),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week1_21) as double precision)/nullif(max(qty_sales_subcat_format_week1),0)) end qtyms_iteminsubcat_week1
			,case when coalesce(max(qty_sales_subcat_format_week2),0) <= 0 or coalesce(sum(qtystore_iteminformat_week2_22),0) <= 0 then null else (cast(sum(qtystore_iteminformat_week2_22) as double precision)/nullif(max(qty_sales_subcat_format_week2),0)) end qtyms_iteminsubcat_week2

			,case when coalesce(max(qty_sales_subcat_format_week1),0) <= 0 or coalesce(sum(qtystore_iteminformat_week1_21),0) <= 0 or coalesce(max(qty_sales_subcat_format_week0),0) <= 0 or coalesce(sum(qtystore_iteminformat_week0_20),0) <= 0
			then null else ((cast(sum(qtystore_iteminformat_week0_20) as double precision)/nullif(max(qty_sales_subcat_format_week0),0))/(nullif(cast(sum(qtystore_iteminformat_week1_21) as double precision)/nullif(max(qty_sales_subcat_format_week1),0),0)))-1 end qtymschange_iteminsubcat_week0to1
			,case when coalesce(max(qty_sales_subcat_format_week2),0) <= 0 or coalesce(sum(qtystore_iteminformat_week2_22),0) <= 0 or coalesce(max(qty_sales_subcat_format_week0),0) <= 0 or coalesce(sum(qtystore_iteminformat_week0_20),0) <= 0
			then null else ((cast(sum(qtystore_iteminformat_week0_20) as double precision)/nullif(max(qty_sales_subcat_format_week0),0))/(nullif(cast(sum(qtystore_iteminformat_week2_22) as double precision)/nullif(max(qty_sales_subcat_format_week2),0),0)))-1 end qtymschange_iteminsubcat_week0to2


         -- ,case when max(num_store_pos_sales_week0_140)>0 then cast(count(distinct case when salesstore_iteminformat_week0_5>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_week0_140),0) end  distribution_iteminselectedstores_week0_110
         -- ,case when max(num_store_pos_sales_week1_141)>0 then cast(count(distinct case when salesstore_iteminformat_week1_6>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_week1_141),0) end  distribution_iteminselectedstores_week1_111
         -- ,case when max(num_store_pos_sales_week2_142)>0 then cast(count(distinct case when salesstore_iteminformat_week2_7>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_week2_142),0) end   distribution_iteminselectedstores_week2_112
         -- ,case when max(num_store_pos_sales_3weeks)>0 then cast(count(distinct case when salesstore_iteminformat_3weeks>0 then dw_store_key end) as double precision)/nullif(max(num_store_pos_sales_3weeks),0) end  distribution_iteminselectedstores_3weeks

		 		  			---------------------------------------------------------distribution fix 250819-------------------------------------
		
		  ,case when max(count_selectedstores_week0)>0 then cast(count(distinct case when salesstore_iteminformat_week0_5>0 then dw_store_key end) as double precision)/max(count_selectedstores_week0) end  distribution_iteminselectedstores_week0_110
		  ,case when max(count_selectedstores_week1)>0 then cast(count(distinct case when salesstore_iteminformat_week1_6>0 then dw_store_key end) as double precision)/max(count_selectedstores_week1) end  distribution_iteminselectedstores_week1_111
	   	  ,case when max(count_selectedstores_week2)>0 then cast(count(distinct case when salesstore_iteminformat_week2_7>0 then dw_store_key end) as double precision)/max(count_selectedstores_week2) end   distribution_iteminselectedstores_week2_112
		  ,case when max(count_selectedstores_3weeks)>0 then cast(count(distinct case when salesstore_iteminformat_3weeks>0 then dw_store_key end) as double precision)/max(count_selectedstores_3weeks) end  distribution_iteminselectedstores_3weeks
			
		 
          ,case when coalesce(sum(qtystore_iteminformat_ss_week1),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_week0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_week0) as double precision)/nullif(sum(qtystore_iteminformat_ss_week1),0))-1 end qtychange_iteminselectedstores_ss_week0to1
          ,case when coalesce(sum(qtystore_iteminformat_ss_week2),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_week1),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_week1) as double precision)/nullif(sum(qtystore_iteminformat_ss_week2),0))-1 end qtychange_iteminselectedstores_ss_week1to2
          ,case when coalesce(sum(qtystore_iteminformat_ss_week2),0) <= 0 or coalesce(sum(qtystore_iteminformat_ss_week0),0) <= 0 then null else (cast(sum(qtystore_iteminformat_ss_week0) as double precision)/nullif(sum(qtystore_iteminformat_ss_week2),0))-1 end qtychange_iteminselectedstores_ss_week0to2

            {MAX_RETAILER_CATALOG_PLACEHOLDER}
            ,max(is_special_new_item) is_special_item

            ,launches_week_1
            ,total_weeks_since_launche_2
            ,item_format_classification_3
            ,is_item_classification_equal_for_all_format_4
            ,is_4weeks_after_launche_date_164

            ,max(sales_brand_subcat_ss_week0) sales_brandinsubcat_ss_week0
            ,max(sales_brand_subcat_ss_week1) sales_brandinsubcat_ss_week1
            ,max(sales_brand_subcat_ss_week2) sales_brandinsubcat_ss_week2

          ,case when coalesce(max(sales_brand_subcat_ss_week1),0) <= 0 or coalesce(max(sales_brand_subcat_ss_week0),0) <= 0 then null else (max(sales_brand_subcat_ss_week0)/nullif(max(sales_brand_subcat_ss_week1),0))-1 end saleschange_brandinsubcat_ss_week0to1
          ,case when coalesce(max(sales_brand_subcat_ss_week2),0) <= 0 or coalesce(max(sales_brand_subcat_ss_week1),0) <= 0 then null else (max(sales_brand_subcat_ss_week1)/nullif(max(sales_brand_subcat_ss_week2),0))-1 end saleschange_brandinsubcat_ss_week1to2

            ,max(salesstore_format_week0_17) sales_selectedstores_week0_17
            ,max(salesstore_format_week1_18) sales_selectedstores_week1_18
            ,max(salesstore_format_week2_19) sales_selectedstores_week2_19

          ,case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_category_format_week0),0) <= 0 then null else (max(sales_category_format_week0)/nullif(max(salesstore_format_week0_17),0)) end salesms_categoryinselectedstores_week0
          ,case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 then null else (max(sales_subcat_format_week0)/nullif(max(salesstore_format_week0_17),0)) end salesms_subcatinselectedstores_week0
			,case when ((max(salesmsmarket_categoryinmarket_week0)/nullif((case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_category_format_week0),0) <= 0 then null else (max(sales_category_format_week0)/nullif(max(salesstore_format_week0_17),0)) end),0))-1>=0.05)
			and ((max(salesmsmarket_subcatinmarket_week0)/nullif((case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 then null else (max(sales_subcat_format_week0)/nullif(max(salesstore_format_week0_17),0)) end),0))-1>=0.05) then 1 else 0 end is_pass_potential_ms_test

			,case when ((max(salesmsmarket_categoryinmarket_week0)/nullif((case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_category_format_week0),0) <= 0 then null else (max(sales_category_format_week0)/nullif(max(salesstore_format_week0_17),0)) end),0))-1>=0.05)
			 and ((max(salesmsmarket_subcatinmarket_week0)/nullif((case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 then null else (max(sales_subcat_format_week0)/nullif(max(salesstore_format_week0_17),0)) end),0))-1>=0.05)
			 and (case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0)) end) >0
			 and max(salesmsmarket_iteminsubcat_week0_47)>0 and max(sales_subcat_format_week0)>0
			 then ((max(salesmsmarket_iteminsubcat_week0_47)/nullif((case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/max(sales_subcat_format_week0)) end),0))-1)*max(sales_subcat_format_week0)
			 else null end sales_potential_market_to_chain_week0_186

            ,sum(salesstore_iteminformat_week0_5) sales_iteminselectedstores_week0_5
            ,sum(salesstore_iteminformat_week1_6) sales_iteminselectedstores_week1_6
            ,sum(salesstore_iteminformat_week2_7) sales_iteminselectedstores_week2_7
            ,sum(salesstore_iteminformat_ss_week0) sales_iteminselectedstores_ss_week0
            ,sum(salesstore_iteminformat_ss_week1) sales_iteminselectedstores_ss_week1
            ,sum(salesstore_iteminformat_ss_week2) sales_iteminselectedstores_ss_week2

            ,max(sales_category_format_week0) sales_categoryinselectedstores_week0_11
            ,max(sales_category_format_week1) sales_categoryinselectedstores_week1_12
            ,max(sales_category_format_week2) sales_categoryinselectedstores_week2_13

            ,case when coalesce(sum(salesstore_iteminformat_ss_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_week0),0) <= 0 then null else (sum(salesstore_iteminformat_ss_week0)/nullif(sum(salesstore_iteminformat_ss_week1),0))-1 end salechange_iteminselectedstores_ss_week0to1
            ,case when coalesce(sum(salesstore_iteminformat_ss_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_week1),0) <= 0 then null else (sum(salesstore_iteminformat_ss_week1)/nullif(sum(salesstore_iteminformat_ss_week2),0))-1 end salechange_iteminselectedstores_ss_week1to2
            ,case when coalesce(sum(salesstore_iteminformat_ss_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_ss_week0),0) <= 0 then null else (sum(salesstore_iteminformat_ss_week0)/nullif(sum(salesstore_iteminformat_ss_week2),0))-1 end salechange_iteminselectedstores_ss_week0to2

            ,case when coalesce(max(salesstore_format_week0_17),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(max(salesstore_format_week0_17),0)) end salesms_iteminselectedstores_week0_39
            ,case when coalesce(max(salesstore_format_week1_18),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 then null else (sum(salesstore_iteminformat_week1_6)/nullif(max(salesstore_format_week1_18),0)) end salesms_iteminselectedstores_week1_40
            ,case when coalesce(max(salesstore_format_week2_19),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 then null else (sum(salesstore_iteminformat_week2_7)/nullif(max(salesstore_format_week2_19),0)) end salesms_iteminselectedstores_week2_41

            ,case when coalesce(max(sales_category_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_category_format_week0),0)) end salesms_itemincategory_week0_45
            ,case when coalesce(max(sales_category_format_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 then null else (sum(salesstore_iteminformat_week1_6)/nullif(max(sales_category_format_week1),0)) end salesms_itemincategory_week1_46
            ,case when coalesce(max(sales_category_format_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 then null else (sum(salesstore_iteminformat_week2_7)/nullif(max(sales_category_format_week2),0)) end salesms_itemincategory_week2_47

            ,max(sales_brand_subcat_week0) sales_brandinsubcat_week0_14
            ,max(sales_brand_subcat_week1) sales_brandinsubcat_week1_15
            ,max(sales_brand_subcat_week2) sales_brandinsubcat_week2_16

			--,case when coalesce(sales_categoryinselectedstores_week1_12,0) <= 0 or coalesce(sales_categoryinselectedstores_week0_11,0) <= 0 then null else (sales_categoryinselectedstores_week0_11/sales_categoryinselectedstores_week1_12)-1 end saleschange_categoryinselectedstores_week0to1_24
			--,case when coalesce(sales_categoryinselectedstores_week2_13,0) <= 0 or coalesce(sales_categoryinselectedstores_week1_12,0) <= 0 then null else (sales_categoryinselectedstores_week1_12/sales_categoryinselectedstores_week2_13)-1 end saleschange_categoryinselectedstores_week1to2_25
			,case when coalesce(max(sales_brand_subcat_week1),0) <= 0 or coalesce(max(sales_brand_subcat_week0),0) <= 0 then null else (max(sales_brand_subcat_week0)/nullif(max(sales_brand_subcat_week1),0))-1 end saleschange_brandinsubcat_week0to1_26
			,case when coalesce(max(sales_brand_subcat_week2),0) <= 0 or coalesce(max(sales_brand_subcat_week1),0) <= 0 then null else (max(sales_brand_subcat_week1)/nullif(max(sales_brand_subcat_week2),0))-1 end saleschange_brandinsubcat_week1to2_27


            ,case when coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(sum(salesstore_iteminformat_week1_6),0))-1 end salechange_iteminselectedstores_week0to1_20
            ,case when coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 then null else (sum(salesstore_iteminformat_week1_6)/nullif(sum(salesstore_iteminformat_week2_7),0))-1 end salechange_iteminselectedstores_week1to2_21
            ,case when coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(sum(salesstore_iteminformat_week2_7),0))-1 end salechange_iteminselectedstores_week0to2

            ,max(sales_subcat_format_week0) sales_subcatinselectedstores_week0_8
            ,max(sales_subcat_format_week1) sales_subcatinselectedstores_week1_9
            ,max(sales_subcat_format_week2) sales_subcatinselectedstores_week2_10

			,case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(max(sales_brand_subcat_week0),0) <= 0 then null else (max(sales_brand_subcat_week0)/nullif(max(sales_subcat_format_week0),0)) end salesms_brandinsubcat_week0_51
			,case when coalesce(max(sales_subcat_format_week1),0) <= 0 or coalesce(max(sales_brand_subcat_week1),0) <= 0 then null else (max(sales_brand_subcat_week1)/nullif(max(sales_subcat_format_week1),0)) end salesms_brandinsubcat_week1_52
			,case when coalesce(max(sales_subcat_format_week2),0) <= 0 or coalesce(max(sales_brand_subcat_week2),0) <= 0 then null else (max(sales_brand_subcat_week2)/nullif(max(sales_subcat_format_week2),0)) end salesms_brandinsubcat_week2_53

          ---
          ,sum(weightstore_iteminformat_week0) weight_iteminselectedstores_week0
          ,sum(weightstore_iteminformat_week1) weight_iteminselectedstores_week1
          ,sum(weightstore_iteminformat_week2) weight_iteminselectedstores_week2

          --,case when sum(salesstore_iteminformat_week0_5)<=0 or sum(salesstore_iteminformat_week0_5) is null or sum(qtystore_iteminformat_week0_20)<=0 or sum(qtystore_iteminformat_week0_20) is null then null else sales_iteminselectedstores_week0_5/sum(qtystore_iteminformat_week0_20) end qty_price_item_iteminselectedstores_week0
          ,case when max(weighted)='True' and sum(weightstore_iteminformat_week0)=0   then null
                when max(weighted)='True' and sum(weightstore_iteminformat_week0)<>0  then (1000/nullif(sum(weightstore_iteminformat_week0),0))*sum(salesstore_iteminformat_week0_5)
              when sum(salesstore_iteminformat_week0_5)<=0 or sum(salesstore_iteminformat_week0_5) is null or sum(qtystore_iteminformat_week0_20)<=0 or sum(qtystore_iteminformat_week0_20) is null then null
              else sum(salesstore_iteminformat_week0_5)/nullif(sum(qtystore_iteminformat_week0_20),0) end qty_price_item_iteminselectedstores_week0

          ,case when max(weighted)='True' and sum(weightstore_iteminformat_week0)=0   then null
                when max(weighted)='True' and sum(weightstore_iteminformat_week0)<>0  then (((1000/nullif(sum(weightstore_iteminformat_week0),0))*sum(salesstore_iteminformat_week0_5))/nullif(max(qty_price_item_format_market_week0),0))-1
              when sum(salesstore_iteminformat_week0_5)<=0 or sum(salesstore_iteminformat_week0_5) is null or sum(qtystore_iteminformat_week0_20)<=0 or sum(qtystore_iteminformat_week0_20) is null then null
              else ((sum(salesstore_iteminformat_week0_5)/nullif(sum(qtystore_iteminformat_week0_20),0))/nullif(max(qty_price_item_format_market_week0),0))-1 end qty_price_change_iteminselectedstores_cahintomarket_week0

          --,case when (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))<=0 or (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)) is null or (sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22))<=0 or (sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22)) is null then null else (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/(sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22)) end qty_price_item_iteminselectedstores_3weeks
          ,case when max(weighted)='True' and (sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2))=0   then null
                when max(weighted)='True' and (sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2))<>0  then (1000/nullif((sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2)),0))*(sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))
                when (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))<=0 or (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)) is null or (sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22))<=0 or (sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22)) is null then null
                else (sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22)),0) end qty_price_item_iteminselectedstores_3weeks

          ,case when max(weighted)='True' and (sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2))=0
             or coalesce(max(qty_price_item_format_market_3weeks),0)<=0
              then null
              when max(weighted)='True' and coalesce(max(qty_price_item_format_market_3weeks),0)>0
              and (sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2))<>0
              then ((1000/nullif((sum(weightstore_iteminformat_week0)+sum(weightstore_iteminformat_week1)+sum(weightstore_iteminformat_week2)),0))*(sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif(max(qty_price_item_format_market_3weeks),0))-1
              when coalesce(sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7),0)<=0
              or coalesce(sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22),0)<=0
              or coalesce(max(qty_price_item_format_market_3weeks),0)<=0
              then null
              else ((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((sum(qtystore_iteminformat_week0_20)+sum(qtystore_iteminformat_week1_21)+sum(qtystore_iteminformat_week2_22)),0)/nullif(max(qty_price_item_format_market_3weeks),0))-1  end qty_price_change_iteminselectedstores_cahintomarket_3weeks



          ,case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0)) end salesms_iteminsubcat_week0_42
          ,case when coalesce(max(sales_subcat_format_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 then null else (sum(salesstore_iteminformat_week1_6)/nullif(max(sales_subcat_format_week1),0)) end salesms_iteminsubcat_week1_43
          ,case when coalesce(max(sales_subcat_format_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 then null else (sum(salesstore_iteminformat_week2_7)/nullif(max(sales_subcat_format_week2),0)) end salesms_iteminsubcat_week2_44
          ,case when coalesce((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)),0) <= 0 or coalesce(max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2),0) <= 0 then null
          else ((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2)),0)) end salesms_iteminsubcat_3weeks

            ,case when coalesce(max(sales_brand_subcat_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_brand_subcat_week0),0)) end salesms_iteminbrand_week0_48
            ,case when coalesce(max(sales_brand_subcat_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0 then null else (sum(salesstore_iteminformat_week1_6)/nullif(max(sales_brand_subcat_week1),0)) end salesms_iteminbrand_week1_49
            ,case when coalesce(max(sales_brand_subcat_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 then null else (sum(salesstore_iteminformat_week2_7)/nullif(max(sales_brand_subcat_week2),0)) end salesms_iteminbrand_week2_50

            ,max(salesmsmarket_iteminbrand_week0_53) salesmsmarket_iteminbrand_week0_95
            ,max(salesmsmarket_itemincategory_week0_50) salesmsmarket_itemincategory_week0_92

			,case when coalesce(max(salesmsmarket_iteminsubcat_week0_47),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			then null else ((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0))/nullif(max(salesmsmarket_iteminsubcat_week0_47),0))-1 end salesmschange_iteminsubcat_cahintomarket_week0_107
			,case when coalesce(max(salesmsmarket_iteminbrand_week0_53),0) <= 0 or coalesce(max(sales_brand_subcat_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			then null else ((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_brand_subcat_week0),0))/nullif(max(salesmsmarket_iteminbrand_week0_53),0))-1 end salesmschange_iteminbrand_cahintomarket_week0_108
			,case when coalesce(max(salesmsmarket_itemincategory_week0_50),0) <= 0 or coalesce(max(sales_category_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			 then null else ((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_category_format_week0),0))/nullif(max(salesmsmarket_itemincategory_week0_50),0))-1 end salesmschange_itemincategory_cahintomarket_week0_109

			,case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0  
			then 0-max(salesmsmarket_iteminsubcat_week0_47)
				when max(salesmsmarket_iteminsubcat_week0_47) is null then (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0))-0
				else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0))-max(salesmsmarket_iteminsubcat_week0_47) end salesmsdiff_iteminsubcat_cahintomarket_week0_104
			,case when coalesce(max(sales_brand_subcat_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then 0-max(salesmsmarket_iteminbrand_week0_53)
				when max(salesmsmarket_iteminbrand_week0_53) is null then (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_brand_subcat_week0),0))-0 else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_brand_subcat_week0),0))-max(salesmsmarket_iteminbrand_week0_53) end salesmsdiff_iteminbrand_cahintomarket_week0_105
			,case when coalesce(max(sales_category_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			 then 0-max(salesmsmarket_itemincategory_week0_50)
				when max(salesmsmarket_itemincategory_week0_50) is null then (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_category_format_week0),0))-0
				else (sum(salesstore_iteminformat_week0_5)/nullif(max(sales_category_format_week0),0))-max(salesmsmarket_itemincategory_week0_50) end salesmsdiff_itemincategory_cahintomarket_week0_106

			,case when coalesce(max(sales_subcat_format_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0  or coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			 then null else ((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0))/nullif((sum(salesstore_iteminformat_week1_6)/nullif(max(sales_subcat_format_week1),0)),0))-1 end salesmschange_iteminsubcat_week0to1_57
			,case when coalesce(max(sales_subcat_format_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 or coalesce(max(sales_subcat_format_week1),0) <= 0 or coalesce(sum(salesstore_iteminformat_week1_6),0) <= 0
			then null else ((sum(salesstore_iteminformat_week1_6)/nullif(max(sales_subcat_format_week1),0))/nullif((sum(salesstore_iteminformat_week2_7)/max(sales_subcat_format_week2)),0))-1 end salesmschange_iteminsubcat_week1to2_58
			,case when coalesce(max(sales_subcat_format_week2),0) <= 0 or coalesce(sum(salesstore_iteminformat_week2_7),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0
			 then null else ((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0))/nullif((sum(salesstore_iteminformat_week2_7)/nullif(max(sales_subcat_format_week2),0)),0))-1 end salesmschange_iteminsubcat_week0to2


			,case when (case when coalesce((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)),0) <= 0 or coalesce(max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2),0) <= 0 then null
          else ((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2)),0)) end )>0
          and max(salesmsmarket_iteminsubcat_3weeks)>0
			and (max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2))>0
			and max(salesmsmarket_iteminsubcat_3weeks)-
			(case when coalesce((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)),0) <= 0 or coalesce(max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2),0) <= 0 then null
          else ((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2)),0)) end )>0
			then (max(salesmsmarket_iteminsubcat_3weeks)-
			(case when coalesce((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7)),0) <= 0 or coalesce(max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2),0) <= 0 then null
          else ((sum(salesstore_iteminformat_week0_5)+sum(salesstore_iteminformat_week1_6)+sum(salesstore_iteminformat_week2_7))/nullif((max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2)),0)) end ))*(max(sales_subcat_format_week0)+max(sales_subcat_format_week1)+max(sales_subcat_format_week2)) else null end inc_sales_by_iteminsubcat_salesms_3weeks

          ,case when coalesce(max(sales_subcat_format_week1),0) <= 0 or coalesce(max(sales_subcat_format_week0),0) <= 0 then null else (max(sales_subcat_format_week0)/nullif(max(sales_subcat_format_week1),0))-1 end saleschange_subcatinselectedstores_week0to1_22
          ,case when coalesce(max(sales_subcat_format_week2),0) <= 0 or coalesce(max(sales_subcat_format_week1),0) <= 0 then null else (max(sales_subcat_format_week1)/nullif(max(sales_subcat_format_week2),0))-1 end saleschange_subcatinselectedstores_week1to2_23

            ,max(sales_subcat_format_ss_week0) sales_subcatinselectedstores_ss_week0
            ,max(sales_subcat_format_ss_week1) sales_subcatinselectedstores_ss_week1
            ,max(sales_subcat_format_ss_week2) sales_subcatinselectedstores_ss_week2

            ,case when coalesce(max(sales_subcat_format_ss_week1),0) <= 0 or coalesce(max(sales_subcat_format_ss_week0),0) <= 0 then null else (max(sales_subcat_format_ss_week0)/nullif(max(sales_subcat_format_ss_week1),0))-1 end saleschange_subcatinselectedstores_ss_week0to1
            ,case when coalesce(max(sales_subcat_format_ss_week2),0) <= 0 or coalesce(max(sales_subcat_format_ss_week1),0) <= 0 then null else (max(sales_subcat_format_ss_week1)/nullif(max(sales_subcat_format_ss_week2),0))-1 end saleschange_subcatinselectedstores_ss_week1to2


            ,max(sales_iteminformat_week0_92) sales_iteminallformat_week0
            ,max(sales_subcatinformat_week0_95) sales_subcatallformat_week0

			,case when (max(salesmsmarket_iteminsubcat_week0_47)-
			(case when coalesce(max(sales_iteminformat_week0_92),0) <= 0 or coalesce(max(sales_subcatinformat_week0_95),0) <= 0 then null else (max(sales_iteminformat_week0_92)/max(sales_subcatinformat_week0_95)) end))<0  then null
			  else (max(salesmsmarket_iteminsubcat_week0_47)-(case when coalesce(max(sales_iteminformat_week0_92),0) <= 0 or coalesce(max(sales_subcatinformat_week0_95),0) <= 0 then null else (max(sales_iteminformat_week0_92)/max(sales_subcatinformat_week0_95)) end))*max(sales_iteminformat_week0_92) end item_format_inc_in_chain_week0

  			  ,case when coalesce(max(sales_iteminformat_week0_92),0) <= 0 or coalesce(max(sales_subcatinformat_week0_95),0) <= 0 then null else (max(sales_iteminformat_week0_92)/max(sales_subcatinformat_week0_95)) end salesms_iteminsubcat_allformat_week0


            ,max(salesmstrend_iteminsubcat_week0to2_29) salesmstrend_iteminsubcat_week0and2_59
            ,max(salesmsavgtop20percentile_iteminformat_week0_30) salesmsavgtop20percentile_iteminformat_week0_60
            --,max(salesmsavgtop20percentile_iteminsubcat_week0_31) salesmsavgtop20percentile_iteminsubcat_week0_61

            --,case when salesms_iteminselectedstores_week0_39 is null then 0 else dense_rank() over (partition by format_code order by salesms_iteminselectedstores_week0_39 desc) end salesrank_iteminformat_week0_62
            ,max(salesrank_iteminformat_week0_168) salesrank_iteminformat_week0_62
            ,max(items_count_format_week0_116) salesmaxrank_iteminformat_week0_63
            ,case when max(salesrank_iteminformat_week0_168)<>0 then max(salesrank_iteminformat_week0_168)||' / '||max(items_count_format_week0_116) else null end as salesranking_iteminformat_week0_64
              ,max(salesrank_iteminsubcat_week0_166) salesrank_iteminsubcat_week0_65
            ,max(items_count_subcatinformat_week0_114) salesmaxrank_iteminsubcat_week0_66
            ,case when max(salesrank_iteminsubcat_week0_166)<>0 then max(salesrank_iteminsubcat_week0_166)||' / '||max(items_count_subcatinformat_week0_114) else null end as salesranking_iteminsubcat_week0_67

            ,max(salesrank_iteminsubcat_week1_171) salesrank_iteminsubcat_week1_187
            ,max(items_count_subcatinformat_week1_170) salesmaxrank_iteminsubcat_week1_188
            ,case when max(salesrank_iteminsubcat_week1_171)<>0 then max(salesrank_iteminsubcat_week1_171)||' / '||max(items_count_subcatinformat_week1_170) else null end as salesranking_iteminsubcat_week1_189

            ,max(salesrank_iteminbrand_week0_167) salesrank_iteminbrand_week0_68
            ,max(items_count_brandinsubcat_week0_113) salesmaxrank_iteminbrand_week0_69
            ,case when max(salesrank_iteminbrand_week0_167)<>0 then max(salesrank_iteminbrand_week0_167)||' / '||max(items_count_brandinsubcat_week0_113) else null end as salesranking_iteminbrand_week0_70
            ,max(salesrank_itemincategory_week0_165) salesrank_itemincategory_week0_71
            ,max(items_count_categoryinformat_week0_115) salesmaxrank_itemincategory_week0_72
            ,case when max(salesrank_itemincategory_week0_165)<>0 then max(salesrank_itemincategory_week0_165)||' / '||max(items_count_categoryinformat_week0_115) else null end as salesranking_itemincategory_week0_73

            ,max(salesmarket_item_week0_32) salesmarket_item_week0
            ,max(salesmarket_item_week1_33) salesmarket_item_week1
            ,case when coalesce(max(salesmarket_item_week1_33),0) <= 0 or coalesce(max(salesmarket_item_week0_32),0) <= 0 then null else (max(salesmarket_item_week0_32)/max(salesmarket_item_week1_33))-1 end salechange_market_iteminmarket_week0to1

            ,max(salesmarketranking_iteminsubcat_week0_61) salesmarketranking_iteminsubcat_week0_101
            ,max(salesmarketranking_itemincategory_week0_64) salesmarketranking_itemincategory_week0_102
            ,max(salesmarketranking_iteminmarket_week0_67) salesmarketranking_iteminmarket_week0_103

            ,max(total_sales_inc_allstores_group_a_3weeks) total_sales_inc_allstores_group_a_3weeks

            ,max(salesmsmarket_iteminsubcat_week0_47) salesmsmarket_iteminsubcat_week0_89
            ,max(salesmsmarket_iteminsubcat_week1_48) salesmsmarket_iteminsubcat_week1_90
            ,max(salesmsmarket_iteminsubcat_week2_49) salesmsmarket_iteminsubcat_week2_91

            ,max(salesmsmarket_iteminbrand_week1_54) salesmsmarket_iteminbrand_week1_96
            ,max(salesmsmarket_iteminbrand_week2_55) salesmsmarket_iteminbrand_week2_97
            ,max(salesmsmarket_iteminmarket_week0_56) salesmsmarket_iteminmarket_week0_98


            ,max(salesmschange_market_iteminsubcat_week0to1_172) salesmschange_market_iteminsubcat_week0to1_190
            ,max(salesmschange_market_iteminsubcat_week0to2_173) salesmschange_market_iteminsubcat_week0to2_191

            ,avg(item_format_availability_in_store_3weeks) avg_availability_item_format_in_selectedstores_3weeks
            ,max(avgavailability_stores_group_a_item_format_3weeks) avgavailability_stores_group_a_item_format_3weeks
            ,max(avgavailability_stores_group_b_item_format_3weeks) avgavailability_stores_group_b_item_format_3weeks
            ,max(avgavailability_stores_group_c_item_format_3weeks) avgavailability_stores_group_c_item_format_3weeks
            ,max(avgavailability_ratio_stores_group_a_to_c_item_format_3weeks)  avgavailability_ratio_stores_group_a_to_c_item_format_3weeks
            ,max(avgsalesms_ratio_stores_group_a_to_c_item_format_3weeks) avgsalesms_ratio_stores_group_a_to_c_item_format_3weeks
            ,max(item_format_sales_potential_inc_3weeks) item_format_sales_potential_inc_3weeks

            ,max(avgsales_stores_group_a_item_format_week0_128) avgsales_stores_group_a_item_format_week0_159
            ,max(avgsales_stores_group_b_item_format_week0_129) avgsales_stores_group_b_item_format_week0_160
            ,max(avgsales_stores_group_c_item_format_week0_130) avgsales_stores_group_c_item_format_week0_161

            ,max(avgsales_stores_group_a_item_format_week1_131) avgsales_stores_group_a_item_format_week1_162
            ,max(avgsales_stores_group_b_item_format_week1_132) avgsales_stores_group_b_item_format_week1_163
            ,max(avgsales_stores_group_c_item_format_week1_133) avgsales_stores_group_c_item_format_week1_164

			,case when coalesce(max(avgsales_stores_group_a_item_format_week1_131),0) <= 0 or coalesce(max(avgsales_stores_group_a_item_format_week0_128),0) <= 0
			then null else (max(avgsales_stores_group_a_item_format_week0_128)/nullif(max(avgsales_stores_group_a_item_format_week1_131),0))-1 end avgsaleschange_stores_group_a_iteminformat_week0to1
			,case when coalesce(max(avgsales_stores_group_c_item_format_week1_133),0) <= 0 or coalesce(max(avgsales_stores_group_c_item_format_week0_130),0) <= 0
			then null else (max(avgsales_stores_group_c_item_format_week0_130)/nullif(max(avgsales_stores_group_c_item_format_week1_133),0))-1 end avgsaleschange_stores_group_c_iteminformat_week0to1

            ,max(avgsales_stores_group_a_item_format_week2_134) avgsales_stores_group_a_item_format_week2_165
            ,max(avgsales_stores_group_b_item_format_week2_135) avgsales_stores_group_b_item_format_week2_166
            ,max(avgsales_stores_group_c_item_format_week2_136) avgsales_stores_group_c_item_format_week2_167

            ,max(avgsalesms_group_a_iteminsubcatformat_week0_155) avgsalesms_group_a_iteminsubcatformat_week0_168
            ,max(avgsalesms_group_b_iteminsubcatformat_week0_156) avgsalesms_group_b_iteminsubcatformat_week0_169
            ,max(avgsalesms_group_c_iteminsubcatformat_week0_157) avgsalesms_group_c_iteminsubcatformat_week0_170

			--,((1+salesms_iteminselectedstores_week0_39)/(1+max(avgsalesms_group_a_iteminsubcatformat_week0_155)))-1  salesmsdiff_iteminselectedstores_leadingtochainavg_week0_115
			,case when coalesce(max(sales_subcat_format_week0),0) <= 0 or coalesce(sum(salesstore_iteminformat_week0_5),0) <= 0 then null
			else ((1+((sum(salesstore_iteminformat_week0_5)/nullif(max(sales_subcat_format_week0),0)) ))/nullif((1+max(avgsalesms_group_a_iteminsubcatformat_week0_155)),0))-1 end salesmsdiff_iteminsubcat_leadingtochainavg_week0_116


            ,max(avgsalesms_group_a_iteminsubcatformat_week1_158) avgsalesms_group_a_iteminsubcatformat_week1_171
            ,max(avgsalesms_group_b_iteminsubcatformat_week1_159) avgsalesms_group_b_iteminsubcatformat_week1_172
            ,max(avgsalesms_group_c_iteminsubcatformat_week1_160) avgsalesms_group_c_iteminsubcatformat_week1_173

            ,max(avgsalesms_group_a_iteminsubcatformat_week2_161) avgsalesms_group_a_iteminsubcatformat_week2_174
            ,max(avgsalesms_group_b_iteminsubcatformat_week2_162) avgsalesms_group_b_iteminsubcatformat_week2_175
            ,max(avgsalesms_group_c_iteminsubcatformat_week2_163) avgsalesms_group_c_iteminsubcatformat_week2_176

            ,max(avgsalesms_group_a_iteminsubcatformat_3weeks) avgsalesms_group_a_iteminsubcatformat_3weeks
            ,max(avgsalesms_group_b_iteminsubcatformat_3weeks) avgsalesms_group_b_iteminsubcatformat_3weeks
            ,max(avgsalesms_group_c_iteminsubcatformat_3weeks) avgsalesms_group_c_iteminsubcatformat_3weeks

            ,max(avgqty_stores_group_a_item_format_week0_146) avgqty_stores_group_a_item_format_week0_177
            ,max(avgqty_stores_group_b_item_format_week0_147) avgqty_stores_group_b_item_format_week0_178
            ,max(avgqty_stores_group_c_item_format_week0_148) avgqty_stores_group_c_item_format_week0_179

            ,max(avgqty_stores_group_a_item_format_week1_149) avgqty_stores_group_a_item_format_week1_180
            ,max(avgqty_stores_group_b_item_format_week1_150) avgqty_stores_group_b_item_format_week1_181
            ,max(avgqty_stores_group_c_item_format_week1_151) avgqty_stores_group_c_item_format_week1_182

            ,max(avgqty_stores_group_a_item_format_week2_152) avgqty_stores_group_a_item_format_week2_183
            ,max(avgqty_stores_group_b_item_format_week2_153) avgqty_stores_group_b_item_format_week2_184
            ,max(avgqty_stores_group_c_item_format_week2_154) avgqty_stores_group_c_item_format_week2_185

            ,max(avgsales_stores_group_a_item_format_ss_week0) avgsales_stores_group_a_item_format_ss_week0
            ,max(avgsales_stores_group_c_item_format_ss_week0) avgsales_stores_group_c_item_format_ss_week0

            ,max(avgsales_stores_group_a_item_format_ss_week1) avgsales_stores_group_a_item_format_ss_week1
            ,max(avgsales_stores_group_c_item_format_ss_week1) avgsales_stores_group_c_item_format_ss_week1

            ,case when coalesce(max(avgsales_stores_group_a_item_format_ss_week1),0) <= 0 or coalesce(max(avgsales_stores_group_a_item_format_ss_week0),0) <= 0 then null else (max(avgsales_stores_group_a_item_format_ss_week0)/nullif(max(avgsales_stores_group_a_item_format_ss_week1),0))-1 end avgsaleschange_stores_group_a_iteminformat_ss_week0to1
            ,case when coalesce(max(avgsales_stores_group_c_item_format_ss_week1),0) <= 0 or coalesce(max(avgsales_stores_group_c_item_format_ss_week0),0) <= 0 then null else (max(avgsales_stores_group_c_item_format_ss_week0)/nullif(max(avgsales_stores_group_c_item_format_ss_week1),0))-1 end avgsaleschange_stores_group_c_iteminformat_ss_week0to1


           -- ,max(avgsales_brandinsubcat_prelaunche_68) avgsales_brandinsubcat_prelaunche_119
           -- ,max(avgsales_subcatinformat_prelaunche_69) avgsales_subcatinformat_prelaunche_120
           -- ,max(avgsales_format_prelaunche_70) avgsales_format_prelaunche_121

            --    ,max(salesdiff_brand_prelaunchetoweek0_115) salesdiff_brand_prelaunchetoweek0_122
             --   ,max(salesdiff_subcat_prelaunchetoweek0_116) salesdiff_subcat_prelaunchetoweek0_123
             --   ,max(saleschange_brand_prelaunchetoweek0_117) saleschange_brand_prelaunchetoweek0_124

            --,max(saleschange_subcat_prelaunchetoweek0_118) saleschange_subcat_prelaunchetoweek0_125
           -- ,max(avgsalesms_brandinsubcat_prelaunche_119) avgsalesms_brandinsubcat_prelaunche_126
            --,max(avgsalesms_subcatinformat_prelaunche_120) avgsalesms_subcatinformat_prelaunche_127
            ,max(salesms_brandinsubcatformat_allformat_week0_113) salesms_brandinsubcatformat_allformat_week0_128
            ,max(salesms_subcatinformat_allformat_week0_114) salesms_subcatinformat_allformat_week0_129
           -- ,max(avgsalesmsdiff_brandinsubcat_prelaunchetoweek0_121) avgsalesmsdiff_brandinsubcat_prelaunchetoweek0_130
           -- ,max(avgsalesmsdiff_subcatinformat_prelaunchetoweek0_122) avgsalesmsdiff_subcatinformat_prelaunchetoweek0_131
           -- ,max(avgsalesmschange_brandinsubcat_prelaunchetoweek0_123) avgsalesmschange_brandinsubcat_prelaunchetoweek0_132
           -- ,max(avgsalesmschange_subcatinformat_prelaunchetoweek0_124) avgsalesmschange_subcatinformat_prelaunchetoweek0_133

            ,max(penetration_iteminsubcat_week0_71) penetration_iteminsubcat_week0_134
            ,max(penetration_iteminsubcat_week1_72) penetration_iteminsubcat_week1_135
            ,max(penetration_iteminsubcat_week2_73) penetration_iteminsubcat_week2_136
            ,max(penetrationchange_iteminsubcat_week0to1_74) penetrationchange_iteminsubcat_week0to1_137
            ,max(penetrationchange_iteminsubcat_week1to2_75) penetrationchange_iteminsubcat_week1to2_138
            ,max(avgsalesbasketsize_week0_76) avgsalesbasketsize_week0_139
            ,max(avgsalesbasketsize_week1_77) avgsalesbasketsize_week1_140
            ,max(avgsalesbasketsize_week2_78) avgsalesbasketsize_week2_141
            ,max(avgsalesbasketsize_week0to1_79) avgsalesbasketsize_week0to1_142
            ,max(avgsalesbasketsize_week1to2_80) avgsalesbasketsize_week1to2_143
            ,max(avgqtybasketsize_week0_81) avgqtybasketsize_week0_144
            ,max(avgqtybasketsize_week1_82) avgqtybasketsize_week1_145
            ,max(avgqtybasketsize_week2_83) avgqtybasketsize_week2_146
            ,max(avgqtybasketsize_week0to1_84) avgqtybasketsize_week0to1_147
            ,max(avgqtybasketsize_week1to2_85) avgqtybasketsize_week1to2_148
            ,max(repeatbuyingrank_week0_86) repeatbuyingrank_week0_149
            ,max(repeatbuyingrank_week1_87) repeatbuyingrank_week1_150
            ,max(repeatbuyingrank_week2_88) repeatbuyingrank_week2_151
            --,max(salesavgms_iteminsubcat_week0_89) salesavgms_iteminsubcat_week0_152
            --,max(salesavgms_iteminsubcat_week1_90) salesavgms_iteminsubcat_week1_153
            --,max(salesavgms_iteminsubcat_week2_91) salesavgms_iteminsubcat_week2_154

          ,max(qty_price_item_format_week0) qty_price_item_allformat_week0

          ,max(avgqty_price_item_stores_group_a_item_format_week0) avgqty_price_item_stores_group_a_item_format_week0
          ,max(avgqty_price_item_stores_group_b_item_format_week0) avgqty_price_item_stores_group_b_item_format_week0
          ,max(avgqty_price_item_stores_group_c_item_format_week0) avgqty_price_item_stores_group_c_item_format_week0

          ,max(qty_price_item_format_3weeks) qty_price_item_allformat_3weeks

          ,max(qty_price_item_format_market_week0) qty_price_item_format_market_week0
          ,max(qty_price_item_format_market_3weeks) qty_price_item_format_market_3weeks

          ,max(avgqty_price_item_stores_group_a_item_format_3weeks) avgqty_price_item_stores_group_a_item_format_3weeks
          ,max(avgqty_price_item_stores_group_b_item_format_3weeks) avgqty_price_item_stores_group_b_item_format_3weeks
          ,max(avgqty_price_item_stores_group_c_item_format_3weeks) avgqty_price_item_stores_group_c_item_format_3weeks

          FROM migvan.new_items_launche_period_measures ni left join selected_store_agg agg_inc on ni.sub_brand_hierarchy_2_key=agg_inc.sub_brand_hierarchy_2_key
          and  ni.sub_category_key=agg_inc.sub_category_key and  ni.category_key=agg_inc.category_key and  ni.format_code=agg_inc.format_code
          where item_format_classification_3 = :item_classification --- variable group filter
          and {PREFIX_STORES_FILTER_PLACEHOLDER}
          -- where item_format_classification_3 =  6 --- variable group filter
          -- and  ni.dw_store_key is not null and  ni.format_code in (1395515)
          group by  dw_item_key, ni.sub_category_key,  ni.category_key,  ni.sub_brand_hierarchy_2_key
          ,class_key,  supplier_key, ni.format_code, general_status_key, general_status_name,is_united_dw_item_key,is_private_label ,launches_week_1 ,total_weeks_since_launche_2
          ,item_format_classification_3,is_item_classification_equal_for_all_format_4,is_4weeks_after_launche_date_164
) d
WHERE {CATALOG_FILTER_PLACEHOLDER}
 -- supplier_key is not null  and sub_category_key is not null



