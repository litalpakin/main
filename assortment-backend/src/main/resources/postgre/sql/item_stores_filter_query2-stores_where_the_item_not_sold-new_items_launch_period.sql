----item_stores_filter_query2-stores_where_the_item_not_sold-new_items_launch_period----

with item_store_indexes as (
	select format_code,dw_store_key, max(sales_store_format_week0) sales_store_format_week0
	from migvan.assortment_item_store_indices
	where  format_code=:format_code
	and {FILTER_PLACEHOLDER}
	group by format_code,dw_store_key
)
select t.format_code,
--(select format_name from migvan.assortment_item_store_indices kk
--	where  kk.format_code=t.format_code limit 1) as format_name,
t.dw_store_key
--,(select store_name from migvan.assortment_item_store_indices kk
--	where  kk.dw_store_key=t.dw_store_key limit 1) as store_name
from item_store_indexes t
where sales_store_format_week0 is not null and dw_store_key not in (select dw_store_key
		from migvan.new_items_launche_period_measures
		where dw_item_key=:dw_item_key
			and format_code=:format_code
			and (salesstore_iteminformat_week0_5>0 or salesstore_iteminformat_week1_6>0 or salesstore_iteminformat_week2_7>0)
			and {FILTER_PLACEHOLDER}
		)
  {DW_STORE_KEY_PLACEHOLDER}


