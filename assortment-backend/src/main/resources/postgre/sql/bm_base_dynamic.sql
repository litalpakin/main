
-------------- bm_balance_model --------------

WITH data as (
    select
        /*s.category_key*/ {BALANCE_LEVEL_PLACEHOLDER} as loa
         , s.year_mat
         , s.base_sales
		 , s.is_related_to_customer
    from migvan.assortment_fact_rank_1_24_shorted s

    where
        /*s.sub_category_key is not null*/ {FORMAT_CODES_PLACEHOLDER}
        and {USER_PERMISSION_PLACEHOLDER}
        /*and s.format_code in (10001)*/  
      and (/*s.year_mat = 1*/ {PERIODS_FILTER_1} or /*s.year_mat = 2*/ {PERIODS_FILTER_2} )
     
      and s.base_sales > 0
)

select *
, (bssdde.bm_sum_sales_ms_diff_pos / NULLIF(bssdde.bm_sum_sales_ms_diff, 0)) * bssdde.bm_sum_sales_ms_change 					as bm_sales_ms_diff_pos_balance
, (bssdde.bm_sum_sales_ms_diff_neg / NULLIF(bssdde.bm_sum_sales_ms_diff, 0)) * bssdde.bm_sum_sales_ms_change 					as bm_sales_ms_diff_neg_balance
, case when bssdde.bm_sales_ms_diff >= 0 then bssdde.bm_sales_ms_diff else 0 end / NULLIF(bssdde.bm_sum_sales_ms_diff_pos,0) 	as bm_sum_sales_ms_diff_pos_balance_ms
, case when bssdde.bm_sales_ms_diff < 0 then bssdde.bm_sales_ms_diff else 0 end / NULLIF(bssdde.bm_sum_sales_ms_diff_neg,0)     as bm_sum_sales_ms_diff_neg_balance_ms

from
(
		select *
		, sum(case when bssdd.bm_sales_ms_diff > 0 then bssdd.bm_sales_ms_diff else 0 end) over () as bm_sum_sales_ms_diff_pos
		, sum(case when bssdd.bm_sales_ms_diff < 0 then bssdd.bm_sales_ms_diff else 0 end) over () as bm_sum_sales_ms_diff_neg
		
		from(
				
				select bssd.loa
					, bssd.bm_sales_0 		as bm_sales_cur
					, bssd.bm_sales_1 		as bm_sales_prev 
					, bssd.bm_sales_diff
					, bssd.bm_sum_sales_0	as bm_sum_sales_cur
					, bssd.bm_sum_sales_1	as bm_sum_sales_prev
					, bssd.bm_sales_change 
					, bssd.bm_sum_sales_diff
					, bssd.bm_sum_sales_change
					, bssd.bm_sum_sales_diff_pos
					, bssd.bm_sum_sales_diff_neg 
					, (bssd.bm_sum_sales_diff_pos / NULLIF(bssd.bm_sum_sales_diff, 0)) * bssd.bm_sum_sales_change as bm_sales_diff_pos_balance
					, (bssd.bm_sum_sales_diff_neg / NULLIF(bssd.bm_sum_sales_diff, 0)) * bssd.bm_sum_sales_change as bm_sales_diff_neg_balance
					, (case when bssd.bm_sales_diff >= 0 then bssd.bm_sales_diff else 0 end /
						NULLIF(bssd.bm_sum_sales_diff_pos, 0))                                                     as bm_sales_diff_pos_balance_ms
					, (case when bssd.bm_sales_diff < 0 then bssd.bm_sales_diff else 0 end /
						NULLIF(bssd.bm_sum_sales_diff_neg, 0))                                                     as bm_sales_diff_neg_balance_ms
					, bssd.bm_sales_ms_0	as bm_sales_ms_cur
					, bssd.bm_sales_ms_1	as bm_sales_ms_prev
					, bssd.bm_sales_ms_change
					, bssd.bm_sales_ms_diff
					, bssd.bm_sum_sales_ms_0	as bm_sum_sales_ms_cur
					, bssd.bm_sum_sales_ms_1	as bm_sum_sales_ms_prev
					, sum(bssd.bm_sales_ms_diff) over ()   as bm_sum_sales_ms_diff
					, (bssd.bm_sum_sales_ms_0 / NULLIF(bssd.bm_sum_sales_ms_1, 0)) - 1		as bm_sum_sales_ms_change
					
				
				from (
						select bss.loa
							, bss.bm_sales_0
							, bss.bm_sales_1
							, bss.bm_sales_diff
							, bss.bm_sum_sales_0
							, bss.bm_sum_sales_1
							, bss.bm_sales_change
							, bss.bm_sum_sales_diff
							, bss.bm_sum_sales_change
							, sum(case when bss.bm_sales_diff > 0 then bss.bm_sales_diff else 0 end) over () as bm_sum_sales_diff_pos
							, sum(case when bss.bm_sales_diff < 0 then bss.bm_sales_diff else 0 end) over () as bm_sum_sales_diff_neg
							, bss.bm_sales_ms_0
							, bss.bm_sales_ms_1
							, coalesce(bss.bm_sales_ms_0, 0) - coalesce(bss.bm_sales_ms_1, 0) 	as bm_sales_ms_diff
							, (bss.bm_sales_ms_0 / NULLIF(bss.bm_sales_ms_1, 0)) - 1    			as bm_sales_ms_change
							, sum(bss.bm_sales_ms_0)  over ()  									as bm_sum_sales_ms_0
							, sum(bss.bm_sales_ms_1)  over ()  									as bm_sum_sales_ms_1 
							
							
							
						from (
								select bs.loa
									, bs.bm_sales_0
									, bs.bm_sales_1
									, bs.bm_sales_diff
									, bs.bm_sum_sales_0
									, bs.bm_sum_sales_1
									, (bs.bm_sales_0 / NULLIF(bs.bm_sales_1, 0)) - 1         		as bm_sales_change
									, sum(bs.bm_sales_diff) over ()                          		as bm_sum_sales_diff
									, (bs.bm_sum_sales_0 / NULLIF(bs.bm_sum_sales_1, 0)) - 1		as bm_sum_sales_change
									, bs.bm_sum_sub_chain_sales_0/ NULLIF(bs.bm_sum_sales_0,0 ) 	as bm_sales_ms_0
									, bs.bm_sum_sub_chain_sales_1/ NULLIF(bs.bm_sum_sales_1,0 )     as bm_sales_ms_1
									
								from (
										select ss.loa
												, ss.sum_sales_0                                            as bm_sales_0
												, ss.sum_sales_1                                            as bm_sales_1
												, ss.sum_sub_chain_sales_0									as bm_sum_sub_chain_sales_0
												, ss.sum_sub_chain_sales_1									as bm_sum_sub_chain_sales_1
												, coalesce(ss.sum_sales_0, 0) - coalesce(ss.sum_sales_1, 0) as bm_sales_diff
												, sum(ss.sum_sales_0)  over ()                              as bm_sum_sales_0
												, sum(ss.sum_sales_1)  over ()                              as bm_sum_sales_1
										from (
													select d.loa
														, sum(case when d.year_mat = 1 and is_related_to_customer=1 then d.base_sales else 0 end) sum_sales_0
														, sum(case when d.year_mat = 2 and is_related_to_customer=1 then d.base_sales else 0 end) sum_sales_1
														, sum(case when d.year_mat = 1 and is_related_to_customer=0 then d.base_sales else 0 end) sum_sub_chain_sales_0
														, sum(case when d.year_mat = 2 and is_related_to_customer=0 then d.base_sales else 0 end) sum_sub_chain_sales_1
													from data  d
													group by d.loa
												) ss
										group by ss.loa, ss.sum_sales_0, ss.sum_sales_1, ss.sum_sub_chain_sales_0, ss.sum_sub_chain_sales_1
									) bs
								group by bs.loa, bs.bm_sales_0, bs.bm_sales_1, bs.bm_sales_diff,
										bs.bm_sum_sales_0,
										bs.bm_sum_sales_1,
										bs.bm_sum_sub_chain_sales_0,
										bs.bm_sum_sub_chain_sales_1
										
										
							) bss
						group by bss.loa, bss.bm_sales_0, bss.bm_sales_1, bss.bm_sales_diff, bss.bm_sum_sales_0, bss.bm_sum_sales_1,
								bss.bm_sales_change,
								bss.bm_sum_sales_diff,
								bss.bm_sum_sales_change,
								bss.bm_sales_ms_0,
								bss.bm_sales_ms_1
								
					) bssd
					
					group by bssd.loa, bssd.bm_sales_0, bssd.bm_sales_1, bssd.bm_sales_diff, bssd.bm_sum_sales_0, bssd.bm_sum_sales_1,
								bssd.bm_sales_change,
								bssd.bm_sum_sales_diff,
								bssd.bm_sum_sales_change,
								bssd.bm_sales_ms_0,
								bssd.bm_sales_ms_1,
								bssd.bm_sales_ms_change,
								bssd.bm_sales_ms_diff,
								bssd.bm_sum_sales_diff_pos ,
								bssd.bm_sum_sales_diff_neg,
								bm_sales_diff_pos_balance,
								bm_sales_diff_neg_balance,
								bm_sales_diff_pos_balance_ms,
								bm_sales_diff_neg_balance_ms,
								bssd.bm_sum_sales_ms_0,
								bssd.bm_sum_sales_ms_1
			) bssdd
		
		
			group by bssdd.loa, bssdd.bm_sales_cur, bssdd.bm_sales_prev, bssdd.bm_sales_diff, bssdd.bm_sum_sales_cur, bssdd.bm_sum_sales_prev,
								bssdd.bm_sales_change,
								bssdd.bm_sum_sales_diff,
								bssdd.bm_sum_sales_change,
								bssdd.bm_sales_ms_cur,
								bssdd.bm_sales_ms_prev,
								bssdd.bm_sales_ms_change,
								bssdd.bm_sales_ms_diff,
								bssdd.bm_sum_sales_diff_pos ,
								bssdd.bm_sum_sales_diff_neg,
								bssdd.bm_sales_diff_pos_balance,
								bssdd.bm_sales_diff_neg_balance,
								bssdd.bm_sales_diff_pos_balance_ms,
								bssdd.bm_sales_diff_neg_balance_ms,
								bssdd.bm_sum_sales_ms_cur,
								bssdd.bm_sum_sales_ms_prev,
								bssdd.bm_sales_ms_change,
								bssdd.bm_sales_ms_diff,
								bssdd.bm_sum_sales_ms_change,
								bssdd.bm_sum_sales_ms_diff
								
		) bssdde
		
		group by bssdde.loa, bssdde.bm_sales_cur, bssdde.bm_sales_prev, bssdde.bm_sales_diff, bssdde.bm_sum_sales_cur, bssdde.bm_sum_sales_prev,
								bssdde.bm_sales_change,
								bssdde.bm_sum_sales_diff,
								bssdde.bm_sum_sales_change,
								bssdde.bm_sales_ms_cur,
								bssdde.bm_sales_ms_prev,
								bssdde.bm_sales_ms_change,
								bssdde.bm_sales_ms_diff,
								bssdde.bm_sum_sales_diff_pos ,
								bssdde.bm_sum_sales_diff_neg,
								bssdde.bm_sales_diff_pos_balance,
								bssdde.bm_sales_diff_neg_balance,
								bssdde.bm_sales_diff_pos_balance_ms,
								bssdde.bm_sales_diff_neg_balance_ms,
								bssdde.bm_sum_sales_ms_cur,
								bssdde.bm_sum_sales_ms_prev,
								bssdde.bm_sales_ms_change,
								bssdde.bm_sales_ms_diff,
								bssdde.bm_sum_sales_ms_change,
								bssdde.bm_sum_sales_ms_diff,
								bssdde.bm_sum_sales_ms_diff_pos,
								bssdde.bm_sum_sales_ms_diff_neg
		