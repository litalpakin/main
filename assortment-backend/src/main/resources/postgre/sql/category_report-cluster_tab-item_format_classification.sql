
SELECT dw_item_key
,sub_brand_hierarchy_2_key, retailer_sub_brand_hierarchy_2_key
,sub_category_key, retailer_sub_category_key
,category_key, retailer_category_key
,class_key, retailer_class_key
,supplier_key , retailer_supplier_key
,format_code, item_format_type_classification_code, item_format_type_classification_name
,is_item_classification_equal_for_all_format
,general_status_key, general_status_name,is_united_dw_item_key,is_private_label

FROM migvan.assortment_item_format_indices
where item_format_type_classification_code not in (24,25,26)  
and dw_item_key in 
(
select dw_item_key
from migvan.assortment_item_format_indices
where item_format_type_classification_code not in (24,25,26) {FILTER_PLACEHOLDER}
)

 --and {FILTER_PLACEHOLDER}
 and {TIME_FILTER_FIELD} > 0
 

