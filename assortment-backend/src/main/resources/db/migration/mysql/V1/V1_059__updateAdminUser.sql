update user
set password                 = '$2a$10$4NfcFJZ0SEf0tzyPXHe2.ehlV4T0Ys3MXgMF.7.5q5GchQK1QNqaO',
    last_password_reset_date = now()
where username = 'admin@storenext.co.il';