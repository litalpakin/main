CREATE TABLE users_suppliers (
   user_id bigint(20) NOT NULL,
   supplier_id bigint(20) NOT NULL,
   sub_category_id bigint(20) NOT NULL,
   PRIMARY KEY (user_id, supplier_id, sub_category_id),
   CONSTRAINT FK_USERS_SUPPLIERS_USER_ID FOREIGN KEY (user_id) REFERENCES user (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE user ADD group_shop_id bigint(20) AFTER company_id;
ALTER TABLE user ADD CONSTRAINT FK_USER_GROUP_SHOP_ID FOREIGN KEY(group_shop_id) REFERENCES shop_group (id);




