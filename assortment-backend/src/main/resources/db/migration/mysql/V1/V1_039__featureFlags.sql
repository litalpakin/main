

update feature_configuration set feature = 'ITEM_FLAG' where feature = 'HIDE_ITEM_FLAG';
update feature_configuration set is_enabled = 1 where feature = 'ITEM_FLAG';
insert into feature_configuration (feature,is_enabled) values ('OCCASION_REPORT',1);
insert into feature_configuration (feature,is_enabled) values ('RETAILER_CATALOG_TYPE',1);

update feature_configuration set feature = 'RETAILER_STORES_DATA' where feature = 'HIDE_CLUSTERS_AND_STORES_NAMES';
update feature_configuration set is_enabled = 1 where feature = 'RETAILER_STORES_DATA';