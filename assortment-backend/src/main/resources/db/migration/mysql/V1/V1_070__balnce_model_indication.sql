ALTER TABLE user_function ADD COLUMN balance_model_access BIT(1) DEFAULT FALSE;


UPDATE user_function SET balance_model_access =
    CASE WHEN type = 'HEADQUARTERS_MANAGER' THEN 1
        WHEN type = 'CATALOG_MANAGER' THEN 1
        WHEN type = 'REGION_MANAGER' THEN 1
         WHEN type = 'CATALOG_REGION_MANAGER' THEN 1
        WHEN type = 'STORE_MANAGER' THEN 0
    ELSE balance_model_access END;
