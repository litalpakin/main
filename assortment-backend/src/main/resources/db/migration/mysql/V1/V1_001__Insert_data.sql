INSERT INTO company 
(id,create_date,update_date,name,enabled,type) VALUES(1,NOW(), NOW(),'Stornext',1,'ADMIN');


INSERT INTO USER (create_date, update_date,username, enabled, first_name, last_name, last_password_reset_date, password, authority,company_id) VALUES (NOW(), NOW(), 'admin@storenext.co.il', 1, 'Storenext', 'Admin', NOW(), '$2a$10$FTfokeJ9JTSPz3IYZfJ4ieIC7pGJb8CPaaIuJT/8ettVrfjkmXFTO', 'ADMIN',1);
