ALTER TABLE user_function ADD COLUMN type VARCHAR(256) DEFAULT NULL;

UPDATE user_function SET name=
    CASE WHEN name = 'Headquarters Manager' THEN '(1)Headquarters Manager'
        WHEN name = 'Catalog Manager' THEN '(1)Catalog Manager'
        WHEN name = 'Region Manager' THEN '(1)Region Manager'
        WHEN name = 'Store Manager' THEN '(1)Store Manager'
    ELSE name END;

INSERT INTO user_function(create_date, update_date, name,all_stores_support,area_manager,company_type,full_catalog_support,item_classificator, is_default, type)
VALUES(NOW(), NOW(), 'Headquarters Manager', 1, 0, 'MAIN', 1, 0, 1, 'HEADQUARTERS_MANAGER'),
      (NOW(), NOW(), 'Catalog Manager', 1, 0, 'MAIN', 0, 1, 1, 'CATALOG_MANAGER'),
      (NOW(), NOW(), 'Region Manager', 0, 1, 'MAIN', 1, 0, 1, 'REGION_MANAGER'),
      (NOW(), NOW(), 'Store Manager', 0, 0, 'MAIN',1 ,0, 1, 'STORE_MANAGER');