alter table user add column initial_password_used bit(1) DEFAULT 0;

update user set initial_password_used = 1;