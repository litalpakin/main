UPDATE user_function SET balance_model_access =
    CASE WHEN name = 'Headquarters Manager' THEN 1
        WHEN name = 'Catalog Manager' THEN 1
        WHEN name = 'Region Manager' THEN 1
        WHEN name = 'Store Manager' THEN 0
    ELSE balance_model_access END;
