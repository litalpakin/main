CREATE TABLE weekly_dashboard (
    id BIGINT NOT NULL AUTO_INCREMENT,
    create_date DATETIME,
    update_date DATETIME,
    week INT(2),
    year INT(4),
    format_code BIGINT,
    PRIMARY KEY (id),
    UNIQUE KEY UK_WEEKLY_DASHBOARD (week,year,format_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE weekly_dashboard_item (
    id BIGINT NOT NULL AUTO_INCREMENT,
    create_date DATETIME,
    update_date DATETIME,
    weekly_dashboard_id BIGINT NOT NULL,
    value DOUBLE,
    color VARCHAR(128),
    classification_group longtext NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT FK_weekly_dashboard_item_weekly_dashboard_id FOREIGN KEY (weekly_dashboard_id) REFERENCES weekly_dashboard (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;