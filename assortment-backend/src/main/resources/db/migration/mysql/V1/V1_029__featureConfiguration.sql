CREATE TABLE feature_configuration (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   feature varchar(250) NOT NULL,
   is_enabled bit(1),
   PRIMARY KEY (id),
   UNIQUE KEY UK_feature_NAME (feature)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 insert into feature_configuration (feature,is_enabled) values ('HIDE_CLUSTERS_AND_STORES_NAMES',0);