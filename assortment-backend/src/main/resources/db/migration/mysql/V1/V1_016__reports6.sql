
drop table professional_tab_column;
drop table user_selection_column;
drop table user_report_columns_sort;
drop table USER_REPORT_ADVANCED_FILTER;
drop table user_report;

CREATE TABLE user_report (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   user_id bigint(20) NOT NULL,
   type varchar(250) NOT NULL,
   classification varchar(250),
   categories varchar(2500),
   classes varchar(2500),
   suppliers varchar(2500),
   sub_categories varchar(2500),
   brands varchar(2500),
   stores varchar(2500),
   clusters varchar(2500),
   time_filter varchar(2500),
   PRIMARY KEY (id),
   UNIQUE KEY UK_FUNCTION_NAME (user_id,name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
  CREATE TABLE user_report_tab(
   id bigint(20) NOT NULL AUTO_INCREMENT,
   report_id bigint(20)  NOT NULL,
   tab_identification varchar(250) NOT NULL,
   PRIMARY KEY (id),
   UNIQUE KEY UK_REPORT_TAB (report_id,tab_identification)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;	

 CREATE TABLE user_report_column(
   user_report_tab_id bigint(20) NOT NULL,
   order_id bigint(20),
   column_name varchar(250) NOT NULL,
   PRIMARY KEY (user_report_tab_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user_report_columns_sort (
   user_report_tab_id bigint(20)  NOT NULL,
   level bigint(20)  NOT NULL,
   column_name varchar(250) NOT NULL,
   type varchar(250) NOT NULL,
   PRIMARY KEY (user_report_tab_id,column_name,type,level)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE user_report_advanced_filter (
   user_report_tab_id bigint(20)  NOT NULL,
   column_name varchar(250)  NOT NULL,
   condition_op varchar(250)  NOT NULL,
   value_list varchar(2500),
   PRIMARY KEY (user_report_tab_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
