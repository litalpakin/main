CREATE TABLE shop_group (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   description varchar(1000),
   PRIMARY KEY (id),
   UNIQUE KEY UK_GROUP_NAME (name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE shop_groups_shops (
   shop_group_id bigint(20) NOT NULL,
   shop_id bigint(20) NOT NULL,
   PRIMARY KEY (shop_group_id, shop_id),
   CONSTRAINT FK_SHOP_GROUPS_SHOPS_GROUP_ID FOREIGN KEY (shop_group_id) REFERENCES shop_group (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;