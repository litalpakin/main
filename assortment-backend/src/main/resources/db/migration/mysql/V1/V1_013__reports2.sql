drop table user_report;
drop table professional_tab_column;
drop table user_selection_column;
drop table user_report_columns_sort;

CREATE TABLE user_report (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   user_id bigint(20) NOT NULL,
   type varchar(250),
   classification varchar(250),
   categories varchar(2500),
   classes varchar(2500),
   suppliers varchar(2500),
   sub_categories varchar(2500),
   brands varchar(2500),
   stores varchar(2500),
   clusters varchar(2500),
   PRIMARY KEY (id),
   UNIQUE KEY UK_FUNCTION_NAME (user_id,name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 			  
 CREATE TABLE professional_tab_column(
   report_id bigint(20),
   order_id bigint(20),
   column_name varchar(250),
   PRIMARY KEY (report_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 CREATE TABLE user_selection_column(
   report_id bigint(20),
   order_id bigint(20),
   column_name varchar(250),
   PRIMARY KEY (report_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
CREATE TABLE user_report_columns_sort (
   report_id bigint(20),
   tab_name varchar(250),
   column_name varchar(250),
   type varchar(250),
   PRIMARY KEY (report_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 CREATE TABLE user_report_advanced_filter (
   report_id bigint(20),
   column_name varchar(250),
   condition_op varchar(250),
   value_list varchar(2500),
   PRIMARY KEY (report_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
