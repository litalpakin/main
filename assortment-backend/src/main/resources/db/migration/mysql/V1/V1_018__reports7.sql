alter table user_report drop column time_filter;
alter table user_report add column dw_item_keys varchar(2500);
alter table user_report add column  first_level_period  varchar(250);
alter table user_report add column  second_level_period  varchar(250);
alter table user_report add column  previous_level_period  varchar(250);
	
	
drop table user_report_advanced_filter;

  CREATE TABLE user_report_tab_advanced_filter (
   user_report_tab_id bigint(20)  NOT NULL,
   column_name varchar(250)  NOT NULL,
   condition_op varchar(250)  NOT NULL,
   value_list varchar(2500),
   PRIMARY KEY (user_report_tab_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  CREATE TABLE user_report_advanced_filter (
   user_report_id bigint(20)  NOT NULL,
   column_name varchar(250)  NOT NULL,
   condition_op varchar(250)  NOT NULL,
   value_list varchar(2500),
   PRIMARY KEY (user_report_id,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;