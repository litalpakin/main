INSERT INTO feature_configuration (feature, is_enabled)
VALUES ('PLANOGRAM_FEATURE', 0);
ALTER TABLE user_function
    ADD COLUMN planogram_category BIT(1) DEFAULT 0;
ALTER TABLE user_function
    ADD COLUMN planogram_store BIT(1) DEFAULT 0;

CREATE TABLE IF NOT EXISTS planogram_category
(
    id                   BIGINT NOT NULL AUTO_INCREMENT,
    create_date          DATETIME,
    update_date          DATETIME,
    user_id              BIGINT NOT NULL,
    format_code          BIGINT NOT NULL,
    format_name          VARCHAR(256),
    en_format_name       VARCHAR(256),
    category_id          BIGINT NOT NULL,
    category_name        VARCHAR(256),
    en_category_name     VARCHAR(256),
    min_num_of_fields    INT DEFAULT NULL,
    max_num_of_fields    INT DEFAULT NULL,
    min_faces_allocation INT DEFAULT 1,
    faces_multiplication INT DEFAULT 1,
    PRIMARY KEY (id),
    CONSTRAINT FK_planogram_category_user FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE INDEX idx_user_id_format_code ON planogram_category (user_id, format_code);
CREATE INDEX idx_user_id_format_code_category_key ON planogram_category (user_id, format_code, category_id);
