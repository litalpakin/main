INSERT INTO company
(id,create_date,update_date,name,enabled,type) VALUES(2,NOW(), NOW(),'Bitan Wines',1,'MAIN');

CREATE TABLE companies_suppliers (
   company_id bigint(20) NOT NULL,
   supplier_id bigint(20) NOT NULL,
   PRIMARY KEY (company_id, supplier_id),
   CONSTRAINT FK_COMPANIES_SUPPLIERS_COMPANY_ID FOREIGN KEY (company_id) REFERENCES company (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;