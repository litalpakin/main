delete from user_tab;
alter table user_tab add column id bigint(20) not null;
ALTER TABLE user_tab DROP PRIMARY KEY;
ALTER TABLE user_tab ADD PRIMARY KEY (id);
alter table user_tab CHANGE COLUMN id id bigint(20) not null AUTO_INCREMENT;
ALTER TABLE user_tab CHANGE COLUMN classification_group classification_group VARCHAR(250);

