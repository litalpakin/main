CREATE TABLE IF NOT EXISTS job_data
(
    id          BIGINT       NOT NULL AUTO_INCREMENT,
    create_date DATETIME,
    update_date DATETIME,
    job_type    VARCHAR(128) NOT NULL,
    job_name    VARCHAR(128) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_JOB_DATA (job_type, job_name)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE user_report ADD COLUMN IF NOT EXISTS job_type VARCHAR (128) DEFAULT NULL;