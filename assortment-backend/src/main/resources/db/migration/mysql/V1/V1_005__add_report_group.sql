CREATE TABLE report_group (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   description varchar(1000),
   reports  varchar(1000),
   PRIMARY KEY (id),
   UNIQUE KEY UK_FUNCTION_NAME (name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE report_groups_user_functions (
   user_function_id bigint(20) NOT NULL,
   report_group_id bigint(20) NOT NULL,
   PRIMARY KEY (user_function_id, report_group_id),
   CONSTRAINT FK_REPORT_GROUPS_USER_FUNCTIONS_USER_ID FOREIGN KEY (user_function_id) REFERENCES user_function (id),
   CONSTRAINT FK_REPORT_GROUPS_USER_FUNCTIONS_REPORT_GROUP_ID FOREIGN KEY (report_group_id) REFERENCES report_group (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO report_group(create_date,update_date,name,description,reports)
VALUES(NOW(), NOW(),'All reports','group for all reports.',null);


INSERT INTO report_groups_user_functions(user_function_id, report_group_id)
SELECT (SELECT id FROM user_function WHERE name = 'Storenext'),
       (SELECT id FROM report_group WHERE name = 'All reports')



