DROP TABLE shop_groups_shops;
DROP TABLE shop_group;


CREATE TABLE store_group (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   stores varchar(5000),
   description varchar(1000),
   PRIMARY KEY (id),
   UNIQUE KEY UK_GROUP_NAME (name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE user ADD store_id bigint(20);

drop table report_groups_user_functions;

drop table report_group;

CREATE TABLE report (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   report_type varchar(250) NOT NULL,
   description varchar(1000),
   PRIMARY KEY (id),
   UNIQUE KEY UK_REPORT_NAME (name),
   UNIQUE KEY UK_REPORT_TYPE (report_type)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE report_user_functions (
   user_function_id bigint(20) NOT NULL,
   report_id bigint(20) NOT NULL,
   PRIMARY KEY (user_function_id, report_id),
   CONSTRAINT FK_REPORT_USER_FUNCTIONS_USER_FUNCTION_ID FOREIGN KEY (user_function_id) REFERENCES user_function (id),
   CONSTRAINT FK_REPORT_USER_FUNCTIONS_REPORT_ID FOREIGN KEY (report_id) REFERENCES report(id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO report(id,create_date,update_date,name,description,report_type)
VALUES(1,NOW(), NOW(),'All reports','basic report','MAIN');


INSERT INTO report_user_functions(user_function_id, report_id)
SELECT (SELECT id FROM user_function WHERE name = 'Storenext'),1