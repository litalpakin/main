
CREATE TABLE user_tab (
   classification_group varchar(250) NOT NULL,
   user_id bigint(20) NOT NULL,
   tab_type varchar(250) NOT NULL,
   column_name varchar(250) NOT NULL,
   order_id bigint(20),
   PRIMARY KEY (user_id,classification_group,tab_type,column_name)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 