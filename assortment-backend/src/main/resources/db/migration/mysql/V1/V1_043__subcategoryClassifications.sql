
ALTER TABLE user_report CHANGE COLUMN categories categories VARCHAR(1500) NULL DEFAULT NULL;
ALTER TABLE user_report CHANGE COLUMN classes classes VARCHAR(1500) NULL DEFAULT NULL ;
ALTER TABLE user_report CHANGE COLUMN sub_categories sub_categories VARCHAR(1500) NULL DEFAULT NULL;
ALTER TABLE user_report CHANGE COLUMN brands brands VARCHAR(1500) NULL DEFAULT NULL;
ALTER TABLE user_report CHANGE COLUMN stores stores VARCHAR(1500) NULL DEFAULT NULL;
ALTER TABLE user_report CHANGE COLUMN clusters clusters VARCHAR(500) NULL DEFAULT NULL ;

alter table user_report add column subcategory_classifications VARCHAR(2500);