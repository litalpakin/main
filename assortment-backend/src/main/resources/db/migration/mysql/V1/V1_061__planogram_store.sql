CREATE TABLE IF NOT EXISTS planogram_category_store
(
    id                 BIGINT NOT NULL AUTO_INCREMENT,
    create_date        DATETIME,
    update_date        DATETIME,
    user_id            BIGINT NOT NULL,
    format_code        BIGINT NOT NULL,
    format_name        VARCHAR(256),
    en_format_name     VARCHAR(256),
    category_id        BIGINT NOT NULL,
    category_name      VARCHAR(256),
    en_category_name   VARCHAR(256),
    dw_store_key       BIGINT NOT NULL,
    store_name         VARCHAR(256),
    num_of_fields      INT DEFAULT NULL,
    field_height       INT DEFAULT 180,
    field_width        INT DEFAULT 125,
    field_depth        INT DEFAULT 45,
    measuring_unit      VARCHAR(256),
    shelf_arrange_freq VARCHAR(256),
    PRIMARY KEY (id),
    CONSTRAINT FK_planogram_category_store_user FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE INDEX idx_user_id_format_code_category_id_dw_store_key ON planogram_category_store (user_id, format_code, category_id, dw_store_key);