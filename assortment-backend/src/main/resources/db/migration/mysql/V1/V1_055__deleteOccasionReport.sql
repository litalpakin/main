DELETE
FROM user_report_column
WHERE user_report_tab_id IN (SELECT id FROM user_report_tab WHERE report_id IN (SELECT id FROM user_report WHERE type = 'OCCASION_PAGE'));

DELETE
FROM user_report_columns_sort
WHERE user_report_tab_id IN (SELECT id FROM user_report_tab WHERE report_id IN (SELECT id FROM user_report WHERE type = 'OCCASION_PAGE'));

DELETE
FROM user_report_tab_advanced_filter
WHERE user_report_tab_id IN (SELECT id FROM user_report_tab WHERE report_id IN (SELECT id FROM user_report WHERE type = 'OCCASION_PAGE'));

DELETE
FROM user_report_tab
WHERE report_id IN (SELECT id FROM user_report WHERE type = 'OCCASION_PAGE');

DELETE
FROM user_report_advanced_filter
WHERE user_report_id IN (SELECT id FROM user_report WHERE type = 'OCCASION_PAGE');

DELETE
FROM user_report
WHERE type = 'OCCASION_PAGE';