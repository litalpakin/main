ALTER TABLE user ADD column language varchar(250);

update user set language = 'HEBREW';

ALTER TABLE user ADD column token_created bigint(20);

update user set token_created = 0;

