CREATE TABLE company (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   enabled bit(1) NOT NULL,
   type varchar(250) NOT NULL,
   logo_url varchar(1000),
   PRIMARY KEY (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   username varchar(50) NOT NULL,
   enabled bit(1) NOT NULL,
   first_name varchar(50) NOT NULL,
   last_name varchar(50) NOT NULL,
   last_password_reset_date datetime NOT NULL,
   password varchar(100) NOT NULL,
   authority varchar(100) NOT NULL,
   company_id bigint(20) NOT NULL,
   email_validation_token varchar(255),
   email_validation_token_create_date datetime,
   sex varchar(50),
   date_of_birth datetime,
   PRIMARY KEY (id),
   UNIQUE KEY UK_USER_USERNAME (username),
   CONSTRAINT FK_user_company_id FOREIGN KEY (company_id) REFERENCES company (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
 