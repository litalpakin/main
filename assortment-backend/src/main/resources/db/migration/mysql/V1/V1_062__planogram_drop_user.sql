ALTER TABLE planogram_category
    DROP FOREIGN KEY FK_planogram_category_user;
ALTER TABLE planogram_category
    DROP INDEX idx_user_id_format_code;
ALTER TABLE planogram_category
    DROP INDEX idx_user_id_format_code_category_key;
ALTER TABLE planogram_category
    DROP COLUMN user_id;
ALTER TABLE planogram_category
    DROP COLUMN format_name;
ALTER TABLE planogram_category
    DROP COLUMN en_format_name;
ALTER TABLE planogram_category
    DROP COLUMN category_name;
ALTER TABLE planogram_category
    DROP COLUMN en_category_name;
CREATE INDEX idx_format_code ON planogram_category (format_code);
CREATE INDEX idx_format_code_category_id ON planogram_category (format_code, category_id);

ALTER TABLE planogram_category_store
    DROP FOREIGN KEY FK_planogram_category_store_user;
ALTER TABLE planogram_category_store
    DROP INDEX idx_user_id_format_code_category_id_dw_store_key;
ALTER TABLE planogram_category_store
    DROP COLUMN format_code;
ALTER TABLE planogram_category_store
    DROP COLUMN format_name;
ALTER TABLE planogram_category_store
    DROP COLUMN en_format_name;
ALTER TABLE planogram_category_store
    DROP COLUMN category_name;
ALTER TABLE planogram_category_store
    DROP COLUMN en_category_name;
ALTER TABLE planogram_category_store
    DROP COLUMN store_name;
ALTER TABLE planogram_category_store
    DROP COLUMN user_id;
CREATE INDEX idx_category_id_dw_store_key ON planogram_category_store (category_id, dw_store_key);