CREATE TABLE login_failure (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   user_id bigint(20) NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT FK_LOGIN_FAILURE_USER_ID FOREIGN KEY (user_id) REFERENCES user (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


