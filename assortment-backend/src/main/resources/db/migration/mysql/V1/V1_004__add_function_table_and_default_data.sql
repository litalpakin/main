CREATE TABLE user_function (
   id bigint(20) NOT NULL AUTO_INCREMENT,
   create_date datetime DEFAULT NULL,
   update_date datetime DEFAULT NULL,
   name varchar(250) NOT NULL,
   is_default bit(1) NOT NULL,
   company_type varchar(50) NOT NULL,
   PRIMARY KEY (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE users_user_functions (
   user_id bigint(20) NOT NULL,
   user_function_id bigint(20) NOT NULL,
   PRIMARY KEY (user_id, user_function_id),
   CONSTRAINT FK_USERS_USER_FUNCTIONS_USER_ID FOREIGN KEY (user_id) REFERENCES user (id),
   CONSTRAINT FK_USERS_USER_FUNCTIONS_FUNCTION_ID FOREIGN KEY (user_function_id) REFERENCES user_function (id)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO user_function(create_date, update_date, name,company_type, is_default)
VALUES(NOW(), NOW(), 'Storenext', 'ADMIN', 1);

INSERT INTO users_user_functions(user_id, user_function_id)
VALUES((SELECT id FROM user WHERE username = 'admin@storenext.co.il'),
       (SELECT id FROM user_function WHERE name = 'Storenext'))
