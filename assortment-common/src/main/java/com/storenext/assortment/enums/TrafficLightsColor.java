package com.storenext.assortment.enums;

import java.util.Arrays;
import java.util.List;

public enum TrafficLightsColor {
	
	GREEN, YELLOW, RED;
	
	public static TrafficLightsColor getColor(Integer index) {
		if(index == null) {
			return null;
		}
		for(TrafficLightsColor value : TrafficLightsColor.values()) {
			if(value.ordinal() == index.intValue()) {
				return value;
			}
		}
		return null;
	}
	
	public static TrafficLightsColor getColorSafe(Object value) {
		if(value instanceof TrafficLightsColor) {
			return (TrafficLightsColor) value;
		} else if(value instanceof Number) {
			return getColor(((Number) value).intValue() - 1);
		}
		return null;
	}
	
	public static int indexOf(Object value) {
		if(value instanceof TrafficLightsColor) {
			return Arrays.asList(TrafficLightsColor.values()).indexOf(value);
		}
		return -1;
	}
	
	public static int compare(Object v1, Object v2) {
		int idx1 = -1;
		int idx2 = -1;
		List<TrafficLightsColor> arr = Arrays.asList(TrafficLightsColor.values());
		if(v1 instanceof TrafficLightsColor) {
			idx1 = arr.indexOf(v1);
		}
		if(v2 instanceof TrafficLightsColor) {
			idx2 = arr.indexOf(v2);
		}
		return Integer.compare(idx1, idx2);
	}
}
