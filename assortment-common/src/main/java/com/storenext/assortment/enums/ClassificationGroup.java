package com.storenext.assortment.enums;

public enum ClassificationGroup {

	UNDEFINED(0,12),
	GROWTH_REGULAR(1,1),
	CORE_REGULAR(2,2),
	VARIETY_REGULAR(3,5),
	EXAMINATION_REGULAR(4,8), 
	EXCLUDED_REGULAR(5,9),  
	STAR_LAUNCH_PERIOD(6,3),
	INTERMEDIATE_LAUNCH_PERIOD(7,6),
	INSIGNIFICANT_LAUNCH_PERIOD(8,10),
	STAR_AFTER_LAUNCH_PERIOD(9,4),
	INTERMEDIATE_AFTER_LAUNCH_PERIOD(10,7),
	INSIGNIFICANT_AFTER_LAUNCH_PERIOD(11,11),
	OCCASION_STAR(21,12),
	OCCASION_INTERMEDIATE(22,12),
	OCCASION_INSIGNIFICANT(23,12),
	OCCASION_ITEMS(24,12),
	NON_SELL_ITEMS(25,13),
	NEWBORN_ITEMS(26,14),
	MUST_ITEMS_AVAILABILITY(27,15),
	SHARE_OF_UNNECESSARY_ITEMS(28,16);
	



	int classification;
	int sort;

	private ClassificationGroup( int classification,int sort) {
		this.classification = classification;
		this.sort = sort;
	}

	public int getClassification() {
		return classification;
	}
	
	public static ClassificationGroup getClassification(Integer classification) {
		if(classification == null){
			return null;
		}
		for(ClassificationGroup value:ClassificationGroup.values()){
			if(value.getClassification() == classification.intValue()){
				return value;
			}
		}
		return null;
	}

	public Integer getSort() {
		return sort;
	}




}
