package com.storenext.assortment.enums;

public enum SubcategoryClassification {

	STRATEGIC(1),VITAL(2),SERVICE(3),NEGLIGIBLE(4);
	
	int code;
	
	SubcategoryClassification(int code){
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	public static SubcategoryClassification getClassification(Long classification) {
		if(classification == null){
			return null;
		}
		return getClassification(classification.intValue());
	}
	public static SubcategoryClassification getClassification(Integer classification) {
		if(classification == null){
			return null;
		}
		for(SubcategoryClassification value:SubcategoryClassification.values()){
			if(value.getCode() == classification.intValue()){
				return value;
			}
		}
		return null;
	}
}
