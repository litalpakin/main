package com.storenext.assortment.enums;

public enum ClassificationGroupReason {
	NON_CLASSIFIED,
	OCCASION_ITEM,
	LOW_SALES_ITEM,
	NEWBORN_ITEM,
	WITHOUT_SUFFICIENT_DATA,
	WITHOUT_SUFFICIENT_CONSUMER_DATA
}
