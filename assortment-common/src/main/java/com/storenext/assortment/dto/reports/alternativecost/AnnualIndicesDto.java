package com.storenext.assortment.dto.reports.alternativecost;

import com.storenext.assortment.dto.TableDataDto;

public class AnnualIndicesDto {
	private TableDataDto maxSales;
	private TableDataDto nontransferableIndex;
	
	public TableDataDto getMaxSales() {
		return maxSales;
	}
	
	public void setMaxSales(TableDataDto maxSales) {
		this.maxSales = maxSales;
	}
	
	public TableDataDto getNontransferableIndex() {
		return nontransferableIndex;
	}
	
	public void setNontransferableIndex(TableDataDto nontransferableIndex) {
		this.nontransferableIndex = nontransferableIndex;
	}
}
