package com.storenext.assortment.dto.reports.alternativecost;

import javax.validation.constraints.NotNull;

public class AlternativeCostRequestDto {
	@NotNull
	private Long formatCode;
	@NotNull
	private Long dwItemKey;
	
	public Long getFormatCode() {
		return formatCode;
	}
	
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	
	public Long getDwItemKey() {
		return dwItemKey;
	}
	
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}
}
