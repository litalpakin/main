package com.storenext.assortment.dto.reports;

import com.storenext.assortment.dto.TableDataDto;
import com.storenext.assortment.dto.TableDataWithSummaryDto;

public class StoreManagementReportResponseDto {
	private TableDataDto annualWeeklyTrackingTab;

	private TableDataWithSummaryDto statisticsTab;

	public TableDataDto getAnnualWeeklyTrackingTab() {
		return annualWeeklyTrackingTab;
	}

	public void setAnnualWeeklyTrackingTab(TableDataDto annualWeeklyTrackingTab) {
		this.annualWeeklyTrackingTab = annualWeeklyTrackingTab;
	}

	public TableDataWithSummaryDto getStatisticsTab() {
		return statisticsTab;
	}

	public void setStatisticsTab(TableDataWithSummaryDto statisticsTab) {
		this.statisticsTab = statisticsTab;
	}


}
