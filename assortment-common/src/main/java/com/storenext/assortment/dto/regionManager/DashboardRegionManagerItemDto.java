package com.storenext.assortment.dto.regionManager;

import com.storenext.assortment.dto.ControlAndNavigationDashboardItem;

public class DashboardRegionManagerItemDto {
	private Long formatCode;
	private String formatName;
	private Long dwStoreKey;
	private String storeName;
	private Integer storeRank;
	
	private ControlAndNavigationDashboardItem growth;
	private ControlAndNavigationDashboardItem core;
	private ControlAndNavigationDashboardItem examination;
	private ControlAndNavigationDashboardItem excluded;
	private ControlAndNavigationDashboardItem star;
	private ControlAndNavigationDashboardItem intermediate;
	private ControlAndNavigationDashboardItem insignificant;
	private ControlAndNavigationDashboardItem must;
	private ControlAndNavigationDashboardItem share;
	
	public Long getFormatCode() {
		return formatCode;
	}
	
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	
	public String getFormatName() {
		return formatName;
	}
	
	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}
	
	public Long getDwStoreKey() {
		return dwStoreKey;
	}
	
	public void setDwStoreKey(Long dwStoreKey) {
		this.dwStoreKey = dwStoreKey;
	}
	
	public String getStoreName() {
		return storeName;
	}
	
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
	public Integer getStoreRank() {
		return storeRank;
	}
	
	public void setStoreRank(Integer storeRank) {
		this.storeRank = storeRank;
	}
	
	public ControlAndNavigationDashboardItem getGrowth() {
		return growth;
	}
	
	public void setGrowth(ControlAndNavigationDashboardItem growth) {
		this.growth = growth;
	}
	
	public ControlAndNavigationDashboardItem getCore() {
		return core;
	}
	
	public void setCore(ControlAndNavigationDashboardItem core) {
		this.core = core;
	}
	
	public ControlAndNavigationDashboardItem getExamination() {
		return examination;
	}
	
	public void setExamination(ControlAndNavigationDashboardItem examination) {
		this.examination = examination;
	}
	
	public ControlAndNavigationDashboardItem getExcluded() {
		return excluded;
	}
	
	public void setExcluded(ControlAndNavigationDashboardItem excluded) {
		this.excluded = excluded;
	}
	
	public ControlAndNavigationDashboardItem getStar() {
		return star;
	}
	
	public void setStar(ControlAndNavigationDashboardItem star) {
		this.star = star;
	}
	
	public ControlAndNavigationDashboardItem getIntermediate() {
		return intermediate;
	}
	
	public void setIntermediate(ControlAndNavigationDashboardItem intermediate) {
		this.intermediate = intermediate;
	}
	
	public ControlAndNavigationDashboardItem getInsignificant() {
		return insignificant;
	}
	
	public void setInsignificant(ControlAndNavigationDashboardItem insignificant) {
		this.insignificant = insignificant;
	}
	
	public ControlAndNavigationDashboardItem getMust() {
		return must;
	}
	
	public void setMust(ControlAndNavigationDashboardItem must) {
		this.must = must;
	}
	
	public ControlAndNavigationDashboardItem getShare() {
		return share;
	}
	
	public void setShare(ControlAndNavigationDashboardItem share) {
		this.share = share;
	}
}
