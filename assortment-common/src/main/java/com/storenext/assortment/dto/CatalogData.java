package com.storenext.assortment.dto;

import java.util.Arrays;
import java.util.Set;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.storenext.assortment.enums.CatalogType;

import io.jsonwebtoken.lang.Collections;
import org.springframework.util.CollectionUtils;

public class CatalogData {



	private Set<Long> categories;
	private Set<Long> classes;
	
	private Set<Long> suppliers;
	private Set<Long> subCategories;
	private Set<Long> brands;
	@NotNull
	private CatalogType catalogType;
	
	public CatalogData(){
		
	}
	public CatalogData(CatalogType catalogType){
		this.catalogType = catalogType;
	}
	public CatalogData(Set<Long> categories, Set<Long> classes, Set<Long> suppliers, Set<Long> subCategories,
			Set<Long> brands,CatalogType catalogType) {
		this.categories = categories;
		this.classes = classes;
		this.suppliers = suppliers;
		this.subCategories = subCategories;
		this.brands = brands;
		this.catalogType = catalogType;
	}

	public CatalogData(Set<Long> suppliers, Set<Long> subCategories,CatalogType catalogType) {
		this.suppliers = suppliers;
		this.subCategories = subCategories;
	}

	
	@Transient
	@JsonIgnore
	public  boolean isCatalogDataExists(){
		return !Collections.isEmpty(subCategories) || 
				!Collections.isEmpty(categories) ||
				!Collections.isEmpty(classes) ||
				!Collections.isEmpty(suppliers) ||
				!Collections.isEmpty(brands);
	}
	public Set<Long> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(Set<Long> subCategories) {
		this.subCategories = subCategories;
	}

	public Set<Long> getCategories() {
		return categories;
	}

	public void setCategories(Set<Long> categories) {
		this.categories = categories;
	}

	public Set<Long> getClasses() {
		return classes;
	}

	public void setClasses(Set<Long> classes) {
		this.classes = classes;
	}

	public Set<Long> getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(Set<Long> suppliers) {
		this.suppliers = suppliers;
	}

	public Set<Long> getBrands() {
		return brands;
	}

	public void setBrands(Set<Long> brands) {
		this.brands = brands;
	}
	public CatalogType getCatalogType() {
		return catalogType;
	}
	public void setCatalogType(CatalogType catalogType) {
		this.catalogType = catalogType;
	}

	@Override
	public String toString() {
		return "CatalogData{" +
				"categories=" + print(categories) +
				", classes=" + print(classes) +
				", suppliers=" + print(suppliers) +
				", subCategories=" + print(subCategories) +
				", brands=" + print(brands) +
				", catalogType=" + catalogType +
				'}';
	}

	private String print(Set<Long> suppliers) {
		if (CollectionUtils.isEmpty(suppliers)) {
			return "";
		}

		return Arrays.toString(suppliers.toArray());
	}
}
