package com.storenext.assortment.dto.reports.alternativecost;

import com.storenext.assortment.dto.TableDataDto;

public class AlternativeCostModelDto {
	private TableDataDto sliderData;
	private TableDataDto mainIndices;
	private TableDataDto additionalData;
	
	public TableDataDto getSliderData() {
		return sliderData;
	}
	
	public void setSliderData(TableDataDto sliderData) {
		this.sliderData = sliderData;
	}
	
	public TableDataDto getMainIndices() {
		return mainIndices;
	}
	
	public void setMainIndices(TableDataDto mainIndices) {
		this.mainIndices = mainIndices;
	}
	
	public TableDataDto getAdditionalData() {
		return additionalData;
	}
	
	public void setAdditionalData(TableDataDto additionalData) {
		this.additionalData = additionalData;
	}
}
