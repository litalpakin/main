package com.storenext.assortment.dto;

import java.util.Set;

import javax.persistence.Transient;

public class UserSelectionFilterDto {

	private CatalogData catalogData;
	private Set<Long> stores;
	private Set<Long> clusters;

	public UserSelectionFilterDto(){
		
	}
	public UserSelectionFilterDto( CatalogData catalogData){
		this.catalogData = catalogData; 
	}
	public UserSelectionFilterDto(CatalogData catalogData, Set<Long> stores, Set<Long> clusters) {
		this.catalogData = catalogData;
		this.stores = stores;
		this.clusters = clusters;
	}
	public Set<Long> getStores() {
		return stores;
	}

	public void setStores(Set<Long> stores) {
		this.stores = stores;
	}


	public Set<Long> getClusters() {
		return clusters;
	}

	public void setClusters(Set<Long> clusters) {
		this.clusters = clusters;
	}

	public CatalogData getCatalogData() {
		return catalogData;
	}

	public void setCatalogData(CatalogData catalogData) {
		this.catalogData = catalogData;
	}

	@Transient
	public  boolean isCatalogDataExists(){
		return catalogData != null && catalogData.isCatalogDataExists();
	}

}
