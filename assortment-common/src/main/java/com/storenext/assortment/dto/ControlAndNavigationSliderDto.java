package com.storenext.assortment.dto;

import java.util.Map;

public class ControlAndNavigationSliderDto {

	private Map<String,Object> totalSales;
	private Map<String,Object> stoppedSellingItems;
	private Map<String,Object> newSellingItems;
	private Map<String,Object> coreItemsAvailability;
	private Map<String,Object> excludedItemsNum;
	
	private Map<String,Object> growthItemsContribution;
	private Map<String,Object> monthlyAvgItemSales;
	private Map<String,Object> annualAvgItemSales;
	
	public Map<String, Object> getExcludedItemsNum() {
		return excludedItemsNum;
	}
	public void setExcludedItemsNum(Map<String, Object> excludedItemsNum) {
		this.excludedItemsNum = excludedItemsNum;
	}
	public Map<String, Object> getTotalSales() {
		return totalSales;
	}
	public void setTotalSales(Map<String, Object> totalSales) {
		this.totalSales = totalSales;
	}
	public Map<String, Object> getStoppedSellingItems() {
		return stoppedSellingItems;
	}
	public void setStoppedSellingItems(Map<String, Object> stoppedSellingItems) {
		this.stoppedSellingItems = stoppedSellingItems;
	}
	public Map<String, Object> getNewSellingItems() {
		return newSellingItems;
	}
	public void setNewSellingItems(Map<String, Object> newSellingItems) {
		this.newSellingItems = newSellingItems;
	}
	public Map<String, Object> getCoreItemsAvailability() {
		return coreItemsAvailability;
	}
	public void setCoreItemsAvailability(Map<String, Object> coreItemsAvailability) {
		this.coreItemsAvailability = coreItemsAvailability;
	}
	public Map<String, Object> getGrowthItemsContribution() {
		return growthItemsContribution;
	}
	public void setGrowthItemsContribution(Map<String, Object> growthItemsContribution) {
		this.growthItemsContribution = growthItemsContribution;
	}
	public Map<String, Object> getMonthlyAvgItemSales() {
		return monthlyAvgItemSales;
	}
	public void setMonthlyAvgItemSales(Map<String, Object> monthlyAvgItemSales) {
		this.monthlyAvgItemSales = monthlyAvgItemSales;
	}
	public Map<String, Object> getAnnualAvgItemSales() {
		return annualAvgItemSales;
	}
	public void setAnnualAvgItemSales(Map<String, Object> annualAvgItemSales) {
		this.annualAvgItemSales = annualAvgItemSales;
	}
	

}
