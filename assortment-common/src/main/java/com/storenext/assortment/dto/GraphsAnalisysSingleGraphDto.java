package com.storenext.assortment.dto;

import java.util.List;

public class GraphsAnalisysSingleGraphDto {

	private String name;
	private List<ValueNameDto> values;

	public GraphsAnalisysSingleGraphDto(String name, List<ValueNameDto> values) {
		this.name = name;
		this.values = values;
	}

	public GraphsAnalisysSingleGraphDto(){
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public List<ValueNameDto> getValues() {
		return values;
	}

	public void setValues(List<ValueNameDto> values) {
		this.values = values;
	}

}
