package com.storenext.assortment.dto;

import java.util.List;

public class GraphsAnalisysDataDto{
	
	
	List<GraphsAnalisysSingleGraphDto> additionalData;
	String name;
	List<GraphsAnalisysSingleGraphDto> values;
	
	public List<GraphsAnalisysSingleGraphDto> getAdditionalData() {
		return additionalData;
	}
	public void setAdditionalData(List<GraphsAnalisysSingleGraphDto> additionalData) {
		this.additionalData = additionalData;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<GraphsAnalisysSingleGraphDto> getValues() {
		return values;
	}
	public void setValues(List<GraphsAnalisysSingleGraphDto> values) {
		this.values = values;
	}

}
