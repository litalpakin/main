package com.storenext.assortment.dto;

import java.util.List;

public class ControlAndNavigationResponseDto {

	private ControlAndNavigationSliderDto sliderData;

	private List<ControlAndNavigationDashboardItem> oldItems;

	private List<ControlAndNavigationDashboardItem> newItems;

	private TableDataDto classificationListWithItems;
	
	public TableDataDto getClassificationListWithItems() {
		return classificationListWithItems;
	}

	public void setClassificationListWithItems(TableDataDto classificationListWithItems) {
		this.classificationListWithItems = classificationListWithItems;
	}

	public ControlAndNavigationSliderDto getSliderData() {
		return sliderData;
	}

	public void setSliderData(ControlAndNavigationSliderDto sliderData) {
		this.sliderData = sliderData;
	}
	
	public List<ControlAndNavigationDashboardItem> getOldItems() {
		return oldItems;
	}

	public void setOldItems(List<ControlAndNavigationDashboardItem> oldItems) {
		this.oldItems = oldItems;
	}

	public List<ControlAndNavigationDashboardItem> getNewItems() {
		return newItems;
	}

	public void setNewItems(List<ControlAndNavigationDashboardItem> newItems) {
		this.newItems = newItems;
	}
}
