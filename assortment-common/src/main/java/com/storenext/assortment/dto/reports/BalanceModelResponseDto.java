package com.storenext.assortment.dto.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.storenext.assortment.dto.TableDataDto;

public class BalanceModelResponseDto {
	List<Map<String, Object>> rows;
	List<ClassificationCounterDto> subcatClassifcaLtions = new ArrayList<>();

	
	public List<ClassificationCounterDto> getSubcatClassifcaLtions() {
		return subcatClassifcaLtions;
	}
	public void setSubcatClassifcaLtions(List<ClassificationCounterDto> subcatClassifcaLtions) {
		this.subcatClassifcaLtions = subcatClassifcaLtions;
	}
	public List<Map<String, Object>> getRows() {
		return rows;
	}
	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}
}
