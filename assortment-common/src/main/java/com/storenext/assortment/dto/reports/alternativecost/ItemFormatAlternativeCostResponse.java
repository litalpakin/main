package com.storenext.assortment.dto.reports.alternativecost;

import com.storenext.assortment.dto.TableDataDto;

public class ItemFormatAlternativeCostResponse {
	private TableDataDto itemIndices;
	private AlternativeCostModelDto alternativeCostMode;
	private AnnualIndicesDto annualIndices;
	private AnnualIndicesDto quarterlyIndices;
	private String warningMessage;
	
	public TableDataDto getItemIndices() {
		return itemIndices;
	}
	
	public void setItemIndices(TableDataDto itemIndices) {
		this.itemIndices = itemIndices;
	}
	
	public AlternativeCostModelDto getAlternativeCostMode() {
		return alternativeCostMode;
	}
	
	public void setAlternativeCostMode(AlternativeCostModelDto alternativeCostMode) {
		this.alternativeCostMode = alternativeCostMode;
	}
	
	public AnnualIndicesDto getAnnualIndices() {
		return annualIndices;
	}
	
	public void setAnnualIndices(AnnualIndicesDto annualIndices) {
		this.annualIndices = annualIndices;
	}
	
	public AnnualIndicesDto getQuarterlyIndices() {
		return quarterlyIndices;
	}
	
	public void setQuarterlyIndices(AnnualIndicesDto quarterlyIndices) {
		this.quarterlyIndices = quarterlyIndices;
	}
	
	public String getWarningMessage() {
		return warningMessage;
	}
	
	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
}
