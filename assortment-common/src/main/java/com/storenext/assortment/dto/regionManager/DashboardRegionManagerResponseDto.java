package com.storenext.assortment.dto.regionManager;

import java.util.List;

public class DashboardRegionManagerResponseDto {
	List<DashboardRegionManagerItemDto> items;
	DashboardRegionManagerItemDto summary;
	
	public List<DashboardRegionManagerItemDto> getItems() {
		return items;
	}
	
	public void setItems(List<DashboardRegionManagerItemDto> items) {
		this.items = items;
	}
	
	public DashboardRegionManagerItemDto getSummary() {
		return summary;
	}
	
	public void setSummary(DashboardRegionManagerItemDto summary) {
		this.summary = summary;
	}
}
