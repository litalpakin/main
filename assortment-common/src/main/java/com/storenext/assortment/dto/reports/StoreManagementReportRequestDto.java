package com.storenext.assortment.dto.reports;

import java.util.Set;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.dto.CatalogData;

public class StoreManagementReportRequestDto {
	@NotNull
	private CatalogData catalogData;
	@NotNull
	private Long formatCode;
	
	private Set<Long> stores;

	public CatalogData getCatalogData() {
		return catalogData;
	}

	public void setCatalogData(CatalogData catalogData) {
		this.catalogData = catalogData;
	}

	public Long getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public Set<Long> getStores() {
		return stores;
	}

	public void setStores(Set<Long> stores) {
		this.stores = stores;
	}
}
