package com.storenext.assortment.dto;

import java.util.List;

import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.TrafficLightsColor;

public class ControlAndNavigationDashboardItem {

	private List<ClassificationGroup> classificationList;
	private Double value;
	private TrafficLightsColor trafficLightsColor;

	public ControlAndNavigationDashboardItem(){
		
	}
	
	public ControlAndNavigationDashboardItem(List<ClassificationGroup> classificationList){
		this.classificationList = classificationList;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public TrafficLightsColor getTrafficLightsColor() {
		return trafficLightsColor;
	}
	public void setTrafficLightsColor(TrafficLightsColor trafficLightsColor) {
		this.trafficLightsColor = trafficLightsColor;
	}
	public List<ClassificationGroup> getClassificationList() {
		return classificationList;
	}
	public void setClassificationList(List<ClassificationGroup> classificationList) {
		this.classificationList = classificationList;
	}

}
