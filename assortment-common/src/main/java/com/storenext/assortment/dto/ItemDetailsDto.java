package com.storenext.assortment.dto;

public class ItemDetailsDto {
	private TableDataDto aColumns;
	private TableDataDto bColumns;
	private TableDataDto cColumns;

	public TableDataDto getAColumns() {
		return aColumns;
	}

	public void setAColumns(TableDataDto aColumns) {
		this.aColumns = aColumns;
	}

	public TableDataDto getBColumns() {
		return bColumns;
	}

	public void setBColumns(TableDataDto bColumns) {
		this.bColumns = bColumns;
	}

	public TableDataDto getCColumns() {
		return cColumns;
	}

	public void setCColumns(TableDataDto cColumns) {
		this.cColumns = cColumns;
	}
}
