package com.storenext.assortment.dto.reports.balanceModel;

import java.util.List;

public class BalanceModelReportResponseDataDto {
	private Number score;
	private List<BalanceModelReportResponseDataValueDto> values;

	public Number getScore() {
		return score;
	}

	public void setScore(Number score) {
		this.score = score;
	}

	public List<BalanceModelReportResponseDataValueDto> getValues() {
		return values;
	}

	public void setValues(List<BalanceModelReportResponseDataValueDto> values) {
		this.values = values;
	}
}
