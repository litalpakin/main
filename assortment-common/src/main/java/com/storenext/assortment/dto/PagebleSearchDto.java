package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.dto.reports.UserReportType;

public class PagebleSearchDto {
	
	private Long formatCode;
	@NotNull
	private String name;
	@NotNull
	private Integer pageSize;
	@NotNull
	private Integer pageIndx;
	private UserReportType userReportType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getPageIndx() {
		return pageIndx;
	}
	public void setPageIndx(Integer pageIndx) {
		this.pageIndx = pageIndx;
	}
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	public UserReportType getUserReportType() {
		return userReportType;
	}
	public void setUserReportType(UserReportType userReportType) {
		this.userReportType = userReportType;
	}
}
