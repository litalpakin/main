package com.storenext.assortment.dto.reports;

import com.storenext.assortment.dto.TableDataDto;
import com.storenext.assortment.dto.TableDataWithSummaryDto;

public class OccasionReportResponseDto {
	private TableDataDto occasionReportTabs;
	private TableDataWithSummaryDto statisticsTab;

	public TableDataDto getOccasionReportTabs() {
		return occasionReportTabs;
	}

	public void setOccasionReportTabs(TableDataDto occasionReportTabs) {
		this.occasionReportTabs = occasionReportTabs;
	}

	public TableDataWithSummaryDto getStatisticsTab() {
		return statisticsTab;
	}

	public void setStatisticsTab(TableDataWithSummaryDto statisticsTab) {
		this.statisticsTab = statisticsTab;
	}
}
