package com.storenext.assortment.dto.reports;

import com.storenext.assortment.dto.TableDataWithSummaryDto;

public class CategoryReportResponseDto {

	private TableDataWithSummaryDto  statisticsTab;
	private TableDataWithSummaryDto  itemTab;
	private TableDataWithSummaryDto  supplierTab;
	private TableDataWithSummaryDto  brandTab;
	private TableDataWithSummaryDto clustersTab;



	public TableDataWithSummaryDto getStatisticsTab() {
		return statisticsTab;
	}

	public void setStatisticsTab(TableDataWithSummaryDto statisticsTab) {
		this.statisticsTab = statisticsTab;
	}

	public TableDataWithSummaryDto getItemTab() {
		return itemTab;
	}

	public void setItemTab(TableDataWithSummaryDto itemTab) {
		this.itemTab = itemTab;
	}

	public TableDataWithSummaryDto getSupplierTab() {
		return supplierTab;
	}

	public void setSupplierTab(TableDataWithSummaryDto supplierTab) {
		this.supplierTab = supplierTab;
	}

	public TableDataWithSummaryDto getBrandTab() {
		return brandTab;
	}

	public void setBrandTab(TableDataWithSummaryDto brandTab) {
		this.brandTab = brandTab;
	}

	public TableDataWithSummaryDto getClustersTab() {
		return clustersTab;
	}

	public void setClustersTab(TableDataWithSummaryDto clustersTab) {
		this.clustersTab = clustersTab;
	}

	
}
