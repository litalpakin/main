package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

import com.storenext.assortment.enums.ClassificationGroup;

public class GeneralStateRequestDto extends UserSelectionFilterDto{

	@NotNull
	private ClassificationGroup classification;

	public ClassificationGroup getClassification() {
		return classification;
	}

	public void setClassification(ClassificationGroup classification) {
		this.classification = classification;
	}

}
