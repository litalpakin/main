package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphsAnalisysEntryDto {

	private List<GraphsAnalisysDataDto> graphData = new ArrayList<>();
	private Map<String,Object> additionalData = new HashMap<>();
	private String name;

	public GraphsAnalisysEntryDto(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addGraphData(GraphsAnalisysDataDto dto){
		graphData.add(dto);
	}
	public List<GraphsAnalisysDataDto> getGraphData() {
		return graphData;
	}

	public void setGraphData(List<GraphsAnalisysDataDto> graphData) {
		this.graphData = graphData;
	}
	

	public Map<String, Object> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(Map<String, Object> additionalData) {
		this.additionalData = additionalData;
	}
}
