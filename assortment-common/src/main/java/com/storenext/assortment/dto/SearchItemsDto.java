package com.storenext.assortment.dto;

import java.util.List;

public class SearchItemsDto {
	List<ItemDto> items;
	int totalResponseSize;
	public List<ItemDto> getItems() {
		return items;
	}
	public void setItems(List<ItemDto> items) {
		this.items = items;
	}
	public int getTotalResponseSize() {
		return totalResponseSize;
	}
	public void setTotalResponseSize(int totalResponseSize) {
		this.totalResponseSize = totalResponseSize;
	}
}
