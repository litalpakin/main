package com.storenext.assortment.dto;

import java.util.Set;

import javax.validation.constraints.NotNull;

public class StoreGroupDto {
    private Long id;
    @NotNull
    private String name;
    private String description;

    private Set<StoreDto> stores;



	public StoreGroupDto() {
    }

    public StoreGroupDto(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<StoreDto> getStores() {
		return stores;
	}

	public void setStores(Set<StoreDto> stores) {
		this.stores = stores;
	}
}
