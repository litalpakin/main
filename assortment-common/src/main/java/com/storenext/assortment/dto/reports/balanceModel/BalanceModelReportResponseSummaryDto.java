package com.storenext.assortment.dto.reports.balanceModel;

public class BalanceModelReportResponseSummaryDto {
	private Double salesChangeResult;
	private Double salesMsChangeResult;

	public Double getSalesChangeResult() {
		return salesChangeResult;
	}

	public void setSalesChangeResult(Double salesChangeResult) {
		this.salesChangeResult = salesChangeResult;
	}

	public Double getSalesMsChangeResult() {
		return salesMsChangeResult;
	}

	public void setSalesMsChangeResult(Double salesMsChangeResult) {
		this.salesMsChangeResult = salesMsChangeResult;
	}
}
