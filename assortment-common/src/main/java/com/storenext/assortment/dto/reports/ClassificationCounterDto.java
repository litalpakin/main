package com.storenext.assortment.dto.reports;

import com.storenext.assortment.enums.SubcategoryClassification;

public class ClassificationCounterDto {


	SubcategoryClassification classification;
	Long value;
	
	public ClassificationCounterDto(SubcategoryClassification classification, Long value) {
		this.classification = classification;
		this.value = value;
	}
	
	public SubcategoryClassification getClassification() {
		return classification;
	}
	public void setClassification(SubcategoryClassification classification) {
		this.classification = classification;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
}
