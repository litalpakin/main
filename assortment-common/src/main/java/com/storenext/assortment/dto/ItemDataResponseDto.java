package com.storenext.assortment.dto;

public class ItemDataResponseDto {
	TableDataDto itemsMeasures;
	TableDataDto notSoldStores;
	Boolean displayAbcAnalysis;

	public TableDataDto getItemsMeasures() {
		return itemsMeasures;
	}

	public void setItemsMeasures(TableDataDto itemsMeasures) {
		this.itemsMeasures = itemsMeasures;
	}

	public TableDataDto getNotSoldStores() {
		return notSoldStores;
	}

	public void setNotSoldStores(TableDataDto notSoldStores) {
		this.notSoldStores = notSoldStores;
	}

	public Boolean getDisplayAbcAnalysis() {
		return displayAbcAnalysis;
	}

	public void setDisplayAbcAnalysis(Boolean displayAbcAnalysis) {
		this.displayAbcAnalysis = displayAbcAnalysis;
	}
}
