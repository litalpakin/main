package com.storenext.assortment.dto;

import java.util.Set;

public class ItemDataInStoresDto extends ItemDetailsRequestDto{
	Set<Long> dwStoreKeys;

	public Set<Long> getDwStoreKeys() {
		return dwStoreKeys;
	}

	public void setDwStoreKeys(Set<Long> dwStoreKeys) {
		this.dwStoreKeys = dwStoreKeys;
	}

}
