package com.storenext.assortment.dto.reports.balanceModel;

public enum BalanceModelReportLevel {
	CLASSES, CATEGORIES, SUBCATEGORIES,CLUSTERS,BRANDS,ITEMS
}
