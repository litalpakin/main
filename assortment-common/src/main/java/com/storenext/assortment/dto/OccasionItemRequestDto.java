package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

public class OccasionItemRequestDto {
	@NotNull
	private Long formatCode;
	@NotNull
	private String occasion;
	
	private CatalogData catalogData;

	public Long getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public String getOccasion() {
		return occasion;
	}

	public void setOccasion(String occasion) {
		this.occasion = occasion;
	}

	public CatalogData getCatalogData() {
		return catalogData;
	}

	public void setCatalogData(CatalogData catalogData) {
		this.catalogData = catalogData;
	}
	
}
