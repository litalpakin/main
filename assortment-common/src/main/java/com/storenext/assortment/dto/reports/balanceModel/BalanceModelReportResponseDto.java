package com.storenext.assortment.dto.reports.balanceModel;

public class BalanceModelReportResponseDto {
	private BalanceModelReportResponseDataDto positive;
	private BalanceModelReportResponseDataDto negative;
	private BalanceModelReportResponseSummaryDto summary;

	public BalanceModelReportResponseDataDto getPositive() {
		return positive;
	}

	public void setPositive(BalanceModelReportResponseDataDto positive) {
		this.positive = positive;
	}

	public BalanceModelReportResponseDataDto getNegative() {
		return negative;
	}

	public void setNegative(BalanceModelReportResponseDataDto negative) {
		this.negative = negative;
	}

	public BalanceModelReportResponseSummaryDto getSummary() {
		return summary;
	}

	public void setSummary(BalanceModelReportResponseSummaryDto summary) {
		this.summary = summary;
	}
}
