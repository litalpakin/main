package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class TableDataDto {

	protected List<List<Object>> rows = new ArrayList<>(); 
	protected List<String> columnNames = new ArrayList<>();
	
	public List<List<Object>> getRows() {
		return rows;
	}
	public void setRows(List<List<Object>> rows) {
		this.rows = rows;
	}
	public List<String> getColumnNames() {
		return columnNames;
	}
	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}
}
