package com.storenext.assortment.dto;

import java.util.ArrayList;
import java.util.List;

public class GraphsAnalisysResponseDto {

	List<GraphsAnalisysEntryDto> userComperativeTab = new ArrayList<>();

	public List<GraphsAnalisysEntryDto> getUserComperativeTab() {
		return userComperativeTab;
	}

	public void setUserComperativeTab(List<GraphsAnalisysEntryDto> userComperativeTab) {
		this.userComperativeTab = userComperativeTab;
	}


}
