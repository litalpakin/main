package com.storenext.assortment.dto.reports.balanceModel;

public class BalanceModelReportResponseDataValueDto {
	private Long loaId;
	private String loa;
	private Integer orderId;
	private Object currentSales;
	private Object previousSales;
	private Object salesChange;
	private Object cumulativeContribution;
	private Object contributionToSide;
	private Object sortValue;
	
	public Object getSortValue() {
		return sortValue;
	}

	public void setSortValue(Object sortValue) {
		this.sortValue = sortValue;
	}

	public Long getLoaId() {
		return loaId;
	}

	public void setLoaId(Long loaId) {
		this.loaId = loaId;
	}

	public String getLoa() {
		return loa;
	}

	public void setLoa(String loa) {
		this.loa = loa;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Object getCurrentSales() {
		return currentSales;
	}

	public void setCurrentSales(Object currentSales) {
		this.currentSales = currentSales;
	}

	public Object getPreviousSales() {
		return previousSales;
	}

	public void setPreviousSales(Object previousSales) {
		this.previousSales = previousSales;
	}

	public Object getSalesChange() {
		return salesChange;
	}

	public void setSalesChange(Object salesChange) {
		this.salesChange = salesChange;
	}

	public Object getCumulativeContribution() {
		return cumulativeContribution;
	}

	public void setCumulativeContribution(Object cumulativeContribution) {
		this.cumulativeContribution = cumulativeContribution;
	}

	public Object getContributionToSide() {
		return contributionToSide;
	}

	public void setContributionToSide(Object contributionToSide) {
		this.contributionToSide = contributionToSide;
	}
}
