package com.storenext.assortment.dto;

import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.ClassificationGroupReason;

public class ItemDto {
	private Long dwItemKey;
	private String itemId;
	private String itemName;
	private ClassificationGroup classificationGroup;
	private Boolean disabled = false;
	private ClassificationGroupReason disabledReason;
	public Long getDwItemKey() {
		return dwItemKey;
	}
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ClassificationGroup getClassificationGroup() {
		return classificationGroup;
	}

	public void setClassificationGroup(ClassificationGroup classificationGroup) {
		this.classificationGroup = classificationGroup;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public ClassificationGroupReason getDisabledReason() {
		return disabledReason;
	}

	public void setDisabledReason(ClassificationGroupReason disabledReason) {
		this.disabledReason = disabledReason;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dwItemKey == null) ? 0 : dwItemKey.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDto other = (ItemDto) obj;
		if (dwItemKey == null) {
			if (other.dwItemKey != null)
				return false;
		} else if (!dwItemKey.equals(other.dwItemKey))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		return true;
	}
}
