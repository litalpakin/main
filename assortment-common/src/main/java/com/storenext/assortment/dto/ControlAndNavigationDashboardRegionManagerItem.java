package com.storenext.assortment.dto;

import java.util.List;

import com.storenext.assortment.enums.ClassificationGroup;
import com.storenext.assortment.enums.TrafficLightsColor;

public class ControlAndNavigationDashboardRegionManagerItem {
	private List<ClassificationGroup> classificationList;
	private Double value;
	private TrafficLightsColor trafficLightsColor;
	private Long formatCode = null;
	private String formatName = null;
	private Long dwStoreKey = null;
	private String storeName = "SUMMARY_ROW";
	private Integer storeRank = 0;

	public ControlAndNavigationDashboardRegionManagerItem() {

	}

	public ControlAndNavigationDashboardRegionManagerItem(List<ClassificationGroup> classificationList) {
		this.classificationList = classificationList;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public TrafficLightsColor getTrafficLightsColor() {
		return trafficLightsColor;
	}

	public void setTrafficLightsColor(TrafficLightsColor trafficLightsColor) {
		this.trafficLightsColor = trafficLightsColor;
	}

	public List<ClassificationGroup> getClassificationList() {
		return classificationList;
	}

	public void setClassificationList(List<ClassificationGroup> classificationList) {
		this.classificationList = classificationList;
	}

	public Long getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}

	public String getFormatName() {
		return formatName;
	}

	public void setFormatName(String formatName) {
		this.formatName = formatName;
	}

	public Long getDwStoreKey() {
		return dwStoreKey;
	}

	public void setDwStoreKey(Long dwStoreKey) {
		this.dwStoreKey = dwStoreKey;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Integer getStoreRank() {
		return storeRank;
	}

	public void setStoreRank(Integer storeRank) {
		this.storeRank = storeRank;
	}
}
