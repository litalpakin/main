package com.storenext.assortment.dto;

public class TableDataWithSummaryDto extends TableDataDto{
	private TableDataDto summary = null;

	public TableDataWithSummaryDto(){
		
	}
    public TableDataWithSummaryDto(TableDataDto dto){
		this.rows = dto.getRows();
		this.columnNames = dto.getColumnNames();
	}
	public TableDataDto getSummary() {
		return summary;
	}
	public void setSummary(TableDataDto summary) {
		this.summary = summary;
	}

}
