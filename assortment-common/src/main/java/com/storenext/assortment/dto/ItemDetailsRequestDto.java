package com.storenext.assortment.dto;

import javax.validation.constraints.NotNull;

public class ItemDetailsRequestDto {
	@NotNull
	Long dwItemId;
	@NotNull
	Long formatCode;
	
	public Long getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(Long formatCode) {
		this.formatCode = formatCode;
	}
	public Long getDwItemId() {
		return dwItemId;
	}
	public void setDwItemId(Long dwItemId) {
		this.dwItemId = dwItemId;
	}

	@Override
	public String toString() {
		return "ItemDetailsRequestDto{" +
				"dwItemId=" + dwItemId +
				", formatCode=" + formatCode +
				'}';
	}
}
