package com.storenext.assortment.dto.reports.alternativecost;

import javax.validation.constraints.NotNull;

public class AlternativeCostAllFormatsRequestDto {
	@NotNull
	private Long dwItemKey;
	
	public Long getDwItemKey() {
		return dwItemKey;
	}
	
	public void setDwItemKey(Long dwItemKey) {
		this.dwItemKey = dwItemKey;
	}
}
