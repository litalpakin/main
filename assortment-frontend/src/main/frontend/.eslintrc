{
  "parser": "babel-eslint",
  "ignorePatterns": ["*.scss"],

  "parserOptions": {
    "ecmaFeatures": {
      "legacyDecorators": true,
      "jsx": true
    },
    "project": "./tsconfig.json",
    "ecmaVersion": 2020,
    "sourceType": "module"
  },
  "env": {
    "browser": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended"
  ],
  "plugins": [
    "react",
    "react-hooks",
    "prefer-arrow"
  ],
  "rules": {
    "react/prop-types": 0,
    "react/no-unescaped-entities": 0,
    "react/no-did-update-set-state": 0,
    "react/display-name": 0,
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "indent": ["error", 2],
    "quotes": ["error", "single"],
    "semi": ["error", "never"],
    "space-before-function-paren": ["error", "always"],
    "max-len": [
      "error",
      {
        "code": 120,
        "ignoreRegExpLiterals": true,
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true
      }
    ],
    "arrow-parens": ["error", "as-needed"],
    "spaced-comment": ["error", "always"],
    "object-shorthand": ["error", "always"],
    "prefer-arrow-callback": "error",
    "prefer-const": "error",
    "prefer-spread": "error",
    "comma-dangle": ["error", "always-multiline"],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "no-prototype-builtins": 0,
    "react/jsx-no-target-blank": 0,
    "react/no-string-refs": 0,
    "no-unused-vars": 1,
    "array-bracket-spacing": ["error", "never"]
  },
  "settings": {
    "import/resolver": "webpack",
    "react": {
      "version": "16.13.1"
    }
  },
  "globals": {
    "$": true,
    "jQuery": true,
    "process": true,
    "FileReader": true,
    "localStorage": true,
    "System": true,
    "Promise": true,
    "Uint8Array": true,
    "module": true,
    "require": true
  },
  "overrides": [
    {
      "files": ["*.ts", "*.tsx"],
      "parser": "@typescript-eslint/parser",
      "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:react-hooks/recommended",
        "plugin:@typescript-eslint/recommended"
      ],
      "plugins": [
        "react",
        "react-hooks",
        "prefer-arrow",
        "@typescript-eslint"
      ],
      "rules": {
        "@typescript-eslint/ban-ts-comment": "off",
        "react/prop-types": 0,
        "no-prototype-builtins": 0,
        "react/display-name": 0,
        "jsx-quotes": ["error", "prefer-single"],
        "react/jsx-no-bind": ["error"],
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "space-before-function-paren": ["error", "always"],
        "@typescript-eslint/space-before-function-paren": [
          "error",
          "always"
        ],
        "array-bracket-spacing": ["error", "never"],
        "object-curly-spacing": ["error", "always"],
        "@typescript-eslint/explicit-module-boundary-types": "off"
      }
    }
  ]
}
