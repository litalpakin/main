export interface ITokenResponse {
  aditionalMessage: string
  token: string
}
