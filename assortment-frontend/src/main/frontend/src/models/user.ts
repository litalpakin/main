import { EUserRole, EUserGender, EUserLocales } from 'constants/user'

export interface IUser {
  id: number
  username: string
  firstName: string
  lastName: string
  gender: EUserGender
  authority: EUserRole
  language: EUserLocales
  dateOfBirth: number
  enabled: boolean
  testProfile: boolean
  lastRequestDate: number
  lastPasswordResetDate: number
  companyId: number
  company: any // TODO
  function: any // TODO
  region?: any // TODO
}
