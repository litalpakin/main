export type Nullable<T> = T | null
export type Empty<T> = T | Nullable<T> | undefined | void
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

export interface IError<E> {
  code: E | string,
  message: string
}
