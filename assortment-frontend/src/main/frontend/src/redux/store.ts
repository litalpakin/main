import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit'
import { token } from 'redux/slice/tokenSlice'
import { user } from 'redux/slice/userSlice'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'

const reducer = {
  token,
  user,
}

const store = configureStore({ reducer })

export const useAppSelector: TypedUseSelectorHook<TRootState> = useSelector
export const useAppDispatch = () => useDispatch<TAppDispatch>()

export type TRootState = ReturnType<typeof store.getState>;
export type TAppDispatch = typeof store.dispatch;
export type TAppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  TRootState,
  unknown,
  Action<string>
>;

export default store
