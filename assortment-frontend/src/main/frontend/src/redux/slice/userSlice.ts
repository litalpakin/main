import { createAsyncThunk, createSlice, PayloadAction, SerializedError } from '@reduxjs/toolkit'
import { IUser } from 'models/user'
import { currentUserApi } from 'api'
import { Nullable } from 'models/common'
import { TRootState } from 'redux/store'

const SLICE_PREFIX = 'user'

interface IUserState {
  user: Nullable<IUser>
  error: Nullable<SerializedError>
  loading: boolean
}

const initialState: IUserState = {
  user: null,
  error: null,
  loading: false,
}

export const getUser = createAsyncThunk(
  `${SLICE_PREFIX}/get`,
  async () => {
    return await currentUserApi.get()
  },
)

const userSlice = createSlice({
  name: SLICE_PREFIX,
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload
    },
  },
  extraReducers: builder => {
    for (const thunk of [getUser]) {
      builder.addCase(thunk.pending, state => {
        state.error = null
        state.loading = true
      })
      builder.addCase(thunk.rejected, (state, action) => {
        state.error = action.error
        state.loading = false
      })
    }

    builder
      .addCase(getUser.fulfilled, (state, action) => {
        state.user = action.payload
        state.loading = false
      })
  },
})

export const userSelector = (state: TRootState) => state.user
export const { setUser } = userSlice.actions
export const user = userSlice.reducer


