import { createAsyncThunk, createSlice, PayloadAction, SerializedError } from '@reduxjs/toolkit'
import { tokenApi } from 'api'
import { TRootState } from 'redux/store'
import { Nullable } from 'models/common'
import { ILoginForm } from 'models/auth'
import { setTokenToStorage, getTokenFromStorage, removeTokenFromStorage } from 'helpers/token'

const SLICE_PREFIX = 'token'

interface ITokenState {
  token: Nullable<string>
  error: Nullable<SerializedError>
  loading: boolean
}

const initialState: ITokenState = {
  token: getTokenFromStorage(),
  error: null,
  loading: false,
}

export const getToken = createAsyncThunk(
  `${SLICE_PREFIX}/getToken`,
  async (data: ILoginForm) => {
    return await tokenApi.post({
      body: {
        ...data,
      },
    })
  },
)

const tokenSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {
    setToken: (state, action: PayloadAction<string>) => {
      setTokenToStorage(action.payload)
      state.token = action.payload
    },
    removeToken: state => {
      removeTokenFromStorage()
      state.token = null
    },
  },
  extraReducers: builder =>
    builder
      .addCase(getToken.pending, state => {
        state.error = null
        state.loading = true
      })
      .addCase(getToken.fulfilled, (state, action) => {
        setTokenToStorage(action.payload.token)
        state.token = action.payload.token
        state.loading = false
      })
      .addCase(getToken.rejected, (state, action) => {
        state.token = null
        state.error = action.error
        state.loading = false
      }),
})

export const tokenSelector = (state: TRootState) => state.token
export const { setToken, removeToken } = tokenSlice.actions
export const token = tokenSlice.reducer
