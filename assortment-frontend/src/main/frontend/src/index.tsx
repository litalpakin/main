import React from 'react'
import ReactDOM from 'react-dom'
import './locales'

import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'

import './scss/style.scss'

import App from 'containers/App'

momentDurationFormatSetup(moment as any)

ReactDOM.render(<App />, document.getElementById('root'))
