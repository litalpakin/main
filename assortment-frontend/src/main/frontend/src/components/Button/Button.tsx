import React, { memo, PropsWithChildren } from 'react'
import { Button as AntButton, ButtonProps } from 'antd'
import cn from 'classnames'

import './Button.scss'

interface IProps extends ButtonProps {}

const Button: React.FC<PropsWithChildren<IProps>> = ({ children, type = 'default', className, ...rest }) => {
  return (
    <AntButton className={cn('Button', className)} type={type} {...rest}>
      {children}
    </AntButton>
  )
}

export default memo(Button)

