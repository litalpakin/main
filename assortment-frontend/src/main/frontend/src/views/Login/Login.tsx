import React, { memo, useCallback, useEffect } from 'react'
import Button from 'components/Button/Button'
import TextInput from 'components/FormFields/TextInput/TextInput'
import { useTranslation } from 'react-i18next'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import i18n from 'locales/index'
import { ILoginForm } from 'models/auth'
import { useAppSelector, useAppDispatch } from 'redux/store'
import { getToken, tokenSelector } from 'redux/slice/tokenSlice'
import { useHistory } from 'react-router-dom'
import { APP } from 'constants/routes'

import './Login.scss'
import { toast } from 'react-hot-toast'
import getErrorMessage from 'helpers/getErrorMessage'

interface IProps {
}

const validationSchema = yup.object({
  username: yup
    .string()
    .email()
    .required(i18n.t('validationEmailError')),
  password: yup
    .string()
    .required(i18n.t('validationPasswordError')),
})

const defaultValues: ILoginForm = {
  username: '',
  password: '',
}

const Login: React.FC<IProps> = () => {
  const { handleSubmit, formState: { isSubmitting }, control, reset } = useForm<ILoginForm>({
    defaultValues,
    resolver: yupResolver(validationSchema),
  })
  const { t } = useTranslation()
  const dispatch = useAppDispatch()
  const history = useHistory()
  const { token, error } = useAppSelector(tokenSelector)

  useEffect(() => {
    if (error) {
      toast.error(getErrorMessage(error))
      reset(defaultValues)
      return
    }

    if (token) {
      history.replace(APP)
    }
  }, [error, token, history, reset])

  const onSubmit = useCallback(async (values: ILoginForm) => {
    await dispatch(getToken(values))
  }, [dispatch])

  return (
    <section className='Login'>
      <div className='Login__container'>
        <h1>{t('login')}</h1>
        <form className='Login__container--form' onSubmit={handleSubmit(onSubmit)}>
          <TextInput name={'username'} label={t('userName')} control={control} />
          <TextInput name={'password'} label={t('password')} control={control} inputProps={{
            type: 'password',
          }}/>

          <Button
            disabled={isSubmitting}
            onClick={handleSubmit(onSubmit)}
            className='sign-button'
          >
            {t('login')}
          </Button>
        </form>
      </div>
    </section>
  )
}

export default memo(Login)

