import React, { memo, useCallback } from 'react'
import { Redirect } from 'react-router-dom'
import { useAuth } from 'hooks/useAuth'
import { Button, Layout, Spin } from 'antd'
import { useAppDispatch } from 'redux/store'
import { removeToken } from 'redux/slice/tokenSlice'
import { APP, LOGIN } from 'constants/routes'

import './AppContainer.scss'


const AppContainer = () => {
  const { isReady, user } = useAuth()
  const dispatch = useAppDispatch()

  const handleLogout = useCallback(() => {
    dispatch(removeToken())
  }, [dispatch])

  if (!isReady) {
    return (
      <main className='AppContainer loading'>
        <Spin size={'large'} />
      </main>
    )
  }

  return (
    <main className='AppContainer'>
      {!user && <Redirect from={APP} to={LOGIN} />}

      <Layout>
        <Layout.Content>
          <div className='AppContainer stub'>
            <h1>Storenext</h1>
            <Button onClick={handleLogout}>Logout</Button>
          </div>
        </Layout.Content>
      </Layout>
    </main>
  )
}

export default memo(AppContainer)
