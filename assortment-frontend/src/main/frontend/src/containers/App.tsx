import React, { memo } from 'react'
import { Provider } from 'react-redux'
import { HashRouter, Route, Switch } from 'react-router-dom'
import { Toaster } from 'react-hot-toast'
import AppContainer from 'containers/AppContainer/AppContainer'
import Login from 'views/Login/Login'
import store from 'redux/store'
import { APP, LOGIN } from 'constants/routes'

const App = () => {
  return (
    <Provider store={store}>
      <HashRouter>
        <Switch>
          <Route exact path={LOGIN} component={Login} />
          <Route path={APP} component={AppContainer} />
        </Switch>
      </HashRouter>
      <Toaster />
    </Provider>
  )
}

export default memo(App)
