const TOKEN_KEY = 'storenext-token'

export const getTokenFromStorage = (): string | null => {
  return localStorage.getItem(TOKEN_KEY)
}

export const setTokenToStorage = (token: string): void => {
  localStorage.setItem(TOKEN_KEY, token)
}

export const removeTokenFromStorage = (): void => {
  localStorage.removeItem(TOKEN_KEY)
}
