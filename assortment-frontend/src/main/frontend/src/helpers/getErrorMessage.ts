import { isArray, isString } from 'lodash'

export default function getErrorMessage (error: any) {
  if (!error) {
    return 'Unknown error'
  }

  if (isString(error)) {
    return error
  }

  if (error.message) {
    return error.message
  }

  if (error.code) {
    return error.code
  }

  if (isArray(error)) {
    return error.join(', ')
  }

  return JSON.stringify(error)
}
