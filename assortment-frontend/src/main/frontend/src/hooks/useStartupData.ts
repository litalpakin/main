import { useEffect, useState } from 'react'
import { Empty } from 'models/common'

interface IResult<T> {
  isReady: boolean
  error: null | Error
  data: Empty<T>
}

export const useStartupData = <T> (getData: () => Promise<T>, deps: any[] = [], initialState?: T): IResult<T> => {
  const [isReady, setIsReady] = useState(false)
  const [error, setError] = useState<null | Error>(null)
  const [data, setData] = useState<Empty<T>>(initialState)

  useEffect(() => {
    getData()
      .then(res => {
        setData(res)
        setIsReady(true)
      })
      .catch(err => {
        setError(err)
        setIsReady(true)
      })
  }, deps)

  return { isReady, data, error }
}
