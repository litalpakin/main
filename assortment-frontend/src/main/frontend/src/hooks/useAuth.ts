import { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { currentUserApi } from '../api'
import { APP, LOGIN } from 'constants/routes'
import { IUser } from 'models/user'
import { Empty } from 'models/common'
import { useStartupData } from 'hooks/useStartupData'
import { useAppSelector, useAppDispatch } from 'redux/store'
import { tokenSelector } from 'redux/slice/tokenSlice'
import { setUser } from 'redux/slice/userSlice'

interface IResult {
  isReady: boolean
  user: Empty<IUser>
}

export const useAuth = (): IResult => {
  const history = useHistory()
  const dispatch = useAppDispatch()
  const { token } = useAppSelector(tokenSelector)

  useEffect(() => {
    if (!token) {
      history.replace(LOGIN)
    }
  }, [history, token])
  
  const { isReady, data: user } = useStartupData<IUser>(() => currentUserApi.get())

  useEffect(() => {
    if (isReady && user) {
      dispatch(setUser(user))
      history.replace(APP)
    }
  }, [dispatch, history, isReady, user])

  return { isReady, user }
}
