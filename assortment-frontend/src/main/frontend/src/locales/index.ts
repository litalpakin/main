import moment from 'moment'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import languageDetector from 'i18next-browser-languagedetector'
import { ELocales } from 'constants/locales'
import en from './en.json'
import he from './he.json'


i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: ELocales.ENGLISH,
    lng: ELocales.ENGLISH,
    resources: { he, en },
    react: { wait: true },
    detection: { order: ['navigator'] },
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err)
  })

i18n.on('languageChanged', lng => {
  document.dir = i18n.dir(lng)
  moment.locale(lng)
})

export default i18n
