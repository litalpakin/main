import axios, { AxiosError, AxiosPromise, AxiosResponse } from 'axios'
import { compile, Key, pathToRegexp } from 'path-to-regexp/dist'
import queryString from 'query-string'
import { getTokenFromStorage } from 'helpers/token'

export enum EApiMethods {
  post = 'post',
  get = 'get',
  patch = 'patch',
  put = 'put',
  del = 'del',
}

export type MethodHandler<T = any> = (args?: IMethodParams) => Promise<T>

export interface IPost<T = any> {
  [EApiMethods.post]: MethodHandler<T>,
}

export interface IGet<T = any> {
  [EApiMethods.get]: MethodHandler<T>,
}

export interface IPut<T = any> {
  [EApiMethods.put]: MethodHandler<T>,
}

export interface IPatch<T = any> {
  [EApiMethods.patch]: MethodHandler<T>,
}

export interface IDel<T = any> {
  [EApiMethods.del]: MethodHandler<T>,
}

export interface IApiMethods {
  [EApiMethods.post]: MethodHandler
  [EApiMethods.get]: MethodHandler
  [EApiMethods.put]: MethodHandler
  [EApiMethods.patch]: MethodHandler
  [EApiMethods.del]: MethodHandler
}

export interface IApiStatusHandler {
  [status: number]: () => void
}

export interface IApiParams {
  host: string,
  statusHandlers?: IApiStatusHandler
  onCatch?: () => void
}

export interface IMethodData {
  [key: string]: any
}

export interface IMethodParams {
  body?: IMethodData
  query?: IMethodData
  headers?: IMethodData
  pathParams?: IMethodData
}

export default class Api {
  static getHeaders (headers = {}) {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': getTokenFromStorage(),
      ...headers,
    }
  }

  private readonly host: string
  private readonly statusHandlers: IApiStatusHandler
  private readonly onCatch: () => void

  constructor (params: IApiParams) {
    this.host = params.host
    this.statusHandlers = params.statusHandlers || {}
    this.onCatch = params.onCatch || (() => null)

    axios.defaults.baseURL = this.host
    axios.defaults.withCredentials = true
  }

  public endpoint<T extends Partial<IApiMethods>> (path: string): T {
    return {
      [EApiMethods.post]: this.post(path),
      [EApiMethods.get]: this.get(path),
      [EApiMethods.patch]: this.patch(path),
      [EApiMethods.put]: this.put(path),
      [EApiMethods.del]: this.del(path),
    } as unknown as T
  }

  private getPath (path: string, pathParams?: { [key: string]: string }): string {
    const keys: Key[] = []
    pathToRegexp(path, keys)

    if (!keys.length) {
      return `${this.host}/${path}`
    }

    return `${this.host}/${compile(path)(pathParams)}`
  }

  private getStatusHandler (status: number) {
    if (this.statusHandlers.hasOwnProperty(status) && typeof this.statusHandlers[status] === 'function') {
      return this.statusHandlers[status]
    }

    return () => null
  }

  private perform (req: AxiosPromise): Promise<any> {
    return req
      .then((res: AxiosResponse) => {
        this.getStatusHandler(res.status)()

        return res.data
      })
      .catch((err: AxiosError) => {
        let errBody = {}

        if (!err.response) {
          this.onCatch()

          return Promise.reject(new Error('No response'))
        }

        if (err && err.response) {
          this.getStatusHandler(err.response.status)()
          errBody = err.response.data || {}
        }

        throw errBody
      })
  }

  private post (path: string): MethodHandler {
    return ({ body, pathParams, query, headers }: IMethodParams = {}): Promise<any> => {
      return this.perform(
        axios.post(
          this.getPath(path, pathParams),
          body,
          {
            params: query,
            headers: Api.getHeaders(headers),
          }),
      )
    }
  }

  private get (path: string): MethodHandler {
    return ({ pathParams, query, headers }: IMethodParams = {}): Promise<any> => {
      return this.perform(
        axios.get(
          this.getPath(path, pathParams),
          {
            params: query,
            paramsSerializer: (params: any) => {
              return queryString.stringify(params, { arrayFormat: 'none' })
            },
            headers: Api.getHeaders(headers),
          },
        ),
      )
    }
  }

  private del (path: string): MethodHandler {
    return ({ pathParams, query, headers }: IMethodParams = {}) => {
      return this.perform(
        axios.delete(
          this.getPath(path, pathParams),
          {
            headers: Api.getHeaders(headers),
            params: query,
          },
        ),
      )
    }
  }

  private put (path: string): MethodHandler {
    return ({ body, pathParams, query, headers }: IMethodParams = {}): Promise<any> => {
      return this.perform(
        axios.put(
          this.getPath(path, pathParams),
          body,
          {
            headers: Api.getHeaders(headers),
            params: query,
          },
        ),
      )
    }
  }

  private patch (path: string): MethodHandler {
    return ({ body, pathParams, query, headers }: IMethodParams = {}) => {
      return this.perform(
        axios.patch(
          this.getPath(path, pathParams),
          body,
          {
            params: query,
            headers: Api.getHeaders(headers),
          },
        ),
      )
    }
  }
}
