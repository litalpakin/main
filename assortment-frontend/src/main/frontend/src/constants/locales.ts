export enum ELocales {
  ENGLISH = 'en',
  HEBREW = 'he',
}

export const RTL: { [key in ELocales]: boolean } = {
  [ELocales.ENGLISH]: false,
  [ELocales.HEBREW]: true,
}
