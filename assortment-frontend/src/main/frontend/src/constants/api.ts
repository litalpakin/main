interface IConfig {
  host: string
  ver: string
  baseUrl: string
}

export const cfg: IConfig = {
  host: `${window.location.origin}/rest/api`,
  ver: 'v1',
  baseUrl: '',
}

cfg.baseUrl = `${cfg.host}/${cfg.ver}`
