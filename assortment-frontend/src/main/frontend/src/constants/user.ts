export enum EUserRole {
  ADMIN = 'ADMIN',
  USER = 'USER'
}

export enum EUserGender {
  MALE = 'MALE',
  FEMALE = 'FEMALE'
}

export enum EUserLocales {
  ENGLISH = 'ENGLISH',
  HEBREW = 'HEBREW',
}
