import Api, { IGet, IPost } from '../lib/Api'
import { cfg } from 'constants/api'
import store from 'redux/store'
import { removeToken } from 'redux/slice/tokenSlice'
import { IUser } from 'models/user'
import { ITokenResponse } from 'models/token'

export const restApi = new Api({
  host: cfg.baseUrl,
  statusHandlers: {
    401: () => {
      store.dispatch(removeToken())
    },
  },
})

export const tokenApi = restApi.endpoint<IPost<ITokenResponse>>('auth/token')
export const currentUserApi = restApi.endpoint<IGet<IUser>>('user/current')
