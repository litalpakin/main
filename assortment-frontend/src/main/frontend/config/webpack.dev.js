const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const commonConfig = require('./webpack.common')

const devConfig = {
  mode: 'development',
  devtool: 'eval-source-map',
  output: {
    publicPath: 'http://localhost:9000/',
  },
  devServer: {
    port: 9000,
    historyApiFallback: {
      index: 'index.html',
    },
    proxy: {
      '/rest': {
        target: 'https://dev-sup-assortment.sn-strategy.com/',
        ws: true,
        changeOrigin: true,
        bypass: req => {
          if (req.headers && req.headers.referer) {
            const url = new URL(req.headers.referer)
            url.host = 'dev-sup-assortment.sn-strategy.com/'
            url.port = ''
            req.headers.referer = url.href
          }
        },
        cookieDomainRewrite: '',
      },
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
    }),
  ],
}

module.exports = merge(commonConfig, devConfig)
