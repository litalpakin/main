const { merge } = require('webpack-merge')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const commonConfig = require('./webpack.common')
const path = require('path')

// eslint-disable-next-line no-undef
const BUILD_DIR = path.resolve(__dirname, '../../../../target/frontend')

const prodConfig = {
  mode: 'production',
  output: {
    publicPath: 'auto',
    path: BUILD_DIR,
    filename: 'js/[name].[hash].bundle.js',
    sourceMapFilename: 'js/[name].[chunkhash].bundle.map',
    chunkFilename: 'js/[id].[chunkhash].chunk.js',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      maxSize: 250000,
      maxInitialRequests: 3,
    },
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          compress: false,
          ecma: 6,
          mangle: true,
        },
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.(css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      chunkFilename: '[id].[hash].css',
    }),
    new HtmlWebpackPlugin(
      {
        inject: true,
        template: './public/index.html',
      },
    ),
    new CopyWebpackPlugin({
      patterns: [
        { from: './public/img', to: 'img' },
      ],
    }),
  ],
}

module.exports = merge(commonConfig, prodConfig)
